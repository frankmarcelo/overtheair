<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different urls to chosen controllers and their actions (functions).
 *
 * PHP versions 4 and 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright 2005-2010, Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       cake
 * @subpackage    cake.app.config
 * @since         CakePHP(tm) v 0.2.9
 * @license       MIT License (http://www.opensource.org/licenses/mit-license.php)
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/views/pages/home.ctp)...
 */
Router::mapResources(array('otawebservices','settings'));
Router::parseExtensions('rss','xml','json');
Router::connect('/', array('controller' => 'settings', 'action' => 'index'));
Router::connect('/manifest.plist', array('controller' => 'settings', 'action' => 'manifest'));
Router::connect('/uploader', array('controller' => 'macosx', 'action' => 'upload'));
Router::connect('/uploader/apps', array('controller' => 'macosx', 'action' => 'upload'));
Router::connect('/uploader/images', array('controller' => 'modules', 'action' => 'upload'));
Router::connect('/uploader/materials', array('controller' => 'documents', 'action' => 'upload'));
#Router::connect('/admin', array('admin'=>true,'controller' => 'users', 'action' => 'login'));
Router::connect('/otawebservices', array('controller' => 'otawebservices', 'action' => 'index'));
Router::connect('/otawebservices/v1', array('controller' => 'otawebservices', 'action' => 'index'));
Router::connect('/otawebservices/v1.0', array('controller' => 'otawebservices', 'action' => 'index'));
Router::connect('/otawebservices/v1.0/', array('controller' => 'otawebservices', 'action' => 'index'));
Router::connect('/otawebservices/v1.0/OTA_Test', array('controller' => 'otawebservices', 'action' => 'OTA_Test'));
Router::connect('/otawebservices/v1.0/OTA_AuthorizationRQ', array('controller' => 'otawebservices', 'action' => 'OTA_AuthorizationRQ'));
Router::connect('/otawebservices/v1.0/OTA_GetLatestManifestRQ', array('controller' => 'otawebservices', 'action' => 'OTA_GetLatestManifestRQ'));
Router::connect('/otawebservices/v1.0/OTA_DownloadModuleFileRQ', array('controller' => 'otawebservices', 'action' => 'OTA_DownloadModuleFileRQ'));
Router::connect('/otawebservices/v1.0/OTA_GetFullManifestRQ', array('controller' => 'otawebservices', 'action' => 'OTA_GetFullManifestRQ'));


/**
 * ...and connect the rest of 'Pages' controller's urls.
 */

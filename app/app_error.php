<?php

class AppError extends ErrorHandler {
/**
 * securityError
 *
 * @return void
 */
    public function securityError() {
    	$this->Session->destroy();
        /*$this->controller->set(array(
            'referer' => $this->controller->referer(),
        ));*/
    }

    public function serviceResponse($params) {
        $this->controller->set('errorCode', $params['errorCode']);
        $this->controller->set('errorMessage', $params['errorMessage']);
        $this->controller->layout = 'xml/default';
        $this->_outputMessage('serviceresponse');
    }
    
	public function serviceResponseXml($params) {
        $this->controller->set('errorCode', $params['errorCode']);
        $this->controller->set('errorMessage', $params['errorMessage']);
        $this->controller->set('errorController', $params['errorController']);
        $this->controller->layout = 'xml/default';
        $this->_outputMessage('serviceresponseXml');
    }
}

<?php
/**
 * User
 *
 * PHP version 5
 *
 */
class Setting extends AppModel {
/**
 * Model name
 *
 * @var string
 * @access public
 */
    public $name = 'Setting';
/**
 * Order
 *
 * @var string
 * @access public
 */
    public $useTable = 'settings';
    public $order = 'key ASC';
    public $cacheQueries = true;

    function lookupName($name) {
        $record = $this->find('first', array(
            'cacheQueries' => false,
            'conditions' => array('name' => $name)
        ));
        if (!$record) {
            $this->create();
            $this->save(array('name' => $name));
            $record = $this->find('first', array(
                'cacheQueries' => false,
                'conditions' => array('name' => $name)
            ));
        }
        return $record;
    }
}
<?php
/**
 * Module
 *
 * PHP version 5
 *
 */
class DownloadType extends AppModel {
/**
 * Model name
 *
 * @var string
 * @access public
 */ 
    public $actsAs = array('CacheQueries','Transactional','Containable');
    public $name  = 'DownloadType';
    public $useTable = 'download_type';
    
    public $hasMany   = array(
               'Macosx' => array(
                   'className'  => 'Macosx',
                   'dependent'  => true
               )
    );
    
/**
 * Order
 *
 * @var string
 * @access public
 */
    public $cacheQueries = true;

    function lookupName($name) {
        $record = $this->find('first', array(
            'cacheQueries' => false,
            'conditions' => array('name' => $name)
        ));
        if (!$record) {
            $this->create();
            $this->save(array('name' => $name));
            $record = $this->find('first', array(
                'cacheQueries' => false,
                'conditions' => array('name' => $name)
            ));
        }
        return $record;
    }
}

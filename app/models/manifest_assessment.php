<?php
/**
 * User
 *
 * PHP version 5
 *
 */
class ManifestAssessment extends AppModel {
/**
 * Model name
 *
 * @var string
 * @access public
 */
    public $actsAs = array('CacheQueries','Transactional','Containable');
    public $name = 'ManifestAssessment';
/**
 * Order
 *
 * @var string
 * @access public
 */
    public $useTable = 'manifests_assessments';

    public $belongsTo = array(
               'Manifest'     => array(
                 'className'  => 'Manifest',
                 'dependent'  => true
               ),
               'Assessment'   => array(
                 'className'  => 'Assessment',
                 'dependent'  => true,
                 'conditions' => array('Assessment.status'=> 1),
                 'order'      => array('Assessment.display_sequence')
               )
           );

}

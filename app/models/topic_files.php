<?php
/**
 * Module
 *
 * PHP version 5
 *
 */
class TopicFiles extends AppModel {
/**
 * Model name
 *
 * @var string
 * @access public
 */ 
    public $actsAs = array('CacheQueries','Transactional','Containable');
    public $name  = 'TopicFiles';
    public $useTable = 'topics_files';
	public $hasMany   = array(
               'AssessmentsReference' => array(
                   'className' => 'AssessmentsReference',
                   'order' => 'AssessmentsReference.display_sequence',
                   'conditions' => array('AssessmentsReference.status'=> 1 ),
                   'dependent'=> true
               ) 
    );
    public $belongsTo = array(
               'Topic' => array(
                 'className' => 'Topic',
                 'dependent' => true,
                 'conditions' => array('Topic.status'=> 1),
    			 'order' => array('Topic.display_sequence')
               ),
               'TopicFilesSources' => array(
                 'className' => 'TopicFilesSources',
                 'dependent' => true
               )
           );

    
/**
 * Order
 *
 * @var string
 * @access public
 */
    public $cacheQueries = true;

    function lookupName($name) {
        $record = $this->find('first', array(
            'cacheQueries' => false,
            'conditions' => array('name' => $name)
        ));
        if (!$record) {
            $this->create();
            $this->save(array('name' => $name));
            $record = $this->find('first', array(
                'cacheQueries' => false,
                'conditions' => array('name' => $name)
            ));
        }
        return $record;
    }
}

<?php
/**
 * Module
 *
 * PHP version 5
 *
 */
class AssessmentsReference extends AppModel {
/**
 * Model name
 *
 * @var string
 * @access public
 */ 
    public $actsAs = array('CacheQueries','Transactional','Containable');
    public $name  = 'AssessmentsReference';
    public $useTable = 'assessments_references';
    public $belongsTo   = array(
               'Assessment' => array(
                   'className' => 'Assessment',
                   'conditions' => array('Assessment.status'=> 1 ),
                   'dependent'=> true
               ),
               'TopicFiles' => array(
                   'className' => 'TopicFiles',
                   'conditions' => array('TopicFiles.status'=> 1 ),
                   'dependent'=> true
               ) 
    );
/**
 * Order
 *
 * @var string
 * @access public
 */
    public $cacheQueries = true;

    function lookupName($name) {
        $record = $this->find('first', array(
            'cacheQueries' => false,
            'conditions' => array('name' => $name)
        ));
        if (!$record) {
            $this->create();
            $this->save(array('name' => $name));
            $record = $this->find('first', array(
                'cacheQueries' => false,
                'conditions' => array('name' => $name)
            ));
        }
        return $record;
    }
}

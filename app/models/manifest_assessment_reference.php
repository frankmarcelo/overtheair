<?php
/**
 * User
 *
 * PHP version 5
 *
 */
class ManifestAssessmentReference extends AppModel {
/**
 * Model name
 *
 * @var string
 * @access public
 */
    public $actsAs = array('CacheQueries','Transactional','Containable');
    public $name = 'ManifestAssessmentReference';
/**
 * Order
 *
 * @var string
 * @access public
 */
    public $useTable = 'manifests_assessments_reference';

    public $belongsTo = array(
               'Manifest'     => array(
                 'className'  => 'Manifest',
                 'dependent'  => true
               ),
               'AssessmentsReference' => array(
                 'className'  => 'AssessmentsReference',
                 'dependent'  => true,
                 'conditions' => array('AssessmentsAnswer.status'=> 1)
               )
           );

}

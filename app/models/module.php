<?php
/**
 * Module
 *
 * PHP version 5
 *
 */
class Module extends AppModel {
/**
 * Model name
 *
 * @var string
 * @access public
 */ 
    public $actsAs = array('CacheQueries','Transactional','Containable');
    public $name  = 'Module';
    public $useTable = 'modules';
    public $hasMany   = array(
               'Topic' => array(
                   'className' => 'Topic',
                   'order' => 'Topic.module_id,Topic.display_sequence',
                   'conditions' => array('Topic.status'=> 1 ),
                   'dependent'=> true
               ),
               'Assessment' => array(
                   'className' => 'Assessment',
                   'order' => 'Assessment.id',
                   'conditions' => array('Assessment.status'=> 1 ),
                   'dependent'=> true
               ) 
    );

    public $belongsTo   = array(
               'ModulesImage' => array(
                   'className' => 'ModulesImage',
                   'dependent'=> true
               ),
               'User' => array(
               	   'foreignKey' => 'who_created',
                   'className' => 'User'
               ),
               'ModuleType'=> array(
                   'foreignKey' => 'display_order',
                   'className' => 'ModuleType'
               )
    );


/**
 * Order
 *
 * @var string
 * @access public
 */
    public $cacheQueries = true;

    function lookupName($name) {
        $record = $this->find('first', array(
            'cacheQueries' => false,
            'conditions' => array('name' => $name)
        ));
        if (!$record) {
            $this->create();
            $this->save(array('name' => $name));
            $record = $this->find('first', array(
                'cacheQueries' => false,
                'conditions' => array('name' => $name)
            ));
        }
        return $record;
    }
    
	public function getLikeName( $fullName =null ){
        if( !empty($fullName) ){
             $countSearchName = $this->find('list',array('conditions'=> array("Module.name LIKE '%".addslashes($fullName)."%'")));
             return (count($countSearchName)> 0)? true: false;
        }else{
             return false;
        }
    }
}

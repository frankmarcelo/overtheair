<?php
/**
 * Module
 *
 * PHP version 5
 *
 */
class Macosx extends AppModel {
/**
 * Model name
 *
 * @var string
 * @access public
 */ 
    public $actsAs = array('CacheQueries','Transactional','Containable');
    public $name  = 'Macosx';
    public $useTable = 'macos';
    
    public $belongsTo = array(
     	'DownloadType' => array(
            'className'  => 'DownloadType',
            'dependent'  => true,
    		'conditions' => array( 'Macosx.status' => 1 )
    	),
    	'User' => array(
               	   'foreignKey' => 'who_created',
                   'className' => 'User'
        )	 
    );
    
/**
 * Order
 *
 * @var string
 * @access public
 */
    public $cacheQueries = true;

    function lookupName($name) {
        $record = $this->find('first', array(
            'cacheQueries' => false,
            'conditions' => array('name' => $name)
        ));
        if (!$record) {
            $this->create();
            $this->save(array('name' => $name));
            $record = $this->find('first', array(
                'cacheQueries' => false,
                'conditions' => array('name' => $name)
            ));
        }
        return $record;
    }
}

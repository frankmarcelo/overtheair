<?php
/**
 * User
 *
 * PHP version 5
 *
 */
class ManifestTopic extends AppModel {
/**
 * Model name
 *
 * @var string
 * @access public
 */
    public $actsAs = array('CacheQueries','Transactional','Containable');
    public $name = 'ManifestTopic';
/**
 * Order
 *
 * @var string
 * @access public
 */
    public $useTable = 'manifests_topics';

    public $belongsTo = array(
               'Manifest'     => array(
                 'className'  => 'Manifest',
                 'dependent'  => true
               ),
               'Topic'       => array(
                 'className'  => 'Topic',
                 'dependent'  => true,
                 'conditions' => array('Topic.status'=> 1),
                 'order'      => array('Topic.display_sequence')
               )
           );

}

<?php
/**
 * User
 *
 * PHP version 5
 *
 */
class Revision extends AppModel {
/**
 * Model name
 *
 * @var string
 * @access public
 */
    public $name = 'Revision';
/**
 * Order
 *
 * @var string
 * @access public
 */
    public $order = 'name ASC';
    public $cacheQueries = true;

    function lookupName($name) {
        $record = $this->find('first', array(
            'cacheQueries' => false,
            'conditions' => array('name' => $name)
        ));
        if (!$record) {
            $this->create();
            $this->save(array('name' => $name));
            $record = $this->find('first', array(
                'cacheQueries' => false,
                'conditions' => array('name' => $name)
            ));
        }
        return $record;
    }
}
<?php
/**
 * Module
 *
 * PHP version 5
 *
 */
class AssessmentsAnswer extends AppModel {
/**
 * Model name
 *
 * @var string
 * @access public
 */ 
    public $actsAs = array('CacheQueries','Transactional','Containable');
    public $name  = 'AssessmentsAnswer';
    public $useTable = 'assessments_answers';
    public $belongsTo   = array(
               'Assessment' => array(
                   'className' => 'Assessment',
                   'conditions' => array('Assessment.status'=> 1 ),
                   'dependent'=> true
               ),
               'AssessmentsChoice' => array(
                   'className' => 'AssessmentsChoice',
                   'conditions' => array('AssessmentsChoice.status'=> 1 ),
                   'dependent'=> true
               )
    );
/**
 * Order
 *
 * @var string
 * @access public
 */
    public $cacheQueries = true;

    function lookupName($name) {
        $record = $this->find('first', array(
            'cacheQueries' => false,
            'conditions' => array('name' => $name)
        ));
        if (!$record) {
            $this->create();
            $this->save(array('name' => $name));
            $record = $this->find('first', array(
                'cacheQueries' => false,
                'conditions' => array('name' => $name)
            ));
        }
        return $record;
    }
}

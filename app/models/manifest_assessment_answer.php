<?php
/**
 * User
 *
 * PHP version 5
 *
 */
class ManifestAssessmentAnswer extends AppModel {
/**
 * Model name
 *
 * @var string
 * @access public
 */
    public $actsAs = array('CacheQueries','Transactional','Containable');
    public $name = 'ManifestAssessmentAnswer';
/**
 * Order
 *
 * @var string
 * @access public
 */
    public $useTable = 'manifests_assessments_answers';

    public $belongsTo = array(
               'Manifest'     => array(
                 'className'  => 'Manifest',
                 'dependent'  => true
               ),
               'AssessmentsAnswer' => array(
                 'className'  => 'AssessmentsAnswer',
                 'dependent'  => true,
                 'conditions' => array('AssessmentsAnswer.status'=> 1)
               )
           );

}

<?php
/**
 * Module
 *
 * PHP version 5
 *
 */
class Topic extends AppModel {
/**
 * Model name
 *
 * @var string
 * @access public
 */ 
    public $actsAs = array('CacheQueries','Transactional','Containable');
    public $name  = 'Topic';
    public $useTable = 'topics';

    public $hasMany   = array(
               'TopicFiles' => array(
                   'className' => 'TopicFiles',
                   'conditions' => array('TopicFiles.status'=> 1 ),
                   'dependent'=> true,
    			   'order' => array('TopicFiles.display_sequence')
               )
               
    ); 
    public $belongsTo = array(
               'Module'       => array(
                 'className'  => 'Module',
                 'dependent'  => true,
                 'conditions' => array('Module.status'=> 1),
    			 'order'      => array('Module.display_order')
               )
           );

    
/**
 * Order
 *
 * @var string
 * @access public
 */
    public $cacheQueries = true;

    function lookupName($name) {
        $record = $this->find('first', array(
            'cacheQueries' => false,
            'conditions' => array('name' => $name)
        ));
        if (!$record) {
            $this->create();
            $this->save(array('name' => $name));
            $record = $this->find('first', array(
                'cacheQueries' => false,
                'conditions' => array('name' => $name)
            ));
        }
        return $record;
    }
}

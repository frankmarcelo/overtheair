<?php
/**
 * User
 *
 * PHP version 5
 *
 */
class Manifest extends AppModel {
/**
 * Model name
 *
 * @var string
 * @access public
 */
    public $name = 'Manifest';
/**
 * Order
 *
 * @var string
 * @access public
 */
    public $actsAs = array('CacheQueries','Transactional','Containable');
    public $useTable = 'manifests';

    public $hasMany   = array(
               'ManifestModule' => array(
                   'className'  => 'ManifestModule',
                   'dependent'  => true
               ),
               'ManifestTopic'  => array(
                   'className'  => 'ManifestTopic',
                   'dependent'  => true
               ),
               'ManifestTopicFiles'  => array(
                   'className'  => 'ManifestTopicFiles',
                   'dependent'  => true
               ),
               'ManifestAssessment'  => array(
                   'className'  => 'ManifestAssessment',
                   'dependent'  => true
               ),
               'ManifestAssessmentAnswer'  => array(
                   'className'  => 'ManifestAssessmentAnswer',
                   'dependent'  => true
               ), 
               'ManifestAssessmentChoice'  => array(
                   'className'  => 'ManifestAssessmentChoice',
                   'dependent'  => true
               ),
               'ManifestAssessmentReference'  => array(
                   'className'  => 'ManifestAssessmentReference',
                   'dependent'  => true
               )
    );

}

<?php
/**
 * User
 *
 * PHP version 5
 *
 */
class ModuleType extends AppModel {
/**
 * Model name
 *
 * @var string
 * @access public
 */
    public $actsAs = array('CacheQueries','Transactional','Containable');
    public $name = 'ModuleType';
/**
 * Order
 *
 * @var string
 * @access public
 */
    public $useTable = 'module_types';

    public $hasMany = array(
               'Module'       => array(
                 'className'  => 'Module',
                 'dependent'  => true,
                 'conditions' => array('ModuleType.status'=> 1),
               )
           );

}

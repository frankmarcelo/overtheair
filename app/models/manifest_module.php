<?php
/**
 * User
 *
 * PHP version 5
 *
 */
class ManifestModule extends AppModel {
/**
 * Model name
 *
 * @var string
 * @access public
 */
    public $actsAs = array('CacheQueries','Transactional','Containable');
    public $name = 'ManifestModule';
/**
 * Order
 *
 * @var string
 * @access public
 */
    public $useTable = 'manifests_modules';

    public $belongsTo = array(
               'Manifest'     => array(
                 'className'  => 'Manifest',
                 'dependent'  => true
               ),
               'Module'       => array(
                 'className'  => 'Module',
                 'dependent'  => true,
                 'conditions' => array('Module.status'=> 1),
               	 'order'      => array('Module.display_order')
               )
           );

}

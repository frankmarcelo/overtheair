<?php
/**
 * User
 *
 * PHP version 5
 *
 */
class ManifestTopicFiles extends AppModel {
/**
 * Model name
 *
 * @var string
 * @access public
 */
    public $actsAs = array('CacheQueries','Transactional','Containable');
    public $name = 'ManifestTopicFiles';
/**
 * Order
 *
 * @var string
 * @access public
 */
    public $useTable = 'manifests_topics_files';

    public $belongsTo = array(
               'Manifest'     => array(
                 'className'  => 'Manifest',
                 'dependent'  => true
               ),
               'TopicFiles'       => array(
                 'className'  => 'TopicFiles',
                 'dependent'  => true,
                 'conditions' => array('TopicFiles.status'=> 1),
                 'order'      => array('TopicFiles.display_sequence')
               )
           );

}

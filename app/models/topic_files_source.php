<?php
/**
 * Module
 *
 * PHP version 5
 *
 */
class TopicFilesSource extends AppModel {
/**
 * Model name
 *
 * @var string
 * @access public
 */ 
    public $actsAs = array('CacheQueries','Transactional','Containable');
    public $name  = 'TopicFilesSource';
    public $useTable = 'topics_files_sources';
	       
    public $hasMany   = array(
               'TopicFiles' => array(
                   'className' => 'TopicFiles',
                   'conditions' => array('TopicFiles.status'=> 1 ),
                   'dependent'=> true
               )
    );
/**
 * Order
 *
 * @var string
 * @access public
 */
    public $cacheQueries = true;

    function lookupName($name) {
        $record = $this->find('first', array(
            'cacheQueries' => false,
            'conditions' => array('name' => $name)
        ));
        if (!$record) {
            $this->create();
            $this->save(array('name' => $name));
            $record = $this->find('first', array(
                'cacheQueries' => false,
                'conditions' => array('name' => $name)
            ));
        }
        return $record;
    }
}

<?php
/**
 * User
 *
 * PHP version 5
 *
 */
class User extends AppModel {
/**
 * Model name
 *
 * @var string
 * @access public
 */
    public $name = 'User';
/**
 * Order
 *
 * @var string
 * @access public
 */
    public $order = 'User.name ASC';
    public $cacheQueries = true;
/**
 * Behaviors used by the Model
 *
 * @var array
 * @access public
 */
    public $actsAs = array(
        'Transactional',
        'Acl' => array('type' => 'requester')
    );
/**
 * Model associations: belongsTo
 *
 * @var array
 * @access public
 */
    public $belongsTo = array('Role','DownloadType');
/**
 * Validation
 *
 * @var array
 * @access public
 */
    public $validate = array(
        'username' => array(
            'isUnique' => array(
                'rule' => 'isUnique',
                'message' => 'The username has already been taken.',
            ),
            'notEmpty' => array(
                'rule' => 'notEmpty',
                'message' => 'This field cannot be left blank.',
            ),
        ),
        'email' => array(
            'email' => array(
                'rule' => 'email',
                'message' => 'Please provide a valid email address.',
            ),
            'isUnique' => array(
                'rule' => 'isUnique',
                'message' => 'Email address already in use.',
            ),
        ),
        'password' => array(
            'rule' => array('minLength', 6),
            'message' => 'Passwords must be at least 6 characters long.',
        ),
        'name' => array(
            'rule' => 'notEmpty',
            'message' => 'This field cannot be left blank.',
        ),
    );

    public function parentNode() {
        if (!$this->id && empty($this->data)) {
            return null;
        }
        $data = $this->data;
        if (empty($this->data)) {
            $data = $this->read();
        }
        if (!isset($data['User']['role_id']) || !$data['User']['role_id']) {
            return null;
        } else {
            return array('Role' => array('id' => $data['User']['role_id']));
        }
    }
    
	function hashPasswords($data) {
		if (isset($data['User']['password']) && isset($data['User']['username']) ) {
			$data['User']['password'] = md5($data['User']['username'] . ':Otawebservices-10019:' . $data['User']['password']);
			return $data;
		}
		return $data;
	}

    public function getLikeFullName( $fullName =null ){
        if( !empty($fullName) ){
             $countSearchName = $this->find('list',array('conditions'=> array("User.username LIKE '%".addslashes($fullName)."%'")));
             return (count($countSearchName)> 0)? true: false;
        }else{
             return false;
        }
    }

    public function afterSave($created) {
        if (!$created) {
            $parent = $this->parentNode();
            $parent = $this->node($parent);
            $node = $this->node();
            $aro = $node[0];
            $aro['Aro']['parent_id'] = $parent[0]['Aro']['id'];
            $this->Aro->save($aro);
        }
    }
}

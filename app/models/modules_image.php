<?php
/**
 * Module
 *
 * PHP version 5
 *
 */
class ModulesImage extends AppModel {
/**
 * Model name
 *
 * @var string
 * @access public
 */ 
    public $actsAs = array('CacheQueries','Transactional','Containable');
    public $name  = 'ModulesImage';
    public $useTable = 'modules_images';
    public $hasMany   = array(
               'Module' => array(
                   'className' => 'Module',
                   'conditions' => array('Module.status'=> 1 ),
                   'dependent'=> true
               ),
    );
/**
 * Order
 *
 * @var string
 * @access public
 */
    public $cacheQueries = true;

    function lookupName($name) {
        $record = $this->find('first', array(
            'cacheQueries' => false,
            'conditions' => array('name' => $name)
        ));
        if (!$record) {
            $this->create();
            $this->save(array('name' => $name));
            $record = $this->find('first', array(
                'cacheQueries' => false,
                'conditions' => array('name' => $name)
            ));
        }
        return $record;
    }
}

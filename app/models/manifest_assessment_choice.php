<?php
/**
 * User
 *
 * PHP version 5
 *
 */
class ManifestAssessmentChoice extends AppModel {
/**
 * Model name
 *
 * @var string
 * @access public
 */
    public $actsAs = array('CacheQueries','Transactional','Containable');
    public $name = 'ManifestAssessmentChoice';
/**
 * Order
 *
 * @var string
 * @access public
 */
    public $useTable = 'manifests_assessments_answers';

    public $belongsTo = array(
               'Manifest'     => array(
                 'className'  => 'Manifest',
                 'dependent'  => true
               ),
               'AssessmentsChoice' => array(
                 'className'  => 'AssessmentsChoice',
                 'dependent'  => true,
                 'conditions' => array('AssessmentsAnswer.status'=> 1)
               )
           );

}

<?php
/**
 * Application helper
 *
 * This file is the base helper of all other helpers
 *
 * PHP version 5
 *
 * @category Helpers
 * @package  Croogo
 * @version  1.0
 * @author   Fahad Ibnay Heylaal <contact@fahad19.com>
 * @license  http://www.opensource.org/licenses/mit-license.php The MIT License
 * @link     http://www.croogo.org
 */
App::import('Vendor', 'UrlCache.url_cache_app_helper');

class AppHelper extends UrlCacheAppHelper {

    public function url($url = null, $full = false) {
        if (!isset($url['locale']) && isset($this->params['locale'])) {
            $url['locale'] = $this->params['locale'];
        }
        return parent::url($url, $full);
    }
}

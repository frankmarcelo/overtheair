<?php
/**
 * 
 * Over The Air Web Service
 * @author fmarcelo
 * Date Created 11th of September 2011
 * @copyright 2011
 * @version 1.0
 */

App::import('Sanitize');
class MacosxController extends AppController {

	public $name = 'Macosx';
	public $components = array(
        'Auth',
		'Email'
    );
    
    public $uses = array(
    	'User',
    	'Module',
    	'Setting',
    	'Topic',
        'TopicFiles',
    	'TopicFilesSources',
        'Assessment',
        'AssessmentsAnswer',
        'AssessmentsChoice',
        'AssessmentsReference'
    );
	
	public function index(){
	}
	
	public function upload(){
		
		$this->TopicFilesSources->contain();
        $topicFilesSource = $this->TopicFilesSources->find('list');
        $this->set(compact('topicFilesSource'));
		
		$this->Topic->contain();
    	$oTopic = $this->Topic->find('first',array(
           	'order' => array('Topic.id' => 'desc')
          )
    	);	
    	$this->set('latest_topic_version', intval($oTopic['Topic']['id'])+1 );
	}
	
	public function download(){
        if( isset($this->params['url']['src']) ){
            $this->autoRender = false;
            $this->layout = 'media';
  
            App::import('Core', 'Folder');
            App::import('Core', 'File');
            $DownloadLink = Configure::read('downloadLink');
        	$SourceFile = Sanitize::clean(trim($this->params['url']['src']));
			if( file_exists($DownloadLink . DS . 'os' . DS . $SourceFile) ){
				$this->Macosx->contain();
				$Macosx = $this->Macosx->findByFileName($SourceFile);
				if(!empty($Macosx)){
	            	header("HTTP/1.1 200 OK");
					header("Content-Description: File Transfer");
					header("Accept-Ranges: bytes");
						
					foreach (getallheaders() as $name => $value) {
						if( strstr(strtolower($name),'range')){
 			   				header("Content-Range: bytes ".$value."/".$Macosx['Macosx']['raw_size']);
						}
					}

                    header('Expires: 0');
                    header('Content-Transfer-Encoding: binary');
                    header("Content-disposition: attachment; filename=".$SourceFile);
                    header("Content-Length: ".$Macosx['Macosx']['raw_size']);
                    if( $Macosx['Macosx']['download_type_id'] == Configure::read('adminRoleId') ){
                    	header("Content-Type: application/octet-stream ipa");
                    }else{
                    	header("Content-Type: application/x-apple-diskimage dmg");
                    }
                    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
                    header("Pragma: public");
                    header("Connection: close");   
                    readfile($DownloadLink.DS . 'os' . DS . $SourceFile);
				}else{
					header("HTTP/1.0 404 File Not Found");
	            }
   			}else{
   				header("HTTP/1.0 404 File Not Found");	
   			}    
        }
    }
    
    public function save(){
        Configure::write('debug',0);
    	if( isset($this->params['url']['app_id'])&&isset($this->params['url']['download_type_id']) ){
            $this->layout = 'ajax';
    		
    		$appId = Sanitize::clean(trim($this->params['url']['app_id']));
    		$downloadTypeId = Sanitize::clean(trim($this->params['url']['download_type_id']));
    		$Macosx = $this->Macosx->findById($appId);
			if(!empty($Macosx)){
				$this->Macosx->begin();
				$this->Macosx->id = $Macosx['Macosx']['id'];
				$mac['Macosx']['download_type_id'] = $downloadTypeId;
     			if( $this->Macosx->save($mac)){
     				$this->Macosx->commit();
     				
     				
     				if (Configure::read('debug') > 1) {
						$this->Email->delivery = 'debug';	
	    			}

	    			$users = $this->User->find('all',array(
	    					'conditions' => array(
	    						'User.download_type_id IN ('.$downloadTypeId.','.Configure::read('bothSoftware').') 
                                 AND User.status IN ('.Configure::read('status_live').') 
                                 AND User.role_id IN ('.Configure::read('userRoleId').') 
                                 ')
	    				)
	    			);
	    		
	    			if(!empty($users)){
	    				foreach( $users as $user ){
	    					$activationKey = $user['User']['invitation_key'];
							$this->Email->from = Configure::read('adminEmail');
							$this->Email->to = $user['User']['email'];
							
							$this->set(compact('user', 'activationKey'));
							$this->set('source_file',$Macosx['Macosx']['file_name']);
							$this->set('downloadTypeId',$downloadTypeId);
					        $this->Email->subject = __('Please download your Greensplash '.$Macosx['DownloadType']['alias'].' app', true);
					        $this->Email->template = 'download_app2';
					        $this->Email->sendAs = 'html';
                                                if( $downloadTypeId > 0 ){
							$this->Email->send();		
						}	
	    					if (Configure::read('debug') > 1) {
	    						pr($this->Session->read('Message.email'));
	    						//$this->Session->delete('Message.email');
	    						die;
	    					}
	    				}		
	    			}
	    			$this->set('data',true);
				}	
     		}
    	
    	}else{
    		$this->set('data',false);	
    	}
    }
}

<?php
/**
 * Settings Controller
 *
 * PHP version 5
 *
 */
App::import('Sanitize');
class SettingsController extends AppController {
/**
 * Controller name
 *
 * @var string
 * @access public
 */
    public $name = 'Settings';
    public $components = array(
        'Auth'
    );
/**
 * Models used by the Controller
 *
 * @var array
 * @access public
 */
    public $uses = array('User','Module','Setting');
    
    public $helpers = array('Modules');

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('manifest');
    }

    public function index(){
    	$this->Module->contain('ModuleType');
    	$modules = $this->Module->find('all',array(
        		'conditions' => array('Module.status' => Configure::read("status_live") ),
            	'order' => array( 'Module.display_order' ),
    			'limit' => 10
            )
        );
        
        $total_modules = $this->Module->find('all',array(
        		'conditions' => array('Module.status' => Configure::read("status_live") ),
            )
        );
        
        $unpublished = $this->Module->find('all',array(
        		'conditions' => array('Module.status' => Configure::read("status_onhold") ),
            	'order' => array( 'Module.display_order,Module.date_created DESC' ),
       			'limit' => 10
            )
        );
        
        $total_unpublished = $this->Module->find('count',array(
        		'conditions' => array('Module.status' => Configure::read("status_onhold") )
        	)
        );
        
        $macosapp = $this->Macosx->find('all',array(
        		'conditions' => array('Macosx.status' => Configure::read("status_live") ),
        		'order' => array( 'Macosx.date_created DESC' ),
       			'limit' => 10
        	)
        );
        
        $total_macos = $this->Macosx->find('count',array(
        		'conditions' => array('Macosx.status' => Configure::read("status_live") )
        	)
        );
        
        
        $usersList = $this->User->find('all',array(
        		'conditions' => array('User.status' => Configure::read("status_live") ),
            	'order' => array( 'User.date_created' => "DESC "),
        		'limit' => 10
            )
        );
        
        $total_users = $this->User->find('count',array(
        		'conditions' => array('User.status' => Configure::read("status_live") ),
            )
        );
        
        $roles = $this->Role->find('list');
    	$settings = $this->Setting->find('all');
    	$this->set(compact('modules','settings','usersList','unpublished','total_unpublished','total_modules','total_users','roles'));
    	$this->set(compact('macosapp','total_macos'));
    	$this->set('title_for_layout', __('Administration Page', true));
    }
    
    public function manifest(){
    	$this->RequestHandler->respondAs('xml');
    	$this->header('Content-Type: text/xml plist');
    	$this->header('HTTP/1.1 200 '.$this->__httpCodes[200]); 
    	$this->layout = 'manifest';
    	
    	if(isset($this->macos) && sizeof($this->macos)>0){
	    	foreach( $this->macos as $mac_app ){
				if( intval($mac_app['Macosx']['download_type_id'])== Configure::read('adminRoleId') ){
					$this->set('version',intval($mac_app['Macosx']['id']));
					$this->set('source_file',$mac_app['Macosx']['file_name']);
				} 
			}
    	}
    }
    
    public function download(){
        if( isset($this->params['url']['src']) ){
            $this->autoRender = false;
            $this->layout = 'ajax';
  
            App::import('Core', 'Folder');
            App::import('Core', 'File');
            $DownloadLink = Configure::read('downloadLink');
        	$SourceFile = Sanitize::clean(trim($this->params['url']['src']));
			if( file_exists($DownloadLink . DS . 'os' . DS . $SourceFile) ){
				header("Content-Description: File Transfer");
                header("Content-disposition: attachment; filename=".$SourceFile);
                header("Content-Encoding: ".mime_content_type($DownloadLink . DS . 'os' . DS . $SourceFile));
                header("Expires: 0");
                header("Content-Length: " . filesize($DownloadLink.DS . 'os' . DS . $SourceFile));
                header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
                header("Pragma: public");
                //echo gzencode($DownloadLink . DS . 'os' . DS . $SourceFile);
                readfile($DownloadLink.DS . 'modules' . DS . $SourceFile);
                exit;
   			}    
        }
    }
}

<?php
/**
 * Modules Controller
 * 
 *
 * PHP version 5
 *
 */
App::import('Sanitize');
class TopicsController extends AppController {
/**
 * Controller name
 *
 * @var string
 * @access public
 */
    public $name = 'Topics';
    public $components = array(
        'Auth'
    );
/**
 * Models used by the Controller
 *
 * @var array
 * @access public
 */
    public $uses = array('User','Topic','Setting');

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Security->validatePost = false;
        $this->Topic->contain();
    	$oTopic = $this->Topic->find('first',array(
           	'order' => array('Topic.id' => 'desc')
          )
    	);	
    	$this->set('latest_topic_version', intval($oTopic['Topic']['id'])+1 );
    }

  	public function index(){
  	}
    
    public function add(){
    	
    }
    
    public function view($id){
    }
    
    public function stepTwo(){
    	pr($this->data);
    	Configure::write('debug', 0);
        $this->layout ='ajax';
        
        if( $this->RequestHandler->isPost() && !empty($this->data)){
        	pr($this->data);
    	}else{
    		$this->set('data',false);
    	}
    }
}
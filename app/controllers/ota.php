<?php
/**
 * 
 * Over The Air Web Service
 * @author fmarcelo
 * Date Created 11th of September 2011
 * @copyright 2011
 * @version 1.0
 */

/*TODO
 * Database Logger
 * Session Token
 * 
 * 
 * 
 */
App::import('Sanitize');

class OtawebservicesController extends AppController {
	
    public $name = 'Otawebservices';
    public $uses = array('User','Role','Manifest','Log','Module','Topic');
    //public $components = array('OtaApi');
   
    private $version = 'v1.0'; 
    
    private $methodActions = array(
                               'OTA_Authenticate',
                               'OTA_GetLatestManifestRQ',
                               'OTA_DownloadModuleFileRQ',
                               'OTA_GetFullManifestRQ' ); 

    public function beforeFilter() {
    	parent::beforeFilter();
        $this->disableCache();
        
        App::import('Core', 'HttpSocket');
        App::import('Core', 'Xml'); 
        
	$this->Security->blackHoleCallback = 'forceSSL';
//$token = ($this->Session->read('_Token'));
		
 //       Configure::write('debug',2);
        $this->httpCodes();
        $this->Security->requirePost($this->methodActions);
        $this->Security->requireSecure();
		
        
        //if( $this->RequestHandler->isSSL() ){
        if( true ){
        	
        	$this->Security->loginOptions = array(
               'type'  => 'digest',
               'prompt'=> 'authenticate',
               'login' => 'authenticate',
               'realm' => $this->name
            );
            
            $httpAuthUsers = array();
            $roles = $this->Role->find('first',array(
            		'conditions' => array( 'Role.title' => 'User' ),
            		'fields' => array('Role.id')
            	),array('cache' => 'Users', 'cacheConfig' => 'cache_queries')
            );

    	    $users = $this->User->find('all',array(
    	    		'conditions' => array(
    	    			'User.status'  => Configure::read('status_live'), 
    					'User.role_id' => $roles['Role']['id'] 
    				),
    				'fields' => array('User.username',
    					'User.password',
    				  	'User.token',
    				  	'User.id',
    				  	'User.invitation_key',
    				  	'User.role_id'
    				)
    			),array('cache' => 'Users', 'cacheConfig' => 'cache_queries')
    		);
    		
    		if( !empty($users )){
	    		foreach( $users as $userInfo ){
	    			$httpAuthUsers[$userInfo['User']['username']] = $userInfo['User']['password'];
	    		}
	    			
	    		$this->Security->loginUsers = $httpAuthUsers;
	    		$this->Security->requireLogin();
	    			
            }else{
    			$this->throwExceptionError( $errorCode   = 500, 
                	                        $errorMesage = Configure::read('httpServerErrorMessage') );
    		}
    	}else{
            $this->throwExceptionError( $errorCode = 403, 
                                        $errorMesage = Configure::read('httpForbiddenAccessMessage') );
        }
    }

    private function forceSSL() {
        $this->redirect('https://' . env('SERVER_NAME') . $this->here);
    }

    public function index(){
    	if( $this->RequestHandler->isGet() ){
            $this->set('version', $this->version);	    
                       /*$modules = $this->Module->find('all',array(
                                'conditions' => array( 'Module.status' => Configure::read('status_live') ),
                                'order' => array('Module.display_order')
                       ));*/

    	}elseif ( $this->RequestHandler->isPost() ){
            $this->header('Content-Type: text/xml');
            $this->header('HTTP/1.1 200 '.$this->__httpCodes[200]);      
            $this->layout = 'xml'; 
    	}
    }

    public function v1(){
    }
    
    private function requestLogs($args=array()){
    	$this->data['Log']['requestWith']= $this->RequestHandler->requestedWith();
    	$this->data['Log']['requestIP'] = $this->RequestHandler->getClientIP();
    	$this->data['Log']['requestMethod'] = $args['requestMethod'];
    	$this->data['Log']['requestRQ'] = $args['requestRQ'];//request XML data
    	$this->data['Log']['requestType'] = ($this->RequestHandler->isPost()) ? 'post':'get';
    	$this->data['Log']['isMobile'] = ($this->RequestHandler->isMobile()) ? 1:0;
    	$this->data['Log']['username'] = $args['user'];//TODO set id
    	$this->data['Log']['date_created'] = date("Y-m-d H:i:s");
    	if($this->Log->save($this->data)){
            $this->Log->commit();
            $id = $this->Log->query("SELECT LAST_INSERT_ID() as id");
            return $id[0][0]['id'];
    	}else{
            $this->Log->rollback();
            return 0;
        }
    }

    private function requestLogsUpdate($args=array(),$logId=0){
        $this->Log->id = $logId;
        $this->Log->begin();
        $this->data['Log']['requestRS'] = Sanitize::clean($args['requestRS']);
        if($this->Log->save($this->data)){
            $this->Log->commit();
        }
    }

    public function authenticate($args=array()) {
       	if( $args && isset($this->Security->loginUsers[$args['username']])) {
 	    if ($args['response'] == $this->Security->generateDigestResponseHash($args)) {
                $aUserInfo = $this->User->findByUsername(Sanitize::clean($args['username']));
                $this->Session->write('TokenUser',$args['username']);
                $this->Session->write('TokenId',$aUserInfo['User']['token']);
                #$this->log(print_r($args,true), 'debug');
                #$this->log(print_r($this->Session,true), 'debug');
                #$this->log(print_r($aUserInfo,true), 'debug');
                #$this->log(print_r($this->Session->read(),true), 'debug');
	      	return true;
  	    }else{
	      	$this->throwExceptionError( $errorCode = 401, $errorMessage = Configure::read('httpInvalidAuthMessage') );	
	    }
       	}else{
            $this->throwExceptionError( $errorCode = 401, 
            $errorMesage = Configure::read('httpInvalidAuthMessage') );
       	} 	
    } 

    public function OTA_AuthorizationRQ(){
    	
    	$this->header('Content-Type: text/xml');
        $this->header('HTTP/1.1 200 '.$this->__httpCodes[200]);      
	if( $this->RequestHandler->isGet() ){
    		$this->set('version', $this->version);	    	
        }elseif ( $this->RequestHandler->isPost() ){
            if( !empty($this->data[$this->name]['xmlRequest']) ){

                $this->layout = 'xml/ota_authorization_rs';

       		$requestXML = trim($this->data[$this->name]['xmlRequest']);
       		$domRQ = new DOMDocument();
                $domRQ->loadXML($requestXML);
                $domRQ->formatOutput = true;
                $requestXML = $domRQ->saveXML();

                $transactionIdentifier = $this->requestLogs(array('user'=>$this->Session->read('TokenUser'),
                                            'requestRQ'=> $requestXML, 'requestMethod' => __FUNCTION__));

                $sUsername = null;
                $sPassword = null;

                $OTA_Authorization = new Xml($requestXML);
                $xmlAsArray = $OTA_Authorization->toArray();
                 
                //Security::hash($this->data['User']['current_password']
                $sUsername = Sanitize::clean($xmlAsArray['OTAAuthorizationRQ']['Authentication']['username']);
                $sPassword = Sanitize::clean($xmlAsArray['OTAAuthorizationRQ']['Authentication']['password']);
                $aUserInfo = $this->User->findByUsername(Sanitize::clean($sUsername));

                $this->set('transactionIdentifier',$transactionIdentifier);
                $this->set('requestingsource',$sUsername);
                if( strtolower($aUserInfo['User']['username']) == strtolower($sUsername) &&
                    strtolower($aUserInfo['User']['password']) == strtolower($sPassword) &&
                    strtolower($aUserInfo['Role']['title']) == 'user'
                ){
                    $manifest = $this->Manifest->find('all', array('order' => array('Manifest.id' => 'desc'),'limit'=>1));
                    $this->set('manifestversion',$manifest[0]['Manifest']['id']);
                    $this->set('manifestcreatedate',$manifest[0]['Manifest']['date_created']);
	            $this->set('valid',true);                 
                    $this->set('token',$aUserInfo['User']['token']); 
                }else{//Failed authentication
                    $this->set('valid',false);
                }
       	    }else{
       		$this->throwExceptionError( $errorCode = 400, $errorMesage = Configure::read('httpBadRequestMessage'),$isXml=true,__FUNCTION__ );
       	    }
        }
    }

    public function OTA_GetLatestManifestRQ(){
    	$this->header('Content-Type: text/xml');
        $this->header('HTTP/1.1 200 '.$this->__httpCodes[200]);      
	if( $this->RequestHandler->isGet() ){
            $this->set('version', $this->version);
        }elseif ( $this->RequestHandler->isPost() ){
	    if( !empty($this->data[$this->name]['xmlRequest']) ){

        	$this->layout = 'xml/ota_getlatestmanifest_rs';

                $requestXML = trim($this->data[$this->name]['xmlRequest']);
                $domRQ = new DOMDocument();
                $domRQ->loadXML($requestXML);
                $domRQ->formatOutput = true;
                $requestXML = $domRQ->saveXML();
                $transactionIdentifier = $this->requestLogs(array('user'=>$this->Session->read('TokenUser'),
                                            'requestRQ'=> $requestXML, 'requestMethod' => __FUNCTION__));

                $OTA_GetLatestManifestRQ = new Xml($requestXML);
                $xmlAsArray = $OTA_GetLatestManifestRQ->toArray();

               
        	/**
        	 * TODO
        	 * 
        	 * create another component or vendor
        	 */
           }	
       }
    }

    public function OTA_DownloadModuleFileRQ(){
        $this->header('Content-Type: text/xml');
        $this->header('HTTP/1.1 200 '.$this->__httpCodes[200]);
        if( $this->RequestHandler->isGet() ){
            $this->set('version', $this->version);
        }elseif ( $this->RequestHandler->isPost() ){
            if( !empty($this->data[$this->name]['xmlRequest']) ){

                $this->layout = 'xml/ota_downloadmodulefile_rs';

                $requestXML = trim($this->data[$this->name]['xmlRequest']);
                $domRQ = new DOMDocument();
                $domRQ->loadXML($requestXML);
                $domRQ->formatOutput = true;
                $requestXML = $domRQ->saveXML();
                $transactionIdentifier = $this->requestLogs(array('user'=>$this->Session->read('TokenUser'),
                                            'requestRQ'=> $requestXML, 'requestMethod' => __FUNCTION__));


                $OTA_GetFullManifestRQ = new Xml($requestXML);
                $xmlAsArray = $OTA_GetFullManifestRQ->toArray();

                $this->set('transactionIdentifier',$transactionIdentifier);
                /*$this->set('requestingsource',$sUsername);*/
                $manifest = $this->Manifest->find('all', array('order' => array('Manifest.id' => 'desc'),'limit'=>1));
                    $this->set('manifestversion',$manifest[0]['Manifest']['id']);
                    $this->set('manifestcreatedate',$manifest[0]['Manifest']['date_created']);
	            $this->set('valid',true);                 

                /**
                 * TODO
                 * 
                 * create another component or vendor
                 */
           }
        } 
    }

    public function OTA_GetFullManifestRQ(){
        $this->header('Content-Type: text/xml');
        $this->header('HTTP/1.1 200 '.$this->__httpCodes[200]);      
	if( $this->RequestHandler->isGet() ){
            $this->set('version', $this->version);
        }elseif ( $this->RequestHandler->isPost() ){
	    if( !empty($this->data[$this->name]['xmlRequest']) ){

        	$this->layout = 'xml/ota_getfullmanifest_rs';

                $requestXML = trim($this->data[$this->name]['xmlRequest']);
                $domRQ = new DOMDocument();
                $domRQ->loadXML($requestXML);
                $domRQ->formatOutput = true;
                $requestXML = $domRQ->saveXML();
                $transactionIdentifier = $this->requestLogs(array('user'=>$this->Session->read('TokenUser'),
                                            'requestRQ'=> $requestXML, 'requestMethod' => __FUNCTION__));


                $OTA_GetFullManifestRQ = new Xml($requestXML);
                $xmlAsArray = $OTA_GetFullManifestRQ->toArray();
  
                $this->set('transactionIdentifier',$transactionIdentifier);
                $manifest = $this->Manifest->find('all', array('order' => array('Manifest.id' => 'desc'),'limit'=>1));
                $this->set('manifestversion',$manifest[0]['Manifest']['id']);
                $this->set('manifestcreatedate',$manifest[0]['Manifest']['date_created']);
                $this->set('requestingsource',$this->Session->read('TokenUser'));

                $TokenId = Sanitize::clean($xmlAsArray['OTAGetFullManifestRQ']['POS']['Source']['Requestor']['TokenId']);
                if( isset($xmlAsArray['OTAGetFullManifestRQ']['POS']['Source']['Requestor']) ){
                   if( trim(strtolower($this->Session->read('TokenId'))) == trim(strtolower($TokenId)) ){ 

                       $modules = $this->Module->find('all',array(
                                'conditions' => array( 'Module.status' => Configure::read('status_live') ),
                                'order' => array('Module.display_order')
                       ));

                       $this->set(compact('modules'));
                       $this->set('valid',true);
                   }else{
                       $this->set('errorCode','10003');
                       $this->set('errorMessage','Invalid token parameter request. Please try again.');
                       $this->set('valid',false);
                   }
                }else{
                   $this->set('errorCode','10002');
                   $this->set('errorMessage','Invalid '.__FUNCTION__.'  request. Please try again.');
                   $this->set('valid',false);
                }
           }	
       }
    }

    private function throwExceptionError($errorCode=0, $errorMessage = null, $isXML = false, $errorMethod=''){
    	if( $isXML == false){
    		$this->RequestHandler->respondAs('xml');
    		$this->header('Content-Type: text/xml'); 
        	$this->header('HTTP/1.1 '.$errorCode.' '.$this->__httpCodes[$errorCode]);
        	$this->cakeError('serviceResponse', array(
        		'errorMessage'     => $errorMessage,
        		'errorCode' => $errorCode.' '.$this->__httpCodes[$errorCode]
        		)
        	);	
    	}else{
    		$this->RequestHandler->respondAs('xml');
    		$this->header('Content-Type: text/xml'); 
        	$this->header('HTTP/1.1 '.$errorCode.' '.$this->__httpCodes[$errorCode]);
    		$this->cakeError('serviceResponseXml', array(
        		'errorMessage' => $errorMessage,
        		'errorCode'    => $errorCode,
    			'errorController'   => $errorMethod 
        		)
        	);
    	}
    }
}

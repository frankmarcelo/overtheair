<style type="text/css">
.wrap div.updated, .wrap div.error {
    margin: 5px 0 15px;
}
div.updated, .login .message {
    background-color: #FFFFE0;
    border-color: #E6DB55;
}
div.updated, div.error {
    border-radius: 3px 3px 3px 3px;
    border-style: solid;
    border-width: 1px;
    margin: 5px 15px 2px;
    padding: 0 0.6em;
}

.form-invalid textarea, .form-invalid select {
    border-color: #CC0000 !important;
}

.form-table_step4 {
    border-collapse: collapse;
    clear: both;
    margin-bottom: -8px;
    margin-top: 0.5em;
    width: 100%;
}
</style>
<script type="text/javascript">
/* <![CDATA[ */
$(document).ready(function(){
	$('.publish').live('click',function(){
		var attribute_id = $.trim($(this).attr('id'));
		var attributes = attribute_id.split('_');
		
		if( attributes[0] == 'publish' ){
			$.post("/over-the-air/js/ajax/publish.php", { module_id: attributes[1], change_type_id: 1 },
			    function(data) {
			    	if(data==true){
			    		var dialog_message ='<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>';
			    		dialog_message +='Successfuly published the module</p>';
			    		$("#confirm_msg").empty().html(dialog_message);
			    		$('#dialog').dialog({modal:true,hide:'explode'});
			    		this.timer = setTimeout(function (){
			    			$("#dialog").dialog('close');
			    			window.location.reload();
			    		},2000);
			        }else{
			        	var dialog_message ='<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>';
			    		dialog_message +='Unsucessful you are only allowed to publish a maximum of 4 modules.</p>';
			    		$("#confirm_msg").empty().html(dialog_message);
			    		$('#dialog').dialog({modal:true,hide:'explode'});
			    		this.timer = setTimeout(function (){
			    			$("#dialog").dialog('close');
			    			window.location.reload();
			    		},2000);	
				    }
				}
		    );
			
		}else if( attributes[0] =='unpublish' ){
			$.post("<?php echo $this->Html->url(array("controller" => "modules","action"=>"unpublish"));?>"+'/'+attributes[1],
				    function(data) {
				    	if(data=='true'){
				    		var dialog_message ='<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>';
				    		dialog_message +='Successfuly unpublished the module</p>';
				    		$("#confirm_msg").empty().html(dialog_message);
				    		$('#dialog').dialog({modal:true,hide:'explode'});
				    		this.timer = setTimeout(function (){
				    			$("#dialog").dialog('close');
				    			window.location.reload();
				    		},2000);
				        }
					}
			 );
		}	
	});

	$('.delete').live('click',function(){
		var attribute_id = $(this).attr('id');
		var attributes = attribute_id.split('_');
		$.post("<?php echo $this->Html->url(array("controller" => "modules","action"=>"delete"));?>"+'/'+attributes[1],
			    function(data) {
		    		if(data=='true'){
			    		var dialog_message ='<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>';
			    		dialog_message +='You have successfuly deleted the module</p>';
			    		$("#confirm_msg").empty().html(dialog_message);
			    		$('#dialog').dialog({modal:true,hide:'explode'});
			    		this.timer = setTimeout(function (){
			    			$("#dialog").dialog('close');
			    			window.location.reload();
			    		},2000);
			        }
				}
		 );	
	});
	
	$(window).resize(function() {
		$("#dialog").dialog("option", "position", "center");
	});
		
	$(window).scroll(function() {
	    $("#dialog").dialog("option", "position", "center");
	});	
});
/* ]]> */
</script>
<div id="wpwrap">
	<div id="adminmenuback"></div>
	<div id="adminmenuwrap">
		<div id="adminmenushadow"></div>
		<?php echo $this->element('menu');?>
	</div>
	<div id="wpcontent">
		<!--header menu -->
		<?php echo $this->element('user_navigation'); ?>
		<!-- end of header menu -->
		<div id="wpbody">
			<div id="wpbody-content">
				<div class="wrap">
					<div class="icon32 icon32-posts-post" id="icon-edit"><br></div>
					<h2>Modules search results for <strong>"<?php echo trim($this->params['url']['q']);?>"</strong><a class="add-new-h2" href="<?php echo $this->Html->url(array("controller" => "modules","action"=>"add"));?>">Add New</a></h2>
					<br/>
					<form id="frmSearch" name="frmSearch" method="get" action="<?php echo $this->Html->url(array("controller" => "modules","action"=>"search"));?>">
						<p class="search-box">
							<label for="user-search-input" class="screen-reader-text">Search Modules:</label>
							<input type="text" name="q" value="" id="q" class="text ui-widget-content ui-corner-all" />
							<input type="submit" value="Search Modules" class="button" id="search-users" />
						</p>
						<div class="tablenav top">
							<table cellspacing="0" class="wp-list-table widefat fixed users">
								<thead>
								<tr>
									<th class="role column-role">
									<a href="<?php echo $this->Html->url(array("controller" => "modules","action"=>"index"));?>/index/sort:name/direction:asc"><span>Module Name</span>
									<span class="sorting-indicator"></span></a></th>
									<th class="role column-role"><span>Module Short Description</span></th>
									<th class="role column-role"><span>Long Description</span></th>
									<th class="role column-role">Module Image</th>
									<th class="role column-role">Status</th>
									<th class="role column-role">Publish</th>
									<th class="role column-role">Delete</th>
								</tr>
								</thead>
								<tfoot>
								<tr>
									<th class="role column-role">
									<a href="<?php echo $this->Html->url(array("controller" => "modules","action"=>"index"));?>/index/sort:name/direction:asc"><span>Module Name</span>
									<span class="sorting-indicator"></span></a></th>
									<th class="role column-role"><span>Module Short Description</span></th>
									<th class="role column-role"><span>Long Description</span></th>
									<th class="role column-role">Module Image</th>
									<th class="role column-role">Status</th>
									<th class="role column-role">Publish</th>
									<th class="role column-role">Delete</th>
								</tr>
								</tfoot>
								<tbody class="list:user" id="the-list">
								<?php
								if(is_array($modules) && !empty($modules) ){
									foreach( $modules as $module ){
										$this->Modules->loadDataById( $module );	
								?>
								<tr class="alternate" id="user-1">
									<td class="role column-role">
									<strong><a href="<?php echo $this->Html->url(array("controller" => "modules","action"=>"view",$module['Module']['id']));?>"><?php echo trim($module['Module']['name']);?></a></strong>
									</td>
									<td class="role column-role"><?php echo $this->Modules->getShortDescription();?></td>
									<td class="role column-role"><?php echo $this->Modules->getLongDescription();?></td>
									<td class="role column-role"><img width="90" height="80" src="<?php echo $this->Modules->getImageSource();?>" /></td>
									<td class="role column-role"><?php echo $this->Modules->getDisplayStatus();?></td>    
									<th class="role column-role"><?php echo ($total_live_modules< 4)? $this->Modules->getContentStatus():'';?></th>
									<th class="role column-role"><?php echo $this->Modules->getDisplayDelete();?></th>
								</tr>
								<?php
										
									}//end of foreach
								}
								?>	
								</tbody>
							</table>
						</div>
					</form>
				</div>
				<div class="clear"></div>
			</div><!-- end wpbody-content -->
		</div><!-- end of wpbody -->
	</div>
</div>
<div id="dialog" title="" style="display:none;"><div id="confirm">&nbsp;</div></div>
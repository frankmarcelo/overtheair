<?php
/**
 * Modules Controller
 * 
 *
 * PHP version 5
 *
 */
App::import('Sanitize');
class ModulesController extends AppController {
/**
 * Controller name
 *
 * @var string
 * @access public
 */
    public $name = 'Modules';
    public $components = array(
        'Auth'
    );
/**
 * Models used by the Controller
 *
 * @var array
 * @access public
 */
    public $uses = array(
    	'User',
    	'Module',
    	'ModulesImage',
    	'Manifest',
    	'ManifestModule',
    	'ManifestTopic',
    	'ModuleType',
    	'ManifestAssessment',
    	'Setting',
    	'Topic',
        'TopicFiles',
    	'TopicFilesSources',
        'Assessment',
        'AssessmentsAnswer',
        'AssessmentsChoice',
        'AssessmentsReference'
    );
    
    public $helpers = array(
    	'Modules',
        'TopicFiles',
        'AssessmentsChoices',
        'AssessmentsReferences' ,
        'AssessmentsAnswer'
    );
    
    private $hasModule = false;

    public function beforeFilter() {
    	//Configure::write('debug',2);
        parent::beforeFilter();
        
        $this->Security->validatePost = false;
        $this->Auth->allow('stepOne','stepTwo');
        $this->TopicFilesSources->contain();
        $topicFilesSource = $this->TopicFilesSources->find('list');
        $this->set(compact('topicFilesSource'));
        
        
        $this->Module->contain();
    	$total_live_modules = $this->Module->find('count',array(
           	'conditions' => array( 'Module.status IN ('. Configure::read('status_live').')' ) 
          )
    	);
    	$this->set(compact('total_live_modules'));
    	
        
        $this->Module->contain();
    	$oModule = $this->Module->find('first',array(
           	'conditions' => array( 'Module.status IN ('. Configure::read('status_live').','.Configure::read('status_onhold').')' ), 
           	'order' => array('Module.id' => 'desc')
          )
    	);
    	
    	if( intval($oModule['Module']['id']) > 0){
    		$this->hasModule = true;
    		$this->set('hasModule',true);
    	}
    }

  	public function index(){
  		if( $this->hasModule == false ){
    		$this->redirect(array('controller'=>'modules',"action"=>"add"));	
    	}
    	
    	$modules = $this->Module->find('all',array(
           	'conditions' => array( 'Module.status IN ('. Configure::read('status_live').','.Configure::read('status_onhold').')' ), 
           	'order' => array('Module.display_order, Module.date_created DESC')
          )
    	);
    	$this->set(compact('modules'));
    }
    
	public function search(){
    	$this->set('title_for_layout', __('Search Modules', true));
        
    	$query = null;
        $sOrder = null;
        if( isset($this->params['url']['q']) && strlen(trim($this->params['url']['q']))> 0 ){
        	$query = trim($this->params['url']['q']);
        	if(strlen($query)<1){
        		$this->Session->setFlash(__('Cannot process request invalid modules search.', true), 'default', array('class' => 'error'));
            	$this->redirect(array('controller'=>'modules','action' => 'index'));	
        	}
        }else{
        	$this->Session->setFlash(__('Cannot process request invalid modules search.', true), 'default', array('class' => 'error'));
            $this->redirect(array('controller'=>'modules','action' => 'index'));	
        }
        
        $conditions = array();
        if( !empty($query) && strlen($query)> 0 ){
        	
        	App::import('Sanitize');
        	$full_text_search = explode (" ", $query );
     	   	foreach ($full_text_search as $wordindex => $wordvalue) {
				if( strlen(trim($wordvalue))==0) {
					unset($full_text_search[$wordindex]);
            	}
			}
    
			$fulltext_array = array();
			$like_array = array();
			$original_search = array();
			foreach ($full_text_search as $wordindex => $wordvalue) {
				if( strlen ($wordvalue)<=3 ) {
					$like_array[] = Sanitize::clean($wordvalue);
            	}else{
            	    $fulltext_array[] = "+".Sanitize::clean($wordvalue)."*";
            	}
            }
        	
			$original_search = array_merge($like_array,$fulltext_array);
			$boolSearchByLike = $this->Module->getLikeName($query);
        	if( $boolSearchByLike == true){
        		$conditions[] = array(
	                           		"(TRIM(UPPER(Module.name))) LIKE '%".addslashes($query)."%'",
        							"Module.name IS NOT NULL",
        							"LENGTH(TRIM(Module.name)) > 0",			
	        						"Module.status IN (".Configure::read('status_live').','.Configure::read('status_onhold').")"
	        	);
	        	$sOrder = "Module.display_order, Module.date_created DESC";
	        	
        	}else{
        		
        		$fullword_query = trim(implode(' ', $original_search));
        		$conditions[] = array(
	        					"( 
									(MATCH(Module.name,Module.short_description,Module.long_description) AGAINST ('".addslashes(trim($fullword_query))."' IN boolean MODE)) OR 
	                       			(TRIM(UPPER(Module.name))) LIKE '%".addslashes($query)."%'
	                       		)",
	        					"( Module.name IS NOT NULL )",
        						"( LENGTH(TRIM(Module.name))> 0 )",
	        					"Module.status IN (".Configure::read('status_live').','.Configure::read('status_onhold').")"
	        	);
	        	$sOrder = "MATCH(Module.name,Module.short_description,Module.long_description) AGAINST ('".addslashes(trim($query))."') DESC,Module.display_order, Module.date_created DESC";
	        	
        	}
	    }else{
            $conditions[] = array(
            					"( Module.name IS NOT NULL )",
        						"( LENGTH(TRIM(Model.name))> 0 )",
            					"Model.status IN (".Configure::read('status_live').",".Configure::read('status_onhold').")"
            				);
        }
        
      	$sPage = 1;
    	if( isset($this->params['url']['page']) ){
    		if( is_numeric($this->params['url']['page']) ){
    			$sPage = $this->params['url']['page'];
    		}
    	}
    	
    	$order = array();
    	$recursive = 1;
    	if( !empty($query) && $query ){
    		$order = array($sOrder);
    	}else{
            $order = array( 'Module.display_order, Module.date_created DESC' );
        }
    	
        $this->paginate['Module'] = array(
    				'recursive' => $recursive,
    				'conditions'=> $conditions,
    				'page'  => $sPage,
                    'order' => $order
        );
    	
    	$this->set('modules',$this->paginate('Module'));
        
    }
    
    public function unpublish($id){
    	//Configure::write('debug',2);
    	
    	if( $this->RequestHandler->isPost() && !empty($id) ){
	    	if (!$id || is_null($id) || empty($id) ) {
	            $this->Session->setFlash(__('Invalid id for Modules', true), 'default', array('class' => 'error'));
	            $this->redirect(array('action' => 'index'));
	        }
	
	        $this->Module->contain();
	        $module = $this->Module->findById($id);
	        if( !isset($module['Module']['id'])  ) {
	            $this->set('data',false);
	        }
	
	        
	        $this->Module->id = $module['Module']['id'];
	        $this->Module->begin();
	        $this->data['Module']['status'] = Configure::read("status_onhold");
	        $this->data['Module']['date_modified'] = date("Y-m-d H:i:s",strtotime("now"));
	        $this->data['Module']['who_modified'] = $this->Session->read('Auth.User.id');
	        if ($this->Module->save($this->data)) {
	        	$this->Module->commit();
	        	
	        	$manifestData = array();
	        	$this->Manifest->begin();
	        	$manifestData['Manifest']['change_type'] = 'module';
	        	$manifestData['Manifest']['status'] = Configure::read("status_live");
	        	$manifestData['Manifest']['comments'] = 'Module Unpublished By '.$this->Session->read('Auth.User.username');
	        	$manifestData['Manifest']['date_modified'] = date("Y-m-d H:i:s",strtotime("now"));
	        	$manifestData['Manifest']['date_created'] = date("Y-m-d H:i:s",strtotime("now"));
	        	$manifestData['Manifest']['who_created'] = $this->Session->read('Auth.User.id');	
	        	if( $this->Manifest->save($manifestData) ){
	        		$this->Manifest->commit();
	        	}else{
	        		$this->Manifest->rollback();
	        		$this->Module->rollback();	
	        	}
	            $this->set('data',true);
	        }else{
	            $this->Module->rollback();
	            $this->set('data',false);
	        }	
    	}
    }
    
    public function delete($id){
    	
    	//Configure::write('debug',2);
    	if( $this->RequestHandler->isPost() && !empty($id) ){
	    	if (!$id || is_null($id) || empty($id) ) {
	            $this->set('data',false);
	        }
			
	        $this->Module->contain();
	        $module = $this->Module->findById($id);
	        if( !isset($module['Module']['id'])  ) {
	            $this->set('data',false);
	        }
	
	        
	        $this->Module->id = $module['Module']['id'];
	        $this->Module->begin();
	        $this->data['Module']['status'] = Configure::read("status_deleted");
	        $this->data['Module']['date_modified'] = date("Y-m-d H:i:s",strtotime("now"));
	        $this->data['Module']['who_modified'] = $this->Session->read('Auth.User.id');
	        if ($this->Module->save($this->data)) {
	        	$this->Module->commit();
	        	
	        	$manifestData = array();
	        	$this->Manifest->begin();
	        	$manifestData['Manifest']['change_type'] = 'module';
	        	$manifestData['Manifest']['status'] = Configure::read("status_live");
	        	$manifestData['Manifest']['comments'] = 'Module Deleted By '.$this->Session->read('Auth.User.username');
	        	$manifestData['Manifest']['date_modified'] = date("Y-m-d H:i:s",strtotime("now"));
	        	$manifestData['Manifest']['date_created'] = date("Y-m-d H:i:s",strtotime("now"));
	        	$manifestData['Manifest']['who_created'] = $this->Session->read('Auth.User.id');	
	        	if( $this->Manifest->save($manifestData) ){
	        		$this->Manifest->commit();
	        		
	        		$this->ManifestModule->begin();
	        		$manifestData = array();
		        	$manifestModuleData['ManifestModule']['manifest_id'] = $this->Manifest->getInsertID();
		        	$manifestModuleData['ManifestModule']['module_id'] = $this->Module->id;
		        	$manifestModuleData['ManifestModule']['date_modified'] = date("Y-m-d H:i:s",strtotime("now"));
		        	$manifestModuleData['ManifestModule']['date_created'] = date("Y-m-d H:i:s",strtotime("now"));
		        	$manifestModuleData['ManifestModule']['who_created'] = $this->Session->read('Auth.User.id');
		        	if( $this->ManifestModule->save($manifestModuleData) ){
	        			$this->ManifestModule->commit();
		        	}	
	        		
	        	}else{
	        		$this->Manifest->rollback();
	        		$this->Module->rollback();	
	        	}
	            $this->set('data',true);
	        }else{
	            $this->Module->rollback();
	            $this->set('data',false);
	        }	
    	}
    }
    
    public function add(){
    	//Configure::write('debug',2);
    	if( !Cache::read('create_module_'.$this->Session->read('Auth.User.username'),'short') ) {
        	Cache::write('create_module_'.$this->Session->read('Auth.User.username'),$this->getLatestModuleId(), 'short');  	
        }
    	$this->set('latest_module_version', Cache::read('create_module_'.$this->Session->read('Auth.User.username'),'short'));
    	
    	
    	$this->ModuleType->contain();
    	$moduleTypes = $this->ModuleType->find('all');
    	
    	
    	$this->TopicFiles->contain();
    	$topicFiles = $this->TopicFiles->find('all',array(
    			'conditions' => array(
    				'LENGTH(TRIM(TopicFiles.raw_name)) > 0',
    				'TopicFiles.raw_name IS NOT NULL',
    	 			'TopicFiles.topic_id < 1',
    				'TopicFiles.status IN ('.Configure::read('status_live').')'
    			)
    		)
    	);
    	$this->set(compact('topicFiles','moduleTypes'));
    	
    	
    	$this->ModulesImage->contain();
    	$oModulesImages = $this->ModulesImage->find('all',
    		array(
    			'group' => array('ModulesImage.raw_name'), //fields to GROUP BY
    		)
    	);
    	
    	if( !empty($oModulesImages) ){
    		App::import('Core', 'File');
        	$DownloadLink = Configure::read('downloadLink');
	    	$aModuleImageLookUp = array();	
    		foreach( $oModulesImages as $oModulesImage ){
    			$SourceFile = $oModulesImage['ModulesImage']['raw_name'];
    			if( file_exists($DownloadLink . DS . 'modules' . DS . $SourceFile) ){
					$aModuleImageLookUp[] = $oModulesImage;
    			}		
    		}
    		if(!empty($aModuleImageLookUp)){
    			$this->set(compact('aModuleImageLookUp'));
    		}
    	}
    }
    
    public function edit($id){
    	//Configure::write('debug',2);
    	if (!$id || is_null($id) || empty($id) ) {
            $this->Session->setFlash(__('Invalid id for Modules', true), 'default', array('class' => 'error'));
            $this->redirect(array('action' => 'index'));
        }

        $module = $this->Module->findById($id);
        if( !isset($module['Module']['id']) || 
        	( $module['Module']['status'] != Configure::read('status_live')  && 
        	  $module['Module']['status'] != Configure::read('status_onhold'))
        ) {
            $this->Session->setFlash(__('Invalid username or username not found.', true), 'default', array('class' => 'error'));
            $this->redirect(array('action' => 'index'));
        }
        
        $total_modules = $this->Module->find('all',array(
        		'conditions' => array('Module.status' => Configure::read("status_live") ),
            )
        );
        
        //print_r($module);
        
        $this->set('latest_module_version', $module['Module']['id']);
        $this->set(compact('module'));
        
        $this->ModulesImage->contain();
    	$oModulesImages = $this->ModulesImage->find('all');
    	if( !empty($oModulesImages) ){
    		App::import('Core', 'File');
        	$DownloadLink = Configure::read('downloadLink');
	    	$aModuleImageLookUp = array();	
    		foreach( $oModulesImages as $oModulesImage ){
    			$SourceFile = $oModulesImage['ModulesImage']['raw_name'];
    			if( file_exists($DownloadLink . DS . 'modules' . DS . $SourceFile) ){
					$aModuleImageLookUp[] = $oModulesImage;
    			}		
    		}
    		if(!empty($aModuleImageLookUp)){
    			$this->set(compact('aModuleImageLookUp'));
    		}
    	}
    	
    	$topicFiles = $this->TopicFiles->find('all',array(
    			'conditions' => array(
    				'LENGTH(TRIM(TopicFiles.raw_name)) > 0',
    				'TopicFiles.raw_name IS NOT NULL',
    	 			'TopicFiles.topic_id < 1',
    				'TopicFiles.status IN ('.Configure::read('status_live').')'
    			)
    		)
    	);
    	$this->set(compact('topicFiles'));
    }
    
    public function view($id){
    	if (!$id || is_null($id) || empty($id) ) {
            $this->Session->setFlash(__('Invalid id for Modules', true), 'default', array('class' => 'error'));
            $this->redirect(array('action' => 'index'));
        }

        $module = $this->Module->findById($id);
        if( !isset($module['Module']['id']) || 
        	( $module['Module']['status'] != Configure::read('status_live')  && 
        	  $module['Module']['status'] != Configure::read('status_onhold'))
        ) {
            $this->Session->setFlash(__('Invalid username or username not found.', true), 'default', array('class' => 'error'));
            $this->redirect(array('action' => 'index'));
        }
        
        $total_modules = $this->Module->find('all',array(
        		'conditions' => array('Module.status' => Configure::read("status_live") ),
            )
        );
    	$this->set(compact('module','total_modules'));
    }
    
    public function upload(){
    }
    
	public function getLatestModuleId(){
    	$this->Module->contain();
    	$oModule = $this->Module->find('first',array(
    			'fields'=> array("Module.id AS InsertId"),
	     		'order' => array('Module.id'=>'desc')
	     	)
	    );
	    return intval($oModule['Module']['InsertId'])+1;
    }
    
	public function getLatestAssessmentAnswerId(){
    	$this->AssessmentsAnswer->contain();
    	$oAssessmentsAnswer = $this->AssessmentsAnswer->find('first',array(
    			'fields'=> array("AssessmentsAnswer.id AS InsertId"),
	     		'order' => array('AssessmentsAnswer.id'=>'desc')
	     	)
	    );
	    return intval($oAssessmentsAnswer['AssessmentsAnswer']['InsertId'])+1;
    }
    
	public function getLatestTopicIdByModuleId($moduleId){
    	$this->Topic->contain();
    	$iTopic = $this->Topic->find('count',array(
    			'conditions' => array('Topic.module_id'=> $moduleId),
    			'order' => array('Topic.id'=>'desc')
	     	)
	    );
	    return intval($iTopic)+1;
    }
    
    public function getLatestAssessmentId(){
    	$this->Assessment->contain();
    	$oAssessments = $this->Assessment->find('first',array(
    			'fields'=> array("Assessment.id AS InsertId"),
	     		'order' => array('Assessment.id'=>'desc')
	     	)
	    );
	    return intval($oAssessments['Assessment']['InsertId'])+1;
    }
    
    public function getLatestAssessmentsReferenceId(){
    	
    	$this->AssessmentsReference->contain();
    	$oAssessmentsReference = $this->AssessmentsReference->find('first',array(
    			'fields'=> array("AssessmentsReference.id AS InsertId"),
	     		'order' => array('AssessmentsReference.id'=>'desc')
	     	)
	    );
	    return intval($oAssessmentsReference['AssessmentsReference']['InsertId'])+1;
    }
    
	public function getLatestAssessmentChoiceId(){
    	$this->AssessmentsChoice->contain();
    	$oAssessments = $this->AssessmentsChoice->find('first',array(
    			'fields'=> array("AssessmentsChoice.id AS InsertId"),
	     		'order' => array('AssessmentsChoice.id'=>'desc')
	     	)
	    );
	    return intval($oAssessments['AssessmentsChoice']['InsertId'])+1;
    }
    
    public function stepFour(){
    	//Configure::write('debug', 2);
        $this->layout ='ajax';
        if( $this->RequestHandler->isPost() && !empty($this->data)){
        	
        	/*
        	$this->TopicFiles->contain();
    		$topicFiles = $this->TopicFiles->find('all',array(
    				'conditions' => array('LENGTH(TRIM(TopicFiles.raw_name)) > 0 AND TopicFiles.raw_name IS NOT NULL')
    			)
    		);
    		$this->set(compact('topicFiles'));*/
        	
        	if(	intval($this->data['Module']['id'])> 0 	&& 
     			isset($this->data['Assessment']) 		&& 
     			isset($this->data['AssessmentsChoice'])	&&
     			isset($this->data['AssessmentsReference']) &&
     			!empty($this->data['Assessment'])		&&
     			!empty($this->data['AssessmentsChoice']) && 
     			!empty($this->data['AssessmentsReference'])	 
     		){
	     		$oAssessments = $this->Assessment->find('all',array(
	     				'conditions' => array('Assessment.module_id'=> intval($this->data['Module']['id']))
	     			)
	     		);
     			
	     		if(!empty($oAssessments)){
	     			foreach( $oAssessments as $oAssessment ){
	     				$this->Assessment->delete($oAssessment['Assessment']['id']);
	     				$oAssessmentsChoices = $this->AssessmentsChoice->find('all',array(
	     						'conditions' => array('AssessmentsChoice.assessment_id'=> intval($oAssessment['Assessment']['id']))
	     					)
	     				);
	     				//delete choices
	     				if(!empty($oAssessmentsChoices)){
	     					foreach( $oAssessmentsChoices as $oAssessmentsChoice ){
	     						$this->AssessmentsChoice->delete($oAssessmentsChoice['AssessmentsChoice']['id']);
	     					}	
	     				}
	     				
	     				
	     				$oAssessmentsAnswers = $this->AssessmentsAnswer->find('all',array(
	     						'conditions' => array('AssessmentsAnswer.assessment_id'=> intval($oAssessment['Assessment']['id']))
	     					)
	     				);
	     				
	     				if(!empty($oAssessmentsAnswers)){
	     					foreach( $oAssessmentsAnswers as $oAssessmentsAnswer ){
	     						$this->AssessmentsChoice->delete($oAssessmentsAnswer['AssessmentsAnswer']['id']);
	     					}	
	     				}
	     				
	     				
	     				$oAssessmentsReferences = $this->AssessmentsReference->find('all',array(
	     						'conditions' => array('AssessmentsReference.assessment_id'=> intval($oAssessment['Assessment']['id']))
	     					)
	     				);
	     				
	     				if(!empty($oAssessmentsReferences)){
	     					foreach( $oAssessmentsReferences as $oAssessmentsReference ){
	     						$this->AssessmentsReference->delete($oAssessmentsReference['AssessmentsReference']['id']);
	     					}	
	     				}
	     			}
	     		}
	     			
	     		$Assessments = $this->data['Assessment']['question'];
	     		$AssessmentsChoices = $this->data['AssessmentsChoice']['text'];
	     		$NumOfQuestions = count($Assessments);
	     			
	     		$AssessmentIds = array();
	     		foreach( $Assessments as $key => $Assessment ){
	     			foreach( $Assessment as $key2 => $AssessmentQuestion ){
	     				$data = array();
	     				$assessmentId = $this->getLatestAssessmentId();
	     				$this->Assessment->id = $assessmentId;
	     				$data['Assessment']['question'] =Sanitize::clean($AssessmentQuestion);
	     				$data['Assessment']['display_sequence'] = $key;
	     				$data['Assessment']['module_id']= intval($this->data['Module']['id']);
	     				$data['Assessment']['status']=Configure::read('status_live');
	     				$data['Assessment']['date_created']=date("Y-m-d H:i:s",strtotime("now"));
	     				$data['Assessment']['who_created']=$this->Session->read('Auth.User.id');
	     				if($this->Assessment->save($data)){
	     					$AssessmentIds[$key] = $this->Assessment->getInsertID();	
	     				}
	     			}
     			}
     			
     			$aAnswerId = array();
     			if(!empty($AssessmentsChoices)){
     				foreach( $AssessmentsChoices as $key => $AssessmentChoice ){
     					$iterator = 1;
     					foreach( $AssessmentChoice as $key2 => $AssessmentChoiceAnswer ){	
     						$dataChoice = array();
     						$assessmentChoiceId = $this->getLatestAssessmentChoiceId();
                            $this->AssessmentsChoice->id = $assessmentChoiceId;
			     			$dataChoice['AssessmentsChoice']['text'] = Sanitize::clean($AssessmentChoiceAnswer);
			     			$dataChoice['AssessmentsChoice']['assessment_id'] = $AssessmentIds[$key];
							$dataChoice['AssessmentsChoice']['display_sequence'] = intval($key2)+1;
							$dataChoice['AssessmentsChoice']['status'] = Configure::read('status_live');
							$dataChoice['AssessmentsChoice']['date_created'] = date("Y-m-d H:i:s",strtotime("now"));
							$dataChoice['AssessmentsChoice']['who_created'] = $this->Session->read('Auth.User.id');
							if( $this->AssessmentsChoice->save($dataChoice) ){
								if( isset($this->data['AssessmentsChoice']['id']) &&  
									$this->data['AssessmentsChoice']['id'][$key][0] == $iterator ){
									$dataAnswer = array();
									$this->AssessmentsAnswer->id = $this->getLatestAssessmentAnswerId(); 
									$dataAnswer['AssessmentsAnswer']['assessments_choice_id'] = $this->AssessmentsChoice->getInsertID();
									$dataAnswer['AssessmentsAnswer']['assessment_id'] = $AssessmentIds[$key];
									$dataAnswer['AssessmentsAnswer']['status'] = Configure::read('status_live');
									$dataAnswer['AssessmentsAnswer']['date_created'] = date("Y-m-d H:i:s",strtotime("now"));
									$dataAnswer['AssessmentsAnswer']['who_created'] = $this->Session->read('Auth.User.id');
									$this->AssessmentsAnswer->save($dataAnswer);		
								}
							}
							$iterator++;
     					}
     				}
     			}	
     			
     			
     			if(isset($this->data['AssessmentsReference']['id'])){
     				$AssessmentsReferences = $this->data['AssessmentsReference']['id'];
	     			if(!empty($AssessmentsReferences)){
	     				foreach( $AssessmentsReferences as $key => $AssessmentsReference ){
	     					foreach($AssessmentsReference as $key2 => $AssessmentTopicId ){
	     						
	     						$assessmentsReferenceId = $this->getLatestAssessmentsReferenceId();
			     				$this->AssessmentsReference->id = $assessmentsReferenceId;
					     		$data['AssessmentsReference']['assessment_id'] =$AssessmentIds[$key];
					     		$data['AssessmentsReference']['topic_id'] = $AssessmentTopicId;
								$data['AssessmentsReference']['display_sequence'] = (intval($key2) + 1 );
								$data['AssessmentsReference']['status'] = Configure::read('status_live');
								$data['AssessmentsReference']['date_created'] = date("Y-m-d H:i:s",strtotime("now"));
								$data['AssessmentsReference']['who_created'] = $this->Session->read('Auth.User.id');
								$this->AssessmentsReference->save($data);
	     					}
		     			}
		     		}
	     		}
     			
	     		if( Cache::read('create_module_'.$this->Session->read('Auth.User.username'),'short') ) {
        			Cache::delete('create_module_'.$this->Session->read('Auth.User.username'),'short');  	
        		}
        		$this->Session->delete('Module');
        		$this->Session->delete('ModuleType');
        		$this->Session->delete('aDisplayTopicsReference');
        		$this->Session->delete('aShowTopic');
    			$this->Session->delete('Topic');
    			$this->Session->setFlash(__('You have successfully created new module', true), 'default', array('class' => 'success'));
    			$mod = new stdClass();
    			$mod->module_id = intval($this->data['Module']['id']);
    			$this->set('data',$mod);
    		}else{
     			$this->set('data',false);
     		}
		}
    }
    
    public function stepThree(){
    	//Configure::write('debug', 0);
        $this->layout ='ajax';
        if( $this->RequestHandler->isPost() && !empty($this->data)){
        	if(	intval($this->data['Module']['id'])> 0 	&& 
     			isset($this->data['TopicFiles']) 		&& 
     			!empty($this->data['TopicFiles'])		
     		){
     			if(!isset($this->data['Module']['edit'])){
     				$aTopicFiles = $this->data['TopicFiles'];
     				/**set existing data with topic files to zero **/
     				foreach( $aTopicFiles as $topic_id => $topicFilesIds ){
     					$this->TopicFiles->contain();
     					$uTopicFiles = $this->TopicFiles->find('all',array(
     							'conditions'=> array('TopicFiles.topic_id'=> $topic_id)
     						)
     					);
     					
	     				if(!empty($uTopicFiles)){
							$data = array();
							foreach( $uTopicFiles as $topicFile ){
								
								$this->TopicFiles->begin();
								$this->TopicFiles->id = $topicFile['TopicFiles']['id'];
								$data['TopicFiles']['topic_id'] = Configure::read('status_onhold');
								$data['TopicFiles']['date_modified'] = date("Y-m-d H:i:s",strtotime("now"));
								$data['TopicFiles']['who_modified'] = $this->Session->read('Auth.User.id');
								if($this->TopicFiles->save($data)){
									$this->TopicFiles->commit();
								}else{
									$this->TopicFiles->rollback();
								}		
							}
						}
						unset($uTopicFiles);
     				}
     				
     				
     				$aTopicIds = array();
	     			foreach( $aTopicFiles as $topic_id => $topicFilesIds ){
	     				$aTopicIds[] = $topic_id;
	     				foreach( $topicFilesIds as $topicFileId ){
	     					$this->TopicFiles->contain();
							$oTopic = $this->TopicFiles->findById($topicFileId);
							if(!empty($oTopic)){
								$data = array();
								$this->TopicFiles->begin();
								$this->TopicFiles->id = $topicFileId;
								$data['TopicFiles']['topic_id'] = $topic_id;
								$data['TopicFiles']['date_modified'] = date("Y-m-d H:i:s",strtotime("now"));
								$data['TopicFiles']['who_modified'] = $this->Session->read('Auth.User.id');
								if($this->TopicFiles->save($data)){
									$this->TopicFiles->commit();
								}else{
									$this->TopicFiles->rollback();
								}
							}
							unset($oTopic);
	     				}
	     			}
	     				
	     			$this->Topic->contain();
	     			$aModuleTopics = $this->Topic->find('list',array(
	     					'conditions' => array('Topic.module_id' => intval($this->data['Module']['id']) )
	     				)
	     			);
	     			
	     			$aDisplayModuleTopics = array();
	     			if(is_array($aModuleTopics) && sizeof($aModuleTopics)> 0){
	     				foreach($aModuleTopics as $topic_id => $topic_name ){
	     					$aDisplayModuleTopics[] = $topic_id;
	     				}	
	     			}
	     			
	     			$this->TopicFiles->contain();
					$referenceTopics = $this->TopicFiles->find('list',array(
						'conditions' => array('TopicFiles.topic_id IN ('.implode(",",$aDisplayModuleTopics).')' )
						)
					);
					
					$aDisplayTopics = array();
					if(!empty($referenceTopics)){
						foreach( $referenceTopics as $key => $value ){
							$aDisplayTopics[] =array( 'id'=> $key , 'value'=> $value);	
						}
					}
					$this->Session->write('aDisplayTopicsReference',$aDisplayTopics);
				}else{
					$oTopicFiles = array();
 					if( !empty($this->data['Topic']['id'])){
 						foreach( $this->data['Topic']['id'] as $key => $topic_id ){
 							$this->TopicFiles->contain();
 							$oTopics = $this->TopicFiles->find('all',array(
 									'conditions' => array(
 										'TopicFiles.topic_id' =>  $topic_id
 									)
 								)
 							);
 							$oTopicFiles[$topic_id] = $oTopics;
 						}
 					}

 					print_r($oTopicFiles);
     				
     				
     				/*$uTopicFiles = $this->TopicFiles->find('all',array(
	     						'conditions'=> array('TopicFiles.topic_id'=> $topic_id)
	     					)
	     				);
	     				if(!empty($uTopicFiles)){
							$data = array();
							foreach( $uTopicFiles as $topicFile ){
								
								$this->TopicFiles->begin();
								$this->TopicFiles->id = $topicFile['TopicFiles']['id'];
								$data['TopicFiles']['topic_id'] = Configure::read('status_onhold');
								$data['TopicFiles']['date_modified'] = date("Y-m-d H:i:s",strtotime("now"));
								$data['TopicFiles']['who_modified'] = $this->Session->read('Auth.User.id');
								if($this->TopicFiles->save($data)){
									$this->TopicFiles->commit();
								}else{
									$this->TopicFiles->rollback();
								}		
							}
						}
     				*/
     			}
			}else{
     			$this->set('data',false);		
     		}
     	}
    }
    
    public function getModuleDisplayOrder(){
    	$this->Module->contain();
    	$totalRows = $this->Module->find('count',array(
    			'conditions'=> array("Module.status"=>Configure::read('status_live'))
	     	)
	    );
	    $totalRows++;
	    $totalRows = ($totalRows <1 )? 1: $totalRows;
	    return $totalRows;
    }
    
    
    
    private function findTopicByArray( $topicId , $oTopics = array()){
    	$found = false;
    	if( !empty($oTopics) ){
    		foreach( $oTopics as $key => $oTopic ){
    			if( $oTopic['Topic']['id'] == $topicId ){
    				$found = $oTopic;
    				break;
    			}
    		}
    	}
    	return $found;
    } 
    
    public function stepTwo(){
    	//Configure::write('debug', 2);
        $this->layout ='ajax';
        if( $this->RequestHandler->isPost() && !empty($this->data)){
        	$this->TopicFiles->contain();
    		$topicFiles = $this->TopicFiles->find('all',array(
    			'conditions' => array('LENGTH(TRIM(TopicFiles.raw_name)) > 0 AND TopicFiles.raw_name IS NOT NULL AND TopicFiles.topic_id < 1')
    			)
    		);
    	
    		$this->set(compact('topicFiles'));
        	if(	intval($this->data['Module']['id'])> 0 && !empty($this->data['Topic']['name']) ){
     			$aInvalid = array();
     			$topics = $this->data['Topic'];
     			foreach( $topics['name'] as $key => $topicData ){
     				
     				if( strlen(trim($topicData)) <1 ){
     					$aInvalid['name'][] = $key;
     				}
     			}
     				
     			/*foreach( $topics['description'] as $key => $topicData ){
     				if( strlen(trim($topicData)) <1 ){
     					$aInvalid['description'][] = $key;
     				}
     			}*/	
     			if( empty($aInvalid) ){
     				
     				if( !isset($this->data['Module']['edit'])){
     					
						$this->Session->write('Topic',$this->data['Topic']);
						$this->Module->contain();
						
						$oModule = $this->Module->findBySessionToken($this->Session->read('Module.id'));
		    			if(!empty($oModule) && count($oModule)>0){
		    				$this->Module->id = $oModule['Module']['id'];
		    			}

		    			$this->set('latest_module_version',$this->data['Module']['id']);
						
		    			$this->Module->begin();
						$this->data['Module']['modules_image_id'] = $this->Session->read('Module.modules_image_id');
						$this->data['Module']['session_token'] = $this->Session->read('Module.id');
						$this->data['Module']['who_modified'] = $this->Session->read('Auth.User.id');
						$this->data['Module']['date_modified'] = date("Y-m-d H:i:s",strtotime("now"));
						$this->data['Module']['status'] = Configure::read('status_onhold');
						$this->data['Module']['name']= Sanitize::clean($this->Session->read('Module.name'),array('encode' => true,'remove_html'=>true));
						$this->data['Module']['long_description'] = Sanitize::clean(strip_tags(trim($this->Session->read('Module.long_description'))),array('encode' => true,'remove_html'=>true));
						$this->data['Module']['short_description'] = Sanitize::clean(strip_tags(trim($this->Session->read('Module.short_description'))),array('encode' => true,'remove_html'=>true));
						$this->data['Module']['display_order'] =  $this->Session->read('Module.display_order');
						$this->data['Module']['date_created'] = $this->data['Module']['date_modified'];
						$this->data['Module']['who_created'] = $this->Session->read('Auth.User.id');
						
	    				if( $this->Module->save($this->data) ){
	    					$this->Module->commit();
	    					$aTopics = $this->Session->read('Topic');
	    					if( !empty($oModule)){
	    						$moduleId = ($oModule['Module']['id']);
	    						
	    					}else{
	    						$moduleId = $this->Module->getInsertID();
	    					}
	    					
	    					$this->Topic->query(" DELETE FROM topics WHERE module_id ='".$moduleId."'");
	    					unset($this->data['Module']);
	    					
	    					$data = array();
	    					$aTopicsData = array();
							
							for( $i = 0; $i < count($aTopics['name']); $i++ ){
								if( strlen(trim($aTopics['name'][$i]))>0 ){//only insert the one with valid value
		    						$sql = "INSERT INTO topics 
		    								SET module_id='".$moduleId."',
		    									who_created='".$this->Session->read('Auth.User.id')."',
												date_modified='".date("Y-m-d H:i:s",strtotime("now"))."',
												date_created='".date("Y-m-d H:i:s",strtotime("now"))."',
												status='".Configure::read('status_live')."',
												cache_views=0,
												has_children=0,
												name='".Sanitize::clean($aTopics['name'][$i])."',
												display_sequence='".($i+1)."';";
		    						$this->Topic->query($sql);
								}
	    					}
	    					
	    					$this->Topic->contain();
	    					$aTopicsData = $this->Topic->find('all',array(
	    							'conditions' => array('Topic.module_id' => $moduleId ),
	    							'order' => array('Topic.id')
	    						)
	    					);
	    					
	    					$aShowTopic = array();
	    					$aTopicId = array();
	    					if(!empty($aTopicsData)){
	    						foreach( $aTopicsData as $aTopicsInfo ){
	    							$aTopicId[] = $aTopicsInfo['Topic']['id'];
	    							$aShowTopic[] = array('id'=> $aTopicsInfo['Topic']['id'],'name'=> $aTopicsInfo['Topic']['name']);	
	    						}
							}
							
							$this->TopicFiles->contain();
							$aTopicsFiles = $this->TopicFiles->find('all',array(
	    							'conditions' => array('TopicFiles.topic_id IN ('.implode(",",$aTopicId).')' ),
	    						)
	    					);
							
	    					if(!empty($aTopicsFiles)){
	    						foreach( $aTopicsFiles as $aTopicFile ){	
	    							$this->TopicFiles->begin();
									$this->TopicFiles->id = $aTopicFile['TopicFiles']['id'];
									$data['TopicFiles']['topic_id'] = Configure::read('status_onhold');
									$data['TopicFiles']['date_modified'] = date("Y-m-d H:i:s",strtotime("now"));
									$data['TopicFiles']['who_modified'] = $this->Session->read('Auth.User.id');
									if($this->TopicFiles->save($data)){
										$this->TopicFiles->commit();
									}else{
										$this->TopicFiles->rollback();
									}							
	    						}
	    					}
							$this->Session->write('aShowTopic',$aShowTopic);
						}else{
	    					$this->Module->rollback();
	    					$this->set('data',false);
	    					$this->Session->delete('Module');
	    					$this->Session->delete('Topic');	
	    				}
	    			}else{//edit
     					
     					$this->Module->contain();
     					$oModule = $this->Module->findById($this->data['Module']['id']);
     					if(!empty($oModule) && sizeof($oModule)>0){

     						$this->Topic->contain();
     						$oTopics = $this->Topic->find('all', array(
     								'conditions' => array('Topic.module_id'=> $this->data['Module']['id'])
     							)
     						);

     						if(  !empty($oTopics) && 
     							 isset($this->data['Topic']['name']) && 
     							 !empty($this->data['Topic']['name'])
     						){
     							 $aFound = array();
     							 $aNew = array();
     							 $iterator = 0;
     							 
     							 foreach( $this->data['Topic']['name'] as $topic_id => $topic_value ){
     								
     								if( $aTopicData = $this->findTopicByArray($topic_id,$oTopics) ){
     									
     									$aFound[] = $topic_id;
     									if( trim($aTopicData['Topic']['name']) != trim($topic_value) || 
     										trim($aTopicData['Topic']['description']) != trim($this->data['Topic']['description'][$topic_id]) 
     									){
     										
     										$this->Topic->contain();	
     										$oTopic = $this->Topic->findById($topic_id);
     										
											if($oTopic){
     											$this->Topic->id = $topic_id;
     											$aTopicArray = array();
     											$aTopicArray['Topic']['name'] = Sanitize::clean($topic_value);
     											$aTopicArray['Topic']['description'] =  Sanitize::clean($this->data['Topic']['description'][$topic_id]);
     											$aTopicArray['Topic']['date_modified'] = date("Y-m-d H:i:s",strtotime("now"));
     											$aTopicArray['Topic']['who_modified'] = $this->Session->read('Auth.User.id');
     											if( $this->Topic->save($aTopicArray)){
     												$this->Topic->commit();
     												
     												$iManifestSearch = $this->ManifestTopic->find('count',array(
					    									'conditions' => array(
					    										'ManifestTopic.topic_id' => $topic_id,
					    										'ManifestTopic.manifest_id' => $this->data['Module']['manifestversion']
					    									)
					    								)
					    							);
					    							
					    							if( $iManifestSearch < 1 ){
			    										$this->ManifestTopic->begin();
	     												$manifestTopic['ManifestTopic']['manifest_id'] = $this->data['Module']['manifestversion'];
	     												$manifestTopic['ManifestTopic']['topic_id'] = $topic_id;
	     												$manifestTopic['ManifestTopic']['date_created'] = date("Y-m-d H:i:s",strtotime("now"));
	     												$manifestTopic['ManifestTopic']['date_modified'] = date("Y-m-d H:i:s",strtotime("now"));
	     												$manifestTopic['ManifestTopic']['who_created'] = $this->Session->read('Auth.User.id');
	     												if( $this->ManifestTopic->save($manifestTopic)){
	     													$this->ManifestTopic->commit();
	     												}else{
	     													$this->ManifestTopic->rollback();
	     												}
					    							}
     											}
     										}	
     									} 				
     								}else{
     									$aNew[] =  array(
     										'id'=> $topic_id ,
     										'name'=> Sanitize::clean($topic_value), 
     										'description'=> trim(Sanitize::clean($this->data['Topic']['description'][$topic_id])),
     										'iterator' => $iterator 
     									);
     								}
     								$iterator++;
	     						}
	     						
	     						if( count($aFound) < count($oTopics) ){//all update
	     							$sql  = " UPDATE topics SET status='".Configure::read('status_deleted')."',date_modified='".date("Y-m-d H:i:s",strtotime("now"))."',who_modified='".$this->Session->read('Auth.User.id')."' "; 
		    						$sql .= " WHERE id NOT IN (".implode(",",$aFound).") AND module_id='".intval($this->data['Module']['id'])."' ";		
		    						$this->Topic->query($sql);
		    						
		    						$this->Topic->contain();
		    						$aDelete = $this->Topic->find('list',array(
		    								'conditions' => array(
		    									'Topic.module_id' => intval($this->data['Module']['id']),
		    									'Topic.id NOT IN ('.implode(",",$aFound).')'
		    								)
		    							)
		    						);
		    						
		    						if( count($aDelete)> 0 && !empty($aDelete) ){
			    						foreach( $aDelete as $delete_id => $delete_name ){
			    							
			    							$iManifestSearch = $this->ManifestTopic->find('count',array(
			    									'conditions' => array(
			    										'ManifestTopic.topic_id' => $delete_id,
			    										'ManifestTopic.manifest_id' => $this->data['Module']['manifestversion']
			    									)
			    								)
			    							);
			    							
			    							if( $iManifestSearch < 1 ){
				     							$this->ManifestTopic->begin();
				     							$manifestTopic['ManifestTopic']['manifest_id'] = $this->data['Module']['manifestversion'];
			     								$manifestTopic['ManifestTopic']['topic_id'] = $delete_id;
			     								$manifestTopic['ManifestTopic']['date_created'] = date("Y-m-d H:i:s",strtotime("now"));
			     								$manifestTopic['ManifestTopic']['date_modified'] = date("Y-m-d H:i:s",strtotime("now"));
			     								$manifestTopic['ManifestTopic']['who_created'] = $this->Session->read('Auth.User.id');
			     								if( $this->ManifestTopic->save($manifestTopic)){
			     									$this->ManifestTopic->commit();
			     								}else{
			     									$this->ManifestTopic->rollback();
			     								}
			    							}
			    						}
		    						}
	     						}
	     						
	     						if( count($aNew) && !empty($aNew) ){
	     							foreach( $aNew as $newTopicData ){
	     								
	     								$this->Topic->begin();
     									$aTopicNew = array();
     									$aTopicNew['Topic']['name'] = Sanitize::clean($newTopicData['name']);
     									$aTopicNew['Topic']['description'] =  Sanitize::clean($newTopicData['description']);
     									$aTopicNew['Topic']['date_modified'] = date("Y-m-d H:i:s",strtotime("now"));
     									$aTopicNew['Topic']['who_modified'] = $this->Session->read('Auth.User.id');
     									$aTopicNew['Topic']['display_sequence'] = intval($this->getLatestTopicIdByModuleId($this->data['Module']['id']));
     									$aTopicNew['Topic']['module_id'] = $this->data['Module']['id'];
     									$aTopicNew['Topic']['status'] = Configure::read('status_live');
     									if( $this->Topic->save($aTopicNew)){
     										$this->Topic->commit();
     										$manifestTopic = array();
     										
     										$iManifestSearch = $this->ManifestTopic->find('count',array(
			    									'conditions' => array(
			    										'ManifestTopic.topic_id' =>  $this->Topic->getInsertId(),
			    										'ManifestTopic.manifest_id' => $this->data['Module']['manifestversion']
			    									)
			    								)
			    							);
     										
			    							if( $iManifestSearch < 1 ){
	     										$manifestTopic['ManifestTopic']['manifest_id'] = $this->data['Module']['manifestversion'];
			     								$manifestTopic['ManifestTopic']['topic_id'] = $this->Topic->getInsertId();
			     								$manifestTopic['ManifestTopic']['date_created'] = date("Y-m-d H:i:s",strtotime("now"));
			     								$manifestTopic['ManifestTopic']['date_modified'] = date("Y-m-d H:i:s",strtotime("now"));
			     								$manifestTopic['ManifestTopic']['who_created'] = $this->Session->read('Auth.User.id');
	     										if( $this->ManifestTopic->save($manifestTopic)){
			     									$this->ManifestTopic->commit();
			     								}else{
			     									$this->ManifestTopic->rollback();
			     								}
			    							}
     									}else{
     										$this->Topic->rollback();
     									}
     								}
	     						}
	     						
	     						/*$this->Topic->contain();
		    					$aTopicsData = $this->Topic->find('all',array(
		    							'conditions' => array('Topic.module_id' => $this->data['Module']['id'] ),
		    							'order' => array('Topic.id')
		    						)
		    					);*/
		    					$module = $this->Module->findById($this->data['Module']['id']);
		    					$this->set('module',$module);
		    					$this->set('manifest', $this->data['Module']['manifestversion']);
		    					$this->set('latest_module_version',$this->data['Module']['id']);
	    					
		    					/*if(!empty($aTopicsData)){
		    						$aShowTopic = array();
		    						foreach( $aTopicsData as $aTopicsInfo ){
		    							$aShowTopic[] = array('id'=> $aTopicsInfo['Topic']['id'],'name'=> $aTopicsInfo['Topic']['name']);	
		    						}
									$this->set('data',$aShowTopic);	
		    					}else{
		    						$this->set('data',false);		
		    					}*/
	     					}
						}
     				}
				}else{
     				$this->set('data',$aInvalid);
     				$this->Session->delete('Module');
     				$this->Session->delete('Topic');
     			}
     		}
    	}else{
    		$this->set('data',false);
    		$this->Session->delete('Module');
     		$this->Session->delete('Topic');
    	}
    }
    
    public function stepOne(){
    	//Configure::write('debug', 2);
        $this->layout ='ajax';
        
        if( $this->RequestHandler->isPost() && !empty($this->data)){
        	if(	intval($this->data['Module']['id'])   > 0 &&
				strlen($this->data['Module']['name']) > 0 &&
				strlen($this->data['Module']['long_description']) > 0 &&
				strlen($this->data['Module']['short_description'])> 0
			){
				if(!isset($this->data['Module']['edit'])){
					if( 
						Cache::read('create_module_'.$this->Session->read('Auth.User.username'),'short') > 0 && 
						(Cache::read('create_module_'.$this->Session->read('Auth.User.username'),'short') != 
						intval($this->data['Module']['id'])) ){
						
						$this->Module->contain();
						$oModule = $this->Module->find('all',array(
								'conditions' => array('Module.session_token'=> Sanitize::clean($this->data['Module']['id']))
							)
						);
						
						if(!empty($oModule) && sizeof($oModule)> 0){
							foreach( $oModule as $module ){
								$deleteModuleId = intval($module['Module']['id']);
								if( $deleteModuleId>0 ){
									$this->Module->delete($deleteModuleId,$cascade=true);
								}		
							}
						}
					}
					$this->ModuleType->contain();
					$this->Session->write('Module',Sanitize::clean($this->data['Module'],array('encode' => true,'remove_html'=>true)));
					$this->Session->write('ModuleType',$this->ModuleType->findById($this->data['Module']['display_order']));
					
				}else{
					
					$this->Module->contain();
					$oModule = $this->Module->findById($this->data['Module']['id']);
					
					if(!empty($oModule) && sizeof($oModule)>0 ){
						
						$data = array();
						$this->Module->begin();
						
						$data['Module']['name'] = Sanitize::clean($this->data['Module']['name'],array('encode' => true,'remove_html'=>true));	
						if( trim($oModule['Module']['short_description']) != trim(Sanitize::clean($this->data['Module']['short_description'],array('encode' => true,'remove_html'=>true))) ){
							$data['Module']['short_description'] = Sanitize::clean($this->data['Module']['short_description'],array('encode' => true,'remove_html'=>true));	
						}
						
						if( trim($oModule['Module']['long_description']) != trim(Sanitize::clean($this->data['Module']['long_description'],array('encode' => true,'remove_html'=>true))) ){
							$data['Module']['long_description'] = Sanitize::clean($this->data['Module']['long_description'],array('encode' => true,'remove_html'=>true));	
						}
						
						if( trim($oModule['Module']['modules_image_id']) != trim($this->data['Module']['modules_image_id']) ){
							$data['Module']['modules_image_id'] = intval($this->data['Module']['modules_image_id']);	
						}
						
						if( count($data)>0 ){
							$this->Module->id = $this->data['Module']['id'];
							$data['Module']['modules_image_id'] = $this->data['Module']['modules_image_id']; 
							$data['Module']['who_modified'] = $this->Session->read('Auth.User.id');
							$data['Module']['status'] = Configure::read('status_onhold');
							$data['Module']['date_modified'] = date("Y-m-d H:i:s",strtotime("now"));
							
							if( $this->Module->save($data) ){
		    					$this->Module->commit();
								if( intval($this->data['Module']['manifestversion']) < 1){	
			    					$manifestData = array();
						        	$this->Manifest->begin();
						        	$manifestData['Manifest']['change_type'] = 'module';
						        	$manifestData['Manifest']['status'] = Configure::read("status_onhold");
						        	$manifestData['Manifest']['comments'] = 'Module Id('.$this->Module->id.') Edited By '.$this->Session->read('Auth.User.username');
						        	$manifestData['Manifest']['date_modified'] = date("Y-m-d H:i:s",strtotime("now"));
						        	$manifestData['Manifest']['date_created'] = date("Y-m-d H:i:s",strtotime("now"));
						        	$manifestData['Manifest']['who_created'] = $this->Session->read('Auth.User.id');	
						        	
									if( $this->Manifest->save($manifestData) ){
		        						$this->Manifest->commit();
		        						$manifestId = $this->Manifest->getInsertID();
						        		$this->ManifestModule->begin();
						        		$manifestData = array();
							        	$manifestModuleData['ManifestModule']['manifest_id'] = $manifestId;
							        	$manifestModuleData['ManifestModule']['module_id'] = $this->Module->id;
							        	$manifestModuleData['ManifestModule']['date_modified'] = date("Y-m-d H:i:s",strtotime("now"));
							        	$manifestModuleData['ManifestModule']['date_created'] = date("Y-m-d H:i:s",strtotime("now"));
							        	$manifestModuleData['ManifestModule']['who_created'] = $this->Session->read('Auth.User.id');
							        	if( $this->ManifestModule->save($manifestModuleData) ){
							        		$this->set('data',$manifestId);
						        			$this->ManifestModule->commit();
							        	}	
						        	}else{
						        		$this->Manifest->rollback();
						        	}
		    					}else{
		    						$this->set('data',$this->data['Module']['manifestversion']);
		    					}
					        }else{
		    					$this->set('data',false);
		    				}
						}else{
							$this->set('data',true);
						}
					}
    			}
			}else{	 
    			$this->set('data',false);
			}
    	}else{
    		$this->set('data',false);
    	}
    }
    
    
    public function addTopics(){
    	$this->layout ='ajax';
    	$this->set('iterator',$this->params['url']['iterator']);
    }
    
    public function addTopicFiles(){
    	$this->layout ='ajax';
    	$this->TopicFiles->contain();
    	$topicFiles = $this->TopicFiles->find('all',array(
    		'conditions' => array('LENGTH(TRIM(TopicFiles.raw_name)) > 0 AND TopicFiles.raw_name IS NOT NULL AND TopicFiles.topic_id < 1')
    		)
    	);
    	$this->set('iterator',$this->params['url']['iterator']);
    	$this->set('session_topic_id',$this->params['url']['topic_id']);
    	$this->set(compact('topicFiles'));
    }
    
    public function addAssessments(){
    	$this->layout ='ajax';
    	$this->set('iterator',$this->params['url']['iterator']);
    }
    
    public function addReference(){
    	$this->layout = 'ajax';
    	$this->set('iterator',$this->params['url']['reference_id']);
    }
    
    public function download(){
    	//Configure::write('debug',2);
    	if( isset($this->params['url']['src']) ){
            $this->autoRender = false;
            $this->layout = 'media';
  
        	App::import('Core', 'File');
            $DownloadLink = Configure::read('downloadLink');
        	$SourceFile = Sanitize::clean(trim($this->params['url']['src']));
        	
			if( file_exists($DownloadLink . DS . 'modules' . DS . $SourceFile) ){
				$Module = $this->ModulesImage->findByRawName($SourceFile);
				if(!empty($Module)){
					header("HTTP/1.1 200 OK");
					header("Content-Description: File Transfer");
					header("Accept-Ranges: bytes");
					foreach (getallheaders() as $name => $value) {
						if( strstr(strtolower($name),'range')){
 			   				header("Content-Range: bytes ".$value."/".$Module['ModulesImage']['raw_size']);
						}
					}
					header("Content-Disposition: attachment; filename=".$SourceFile);
	                header("Content-Length: ".$Module['ModulesImage']['raw_size']);
	                header("Content-Type: ".$Module['ModulesImage']['raw_type']);
	                header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	                header("Pragma: public");
	                header("Connection: close");
	                readfile($DownloadLink.DS . 'modules' . DS . $SourceFile);
				}else{
					header("HTTP/1.0 404 File Not Found");
	            }
   			}else{
   				header("HTTP/1.0 404 File Not Found");	
   			}    
        }
    }
    
    public function viewImg(){
    	if( isset($this->params['url']['src']) ){
            $this->autoRender = false;
            $this->layout = 'media';
  
        	App::import('Core', 'File');
            $DownloadLink = Configure::read('downloadLink');
        	$SourceFile = Sanitize::clean(trim($this->params['url']['src']));
        	
			if( file_exists($DownloadLink . DS . 'modules' . DS . $SourceFile) ){
				$Module = $this->ModulesImage->findByRawName($SourceFile);
				if(!empty($Module)){
					header("HTTP/1.1 200 OK");
					header("Content-Description: File Transfer");
					header("Accept-Ranges: bytes");
					foreach (getallheaders() as $name => $value) {
						if( strstr(strtolower($name),'range')){
 			   				header("Content-Range: bytes ".$value."/".$Module['ModulesImage']['raw_size']);
						}
					}
					header("Content-Length: ".$Module['ModulesImage']['raw_size']);
	                header("Content-Type: ".$Module['ModulesImage']['raw_type']);
	                header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	                header("Pragma: public");
	                header("Connection: close");
	                readfile($DownloadLink.DS . 'modules' . DS . $SourceFile);
				}else{
					header("HTTP/1.0 404 File Not Found");
	            }
   			}else{
   				header("HTTP/1.0 404 File Not Found");	
   			}    
        }
    }
}
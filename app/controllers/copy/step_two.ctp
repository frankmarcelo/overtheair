<?php

$aShowTopic = $this->Session->read('aShowTopic');

$found = false;
foreach( $topicFiles as $topicFile ){
	if(file_exists(Configure::read('downloadLink').DS.'topics'.DS.$topicFile['TopicFiles']['source_file'])){
		$extension = pathinfo($topicFile['TopicFiles']['raw_name'], PATHINFO_EXTENSION);
		$topicFile['TopicFiles']['raw_name'] = str_replace(".".$extension,"",$topicFile['TopicFiles']['raw_name']);
		$found =true;
		break;
	}
}


if( isset($latest_module_version) ){
?>
<form name="step3" id="step3" method="post" onSubmit="return false;" accept-charset="utf-8">
<input type="hidden" name="data[Module][id]" value="<?php echo $latest_module_version;?>" />
<div class="step3_wrapper">
<h2><?php echo $this->Session->read('Module.name');?> (<?php echo $this->Session->read('ModuleType.ModuleType.name');?>)</h2>
<div class="topicswrapper">
<?php
foreach( $aShowTopic as $key => $topic ){
?>
	<div class="topic_file_body_content_<?php echo $topic['id'];?>">
  	<table class="form-table" style="width:810px;text-align:center;">
    	<tr style="width:100%;">
    	<td width="30%"><div><strong>Add Material <?php echo $this->Session->read('Module.display_order');?>.<?php echo $key+1;?>.1</strong>:&nbsp;
    	<?php if(!empty($topicFiles)){
		?>
    	<select class="class_hidden_topics" name="data[TopicFiles][<?php echo $topic['id'];?>][]">
<?php

	foreach( $topicFiles as $topicFile ){
		if(file_exists(Configure::read('downloadLink').DS.'topics'.DS.$topicFile['TopicFiles']['source_file'])){
			$extension = pathinfo($topicFile['TopicFiles']['raw_name'], PATHINFO_EXTENSION);
			$topicFile['TopicFiles']['raw_name'] = str_replace(".".$extension,"",$topicFile['TopicFiles']['raw_name']);
			echo '<option value="'.$topicFile['TopicFiles']['id'].'">'.$this->Text->truncate($topicFile['TopicFiles']['name'],'50',array('ending' => '...','html' => true)).'</option>';	
		}
	}
?></select><?php }else{?>Error cannot proceed. Either all materials are assigned to topics or no available materials for assignment. <a href="<?php echo $this->Html->url(array("controller" => "documents","action"=>"upload"));?>" target="_blank">Upload now</a>?<?php }?></div></td><td width="40%" style="text-align:left;">to Topic:<strong><?php echo Sanitize::clean($topic['name']);?></strong></td><td width="30%"><?php if($found==true){?><a style="cursor:pointer" class="topics_<?php echo $topic['id'];?>">Assign another material</a><?php }?></td></tr>
    </table>
	</div>
	<br />
</div>
<?php
}
?>
</div>
</form>
<?php
#	echo json_encode( $data );
}else{
?>
<form name="step3" id="step3" method="post" onSubmit="return false;" accept-charset="utf-8">
	<input type="hidden" name="data[Module][id]" value="<?php echo $latest_module_version;?>" /> 
	<input type="hidden" name="data[Module][edit]" value="true" />
	<input type="hidden" name="data[Module][manifestversion]" id="manifest_version_step3" value="<?php echo $manifest;?>" />
	<div class="topicswrapper" id="topicswrapper">
	<table class="form-table" style="width:600px;">
 	<?php
 	if( isset($module['Topic']) && count($module['Topic'])){
		$topicIterator = 1;
		foreach( $module['Topic'] as $topic ){
			$aTopicFiles =  $this->TopicFiles->loadTopicDataById($topic['id']);
			if( !empty($aTopicFiles) ){
				foreach( $aTopicFiles as $documents => $document ){
	?>
		<tr style="width:100%;text-align:left;"><td>Assign Topic Name: <strong><?php echo trim($topic['name']);?></strong>&nbsp;to Topic File:&nbsp;&nbsp;<select class="class_hidden_topics" name="data[TopicFiles][id][]">
		<?php
		if(!empty($topicFiles) && $found==true){
			foreach( $topicFiles as $topicFile ){
				if(file_exists(Configure::read('downloadLink').DS.'topics'.DS.$topicFile['TopicFiles']['source_file'])){
					$selected = ( $document['TopicFiles']['id']==$topicFile['TopicFiles']['id'] ) ? ' selected="selected"':'';  
					echo '<option value="'.$topicFile['TopicFiles']['id'].'"'.$selected.'>'.$this->Text->truncate($topicFile['TopicFiles']['name'],'50',array('ending' => '...','html' => true)).'</option>';
				}
			}
		}?></select><input type="hidden" autocomplete="off" name="data[Topic][id][]" value="<?php echo $topic['id'];?>" /></td></tr>
		<?php
				}//end of foreach
			}else{
		?>	
		<tr style="width:100%;text-align:left;"><td width="100%">Assign Topic Name: <strong><?php echo trim($topic['name']);?></strong>&nbsp;to Topic File:&nbsp;&nbsp;<select class="class_hidden_topics" name="data[TopicFiles][id][]">
		<?php
		foreach( $topicFiles as $topicFile ){
			echo '<option value="'.$topicFile['TopicFiles']['id'].'"'.$selected.'>'.$this->Text->truncate($topicFile['TopicFiles']['name'],'50',array('ending' => '...','html' => true)).'</option>';
		}
		?></select><input type="hidden" autocomplete="off" name="data[Topic][id][]" value="<?php echo $topic['id'];?>" /></td></tr>			
	<?php	
			}//end else line 25
		}//end of foreach line 10
	}
	?>
	<tr class="form-field"><th scope="row">&nbsp;</th><td>&nbsp;</td></tr>
	</table>
	</div>
</form>		
<?php 
}
?>
<?php #echo $this->element('sql_dump'); ?>

<?php
/**
 * 
 * Over The Air Web Service
 * @author fmarcelo
 * Date Created 11th of September 2011
 * @copyright 2011
 * @version 1.0
 */

/*TODO
 * Database Logger
 * Session Token
 * 
 * 
 * 
 */
App::import('Sanitize');

class OtawebservicesController extends AppController {
	
    public $name = 'Otawebservices';
    public $uses = array(
        'User',
        'Role',
        'Manifest',
        'ManifestModule',
        'ManifestTopic',
        'ManifestTopicFiles',
        'ManifestAssessment',
        'ManifestAssessmentAnswer',
        'Log',
        'Module',
    	'ModulesImage',
        'Topic',
        'TopicFiles',
        'TopicFilesSource',
        'Assessment',
        'AssessmentsAnswer',
        'AssessmentsChoice',
        'AssessmentsReference'
    );  
 
    private $version = 'v1.0'; 
    
    private $methodActions = array(
                               'index',
                               'OTA_Authenticate',
                               'OTA_GetLatestManifestRQ',
                               'OTA_DownloadModuleFileRQ',
                               'OTA_GetFullManifestRQ' ); 
    
    public $helpers = array(
        'TopicFiles',
        'AssessmentsChoices',
        'AssessmentsReferences' ,
        'AssessmentsAnswer'
    );
 

    public function beforeFilter() {
Configure::write('debug',0);

    	parent::beforeFilter();
        $this->disableCache();
        
        App::import('Core', 'HttpSocket');
        App::import('Core', 'Xml'); 
        
		$this->Security->blackHoleCallback = 'forceSSL';
                $this->httpCodes();
        $this->Security->requirePost($this->methodActions);
        $this->Security->requireSecure();
		
        
        #if( $this->RequestHandler->isSSL() ){
        if( true ){
        	
            $this->Security->loginOptions = array(
               'type'  => 'digest',
               'prompt'=> 'authenticate',
               'login' => 'authenticate',
               'realm' =>'Otawebservices' 
            );
            
            $httpAuthUsers = array();
            $roles = $this->Role->find('first',array(
            		'conditions' => array( 'Role.id' => Configure::read('userRoleId') ),
            		'fields' => array('Role.id')
            	),array('cache' => 'Users', 'cacheConfig' => 'cache_queries')
            );

 			$users = $this->User->find('all',array(
    	    		'conditions' => array(
    	    			'User.status'  => Configure::read('status_live'), 
    					'User.role_id' => $roles['Role']['id'] 
    				),
    				'fields' => array('User.username',
    					'User.password',
    				  	'User.token',
    				  	'User.id',
    				  	'User.invitation_key',
    				  	'User.role_id'
    				)
    			),array('cache' => 'Users', 'cacheConfig' => 'cache_queries')
    		);
    		
    		if( !empty($users )){
	    		foreach( $users as $userInfo ){
	    			$httpAuthUsers[$userInfo['User']['username']] = $userInfo['User']['password'];
	    		}
	    			
	    		$this->Security->loginUsers = $httpAuthUsers;
	    		$this->Security->requireLogin($this->methodActions);
	    			
                }else{
    			$this->throwExceptionError( $errorCode   = 500, 
                	                        $errorMesage = Configure::read('httpServerErrorMessage'),$isXml=true,__FUNCTION__ );
    		}
    	}else{
            $this->throwExceptionError( $errorCode = 403, 
                                        $errorMesage = Configure::read('httpForbiddenAccessMessage'),$isXml=true,__FUNCTION__ );
        }
    }

    private function forceSSL() {
        $this->redirect('https://' . env('SERVER_NAME') . $this->here);
    }

    public function index(){
    	if( $this->RequestHandler->isGet() ){
            $this->set('version', $this->version);	    
           
    	}elseif ( $this->RequestHandler->isPost() ){
            $this->header('Content-Type: text/xml');
            $this->header('HTTP/1.1 200 '.$this->__httpCodes[200]);      
            $this->layout = 'xml'; 
    	}
    }

    public function OTA_Test(){
        $modules = $this->Module->find('all',array(
                                'conditions' => array( 'Module.status' => Configure::read('status_live') ),
                                'order' => array('Module.display_order') ,
                                'group' => array('Module.id')
                       ));
					   
		print_r($modules);
        $this->set(compact('modules'));
    }

    
    private function requestLogs($args=array()){
    	$this->data['Log']['requestWith']= $this->RequestHandler->requestedWith();
    	$this->data['Log']['requestIP'] = $this->RequestHandler->getClientIP();
    	$this->data['Log']['requestMethod'] = $args['requestMethod'];
    	$this->data['Log']['requestRQ'] = $args['requestRQ'];//request XML data
    	$this->data['Log']['requestType'] = ($this->RequestHandler->isPost()) ? 'post':'get';
    	$this->data['Log']['isMobile'] = ($this->RequestHandler->isMobile()) ? 1:0;
    	$this->data['Log']['username'] = $args['user'];//TODO set id
    	$this->data['Log']['date_created'] = date("Y-m-d H:i:s");
    	if($this->Log->save($this->data)){
            $this->Log->commit();
            $id = $this->Log->query("SELECT LAST_INSERT_ID() as id");
            return $id[0][0]['id'];
    	}else{
            $this->Log->rollback();
            return 0;
        }
    }

    private function requestLogsUpdate($args=array(),$logId=0){
        $domRQ = new DOMDocument();
        $domRQ->loadXML(trim($args['requestRS']));
        $domRQ->formatOutput = true;
        $requestXML = $domRQ->saveXML();

        $this->Log->id = $logId;
        $this->Log->begin();
        $this->data['Log']['requestRS'] = $requestXML;
        if($this->Log->save($this->data)){
            $this->Log->commit();
        }
    }

    public function authenticate($args=array()) {
        if( $args && isset($this->Security->loginUsers[$args['username']]) && strlen(trim($args['username']))>0 ) {
	    	$aUserInfo = $this->User->findByUsername(trim($args['username']));
	    	$password = trim(strtolower($aUserInfo['User']['password']));
	    	if ($args['response'] == $this->Security->generateDigestResponseHash($args,$password)) {
				$this->User->id = $aUserInfo['User']['id'];
				$this->User->begin();
				$this->data['User']['last_login'] = date("Y-m-d H:i:s");
				$this->User->save($this->data);
				$this->User->commit();
				$this->Session->write('TokenUser',$args['username']);
			    $this->Session->write('TokenId',$aUserInfo['User']['token']);
			    return true;
		    }else{
			    $this->throwExceptionError( $errorCode = 401, $errorMessage = Configure::read('httpInvalidAuthMessage'),$isXml=true,__FUNCTION__  );	
		    }
	    }else{
		    $this->throwExceptionError( $errorCode = 401, 
				    $errorMesage = Configure::read('httpInvalidAuthMessage'),$isXml=true,__FUNCTION__  );
	    } 
    } 

    public function OTA_AuthorizationRQ(){
    	
    	$this->header('Content-Type: text/xml');
        $this->header('HTTP/1.1 200 '.$this->__httpCodes[200]);      
		if( $this->RequestHandler->isGet() ){
    	    $this->set('version', $this->version);	    	
        }elseif ( $this->RequestHandler->isPost() ){
            if( !empty($this->data[$this->name]['xmlRequest']) ){

                $this->layout = 'xml/ota_authorization_rs';

                $transactionIdentifier = $this->requestLogs(array('user'=>$this->Session->read('TokenUser'),
                                            'requestRQ'=> '<HTTP Digest Login>'.$this->Session->read('TokenUser'), 'requestMethod' => __FUNCTION__));

                $sUsername = Sanitize::clean($this->Session->read('TokenUser'));
                $aUserInfo = $this->User->findByUsername(Sanitize::clean($sUsername));

                $this->set('transactionIdentifier',$transactionIdentifier);
                $this->set('requestingsource',$sUsername);
                    
                if( strtolower(trim($this->Session->read('TokenId'))) == strtolower(trim($aUserInfo['User']['token'])) ){ 

                    $manifest = $this->Manifest->find('all',
                           array(
                             'conditions' => array( 'Manifest.status' => Configure::read('status_live') ), 
                             'order' => array('Manifest.id' => 'desc'),'limit'=>1
                           )
                    ); 
                    $this->set('manifestversion',$manifest[0]['Manifest']['id']);
                    $this->set('manifestcreatedate',$manifest[0]['Manifest']['date_created']);
                    $this->set('manifestupdatedate',$manifest[0]['Manifest']['date_modified']);
	            	$this->set('valid',true);                 
                    $this->set('token',$aUserInfo['User']['token']); 
                }else{//Failed authentication
       		    	$this->throwExceptionError( $errorCode = 401, 
                    $errorMesage = Configure::read('httpInvalidAuthMessage'),
                    $isXml=true,__FUNCTION__ );
                }
                $args['requestRS'] = $this->render();
                $this->requestLogsUpdate( $args , $transactionIdentifier );
       	    }else{
       			$this->throwExceptionError( $errorCode = 400, $errorMesage = Configure::read('httpBadRequestMessage'),$isXml=true,__FUNCTION__ );
       	    }
        }
    }

    public function OTA_GetLatestManifestRQ(){
    	$this->header('Content-Type: text/xml');
        $this->header('HTTP/1.1 200 '.$this->__httpCodes[200]);      
		if( $this->RequestHandler->isGet() ){
            $this->set('version', $this->version);
        }elseif ( $this->RequestHandler->isPost() ){
			if( !empty($this->data[$this->name]['xmlRequest']) ){

				$this->layout = 'xml/ota_getlatestmanifest_rs';

                $requestXML = trim($this->data[$this->name]['xmlRequest']);
                $domRQ = new DOMDocument();
                $domRQ->loadXML($requestXML);
                $domRQ->formatOutput = true;
                $requestXML = $domRQ->saveXML();
                $transactionIdentifier = $this->requestLogs(array('user'=>$this->Session->read('TokenUser'),
                                            'requestRQ'=> $requestXML, 'requestMethod' => __FUNCTION__));

                $OTA_GetLatestManifestRQ = new Xml($requestXML);
                $xmlAsArray = $OTA_GetLatestManifestRQ->toArray();

                $this->set('transactionIdentifier',$transactionIdentifier);
                $manifest = $this->Manifest->find('all',
                           array(
                             'conditions' => array( 'Manifest.status IN ('.Configure::read('status_live').','.Configure::read('status_deleted').')' ), 
                             'order' => array('Manifest.id' => 'desc'),'limit'=>1
                           )
                ); 
                $this->set('manifestversion',$manifest[0]['Manifest']['id']);
			    $this->set('manifestcreatedate',$manifest[0]['Manifest']['date_created']);
			    $this->set('manifestupdatedate',$manifest[0]['Manifest']['date_modified']);
			    $this->set('requestingsource',$this->Session->read('TokenUser'));
			    $TokenId = Sanitize::clean($this->Session->read('TokenId'));
			    $VersionNo = intval($xmlAsArray['OTAGetLatestManifestRQ']['Manifest']['VersionNo']);
				$ManifestVersionNo = intval($manifest[0]['Manifest']['id']);

	    		if( $TokenId && isset($xmlAsArray['OTAGetLatestManifestRQ']['Manifest']) ){
				    if( trim(strtolower($this->Session->read('TokenId'))) == trim(strtolower($TokenId)) ){
					    if( $VersionNo < $ManifestVersionNo ){
					      	$aChanges = $this->Manifest->find('all',array( 'conditions'=> array('Manifest.id > '. $VersionNo)));
					      	
					      	$bIsAllChanges = false;
							
							foreach( $aChanges as $Manifest ){
					      		if( strtolower(trim($Manifest['Manifest']['change_type'])) == 'all' ){
					      			//$bIsAllChanges = true;	
					      			//break;
					      		}
					      	}
					      	
					      	/**
					      	 * we have to display the all modules because 
					      	 * we cannot track if they set the changes to all
					      	 */
					      	if( $bIsAllChanges ){
					      		$modules = $this->Module->find('all',array(
									    	'conditions' => array( 'Module.status' => Configure::read('status_live') ),
									    	'order' => array('Module.display_order')
									       ));
								$this->set(compact('modules'));
						    	$this->set('valid',true);
					      	}else{
					      		
								$modules = array();
								foreach( $aChanges as $Manifest ){
									foreach( $Manifest['ManifestModule'] as $ModuleInfo ){

					      				if( strtolower($Manifest['Manifest']['change_type'])=='module_all' ){
						      			}else{
					      				
						      				$searchManifest = $this->Module->findById($ModuleInfo['module_id']);
							      			if(!array_key_exists($searchManifest['Module']['id'], $modules)){
							      				$modules[$searchManifest['Module']['id']] = $searchManifest;	
							      			}
											
											if( strtolower($Manifest['Manifest']['change_type'])=='topic'){
												if( count($Manifest['ManifestTopic']) >0 ){
								      				foreach( $Manifest['ManifestTopic'] as $ManifestTopic ){
								      					$this->Topic->contain('Module.id');
								      					$topics = $this->Topic->findById($ManifestTopic['topic_id']);
								      					if( !empty($topics) && sizeof($topics)>0 ){
								      						if( intval($topics['Module']['id'])== intval($ModuleInfo['module_id']) ){
								      							$modules[$searchManifest['Module']['id']]['Topic'][] = $topics['Topic'];			
								      						} 
								      					}		
								      				}
												}
							      			}elseif( strtolower($Manifest['Manifest']['change_type'])=='document'){
							      				if( count($Manifest['ManifestTopicFiles'])>0 ){
								      				foreach( $Manifest['ManifestTopicFiles'] as $ManifestTopicFiles ){
								      					$this->TopicFiles->contain('TopicFilesSources.name');
								      					$topicFiles = $this->TopicFiles->findById($ManifestTopicFiles['topic_files_id']);
								      					if( !empty($topicFiles) && sizeof($topicFiles)>0 ){
								      						if( isset($topicFiles['Topic']) && intval($topicFiles['Topic']['id'])== intval($ModuleInfo['module_id']) ){
								      							$modules[$searchManifest['Module']['id']]['TopicFiles'][] = $topicFiles['TopicFiles'];			
								      						} 
								      					}
								      				}
							      				}
							      			}elseif( strtolower($Manifest['Manifest']['change_type'])=='assessment'){
							      				if( count($Manifest['ManifestAssessment'])>0 ){
							      					foreach( $Manifest['ManifestAssessment'] as $ManifestAssessment ){
							      						$this->Assessment->contain('Module.id');
								      					$assessment = $this->Assessment->findById($ManifestAssessment['assessment_id']);
								      					if( !empty($assessment) && sizeof($assessment)>0 ){
								      						if( intval($assessment['Module']['id'])== intval($ModuleInfo['module_id']) ){
								      							$modules[$searchManifest['Module']['id']]['Assessment'][] = $assessment['Assessment'];			
								      						} 
								      					}		
								      				}
							      				}
							      			}elseif( strtolower($Manifest['Manifest']['change_type'])=='assessment_answer'){
							      				if( count($Manifest['ManifestAssessmentAnswer'])>0 ){
							      					foreach( $Manifest['ManifestAssessmentAnswer'] as $ManifestAssessmentAnswer ){
							      						$this->AssessmentsAnswer->contain('Assessment.module_id');
							      						$assessmentanswer = $this->AssessmentsAnswer->findById($ManifestAssessmentAnswer['assessment_answer_id']);
							      						if( !empty($assessmentanswer) && sizeof($assessmentanswer)>0 ){
								      						if( intval($assessmentanswer['Assessment']['module_id'])== intval($ModuleInfo['module_id']) ){
								      							$modules[$searchManifest['Module']['id']]['AssessmentAnswer'][] = $assessmentanswer['AssessmentsAnswer'];			
								      						} 
								      					}		
								      				}
							      				}
							      			}elseif( strtolower($Manifest['Manifest']['change_type'])=='assessment_choice'){
							      			}elseif( strtolower($Manifest['Manifest']['change_type'])=='assessment_reference'){
							      				if( count($Manifest['ManifestAssessmentReference'])>0 ){
							      					foreach( $Manifest['ManifestAssessmentReference'] as $ManifestAssessmentReference ){
							      						$this->AssessmentsReference->Contain('Assessment.module_id');
							      						$assessmentreference = $this->AssessmentsReference->findById($ManifestAssessmentReference['assessment_reference_id']);
							      						if( !empty($assessmentreference) && sizeof($assessmentreference)>0 ){
							      							if( intval($assessmentreference['Assessment']['module_id'])== intval($ModuleInfo['module_id']) ){
								      							$modules[$searchManifest['Module']['id']]['AssessmentsReference'][] = $assessmentreference['AssessmentsReference'];			
								      						} 
								      					}		
								      				}
							      				}
							      			}
						      			}
					      			}
					      		}
					      		$this->set(compact('modules'));
						    	$this->set('valid',true);
					      	}
					    }else{
					    	
					    	if( $VersionNo == $ManifestVersionNo ){
					    		$this->set('modules',false);
					    		$this->set('valid',true);	
					    	}else{
						    	$this->set('errorCode','10004');
						    	$this->set('errorMessage','Invalid manifest version request.');
						    	$this->set('valid',false);
					    	}
					    }
				    }else{
					    $this->set('errorCode','10003');
					    $this->set('errorMessage','Invalid token parameter request. Please try again.');
					    $this->set('valid',false);
				    }
			    }else{
				    $this->set('errorCode','10002');
				    $this->set('errorMessage','Invalid '.__FUNCTION__.'  request. Please try again.');
				    $this->set('valid',false);
			    }
                
                $args['requestRS'] = $this->render();
                $this->requestLogsUpdate( $args , $transactionIdentifier );
           }	
       }
    }

    public function OTA_DownloadModuleFileRQ(){
        Configure::write('debug',0);
        if( isset($this->params['url']['src']) ){
            $this->autoRender = false;
            $this->layout = 'ajax';
  
            App::import('Core', 'Folder');
            App::import('Core', 'File');
            $DownloadLink = Configure::read('downloadLink');
        	$SourceFile = Sanitize::clean(trim($this->params['url']['src']));
		
           	if( isset($this->params['url']['mobileapp']) && trim(strtolower($this->params['url']['mobileapp']))=='true' ){
            	if( file_exists($DownloadLink . DS . 'os' . DS . $SourceFile) ){
            		$this->Macosx->contain();
					$Macosx = $this->Macosx->findByFileName($SourceFile);
					if(!empty($Macosx)){
	            		header("HTTP/1.1 200 OK");
						header("Content-Description: File Transfer");
						header("Accept-Ranges: bytes");
						
						foreach (getallheaders() as $name => $value) {
							if( strstr(strtolower($name),'range')){
 			   					header("Content-Range: bytes ".$value."/".$Macosx['Macosx']['raw_size']);
							}
						}
		                header("Content-disposition: attachment; filename=".$SourceFile);
		                header("Content-Length: ".$Macosx['Macosx']['raw_size']);
		                header("Content-Type: ".$Macosx['Macosx']['raw_type'].' ipa');
		                header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		                header("Pragma: public");
		                header("Connection: close");
	            		readfile($DownloadLink.DS . 'os' . DS . $SourceFile);
					}else{
						$this->throwExceptionError( $errorCode   = 404,
                                                $errorMesage = Configure::read('http404Message'),$isXML=true,__FUNCTION__ );	
					}
                 }else{
                    $this->throwExceptionError( $errorCode   = 404,
                                                $errorMesage = Configure::read('http404Message'),$isXML=true,__FUNCTION__ );
				}
            }else{
	           	if( !empty($SourceFile) && strstr($SourceFile,"_") ){
					list( $DownloadType , $DownloadId ) = explode( "_", $SourceFile );
					/**
					 * TODO
					 * set the source file and archive the file
					 * before the download
					 */
	               	if( strtolower($DownloadType) =='modules' ){
	                  	if( file_exists($DownloadLink . DS . 'modules' . DS . $SourceFile) ){
	                  		$this->ModulesImage->contain();
							$Macosx = $this->ModulesImage->findByBackgroundImage($SourceFile);
							if(!empty($Macosx) && is_readable($DownloadLink.DS . 'modules' . DS . $SourceFile)){
							#	header('HTTP/1.1 301 Moved Permanently');
						#		header('Location: '.Configure::read('baseUri').DS.'over-the-air/js/ajax/materials/CVD_TEST.pdf');
    							header("HTTP/1.1 200 OK");
		                  		header("Pragma: no-cache");
								//header("Accept-Ranges: bytes");
								header("Accept-Encoding: gzip,deflate");
								header("Cache-Control: public");
								header("Content-Description: File Transfer");
								header('Content-Type: application/x-gzip');
								header('Content-Disposition: attachment; filename='.$SourceFile);
								header("Content-Length: ".filesize($DownloadLink.DS . 'modules' . DS . $SourceFile));
								header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
								header('Expires: 0');
								header('Content-Transfer-Encoding: binary');
								header("Content-Encoding: gzip");
								ob_clean();
    							flush();
    							readfile($DownloadLink.DS . 'modules' . DS . $SourceFile);
		                  	}else{
		                  		$this->throwExceptionError( $errorCode   = 404,$errorMesage = Configure::read('http404Message'),$isXML=true,__FUNCTION__ );
		                  	}
	                    }else{
	                    	$this->throwExceptionError( $errorCode   = 404,'File not found.',$isXML=true,__FUNCTION__ );
						}
	               	}elseif( strtolower($DownloadType) == 'topics' ){
	                  	if( file_exists($DownloadLink . DS . 'topics' . DS . $SourceFile) ){
	                  		$this->TopicFiles->contain();
							$topicFilesSource = $this->TopicFiles->findBySourceFile($SourceFile);
							if(!empty($topicFilesSource) && file_exists($DownloadLink.DS . 'topics' . DS . $SourceFile)){
	                  			header("HTTP/1.1 200 OK");
		                  		header("Pragma: no-cache");
								//header("Accept-Ranges: bytes");
								header("Accept-Encoding: gzip,deflate");
								header("Cache-Control: public");
								header("Content-Description: File Transfer");
								header('Content-Type: application/x-gzip');
								header('Content-Disposition: attachment; filename='.$SourceFile);
								header("Content-Length: ".filesize($DownloadLink.DS . 'topics' . DS . $SourceFile));
								header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
								header('Expires: 0');
								header('Content-Transfer-Encoding: binary');
								header("Content-Encoding: gzip");
								ob_clean();
    							flush();
    							readfile($DownloadLink.DS . 'topics' . DS . $SourceFile);
		                      	
							}else{
	                      		$this->throwExceptionError( $errorCode = 404, $errorMesage = Configure::read('http404Message'),$isXML=true,__FUNCTION__ );
							}
	                  	}else{
	                      	$this->throwExceptionError( $errorCode = 404, $errorMesage = Configure::read('http404Message'),$isXML=true,__FUNCTION__ );
						}     
	               	}
			   }else{
	               $this->throwExceptionError( $errorCode = 404,$errorMesage = Configure::read('http404Message'),$isXML=true,__FUNCTION__ );
	           }
            }
        }else{
            $this->throwExceptionError( $errorCode   = 404, $errorMesage = Configure::read('http404Message'),$isXml=true,__FUNCTION__ );
		}
    }

    public function OTA_GetFullManifestRQ(){
        $this->header('Content-Type: text/xml');
        $this->header('HTTP/1.1 200 '.$this->__httpCodes[200]);      
		if( $this->RequestHandler->isGet() ){
            $this->set('version', $this->version);
        }elseif ( $this->RequestHandler->isPost() ){
	    	if( !empty($this->data[$this->name]['xmlRequest']) ){

        		$this->layout = 'xml/ota_getfullmanifest_rs';
				$requestXML = trim($this->data[$this->name]['xmlRequest']);
                $domRQ = new DOMDocument();
                $domRQ->loadXML($requestXML);
                $domRQ->formatOutput = true;
                $requestXML = $domRQ->saveXML();
                $transactionIdentifier = $this->requestLogs(array('user'=>$this->Session->read('TokenUser'),
                                            'requestRQ'=> $requestXML, 'requestMethod' => __FUNCTION__));


                $OTA_GetFullManifestRQ = new Xml($requestXML);
                $xmlAsArray = $OTA_GetFullManifestRQ->toArray();
  
                $this->set('transactionIdentifier',$transactionIdentifier);
 
                $manifest = $this->Manifest->find('all',
                           array(
                             'conditions' => array( 'Manifest.status' => Configure::read('status_live') ), 
                             'order' => array('Manifest.id' => 'desc'),'limit'=>1
                           )
                ); 
				$this->set('manifestversion',$manifest[0]['Manifest']['id']);
                $this->set('manifestcreatedate',$manifest[0]['Manifest']['date_created']);
                $this->set('manifestupdatedate',$manifest[0]['Manifest']['date_modified']);
                $this->set('requestingsource',$this->Session->read('TokenUser'));
                $TokenId = Sanitize::clean($this->Session->read('TokenId'));
                if( $TokenId && isset($xmlAsArray['OTAGetFullManifestRQ']['Manifest']) ){
                   if( trim(strtolower($this->Session->read('TokenId'))) == trim(strtolower($TokenId)) ){
                   	   $modules = $this->Module->find('all',array(
                                'conditions' => array( 'Module.status' => Configure::read('status_live') ),
                                'order' => array('Module.display_order')
                       ));
                       if(!empty($modules) && sizeof($modules)>0){
                       	  $this->set(compact('modules'));
                       	  $this->set('valid',true); 		
                       }else{
                       	  $this->set('errorCode','10004');
                       	  $this->set('errorMessage','Empty modules set. Please try again later.');
                          $this->set('valid',false);	
                       }
					}else{
                       $this->set('errorCode','10003');
                       $this->set('errorMessage','Invalid token parameter request. Please try again.');
                       $this->set('valid',false);
                   }
                }else{
                   $this->set('errorCode','10002');
                   $this->set('errorMessage','Invalid '.__FUNCTION__.'  request. Please try again.');
                   $this->set('valid',false);
                }
                $args['requestRS'] = $this->render();
                $this->requestLogsUpdate( $args , $transactionIdentifier );
           }	
       }
    }

    private function throwExceptionError($errorCode=0, $errorMessage = null, $isXML = false, $errorMethod=''){
    	if( $isXML == false){
    		$this->RequestHandler->respondAs('xml');
    		$this->header('Content-Type: text/xml'); 
        	$this->header('HTTP/1.1 '.$errorCode.' '.$this->__httpCodes[$errorCode]);
        	$this->cakeError('serviceResponse', array(
        		'errorMessage'     => $errorMessage,
        		'errorCode' => $errorCode.' '.$this->__httpCodes[$errorCode]
        		)
        	);	
    	}else{
    		$this->RequestHandler->respondAs('xml');
    		$this->header('Content-Type: text/xml'); 
        	$this->header('HTTP/1.1 '.$errorCode.' '.$this->__httpCodes[$errorCode]);
    		$this->cakeError('serviceResponseXml', array(
        		'errorMessage' => $errorMessage,
        		'errorCode'    => $errorCode,
    			'errorController'   => $errorMethod 
        		)
        	);
    	}
    }
}

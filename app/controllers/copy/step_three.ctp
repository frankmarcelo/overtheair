<?php

$aDisplayTopicsReference = $this->Session->read('aDisplayTopicsReference');
?>
<form name="step4" id="step4" method="post" onSubmit="return false;" accept-charset="utf-8">
	<input type="hidden" name="data[Module][id]" value="<?php echo $this->Session->read('Module.id');?>" />
	<div class="assessmentswrapper" id="assessmentswrapper">
		<h2><?php echo $this->Session->read('Module.name');?> (<?php echo $this->Session->read('ModuleType.ModuleType.name');?>)</h2>
		<div class="asessessments">		
			<table class="form-table">
				<tr class="form-field form-required">
					<th scope="row"><label><strong>Question <?php echo $this->Session->read('Module.display_order');?>.1</strong> (required)</label></th>
					<td><input type="text" autocomplete="off" name="data[Assessment][question][1][]" style="width:350px;"/></td>
				</tr>
				<tr class="form-field form-required">
					<th scope="row"><label><strong>Select one as answer</strong> (required)</label></th>
					<td class="form-field form-required">
						<span class="form-field form-required">
						Choice 1: <input type="radio" name="data[AssessmentsChoice][id][1][]" value="1" checked="checked"/><input type="text" name="data[AssessmentsChoice][text][1][]" style="width:250px"/><br/>  
						Choice 2: <input type="radio" name="data[AssessmentsChoice][id][1][]" value="2" /><input type="text" name="data[AssessmentsChoice][text][1][]" style="width:250px"/><br/>
						Choice 3: <input type="radio" name="data[AssessmentsChoice][id][1][]" value="3" /><input type="text" name="data[AssessmentsChoice][text][1][]" style="width:250px"/><br/>
						Choice 4: <input type="radio" name="data[AssessmentsChoice][id][1][]" value="4" /><input type="text" name="data[AssessmentsChoice][text][1][]" style="width:250px"/><br/>
						</span>
					</td>
				</tr>
				<tr class="form-field form-required">
					<th scope="row"><label><strong>Reference Material</strong> (optional)</label></th>
					<td><div class="add_another_reference_1"><?php
					if(!empty($aDisplayTopicsReference)){
				?><select name="data[AssessmentsReference][id][1][]">
					<?php
						foreach( $aDisplayTopicsReference as $referenceMaterials ){
						echo '<option value="'.$referenceMaterials['id'].'">'.$this->Text->truncate($referenceMaterials['value'],'50',array('ending' => '...','html' => true)).'</option>';
					}				
				?>	
					</select><?php
					}
				?><a style="cursor:pointer;" class="add_reference_1">Add another reference</a></div></td>
				</tr>
			</table>
			<br/>
			<div>&nbsp;<span><a style="cursor:pointer;" class="assessments_trigger">Add another assessment</a></span></div>
			<br />
		</div>
	</div>
</form>
<?php #echo $this->element('sql_dump'); ?>
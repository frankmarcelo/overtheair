<?php
/**
 * 
 * Over The Air Web Service
 * @author fmarcelo
 * Date Created 11th of September 2011
 * @copyright 2011
 * @version 1.0
 */

App::import('Sanitize');
class DocumentsController extends AppController {

	public $name = 'Documents';
	public $components = array(
        'Auth'
    );

	public $uses = array(
    	'User',
    	'Module',
    	'Setting',
    	'Topic',
        'TopicFiles',
    	'TopicFilesSources',
        'Assessment',
        'AssessmentsAnswer',
        'AssessmentsChoice',
        'AssessmentsReference'
    );

    public function beforeFilter() {
        parent::beforeFilter();
        $this->TopicFilesSources->contain();
        $topicFilesSource = $this->TopicFilesSources->find('list');
        $this->set(compact('topicFilesSource'));
    }
	
	public function index(){
	}
	
	public function upload(){
            $this->TopicFiles->contain();
	    $topicFiles = $this->TopicFiles->find('all');
            $this->set(compact('topicFiles'));
	}
	
	public function download(){
		//Configure::write('debug',2);
		if( isset($this->params['url']['src']) ){
            $this->autoRender = false;
            $this->layout = 'media';
  
            App::import('Core', 'Folder');
            App::import('Core', 'File');
            $DownloadLink = Configure::read('downloadLink');
        	$SourceFile = Sanitize::clean(trim($this->params['url']['src']));
        	
        	if( file_exists($DownloadLink . DS . 'topics' . DS . $SourceFile) ){
        		$this->TopicFiles->contain();
				$Macosx = $this->TopicFiles->findByRawName($SourceFile);
				if(!empty($Macosx)){
					header("HTTP/1.1 200 OK");
					header("Content-Description: File Transfer");
					header("Accept-Ranges: bytes");
					foreach (getallheaders() as $name => $value) {
						if( strstr(strtolower($name),'range')){
 			   				header("Content-Range: bytes ".$value."/".$Macosx['TopicFiles']['raw_size']);
						}
					}
					
					
	                header("Content-disposition: attachment; filename=".$SourceFile);
	                header("Content-Length: ".$Macosx['TopicFiles']['raw_size']);
	                header("Content-Type: ".$Macosx['TopicFiles']['raw_type']);
	                header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	                header("Pragma: public");
	                header("Connection: close");
	                readfile($DownloadLink.DS . 'topics' . DS . $SourceFile);
				}else{
					header("HTTP/1.0 404 File Not Found");
	            }
   			}else{
   				header("HTTP/1.0 404 File Not Found");	
   			}    
        }
    }
}

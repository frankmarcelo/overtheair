<?php
/**
 * Users Controller
 *
 * PHP version 5
 *
 */
App::import('Sanitize');

class UsersController extends AppController {
/**
 * Controller name
 *
 * @var string
 * @access public
 */
    
    public $name = 'Users';
    public $hasOne = array(
		'Roles' => array(
		'className'  => 'Roles',
		'foreignKey' => 'Roles.id', 
             	'dependent'  => true
		)
	); 
    public $paginate = array(
    	'limit' => 100, 
		'order' => array(
			'User.id' => 'desc'
		)
	);
	
	private $authenticationKey = 'Otawebservices-10019';
	
    
/**
 * Components
 *
 * @var array
 * @access public
 */
    public $components = array(
        'Auth' ,
        'Email'
    );
/**
 * Models used by the Controller
 *
 * @var array
 * @access public
 */
    public $uses = array('User','Manifest');

    public function beforeFilter() {
		Configure::write('debug',2);
    	parent::beforeFilter();
    	$this->Security->validatePost = false;
		$this->Auth->authenticate = ClassRegistry::init('User');
		$this->Auth->userScope = array('User.status' => Configure::read("status_live"),"User.role_id != ".Configure::read("userRoleId") );
		$this->Auth->fields = array( 'username' => 'username','password' => 'password' );
        $this->Auth->authError = "Sorry, you are lacking access.";
        if( isset($this->data['User']['username']) ){
        	$this->Auth->loginError = '<strong>ERROR</strong>: The password you entered for the username <strong>'.$this->data['User']['username'].'</strong> is incorrect. Lost your <a href="/over-the-air/users/forgotpassword/">password</a>?';
        }
        $this->Auth->allow('forgot_password','reset');
        
       	if (in_array($this->params['action'], array('login'))) {
    		$field = $this->Auth->fields['username'];
            if (!empty($this->data) && empty($this->data['User'][$field])) {
                $this->redirect(array('action' => $this->params['action']));
            }
            $cacheName = 'auth_failed_' . $this->data['User'][$field];
            if (Cache::read($cacheName, 'users_login') >= Configure::read('User.failed_login_limit')) {
                $this->Session->setFlash(__('You have reached maximum limit of <strong>'.Configure::read('User.failed_login_limit').'</strong> failed login attempts. Please try again after a few minutes.', true), 'default', array('class' => 'authMessage'));
                $this->redirect(array('action' => $this->params['action']));
            }
        }
    }

    public function beforeRender() {
        parent::beforeRender();

        if (in_array($this->params['action'], array('admin_login', 'login'))) {
            if (!empty($this->data)) {
                $field = $this->Auth->fields['username'];
                $cacheName = 'auth_failed_' . $this->data['User'][$field];
                $cacheValue = Cache::read($cacheName, 'users_login');
                Cache::write($cacheName, (int)$cacheValue + 1, 'users_login');
            }
        }
    }
    

    public function index() {
    	if ( intval($this->Session->read('Auth.User.role_id')) != intval(Configure::read('adminRoleId')) ){
        	$this->Session->setFlash(__("Sorry you're account is not allowed to access that page.", true), 'default', array('class' => 'error'));
            $this->redirect(array('controller'=>'settings'));
        }
    	
    	$this->set('title_for_layout', __('Users', true));
        $users = $this->User->find('all',
        	array(
        		'conditions' => array('User.status' => Configure::read("status_live") ),
            	'order' => array( 'User.date_created' => 'desc' )
            )
        );
        $this->set('users', $users);
    }
    
	public function add() {
		Configure::write('debug',0);
    	if ( intval($this->Session->read('Auth.User.role_id')) != intval(Configure::read('adminRoleId')) ){
        	$this->Session->setFlash(__("Sorry you're account is not allowed to access that page.", true), 'default', array('class' => 'error'));
            $this->redirect(array('controller'=>'settings'));
        }
        
        if ( !empty($this->data) ) {
			if(	strlen($this->data['User']['username']) > 0 &&
				strlen($this->data['User']['name'])     > 0 &&
				strlen($this->data['User']['email'])    > 0 
				/*( 
				   trim($this->data['User']['password']) == 
				   trim($this->data['User']['password2'])
				)
				TODO fix how the password being not to decrypt when catched by cakephp
				*/
			){
				if(preg_match("/^[A-Za-z0-9_]+$/",trim($this->data['User']['username']))){
					$user = $this->User->find('first',array(
							'conditions' => array( 
								'OR' => array(
									'User.username' => Sanitize::clean($this->data['User']['username']),
									'User.email'    => Sanitize::clean($this->data['User']['email'])
								),
								'AND' => array('User.status' => Configure::read("status_live") ) 
							)
						)
					);
				
					if( (!empty($user) && count($user) > 0) ){
						$this->Session->setFlash(__('The username or email already exists. Request could not be saved. Please, try again.', true), 'default', array('class' => 'error'));
					}else{
						$this->User->begin(); 
						
						$this->Manifest->contain();
						$manifest = $this->Manifest->find('all',array(
                             'conditions' => array( 'Manifest.status' => Configure::read('status_live') ), 
                             'order' => array('Manifest.id' => 'desc'),'limit'=>1
                           )
                		); 
						$this->data['User']['username'] = trim($this->data['User']['username']);
						$this->data['User']['token'] = $this->data['User']['password']; 
						$this->data['User']['status'] = Configure::read('status_live');
						$this->data['User']['invitation_key'] = md5(uniqid());
						$this->data['User']['revision_id']  = ( isset($manifest[0]['Manifest']['id']) && intval($manifest[0]['Manifest']['id'])>0 ) ? intval($manifest[0]['Manifest']['id']):0; 
						$this->data['User']['who_modified'] = $this->Session->read('Auth.User.id');
						$this->data['User']['date_created'] = date("Y-m-d H:i:s",strtotime("now"));
						$this->data['User']['date_modified'] = date("Y-m-d H:i:s",strtotime("now"));
						$this->data['User']['email'] = Sanitize::clean($this->data['User']['email']);
						$this->data['User']['name'] = Sanitize::clean($this->data['User']['name']);
						$this->data['User']['download_type_id'] =( intval($this->data['User']['role_id']) == intval(Configure::read('userRoleId')) ) ? $this->data['User']['download_type_id']:0;	
						
						if ($this->User->save($this->data)) {
							$this->User->commit();
							
							$user = $this->User->findById($this->User->getInsertID());
	            			$this->User->id = $user['User']['id'];
	            			$this->User->saveField('password', md5($this->data['User']['username'] . ':'.$this->authenticationKey.':' . trim($this->data['User']['password2'])));
							$activationKey = $this->data['User']['invitation_key'];
							if (Configure::read('debug') > 1) {
								$this->Email->delivery = 'debug';	
	    					}
							 
							$this->Email->from = Configure::read('adminEmail');
							$this->Email->to = $this->data['User']['email'];
							if( $this->data['User']['role_id'] < Configure::read("userRoleId") ){//send this email to non mobile users
								
								$this->set(compact('user', 'activationKey'));
	                			$this->Email->subject = __('Please activate your Greensplash account', true);
	                			$this->Email->template = 'forgot_password';
	                			$this->Email->sendAs = 'html';
								$this->Email->send();
							}else{
								if(isset($this->macos) && sizeof($this->macos)>0){
									foreach( $this->macos as $mac_app ){
										if( intval($mac_app['Macosx']['download_type_id'])==intval($this->data['User']['download_type_id']) || (intval($this->data['User']['download_type_id'])==Configure::read('bothSoftware')) ){
											$this->set(compact('user', 'activationKey'));
											$this->set('source_file',$mac_app['Macosx']['file_name']);
											$this->set('downloadTypeId',$mac_app['Macosx']['download_type_id']);
		                					$this->Email->subject = __('Please download your Greensplash '.$mac_app['DownloadType']['alias'].' app', true);
		                					$this->Email->template = 'download_app';
		                					$this->Email->sendAs = 'html';
											$this->Email->send();				
										} 
									}
								}else{
									$this->set(compact('user', 'activationKey'));
									$this->Email->subject = __('Greensplash mobile app account for '.$this->data['User']['name'].'.', true);
		                			$this->Email->template = 'regular_add';
		                			$this->Email->sendAs = 'html';
									$this->Email->send();
								}
							}
							
							if (Configure::read('debug') > 1) {
	    						//pr($this->Session->read('Message.email'));
	    						//$this->Session->delete('Message.email');
	    						//die;
	    					}
							$this->Session->setFlash(__('You have successfully created an account. An email has been sent with further instructions.', true), 'default', array('class' => 'success'));
							$this->redirect(array('controller'=>'users','action' => 'index'));
	    				} else {
							$this->User->rollback();
							$this->Session->setFlash(__('The User could not be saved. Please, try again.', true), 'default', array('class' => 'error'));
						}
					}
				}else{
					$this->Session->setFlash(__('Username field only accepts alpha numeric and underscore characters .', true), 'default', array('class' => 'error'));
				}
			}else{
				$this->Session->setFlash(__('One of the required fields is empty.', true), 'default', array('class' => 'error'));
			}
        }
    }
    
    public function profile(){
    	$user = $this->User->findById(intval($this->Session->read('Auth.User.id')));
    	if( empty($user) ){
			$this->Session->setFlash(__('Cannot process request invalid user account.', true), 'default', array('class' => 'error'));
            $this->redirect(array('controller'=>'users','action' => 'index'));
		}
		
		if(!empty($this->data)){
			if(	strlen($this->data['User']['username']) > 0 &&
				strlen($this->data['User']['name'])     > 0 &&
				strlen($this->data['User']['email'])    > 0 
				/*( 
				   trim($this->data['User']['password']) == 
				   trim($this->data['User']['password2'])
				)
				TODO fix how the password being not to decrypt when catched by cakephp
				*/
			){
				if(preg_match("/^[A-Za-z0-9_]+$/",trim($this->data['User']['username']))){
					$userSearch = $this->User->find('first',array(
							'conditions' => array( 
								'OR' => array(
									'User.username' => Sanitize::clean($this->data['User']['username']),
									'User.email' => Sanitize::clean($this->data['User']['email'])
								),
								'AND' => array(
									'User.status' => Configure::read("status_live"),
									'User.id <> '.$user['User']['id']
								) 
							)
						)
					);
				
					if( (!empty($userSearch) && count($userSearch) > 0) ){
						$this->Session->setFlash(__('The username or email already exists. Request could not be saved. Please, try again.', true), 'default', array('class' => 'error'));
					}else{
						$this->User->begin(); 
						
						$this->User->id = $user['User']['id'];
						if( trim($this->data['User']['username']) != trim($user['User']['username']) ){
							$this->data['User']['username'] = trim($this->data['User']['username']);	
						}
						
						if( trim($this->data['User']['email']) != trim($user['User']['email']) ){
							$this->data['User']['email'] = Sanitize::clean($this->data['User']['email']);	
						}
						
						if( trim($this->data['User']['name']) != trim($user['User']['name']) ){
							$this->data['User']['name'] = Sanitize::clean($this->data['User']['name']);
						}
						
						$this->data['User']['token'] = $this->data['User']['password']; 
						$this->data['User']['invitation_key'] = md5(uniqid());
						$this->data['User']['who_modified'] = $this->Session->read('Auth.User.id');
						$this->data['User']['date_modified'] = date("Y-m-d H:i:s",strtotime("now"));
						$this->data['User']['password'] = md5($this->data['User']['username'] . ':'.$this->authenticationKey.':' . trim($this->data['User']['password2']));
					        
                        if(!isset($this->data['User']['role_id'])){
                        	$this->data['User']['role_id'] = Configure::read('adminRoleId');
                        } 
	
						if( intval($this->data['User']['role_id']) != intval($user['User']['role_id']) && 
							intval($this->data['User']['role_id']) != intval(Configure::read('userRoleId')) ){
							$this->data['User']['download_type_id'] = 0;
						}
						
						if( intval($this->data['User']['role_id']) != intval(Configure::read('userRoleId')) ){
							$this->data['User']['download_type_id'] = 0;
						}

						
						if ($this->User->save($this->data)) {
							$this->User->commit();
							$activationKey = $this->data['User']['invitation_key'];
							$this->Session->write('Auth.User.role_id', $this->data['User']['role_id']);
							if (Configure::read('debug') > 1) {
								$this->Email->delivery = 'debug';	
	    					}
							 
							$this->Email->from = Configure::read('adminEmail');
							$this->Email->to = $this->data['User']['email'];
							if( intval($this->data['User']['role_id']) == intval(Configure::read("userRoleId")) ){//send this email to non mobile users
								if( intval($this->data['User']['download_type_id']) != intval($user['User']['download_type_id']) ){
									if(isset($this->macos) && sizeof($this->macos)>0){
										foreach( $this->macos as $mac_app ){
											if( intval($mac_app['Macosx']['download_type_id'])==intval($this->data['User']['download_type_id']) || (intval($this->data['User']['download_type_id'])==Configure::read('bothSoftware')) ){
												$this->set(compact('user', 'activationKey'));
												$this->set('source_file',$mac_app['Macosx']['file_name']);
											$this->set('downloadTypeId',$mac_app['Macosx']['download_type_id']);
			                					$this->Email->subject = __('Please download your Greensplash '.$mac_app['DownloadType']['alias'].' app', true);
			                					$this->Email->template = 'download_app';
			                					$this->Email->sendAs = 'html';
												$this->Email->send();				
											} 
										}
									}else{
										$this->set(compact('user', 'activationKey'));
										$this->Email->subject = __('Greensplash mobile app account for '.$this->data['User']['name'].'.', true);
			                			$this->Email->template = 'regular_add';
			                			$this->Email->sendAs = 'html';
										$this->Email->send();
									}
								}
							}
							
							if (Configure::read('debug') > 1) {
	    						//pr($this->Session->read('Message.email'));
	    						//$this->Session->delete('Message.email');
	    						//die;
	    					}
	    					
	    					$this->Session->setFlash(__('You have successfully updated your profile account.', true), 'default', array('class' => 'success'));
	    					$this->redirect(array('controller'=>'users','action' => 'index'));
	    				} else {
							$this->User->rollback();
							$this->Session->setFlash(__('The User could not be saved. Please, try again.', true), 'default', array('class' => 'error'));
						}
					}
				}else{
					$this->Session->setFlash(__('Username field only accepts alpha numeric and underscore characters .', true), 'default', array('class' => 'error'));
				}
			}else{
				$this->Session->setFlash(__('One of the required fields is empty.', true), 'default', array('class' => 'error'));
			}
		}
		
    	$this->set(compact('user'));
    }

    public function edit($id = null, $token=null) {
    	if ( $this->Session->read('Auth.User.role_id') != Configure::read("adminRoleId") ){
			$this->Session->setFlash(__("Sorry you're account is not allowed to access that page.", true), 'default', array('class' => 'error'));
			$this->redirect(array('controller'=>'settings'));
		}
		
		$user = $this->User->findByUsername(trim($id));
		if( !empty($user) && sizeof($user)>0 ){
			if( trim(strtolower($user['User']['token'])) != trim(strtolower($token)) ){
				$this->Session->setFlash(__('Cannot process request invalid user account.', true), 'default', array('class' => 'error'));
            	$this->redirect(array('controller'=>'users','action' => 'index'));	
			}
		}else{
			$this->Session->setFlash(__('Cannot process request invalid user account.', true), 'default', array('class' => 'error'));
            $this->redirect(array('controller'=>'users','action' => 'index'));
		}
		
		if (!empty($this->data)) {
			if(	strlen($this->data['User']['username']) > 0 &&
				strlen($this->data['User']['name'])     > 0 &&
				strlen($this->data['User']['email'])    > 0 
				/*( 
				   trim($this->data['User']['password']) == 
				   trim($this->data['User']['password2'])
				)
				TODO fix how the password being not to decrypt when catched by cakephp
				*/
			){
				if(preg_match("/^[A-Za-z0-9_]+$/",trim($this->data['User']['username']))){
					$userSearch = $this->User->find('first',array(
							'conditions' => array( 
								'OR' => array(
									'User.username' => Sanitize::clean($this->data['User']['username']),
									'User.email' => Sanitize::clean($this->data['User']['email'])
								),
								'AND' => array(
									'User.status' => Configure::read("status_live"),
									'User.id <> '.$user['User']['id']
								) 
							)
						)
					);
				
					if( (!empty($userSearch) && count($userSearch) > 0) ){
						$this->Session->setFlash(__('The username or email already exists. Request could not be saved. Please, try again.', true), 'default', array('class' => 'error'));
					}else{
						$this->User->begin(); 
						$this->Manifest->contain();
						$manifest = $this->Manifest->find('all',array(
                             'conditions' => array( 'Manifest.status' => Configure::read('status_live') ), 
                             'order' => array('Manifest.id' => 'desc'),'limit'=>1
                           )
                		); 
						$this->User->id = $user['User']['id'];
						if( trim($this->data['User']['username']) != trim($user['User']['username']) ){
							$this->data['User']['username'] = trim($this->data['User']['username']);	
						}
						
						if( trim($this->data['User']['email']) != trim($user['User']['email']) ){
							$this->data['User']['email'] = Sanitize::clean($this->data['User']['email']);	
						}
						
						if( trim($this->data['User']['name']) != trim($user['User']['name']) ){
							$this->data['User']['name'] = Sanitize::clean($this->data['User']['name']);
						}
						
						$this->data['User']['token'] = $this->data['User']['password']; 
						$this->data['User']['invitation_key'] = md5(uniqid());
						$this->data['User']['who_modified'] = $this->Session->read('Auth.User.id');
						$this->data['User']['date_modified'] = date("Y-m-d H:i:s",strtotime("now"));
						$this->data['User']['password'] = md5($this->data['User']['username'] . ':'.$this->authenticationKey.':' . trim($this->data['User']['password2']));
						
						if( intval($this->data['User']['role_id']) != intval($user['User']['role_id']) && 
							intval($this->data['User']['role_id']) != intval(Configure::read('userRoleId')) ){
							$this->data['User']['download_type_id'] = 0;
						}
						
						if( intval($this->data['User']['role_id']) != intval(Configure::read('userRoleId')) ){
							$this->data['User']['download_type_id'] = 0;
						}
						
						if ($this->User->save($this->data)) {
							$this->User->commit();
							$activationKey = $this->data['User']['invitation_key'];
							if (Configure::read('debug') > 1) {
								$this->Email->delivery = 'debug';	
	    					}
							 
							$this->Email->from = Configure::read('adminEmail');
							$this->Email->to = $this->data['User']['email'];
							if( intval($this->data['User']['role_id']) == intval(Configure::read("userRoleId")) ){//send this email to non mobile users
								if( intval($this->data['User']['download_type_id']) != intval($user['User']['download_type_id']) ){
									if(isset($this->macos) && sizeof($this->macos)>0){
										foreach( $this->macos as $mac_app ){
											if( intval($mac_app['Macosx']['download_type_id'])==intval($this->data['User']['download_type_id']) || (intval($this->data['User']['download_type_id'])==Configure::read('bothSoftware')) ){
												$this->set(compact('user', 'activationKey'));
												$this->set('source_file',$mac_app['Macosx']['file_name']); 
											        $this->set('downloadTypeId',$mac_app['Macosx']['download_type_id']);
			                					$this->Email->subject = __('Please download your Greensplash '.$mac_app['DownloadType']['alias'].' app', true);
			                					$this->Email->template = 'download_app';
			                					$this->Email->sendAs = 'html';
												$this->Email->send();				
											} 
										}
									}else{
										$this->set(compact('user', 'activationKey'));
										$this->Email->subject = __('Greensplash mobile app account for '.$this->data['User']['name'].'.', true);
			                			$this->Email->template = 'regular_add';
			                			$this->Email->sendAs = 'html';
										$this->Email->send();
									}
								}
							}
							
							if (Configure::read('debug') > 1) {
	    						//pr($this->Session->read('Message.email'));
	    						//$this->Session->delete('Message.email');
	    						//die;
	    					}
							$this->Session->setFlash(__('You have successfully updated the account.', true), 'default', array('class' => 'success'));
							$this->redirect(array('controller'=>'users','action' => 'index'));
	    				} else {
							$this->User->rollback();
							$this->Session->setFlash(__('The User could not be saved. Please, try again.', true), 'default', array('class' => 'error'));
						}
					}
				}else{
					$this->Session->setFlash(__('Username field only accepts alpha numeric and underscore characters .', true), 'default', array('class' => 'error'));
				}
			}else{
				$this->Session->setFlash(__('One of the required fields is empty.', true), 'default', array('class' => 'error'));
			}
		}
        $this->set(compact('user'));
    }

   	public function search(){
    	$this->set('title_for_layout', __('Users', true));
        
    	$query = null;
        $sOrder = null;
        if( isset($this->params['url']['q']) && strlen(trim($this->params['url']['q']))> 0 ){
        	$query = trim($this->params['url']['q']);
        	if(strlen($query)<1){
        		$this->Session->setFlash(__('Cannot process request invalid user search.', true), 'default', array('class' => 'error'));
            	$this->redirect(array('controller'=>'users','action' => 'index'));	
        	}
        }else{
        	$this->Session->setFlash(__('Cannot process request invalid user search.', true), 'default', array('class' => 'error'));
            $this->redirect(array('controller'=>'users','action' => 'index'));	
        }
        
        $conditions = array();
        if( !empty($query) && strlen($query)> 0 ){
        	
        	App::import('Sanitize');
        	$full_text_search = explode (" ", $query );
     	   	foreach ($full_text_search as $wordindex => $wordvalue) {
				if( strlen(trim($wordvalue))==0) {
					unset($full_text_search[$wordindex]);
            	}
			}
    
			$fulltext_array = array();
			$like_array = array();
			$original_search = array();
			foreach ($full_text_search as $wordindex => $wordvalue) {
				if( strlen ($wordvalue)<=3 ) {
					$like_array[] = Sanitize::clean($wordvalue);
            	}else{
            	    $fulltext_array[] = "+".Sanitize::clean($wordvalue)."*";
            	}
            }
        	
			$original_search = array_merge($like_array,$fulltext_array);
			$boolSearchByLike = $this->User->getLikeFullName($query);
        	if( $boolSearchByLike == true){
        		$conditions[] = array(
	                           		"(TRIM(UPPER(User.username))) LIKE '%".addslashes($query)."%'",
        							"User.username IS NOT NULL",
        							"LENGTH(TRIM(User.username)) > 0",			
	        						"User.status"=> Configure::read('status_live')
	        	);
	        	$sOrder = "TRIM(UPPER(User.username)) ASC";
	        	
        	}else{
        		
        		$fullword_query = trim(implode(' ', $original_search));
        		$conditions[] = array(
	        					"( 
									(MATCH(User.username,User.email,User.name) AGAINST ('".addslashes(trim($fullword_query))."' IN boolean MODE)) OR 
	                       			(TRIM(UPPER(User.username))) LIKE '%".addslashes($query)."%'
	                       		)",
	        					"( User.username IS NOT NULL )",
        						"( LENGTH(TRIM(User.username))> 0 )",
	        					"User.status"=> Configure::read('status_live')
	        	);
	        	$sOrder = "MATCH(User.username,User.email,User.name) AGAINST ('".addslashes(trim($query))."') DESC,TRIM(UPPER(User.username)) ASC";
	        	
        	}
	    }else{
            $conditions[] = array(
            					"( User.username IS NOT NULL )",
        						"( LENGTH(TRIM(User.username))> 0 )",
            					"User.status" => Configure::read('status_live')
            				);
        }
        
      	$sPage = 1;
    	if( isset($this->params['url']['page']) ){
    		if( is_numeric($this->params['url']['page']) ){
    			$sPage = $this->params['url']['page'];
    		}
    	}
    	
    	$order = array();
    	$recursive = 1;
    	if( !empty($query) && $query ){
    		$order = array($sOrder);
    	}else{
            $order = array( "TRIM(UPPER(User.username)) ASC" );
        }
    	
        $this->paginate['User'] = array(
    				'recursive' => $recursive,
    				'conditions'=> $conditions,
    				'page'  => $sPage,
                    'order' => $order
        );
    	
    	$this->set('users',$this->paginate('User'));
        
        $users = $this->User->find('all',array(
        		'conditions' => array('User.status' => Configure::read("status_live") ),
            	'order' => array( 'User.date_created' => 'desc' )
            )
        );
        $this->set('userList',$users);
    }
    
    public function resend($id=null,$token=null){
    	Configure::write('debug', 0);
        $this->layout ='ajax';
        //if( $this->RequestHandler->isAjax() ){
    		if ( $this->Session->read('Auth.User.role_id') != Configure::read("adminRoleId") ){
				$this->Session->setFlash(__("Sorry you're account is not allowed to access that page.", true), 'default', array('class' => 'error'));
				$this->set('data',false);
			}
		
	        if (!$id || is_null($token)) {
	            $this->Session->setFlash(__('Invalid delete request', true), 'default', array('class' => 'error'));
	            $this->set('data',false);
	        }
			
			$user = $this->User->findByUsername($id);
	        if( !isset($user['User']['id']) || $user['User']['status'] !=  Configure::read('status_live') ||
				trim(strtolower($user['User']['token'])) != trim(strtolower($token))
			) {
	            $this->Session->setFlash(__('Invalid delete request please try again.', true), 'default', array('class' => 'success'));
	            $this->set('data',false);
	        }
        	
	        if (Configure::read('debug') > 1) {
				$this->Email->delivery = 'debug';	
	    	}
	        
    		$this->Email->from = Configure::read('adminEmail');
			$this->Email->to = $user['User']['email'];
			if( intval($user['User']['role_id']) == intval(Configure::read("userRoleId")) ){//send this email to non mobile users
				if(isset($this->macos) && sizeof($this->macos)>0){
					$isSend = false;
					foreach( $this->macos as $mac_app ){
						if( intval($mac_app['Macosx']['download_type_id'])==intval($user['User']['download_type_id']) || (intval(Configure::read('bothSoftware'))==intval($user['User']['download_type_id']))  ){
							$this->set(compact('user', 'activationKey'));
							$this->set('source_file',$mac_app['Macosx']['file_name']);
                                                        $this->set('downloadTypeId',$mac_app['Macosx']['download_type_id']);
			                $this->Email->subject = __('Please download your Greensplash '.$mac_app['DownloadType']['alias'].' app', true);
			                $this->Email->template = 'download_app';
			                $this->Email->sendAs = 'html';
							$this->Email->send();
							$isSend=true;
							//break;
						} 
					}
					
					if (Configure::read('debug') > 1) {
	    				//pr($this->Session->read('Message.email'));
	    				//$this->Session->delete('Message.email');
	    				//die;
					}
					
					if($isSend==true){
						$this->set('data',true);
					}else{
						$this->set('data',false);
					}
				}else{
					$this->set('data',false);
				}
			}
		//}else{
        //	$this->set('data',false);
        //}
    }
    
	public function reset($username = null, $key = null) {
		if ( $this->Session->read('Auth.User.id') > 0 ){
			$this->Session->setFlash(__("Sorry you're account is not allowed to access that page.", true), 'default', array('class' => 'error'));
			$this->redirect(array('controller'=>'settings'));
		}
		
		$this->Session->destroy();
		$this->set('title_for_layout', __('Reset Password', true));
		if ($username == null || $key == null) {
			$this->Session->setFlash(__('An error occurred.', true), 'default', array('class' => 'error'));
            $this->redirect(array('controller'=>'users','action' => 'login'));
        }
        
        $user = $this->User->find('first', array(
            'conditions' => array(
                'User.username' => $username,
                'User.invitation_key' => $key,
        		'User.status' =>  Configure::read('status_live')
            ),
        ));
        
        
        if (!isset($user['User']['id'])) {
            $this->Session->setFlash(__('An error occurred invalid request for <strong>'.$username."</strong>.", true), 'default', array('class' => 'error'));
            $this->redirect(array('action' => 'login'));
        }
		if (!empty($this->data) && isset($this->data['User']['password'])) {
            $this->User->id = $user['User']['id'];
            $this->User->begin(); 
            $this->data['User']['password'] = md5(trim($username) . ':'.$this->authenticationKey.':' . trim($this->data['User']['password']));
            $this->data['User']['token'] = $this->data['User']['password']; 
            $this->data['User']['invitation_key'] = md5(uniqid());
            $this->data['User']['date_modified'] = date("Y-m-d H:i:s",strtotime("now"));
            
            if ($this->User->save($this->data)) {
            	$this->User->commit();
                $this->Session->setFlash(__('Your password has been reset successfully.', true), 'default', array('class' => 'success'));
                $this->redirect(array('action' => 'login'));
            } else {
            	$this->User->rollback();
                $this->Session->setFlash(__('An error occurred invalid request for <strong>'.$username."</strong>. Please try again.", true), 'default', array('class' => 'error'));
            }
        }

        $this->set(compact('user', 'username', 'key'));
    }

    public function delete($id = null,$token=null) {
    	
        Configure::write('debug', 0);
        $this->layout ='ajax';
        
        if( $this->RequestHandler->isAjax() ){
    		if ( $this->Session->read('Auth.User.role_id') != Configure::read("adminRoleId") ){
				$this->Session->setFlash(__("Sorry you're account is not allowed to access that page.", true), 'default', array('class' => 'error'));
				$this->set('data',false);
			}
		
	        if (!$id || is_null($token)) {
	            $this->Session->setFlash(__('Invalid delete request', true), 'default', array('class' => 'error'));
	            $this->set('data',false);
	        }
			
			$user = $this->User->findByUsername($id);
	        if( !isset($user['User']['id']) || $user['User']['status'] !=  Configure::read('status_live') ||
				trim(strtolower($user['User']['token'])) != trim(strtolower($token))
			) {
	            $this->Session->setFlash(__('Invalid delete request please try again.', true), 'default', array('class' => 'success'));
	            $this->set('data',false);
	        }
	
			$this->User->id = $user['User']['id'];
	        $this->User->begin(); 
			$this->data['User']['status'] = Configure::read("status_deleted");
			$this->data['User']['date_modified'] = date("Y-m-d H:i:s",strtotime("now"));
			$this->data['User']['who_modified'] = $this->Session->read('Auth.User.id');
			if ($this->User->save($this->data)) {
	            $this->User->commit();
				$this->Session->setFlash(__('You have successfully deleted the account of '.$user['User']['name'].'.', true), 'default', array('class' => 'success'));
				$this->set('data',true);
			}else{
				$this->User->rollback();
				$this->Session->setFlash(__('Invalid delete request please try again.', true), 'default', array('class' => 'success'));
				$this->set('data',false);
			}
        }else{
        	$this->Session->setFlash(__('Invalid delete request please try again.', true), 'default', array('class' => 'success'));
        	$this->set('data',false);
        }
    }

    public function login() {
    	$this->Session->delete('ModuleType');
        $this->Session->delete('aShowTopic');
    	$this->Session->delete('Module');
    	$this->Session->delete('Topic');
    	$this->Session->delete('Auth');
    	$this->set('title_for_layout', __('Login', true));
    }
    public function logout() {
    	$this->Session->delete('ModuleType');
        $this->Session->delete('aShowTopic');
    	$this->Session->delete('Module');
    	$this->Session->delete('Topic');
    	$this->Session->setFlash(__('Log out successful.', true), 'default', array('class' => 'success'));
        $this->redirect($this->Auth->logout());
    }
    
    public function forgot_password(){
    	if ( $this->Session->read('Auth.User.id') > 0 ){
			$this->Session->setFlash(__("Sorry you're account is not allowed to access that page.", true), 'default', array('class' => 'error'));
			$this->redirect(array('controller'=>'settings'));
		}
    	
    	$this->Session->destroy();
    	$this->set('title_for_layout', __('Forgot Password', true));
        if (!empty($this->data) && isset($this->data['User']['username'])) {
            $user = $this->User->findByUsername($this->data['User']['username']);
            if (!isset($user['User']['id']) || $user['User']['status'] !=  Configure::read('status_live') ) {
                $this->Session->setFlash(__('Invalid username or username not found.', true), 'default', array('class' => 'error'));
                $this->redirect(array('action' => 'login'));
            }

            if ( strtolower($user['Role']['title']) == 'user' ){
                $this->Session->setFlash(__('User account does not have privileges to login.', true), 'default', array('class' => 'error'));
                $this->redirect(array('action' => 'login'));
            }
           
            $activationKey = md5(uniqid());
            $this->User->id = $user['User']['id'];
            $this->User->begin(); 
            $this->data['User']['invitation_key'] = $activationKey;

            if( $this->User->save($this->data) ){
                $this->User->commit();
                $this->set(compact('user', 'activationKey'));
            	if (Configure::read('debug') > 1) {
					$this->Email->delivery = 'debug';	
	    		}
                
                $this->Email->from = Configure::read('adminEmail');
                $this->Email->to = $user['User']['email'];
                $this->Email->subject = __('Reset Password', true);
                $this->Email->template = 'forgot_password';
                $this->Email->sendAs = 'html';
                if ($this->Email->send()) {
                	if (Configure::read('debug') > 1) {
	    				//pr($this->Session->read('Message.email'));
	    				//$this->Session->delete('Message.email');
	    				//die;
					}
                    $this->Session->setFlash(__('An email has been sent with instructions for resetting your password.', true), 'default', array('class' => 'success'));
                    $this->redirect(array('action' => 'login'));
				} else {
					$this->Session->setFlash(__('An error occurred. Please try again.', true), 'default', array('class' => 'error'));
                    $this->redirect(array('action' => 'login'));
                }
            }else{
                $this->User->rollback();
                $this->Session->setFlash(__('An error occurred. Please try again.', true), 'default', array('class' => 'error'));
                $this->redirect(array('action' => 'login'));
            }
        }
    }
    
	public function paginate($object = null, $scope = array(), $whitelist = array(), $key = null) {
      	$results = parent::paginate($object, $scope, $whitelist);
      	if ($key) {
       		$this->params['paging'][$key] = $this->params['paging'][$object];
       		unset($this->params['paging'][$object]);
      	}
		return $results;
    }
}

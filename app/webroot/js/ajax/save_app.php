<?php

require_once '../../../config/database.php';

$dbConfig = new DATABASE_CONFIG();
$mysqli = new mysqli( $dbConfig->default['host'] , 
                      $dbConfig->default['login'] ,
                      $dbConfig->default['password'] ,
                      $dbConfig->default['database'] );
if( $mysqli->ping() ){
  
   $appId = ( intval($_POST['app_id']) > 0 ) ? intval($_POST['app_id']) : 0;
   $downloadTypeId = ( intval($_POST['download_type_id']) > 0 ) ? intval($_POST['download_type_id']) : 0;

   header('Cache-Control: no-cache, must-revalidate');
   header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
   header('Content-type: application/json');
   $sql = "SELECT * FROM macos WHERE id='".$mysqli->escape_string($appId)."'";
   $aData = false;
   if( $aRes = $mysqli->query($sql) ){
       if( $aRes->num_rows > 0 ){
          $sql =" UPDATE macos SET download_type_id='".$downloadTypeId."' WHERE id='".$mysqli->escape_string($appId)."'";
          $mysqli->query($sql);
          $aData = true;
       }
       $aRes->close();
   }
   echo json_encode($aData);
}else{
   die;
}

$mysqli->close();
?>

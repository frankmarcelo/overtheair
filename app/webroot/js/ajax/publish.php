<?php

require_once '../../../config/database.php';

$dbConfig = new DATABASE_CONFIG();
$mysqli = new mysqli( $dbConfig->default['host'] , 
                      $dbConfig->default['login'] ,
                      $dbConfig->default['password'] ,
                      $dbConfig->default['database'] );
if( $mysqli->ping() ){
  
   $moduleId = ( intval($_POST['module_id']) > 0 ) ? intval($_POST['module_id']) : 0;
   $changeTypeId = ( intval($_POST['change_type_id']) > 0 ) ? intval($_POST['change_type_id']) : 0;

   header('Cache-Control: no-cache, must-revalidate');
   header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
   header('Content-type: application/json');
   $sql = "SELECT * FROM modules WHERE id='".$mysqli->escape_string($moduleId)."'";
   $aData = false;
   if( $aRes = $mysqli->query($sql) ){
       if( $aRes->num_rows > 0 ){
	  
          $sqlnumrows = "SELECT * FROM modules WHERE status=1";
          $aData = false;
          $iTotalLive = 0;
          if( $aRes2 = $mysqli->query($sqlnumrows) ){
              $iTotalLive = $aRes->num_rows;
              $aRes2->close();
          }
          
          if( $changeTypeId == 1 ){
          	  if( $iTotalLive < 4){
          	  	 $aData = true;
          		 $sql =" UPDATE modules SET status='1' WHERE id='".$mysqli->escape_string($moduleId)."'";
          		 $mysqli->query($sql);
          	  }else{
          	  	 $aData = false;
          	  }
          }elseif( $changeTypeId == 2 ){
          	  $aData = true;
          	  $sql =" UPDATE modules SET status='2' WHERE id='".$mysqli->escape_string($moduleId)."'";
          	  $mysqli->query($sql);
          }else{
          	  $sql =" UPDATE modules SET status='0' WHERE id='".$mysqli->escape_string($moduleId)."'";
          	  $mysqli->query($sql);
          	  $aData = true;
          }
       }
       $aRes->close();
   }
   echo json_encode($aData);
}else{
   die;
}

$mysqli->close();
?>

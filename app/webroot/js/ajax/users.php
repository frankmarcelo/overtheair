<?php

require_once '../../../config/database.php';

$dbConfig = new DATABASE_CONFIG();
$mysqli = new mysqli( $dbConfig->default['host'] , 
                      $dbConfig->default['login'] ,
                      $dbConfig->default['password'] ,
                      $dbConfig->default['database'] );
if( $mysqli->ping() ){
  
   $limit = ( intval($_GET['limit']) > 0 ) ? intval($_GET['limit']) : 20;
   $q = trim($_GET['term']);
   $sql = "	SELECT SQL_CACHE DISTINCT(username)
   			FROM users 
   			WHERE ( 
                                 (MATCH(username) AGAINST ('+".$mysqli->escape_string($q)."*' IN boolean MODE) OR 
		 	          username like '%".$mysqli->escape_string($q)."%') AND LENGTH(username) > 0 
                              ) OR 
                              (
                                 (MATCH(name) AGAINST ('+".$mysqli->escape_string($q)."*' IN boolean MODE) OR 
                                  name like '%".$mysqli->escape_string($q)."%') AND LENGTH(name) > 0
                              ) OR
                              (
                                 (MATCH(email) AGAINST ('+".$mysqli->escape_string($q)."*' IN boolean MODE) OR 
                                  email like '%".$mysqli->escape_string($q)."%') AND LENGTH(email) > 0
                              )
                        AND status = 1
   			GROUP BY TRIM(username) 
   		   	ORDER BY (MATCH(username) AGAINST ('+".$mysqli->escape_string($q)."*' IN boolean MODE)) DESC,
                                 (MATCH(name) AGAINST ('+".$mysqli->escape_string($q)."*' IN boolean MODE)) DESC,
                                 (MATCH(email) AGAINST ('+".$mysqli->escape_string($q)."*' IN boolean MODE)) DESC
   		   	LIMIT ".$limit;
   header('Cache-Control: no-cache, must-revalidate');
   header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
   header('Content-type: application/json');
   $aData = array();
   if( $aRes = $mysqli->query($sql) ){
      while( $obj = $aRes->fetch_object() ){
      	 if(strlen($obj->username) > 80 ){
       	     $obj->username = substr($obj->username,0,80);		
       	 }	
         $aData[] = ucwords(trim(strtolower($obj->username))); 
      }
      $aRes->close();
   }


   if( strlen($q) > 0 ){
      echo json_encode($aData);
   }else{
      echo json_encode(null);
   }
}else{
   die;
}

$mysqli->close();
?>

<?php
/*
 * jQuery File Upload Plugin PHP Example 5.2.7
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://creativecommons.org/licenses/MIT/
 */

error_reporting(E_ALL | E_STRICT);
date_default_timezone_set('UTC');

$zipFile = dirname(dirname(dirname(dirname(dirname(__FILE__))))).'/app/config/zip.php';
require_once $zipFile;

class UploadHandler
{
    private $options;
    
    function __construct($options=null) {
        $this->options = array(
            'script_url' => $_SERVER['PHP_SELF'],
            'upload_dir' => dirname(dirname(dirname(dirname(__FILE__)))).'/download_files/modules/',
            'rename_dir' => dirname(dirname(dirname(dirname(__FILE__)))).'/download_files/modules/',
            'upload_url' => '/over-the-air/modules/download?src=',
            'param_name' => 'files',
            // The php.ini settings upload_max_filesize and post_max_size
            // take precedence over the following max_file_size setting:
            'max_file_size' => null,
            'min_file_size' => 1,
            'accept_file_types' => '/(\.|\/)(jpeg|jpg|png|gif)$/i',
            'max_number_of_files' => null,
            'discard_aborted_uploads' => true,
            'image_versions' => array(
                'thumbnail' => array(
                    'upload_dir' => dirname(__FILE__).'/thumbnails/',
                    'upload_url' => dirname($_SERVER['PHP_SELF']).'/thumbnails/',
                    'max_width' => 80,
                    'max_height' => 80
                )
            )
        );
       
        if ($options) {
            $this->options = array_replace_recursive($this->options, $options);
        }
    }
    
    private function get_file_object($file_name) {
        $file_path = $this->options['rename_dir'].$file_name;
        if (is_file($file_path) && $file_name[0] !== '.') {
            global $aData;

            $file = new stdClass();
            $file->name = $file_name;
            $file->size = filesize($file_path);
            $file->url = $this->options['upload_url'].rawurlencode($file->name);
            $file->upload = false;
            foreach($this->options['image_versions'] as $version => $options) {
                if (is_file($options['upload_dir'].$file_name)) {
                    $file->{$version.'_url'} = $options['upload_url']
                        .rawurlencode($file->name);
                }
            }
            $file->delete_url = $this->options['script_url']
                .'?file='.rawurlencode($file->name);
            $file->delete_type = 'DELETE';

            $is_display = $this->is_display($file,$aData);
            
            if( $is_display == false ){
                return null;
            }else{
            	$file->id = $is_display->id;
            }

            return $file;
        }
        return null;
    }

    private function is_display($needle,$search=array()){
        $is_display = false;
        foreach( $search as $file_info ){
           if( strtolower(trim($file_info->raw_name)) == strtolower(trim($needle->name)) ){
              $is_display = $file_info;  
              break;
           }
        }
        return $is_display;
    }
    
    private function get_file_objects() {
        global $aData;

        $file_objects = array_values(array_filter(array_map(
            array($this, 'get_file_object'),
            scandir($this->options['rename_dir'])
        )));

        if( ( !empty($file_objects) && count($file_objects)>0 ) &&
            ( !empty($aData) && count($aData)>0 )
        ){
            foreach( $file_objects as $key => $file_object ){
               $is_display = $this->is_display($file_object,$aData);
               $extension = pathinfo($file_object->name, PATHINFO_EXTENSION);
               if( $is_display == false ){
                  unset($file_objects[$key]);
               }else{
                  $file_object->upload = false;
               	  $file_object->id = $is_display->id;
               	  $file_objects[$key] = $file_object;
               }
            }
        }else{
            $file_objects = array();

        }
        return $file_objects;
    }

    private function create_scaled_image($file_name, $options) {
        $file_path = $this->options['rename_dir'].$file_name;
        $new_file_path = $options['rename_dir'].$file_name;
        list($img_width, $img_height) = @getimagesize($file_path);
        if (!$img_width || !$img_height) {
            return false;
        }
        $scale = min(
            $options['max_width'] / $img_width,
            $options['max_height'] / $img_height
        );
        if ($scale > 1) {
            $scale = 1;
        }
        $new_width = $img_width * $scale;
        $new_height = $img_height * $scale;
        $new_img = @imagecreatetruecolor($new_width, $new_height);
        switch (strtolower(substr(strrchr($file_name, '.'), 1))) {
            case 'jpg':
            case 'jpeg':
                $src_img = @imagecreatefromjpeg($file_path);
                $write_image = 'imagejpeg';
                break;
            case 'gif':
                @imagecolortransparent($new_img, @imagecolorallocate($new_img, 0, 0, 0));
                $src_img = @imagecreatefromgif($file_path);
                $write_image = 'imagegif';
                break;
            case 'png':
                @imagecolortransparent($new_img, @imagecolorallocate($new_img, 0, 0, 0));
                @imagealphablending($new_img, false);
                @imagesavealpha($new_img, true);
                $src_img = @imagecreatefrompng($file_path);
                $write_image = 'imagepng';
                break;
            default:
                $src_img = $image_method = null;
        }
        $success = $src_img && @imagecopyresampled(
            $new_img,
            $src_img,
            0, 0, 0, 0,
            $new_width,
            $new_height,
            $img_width,
            $img_height
        ) && $write_image($new_img, $new_file_path);
        // Free up memory (imagedestroy does not delete files):
        @imagedestroy($src_img);
        @imagedestroy($new_img);
        return $success;
    }
    
    private function has_error($uploaded_file, $file, $error) {
        if ($error) {
            return $error;
        }
        if (!preg_match($this->options['accept_file_types'], $file->name)) {
            return 'acceptFileTypes';
        }
        if ($uploaded_file && is_uploaded_file($uploaded_file)) {
            $file_size = filesize($uploaded_file);
        } else {
            $file_size = $_SERVER['CONTENT_LENGTH'];
        }
        if ($this->options['max_file_size'] && (
                $file_size > $this->options['max_file_size'] ||
                $file->size > $this->options['max_file_size'])
            ) {
            return 'maxFileSize';
        }
        if ($this->options['min_file_size'] &&
            $file_size < $this->options['min_file_size']) {
            return 'minFileSize';
        }
        if (is_int($this->options['max_number_of_files']) && (
                count($this->get_file_objects()) >= $this->options['max_number_of_files'])
            ) {
            return 'maxNumberOfFiles';
        }
        return $error;
    }
    
    private function handle_file_upload($uploaded_file, $name, $size, $type, $error,$currently_logged_in=0) {
        $file = new stdClass();
        // Remove path information and dots around the filename, to prevent uploading
        // into different directories or replacing hidden system files.
        // Also remove control characters and spaces (\x00..\x20) around the filename:
        #$insertId = $this->insertRecord($file_info->file_name);
		$name = preg_replace("/[^A-Za-z0-9_\.-]+/", "_", $name);
        $file->name = trim(basename(stripslashes($name)), ".\x00..\x20");
        $orig_name = $file->name;
        $extension = pathinfo($orig_name, PATHINFO_EXTENSION);
        $new_filename = 'modules_'.strtotime("now").'.'.$extension.'';
        $file->size = intval($size);
        $file->type = $type;
        $error = $this->has_error($uploaded_file, $file, $error);
        if (!$error && $file->name) {
            $file_path = $this->options['upload_dir'].$file->name;
            $append_file = !$this->options['discard_aborted_uploads'] &&
            is_file($file_path) && $file->size > filesize($file_path);
            clearstatcache();
            if ($uploaded_file && is_uploaded_file($uploaded_file)) {
                // multipart/formdata uploads (POST method uploads)
                if ($append_file) {
                    file_put_contents(
                        $file_path,
                        fopen($uploaded_file, 'r'),
                        FILE_APPEND
                    );
                } else {
                    move_uploaded_file($uploaded_file, $file_path);
                }
            } else {
                // Non-multipart uploads (PUT method support)
                file_put_contents(
                    $file_path,
                    fopen('php://input', 'r'),
                    $append_file ? FILE_APPEND : 0
                );
            }

            $file->upload = false;

            $file_size = filesize($file_path);
            if ($file_size === $file->size) {
                 $old = umask(0);
                 chmod($file_path, 0777);
                 umask($old);
                 /**
            	  * do the archiving here
            	  */
                 //$this->archive_uploaded_file($this->options['rename_dir'].$new_filename,$new_filename);

//                 copy($file_path,$this->options['rename_dir'].$new_filename);
                 /**
                  * do the archiving here
                  */
                 $this->archive_uploaded_file($file_path,$this->options['rename_dir'].$new_filename);
		 		 $file->url = $this->options['upload_url'].rawurlencode($file->name);
                 $file->id = $this->insertRecord($new_filename,$file,$currently_logged_in);
            } else if ($this->options['discard_aborted_uploads']) {
                 unlink($file_path);
                 $file->error = 'abort';
                 $file->size = $file_size;
            }
            $file->delete_url = $this->options['script_url']
                .'?file='.rawurlencode($file->name);
            $file->delete_type = 'DELETE';
        } else {
            $file->error = $error;
        }
        return $file;
    }
    
    public function get() {
        global $aData;

        $file_name = isset($_REQUEST['file']) ?
        basename(stripslashes($_REQUEST['file'])) : null; 
        if ($file_name) {
            $info = $this->get_file_object($file_name);
        } else {
            $info = $this->get_file_objects();
        }
        header('Content-type: application/json');
        echo json_encode($info);
    }

    private function archive_uploaded_file($file_path=null,$gzip_filename=null){
        //$gzip = new gzip_file($gzip_filename.'.tgz');
        //$gzip->set_options(array('overwrite' => 1, 'level' => 1));
        //$gzip->add_files(array($file_path));
        //$gzip->create_archive();
          //system('gzip -c '.$file_path.' > '.$gzip_filename.'.gz');
          //$old = umask(0);
          //chmod($gzip_filename.'.gz', 0777);
          //umask($old);
        $this->gzcompressfile($file_path, $gzip_filename);
    }

    function gzcompressfile($source,$target_file,$level=false){
        $dest=$target_file.'.gz';
        $mode='wb'.$level;
        $error=false;
        if($fp_out=gzopen($dest,$mode)){
           if($fp_in=fopen($source,'rb')){
               while(!feof($fp_in))
                  gzwrite($fp_out,fread($fp_in,filesize($source)));
                  fclose($fp_in);
               }
               else $error=true;
               gzclose($fp_out);
        }
        else $error=true;
     
        $old = umask(0);
        chmod($dest, 0777);
        umask($old);
 
        if($error) return false;
        else return $dest;
    } 
    
    public function post() {
        global $mysqli;

        $upload = isset($_FILES[$this->options['param_name']]) ?
            $_FILES[$this->options['param_name']] : array(
                'tmp_name' => null,
                'name' => null,
                'size' => null,
                'type' => null,
                'error' => null
            );
        $info = array();
		
        if (is_array($upload['tmp_name'])) {
            foreach ($upload['tmp_name'] as $index => $value) {
                $info[] = $this->handle_file_upload(
                    $upload['tmp_name'][$index],
                    isset($_SERVER['HTTP_X_FILE_NAME']) ?
                        $_SERVER['HTTP_X_FILE_NAME'] : $upload['name'][$index],
                    isset($_SERVER['HTTP_X_FILE_SIZE']) ?
                        $_SERVER['HTTP_X_FILE_SIZE'] : $upload['size'][$index],
                    isset($_SERVER['HTTP_X_FILE_TYPE']) ?
                        $_SERVER['HTTP_X_FILE_TYPE'] : $upload['type'][$index],
                    $upload['error'][$index],
                    $_POST['currently_logged_in']
                );
            }
        } else {
            $info[] = $this->handle_file_upload(
                $upload['tmp_name'],
                isset($_SERVER['HTTP_X_FILE_NAME']) ?
                    $_SERVER['HTTP_X_FILE_NAME'] : $upload['name'],
                isset($_SERVER['HTTP_X_FILE_SIZE']) ?
                    $_SERVER['HTTP_X_FILE_SIZE'] : $upload['size'],
                isset($_SERVER['HTTP_X_FILE_TYPE']) ?
                    $_SERVER['HTTP_X_FILE_TYPE'] : $upload['type'],
                $upload['error'],
                $_POST['currently_logged_in']
            );
        }
        
        header('Vary: Accept');
        if (isset($_SERVER['HTTP_ACCEPT']) &&
            (strpos($_SERVER['HTTP_ACCEPT'], 'application/json') !== false)) {
            header('Content-type: application/json');
        } else {
            header('Content-type: text/plain');
        }
        echo json_encode($info);
    }
    
    public function delete() {
        global $mysqli;

        $file_name = isset($_REQUEST['file']) ?
        basename(stripslashes($_REQUEST['file'])) : null;
        $file_path = $this->options['rename_dir'].$file_name;

        $success = null;
        $sql = "SELECT * FROM modules_images WHERE raw_name ='".$file_name."' ";
        if( $oRes = $mysqli->query($sql) ){
            if( $oRes->num_rows> 0 ){
                while( $aRows = $oRes->fetch_object() ){
                        if( $aRows->id > 0 ){
                                        $sql =" DELETE FROM modules_images WHERE id='".$aRows->id."'; ";
                                        if( $mysqli->query($sql) ){
                                        	unlink( $this->options['rename_dir'].$aRows->background_image);
                                        $success = is_file($file_path) && $file_name[0] !== '.' && unlink($file_path);
                                                if ($success) {
                                                        foreach($this->options['image_versions'] as $version => $options) {
                                                                $file = $options['upload_dir'].$file_name;
                                                        if (is_file($file)) {
                                                                unlink($file);
                                                        }
                                                        }
                                                }
                                        }
                        }
                }


                }
        }
        header('Content-type: application/json');
        echo json_encode($success);
    }

    private function getLastInsert(){
        global $mysqli;
        $sql = "SELECT MAX(id) as lastInsert FROM modules_images LIMIT 1";
        if( $oRes = $mysqli->query($sql) ){
            $aRows = $oRes->fetch_assoc();
            return intval($aRows['lastInsert'])+1;
        }
        return 1;
    }
    
    private function insertRecord($new_filename,$file,$currently_logged_in=0){
        global $mysqli,$aData;

		if( !is_null($new_filename) 	&& 
			strlen($new_filename)> 0 	&& 
			is_object($file) 			
		){
					$sql = "INSERT INTO modules_images SET raw_name='".$mysqli->escape_string($file->name)."',".
							" raw_type='".$mysqli->escape_string($file->type)."', ".
							" raw_size='".$mysqli->escape_string($file->size)."', ".
							" date_created = NOW(), ".
							" date_modified=NOW(), ".
							" who_modified='".intval($currently_logged_in)."', ".
							" who_created='".intval($currently_logged_in)."', ".
							" background_image='".$mysqli->escape_string($new_filename).".gz' ";
				if(	$mysqli->query($sql) ){
				return  $mysqli->insert_id;
                                }
			}
        return null;
    }
}


require_once '../../../config/database.php';

header('Pragma: no-cache');
header('Cache-Control: private, no-cache');
header('Content-Disposition: inline; filename="files.json"');
header('X-Content-Type-Options: nosniff');

$dbConfig = new DATABASE_CONFIG();
$mysqli = new mysqli( $dbConfig->default['host'] ,
                      $dbConfig->default['login'] ,
                      $dbConfig->default['password'] ,
                      $dbConfig->default['database'] );
if( $mysqli->ping() ){

        $sql = " SELECT * FROM modules_images";
        $aData = array();
        if( $aRes = $mysqli->query($sql) ){
            while( $obj = $aRes->fetch_object() ){
               $aData[] = $obj;
            }
            $aRes->close();
        }                     

        $upload_handler = new UploadHandler();
		switch ($_SERVER['REQUEST_METHOD']) {
            case 'HEAD':
            case 'GET':
               $upload_handler->get();
            break;
            case 'POST':
            $upload_handler->post();
            break;
            case 'DELETE':
            $upload_handler->delete();
            break;
            case 'OPTIONS':
            break;
            default:
            header('HTTP/1.0 405 Method Not Allowed');
        }
}
$mysqli->close();
?>

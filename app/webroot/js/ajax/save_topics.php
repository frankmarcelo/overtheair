<?php

require_once '../../../config/database.php';

$dbConfig = new DATABASE_CONFIG();
$mysqli = new mysqli( $dbConfig->default['host'] , 
                      $dbConfig->default['login'] ,
                      $dbConfig->default['password'] ,
                      $dbConfig->default['database'] );
if( $mysqli->ping() ){
  
   $appId = ( intval($_POST['topic_id']) > 0 ) ? intval($_POST['topic_id']) : 0;
   if( isset($_POST['key_article'])){
   $isKey =  ( intval($_POST['key_article']) > 0 ) ? 1: 0;
   }
   $name =  trim($_POST['name']);
   $description = trim($_POST['description']); 
   $downloadSourceId = ( intval($_POST['document_source']) > 0 ) ? intval($_POST['document_source']) : 0;
   $tags = ( strlen(trim($_POST['tags'])) > 0 ) ? trim($_POST['tags']) : null;
   $author = ( strlen(trim($_POST['author'])) > 0 ) ? trim($_POST['author']) : null;

   header('Cache-Control: no-cache, must-revalidate');
   header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
   header('Content-type: application/json');
   $sql = "SELECT * FROM topics_files WHERE id='".$mysqli->escape_string($appId)."'";

   $aData = false;
   if( $aRes = $mysqli->query($sql) ){
       if( $aRes->num_rows > 0 ){
          if(isset($_POST['key_article'])){
          $sql =" UPDATE topics_files SET 
              description='".$mysqli->escape_string($description)."',
              tags ='".$mysqli->escape_string($tags)."',
              author = '".$mysqli->escape_string($author)."',
              topic_files_sources_id='".intval($downloadSourceId)."',
              isKeyArticle='".intval($isKey)."',
              name='".$mysqli->escape_string($name)."'
           WHERE id='".$mysqli->escape_string($appId)."'";
         }else{
           $sql =" UPDATE topics_files SET 
              description='".$mysqli->escape_string($description)."',
              tags ='".$mysqli->escape_string($tags)."',
              author = '".$mysqli->escape_string($author)."',
              topic_files_sources_id='".intval($downloadSourceId)."',
              name='".$mysqli->escape_string($name)."'
           WHERE id='".$mysqli->escape_string($appId)."'";
         }
          $mysqli->query($sql);
          $aData = true;
       }
       $aRes->close();
   }
   echo json_encode($aData);
}else{
   die;
}

$mysqli->close();
?>

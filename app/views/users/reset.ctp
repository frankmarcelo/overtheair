<style type="text/css">
div.error, .authMessage, #authMessage {
    background-color: #FFEBE8;
    border-color: #CC0000;
}
#authMessage,.authMessage, .message {
    border-radius: 3px 3px 3px 3px;
    border-style: solid;
    border-width: 1px;
    margin: 0 0 16px 8px;
    padding: 12px;
}
</style>
<div id="login">
<div class="users form">
<?php echo $this->Form->create('User', array('url' => array('controller' => 'users', 'action' => 'reset', $username, $key)));?>
<?php
echo $this->Form->input('password', array('tabindex'=>1,'label' => __('New password', true),'class'=>'input','div'=>false));
?><br />
<?php echo $this->Form->end(array('tabindex'=>2,'type'=>'submit','label'=>__('Submit', true),'class' => 'button-primary','name' => 'Submit'     ));?>
</div>
<?php #echo $this->element('sql_dump'); ?>
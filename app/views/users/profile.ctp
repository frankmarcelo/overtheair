<style type="text/css">
#UserProfileForm .form-field input {
    width: 25em;
}

textarea, input[type="text"], input[type="password"], input[type="file"], input[type="button"], input[type="submit"], input[type="reset"], select {
    background-color: #FFFFFF;
    border-color: #DFDFDF;
}
</style>
<?php echo $this->Html->css(array('jquery.autocomplete'));?>
<script type="text/javascript">
/* <![CDATA[ */
             
var downloadTypes = '<?php echo json_encode($downloadTypes);?>';       
var userRoleId = parseInt(<?php echo Configure::read('userRoleId');?>,10);
var showNotice, adminMenu, columns, validateForm, screenMeta;
             
var commonL10n = {
	warnDelete: "You are about to permanently delete the selected items.\n  \'Cancel\' to stop, \'OK\' to delete."
};
try{convertEntities(commonL10n);}catch(e){};
var wpAjax = {
	noPerm: "You do not have permission to do that.",
	broken: "An unidentified error has occurred."
};
try{convertEntities(wpAjax);}catch(e){};
var pwsL10n = {
	empty: "Strength indicator",
	short: "Very weak",
	bad: "Weak",
	good: "Medium",
	strong: "Strong",
	mismatch: "Mismatch"
};
try{convertEntities(pwsL10n);}catch(e){};


validateForm = function( form ) {
    return !$( form ).find('.form-required').filter( function() { 
        return $('input:visible', this).val() == ''; 
    }).addClass( 'form-invalid' ).find('input:visible').change( function() { 
        $(this).closest('.form-invalid').removeClass( 'form-invalid' ); } 
    ).size();
}
             
function convertEntities(o) {
    var c, v;
    c = function(s) {
            if (/&[^;]+;/.test(s)) {
                    var e = document.createElement("div");
                    e.innerHTML = s;
                    return !e.firstChild ? s : e.firstChild.nodeValue;
            }
            return s;
    }

    if ( typeof o === 'string' ) {
            return c(o);
    } else if ( typeof o === 'object' ) {
            for (v in o) {
                    if ( typeof o[v] === 'string' ) {
                            o[v] = c(o[v]);
                    }
            }
    }
    return o;
}

function passwordStrength(password1, username, password2) {
	var shortPass 	= 1, 
		badPass   	= 2, 
		goodPass  	= 3, 
		strongPass 	= 4, 
		mismatch 	= 5, 
		symbolSize 	= 0, 
		natLog, 
		score;

	// password 1 != password 2
	if ( (password1 != password2) && password2.length > 0)
		return mismatch

	//password < 4
	if ( password1.length < 4 )
		return shortPass

	//password1 == username
	if ( password1.toLowerCase() == username.toLowerCase() )
		return badPass;

	if ( password1.match(/[0-9]/) )
		symbolSize +=10;
	if ( password1.match(/[a-z]/) )
		symbolSize +=26;
	if ( password1.match(/[A-Z]/) )
		symbolSize +=26;
	if ( password1.match(/[^a-zA-Z0-9]/) )
		symbolSize +=31;

	natLog = Math.log( Math.pow(symbolSize, password1.length) );
	score = natLog / Math.LN2;

	if (score < 40 )
		return badPass

	if (score < 56 )
		return goodPass

    return strongPass;
}

function check_pass_strength() {
	var pass1 = $('#pass1').val(), user = $('#user_login').val(), pass2 = $('#pass2').val(), strength;
	$('#pass-strength-result').removeClass('short bad good strong');
	if ( ! pass1 ) {
		$('#pass-strength-result').html( pwsL10n.empty );
		return;
	}

	strength = passwordStrength(pass1, user, pass2);
	switch ( strength ) {
		case 2:
		$('#pass-strength-result').addClass('bad').html( pwsL10n['bad'] );
		break;
		case 3:
		$('#pass-strength-result').addClass('good').html( pwsL10n['good'] );
		break;
		case 4:
		$('#pass-strength-result').addClass('strong').html( pwsL10n['strong'] );
		break;
		case 5:
		$('#pass-strength-result').addClass('short').html( pwsL10n['mismatch'] );
		break;
		default:
			$('#pass-strength-result').addClass('short').html( pwsL10n['short'] );
	}
}

$(document).ready(function(){

	$('form').live('submit',function(){
		var $pass1 = $.trim($("#pass1").val());
		var $pass2 = $.trim($("#pass2").val());
		if( $pass1 != $pass2 ){
			$("#password_holder").removeClass().addClass("form-field form-required form-invalid");
			return false;
		}
		if( validateForm($(this)) ){
			return true;
		}else{
			return false;
		}	
	});

	<?php
	if( !empty($macos) && sizeof($macos) ){
	?>
				$('#role').live('change',function(){
					if( parseInt($(this).val(),10)== userRoleId ){
						if( $("#downloadTypeHolder") ){
							$("#downloadTypeHolder").fadeIn("slow",function(){
								$(this).show();
							});	
						}
					}else{
						if( $("#downloadTypeHolder") ){
							$("#downloadTypeHolder").fadeOut("slow",function(){
								$(this).hide();
							});
						}
					}
				});
	<?php
	}
	?>
	
	$('#pass1').val('').keyup( check_pass_strength );
	$('#pass2').val('').keyup( check_pass_strength );
	$('#pass-strength-result').show();
	$('#name').blur(function(){
		var inputs,value;
		var select = $('#name'), current = select.find('option:selected').attr('id'), dub = [],
		inputs = {
			display_username : $('#user_login').val(),
			display_name : $('#name').val()
		};

		if ( inputs.display_name ) {
			inputs['display_name'] = inputs.display_name;
		}

		$('option', select).remove();
		$.each(inputs, function( id, value ) {
			var val = value.replace(/<\/?[a-z][^>]*>/gi, '');

			if ( inputs[id].length && $.inArray( val, dub ) == -1 ) {
				dub.push(val);
				$('<option />', {
					'id': id,
					'text': val,
					'selected': (id == current)
				}).appendTo( select );
			}
		});
	});
});
/* ]]> */
</script>
<div id="wpwrap">
	<div id="adminmenuback"></div>
	<div id="adminmenuwrap">
		<div id="adminmenushadow"></div>
		<?php echo $this->element('menu');?>
	</div>
	<div id="wpcontent">
		<!--header menu -->
		<?php echo $this->element('user_navigation'); ?>
		<!-- end of header menu -->
		<div id="wpbody">
			<div id="wpbody-content">
				<div class="wrap">
					<div class="icon32" id="icon-users"><br></div>
					<h2 id="add-new-user"> Edit Your Profile</strong></h2>
					<?php
					if( ( strstr($this->Session->read('Message.flash.message'),'exists') &&
						  strstr($this->Session->read('Message.flash.message'),'already') ) ||
						( strstr($this->Session->read('Message.flash.message'),'required') )
					){
					?>
					<div class="error below-h2">
						<p><strong>ERROR</strong>: <?php echo $this->Session->read('Message.flash.message');?></p>
					</div>
					<?php
						$this->Session->delete('Message.flash');
					}
					?>
					<p>&nbsp;</p>
					<?php echo str_replace("/over-the-air/users/edit","/over-the-air/users/edit/".$user['User']['username']."/".$user['User']['token'],$this->Form->create('User'));?>
					<table class="form-table">
						<tbody>
							<tr class="form-field form-required">
								<th scope="row"><label for="user_login">Username <span class="description">(required)</span></label></th>
								<td><input type="text" id="user_login" value="<?php echo trim($user['User']['username']);?>" name="data[User][username]" autocomplete="off" /></td>
							</tr>
							<tr class="form-field form-required">
								<th scope="row"><label for="name">Name <span class="description">(required)</span></label></th>
								<td><input type="text" id="name" value="<?php echo trim($user['User']['name']);?>" name="data[User][name]" autocomplete="off" /></td>
							</tr>
							<tr class="form-field form-required">
								<th scope="row"><label for="email">E-mail <span class="description">(required)</span></label></th>
								<td><input type="text" id="email" value="<?php echo trim($user['User']['email']);?>" name="data[User][email]" autocomplete="off" /></td>
							</tr>
							
							<tr class="form-field form-required" id="password_holder">
								<th scope="row"><label for="pass1">Password <span class="description">(twice, required)</span></label></th>
								<td><input type="password" autocomplete="off" id="pass1" name="data[User][password]" />
								<br>
								<input type="password" autocomplete="off" id="pass2" name="data[User][password2]" />
								<br>
								<div id="pass-strength-result" style="display: block;">Strength indicator</div>
								<p class="description indicator-hint">Hint: The password should be at least seven characters long. To make it stronger, use upper and lower case letters, numbers and symbols like ! " ? $ % ^ &amp; ).</p>
								</td>
							</tr>
							<?php
							if( intval($user['User']['id']) != Configure::read('adminRoleId') &&  
								intval($user['User']['role_id']) == Configure::read('adminRoleId')
							){
							?>
							<tr class="form-field">
								<th scope="row"><label for="role">Role</label></th>
								<td>
								<select id="role" name="data[User][role_id]">
									<?php 
									foreach( $roles as $role ){
										$selected = ( intval($role['Role']['id']) == intval($user['User']['role_id']) ) ? ' selected="selected"':'';
										
									?>
									<option value="<?php echo $role['Role']['id'];?>"<?php echo $selected;?>><?php echo $role['Role']['title'];?></option>
									<?php
									}
									?>
								</select>
							 
								</td>
							</tr>
							<?php
							}

							if( intval($user['User']['id']) != Configure::read('adminRoleId') ){
							$style = ( intval($user['User']['role_id']) != intval(Configure::read('userRoleId')) )? ' style="display:none;"':'';
							if( !empty($macos) && sizeof($macos) > 0 ){ 
							?>
							<tr class="form-field" id="downloadTypeHolder" <?php echo $style;?>>
								<th scope="row"><label for="role">Download App</label></th>
								<td>
								<select id="download_type_id" name="data[User][download_type_id]"> 
									<?php 
									foreach( $downloadTypes as $downloadType ){
										foreach( $macos as $mac_app ){
											if( intval($mac_app['Macosx']['download_type_id']) == intval($downloadType['DownloadType']['id']) ){
											$selected= ( intval($user['User']['download_type_id']) == intval($mac_app['Macosx']['download_type_id']) ) ? ' selected="selected"':'';
									?>
									<option value="<?php echo $downloadType['DownloadType']['id'];?>"<?php echo $selected;?>><?php echo $downloadType['DownloadType']['name'];?></option>
									<?php
											}
										}
									}
									?>
								</select>   
								</td>
							</tr>
							<?php
							}else{
							?>					
							<input type="hidden" id="download_type_id" name="data[User][download_type_id]" value="0" />
							<?php
							}
							}
							?>
							<tr class="form-field">
								<th scope="row">&nbsp;</th>
								<td>&nbsp;</td>
							</tr>
						</tbody>
					</table>
					<?php
					$options = array(
						'name' => 'createuser',
						'id'   => 'createuser',
						'value'=> 'Add New User',
						'class'=> 'button-primary',
						
					);
					echo $this->Form->end($options);
					?>
				</div>
				<div class="clear"></div>
			</div><!-- end wpbody-content -->
		</div><!-- end of wpbody -->
	</div>
	<div class="clear"></div>
</div>

<style type="text/css">
.wrap div.updated, .wrap div.error {
    margin: 5px 0 15px;
}
div.updated, .login .message {
    background-color: #FFFFE0;
    border-color: #E6DB55;
}
div.updated, div.error {
    border-radius: 3px 3px 3px 3px;
    border-style: solid;
    border-width: 1px;
    margin: 5px 15px 2px;
    padding: 0 0.6em;
}
</style>
<div id="wpwrap">
	<div id="adminmenuback"></div>
	<div id="adminmenuwrap">
		<div id="adminmenushadow"></div>
		<?php echo $this->element('menu'); ?>
	</div>
	<div id="wpcontent">
		<!--header menu -->
		<?php echo $this->element('user_navigation'); ?>
		<!-- end of header menu -->
		<div id="wpbody">
			<div id="wpbody-content">
				<div class="wrap">
					<div class="icon32" id="icon-tools"><br></div>
					<h2>Mac Os App<a class="add-new-h2" href="<?php echo $this->Html->url(array("controller" => "macosx","action"=>"upload"));?>">Upload New</a></h2>
					<?php
					if( strstr($this->Session->read('Message.flash.message'),'allowed') ){
					?>	
					<div class="error below-h2"><p><strong>ERROR</strong>: <?php echo $this->Session->read('Message.flash.message');?></p></div>
					<?php
						$this->Session->delete('Message.flash');
					}
					?>
					<div id="dashboard-widgets-wrap">
						<div class="metabox-holder" id="dashboard-widgets">
							<div style="width:49%;" class="postbox-container">
								<div class="meta-box-sortables ui-sortable" id="normal-sortables">
									
									<div class="postbox " id="dashboard_recent_comments">
										<div title="Click to toggle" class="handlediv"><br></div>
										<h3 class="hndle"><span>Software Upload History</span></h3>
										<div class="inside">
											<div class="table table_content">
												<p class="sub">Content</p>
												<table>
													<tbody>
														<tr class="first"><td class="first b b-posts"><a href="edit.php">1</a></td><td class="t posts"><a href="edit.php">Post</a></td></tr><tr><td class="first b b_pages"><a href="edit.php?post_type=page">1</a></td><td class="t pages"><a href="edit.php?post_type=page">Page</a></td></tr>
														<tr><td class="first b b-cats"><a href="edit-tags.php?taxonomy=category">1</a></td><td class="t cats"><a href="edit-tags.php?taxonomy=category">Category</a></td></tr><tr><td class="first b b-tags"><a href="edit-tags.php">0</a></td><td class="t tags"><a href="edit-tags.php">Tags</a></td></tr>
													</tbody>
												</table>
												<br />
												<div class="clear"></div>
											</div>
										</div>
									</div>
								</div>	
							</div>
							<div class="clear"></div>
						</div>
					</div><!-- dashboard-widgets-wrap -->
				</div><!-- end of wrap -->
			</div>
		</div>
	</div>
</div>
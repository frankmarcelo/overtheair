<style type="text/css">
.wrap div.updated, .wrap div.error {
    margin: 5px 0 15px;
}
div.updated, .login .message {
    background-color: #FFFFE0;
    border-color: #E6DB55;
}
div.updated, div.error {
    border-radius: 3px 3px 3px 3px;
    border-style: solid;
    border-width: 1px;
    margin: 5px 15px 2px;
    padding: 0 0.6em;
}

.form-invalid textarea, .form-invalid select {
    border-color: #CC0000 !important;
}

.form-table_step4 {
    border-collapse: collapse;
    clear: both;
    margin-bottom: -8px;
    margin-top: 0.5em;
    width: 100%;
}
</style>
<script type="text/javascript">
/* <![CDATA[ */
$(document).ready(function(){
	$('.publish').live('click',function(){
		var attribute_id = $.trim($(this).attr('id'));
		var attributes = attribute_id.split('_');
		
		if( attributes[0] == 'publish' ){
			$.post("<?php echo $this->Html->url(array("controller" => "modules","action"=>"publish"));?>"+'/'+attributes[1],
				    function(data) {
				    		var dialog_message ='<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>';
				    		dialog_message +='Successfuly published the module</p>';
				    		$("#confirm_msg").empty().html(dialog_message);
				    		$('#dialog').dialog({modal:true,hide:'explode'});
				    		this.timer = setTimeout(function (){
				    			$("#dialog").dialog('close');
				    			window.location.reload();
				    		},2000);
					}
			    );
			
		}else if( attributes[0] =='unpublish' ){
			$.post("/over-the-air/js/ajax/publish.php", { module_id: attributes[1], change_type_id: 0 },
				    function(data) {
				    	if(data==true){
				    		var dialog_message ='<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>';
				    		dialog_message +='Successfuly unpublished the module</p>';
				    		$("#confirm_msg").empty().html(dialog_message);
				    		$('#dialog').dialog({modal:true,hide:'explode'});
				    		this.timer = setTimeout(function (){
				    			$("#dialog").dialog('close');
				    			window.location.reload();
				    		},2000);
				        }
					}
			 );
		}	
	});

	$('.delete').live('click',function(){
		var attribute_id = $(this).attr('id');
		var attributes = attribute_id.split('_');
		$.post("/over-the-air/js/ajax/publish.php", { module_id: attributes[1], change_type_id: 2 },
			    function(data) {
			    	if(data==true){
			    		var dialog_message ='<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>';
			    		dialog_message +='You have successfuly deleted the module</p>';
			    		$("#confirm_msg").empty().html(dialog_message);
			    		$('#dialog').dialog({modal:true,hide:'explode'});
			    		this.timer = setTimeout(function (){
			    			$("#dialog").dialog('close');
			    			window.location.reload();
			    		},2000);
			        }
				}
		 );	
	});
	
	$(window).resize(function() {
		$("#dialog").dialog("option", "position", "center");
	});
		
	$(window).scroll(function() {
	    $("#dialog").dialog("option", "position", "center");
	});	
});
/* ]]> */
</script>
<div id="wpwrap">
	<div id="adminmenuback"></div>
	<div id="adminmenuwrap">
		<div id="adminmenushadow"></div>
		<?php echo $this->element('menu');?>
	</div>
	<div id="wpcontent">
		<!--header menu -->
		<?php echo $this->element('user_navigation'); ?>
		<!-- end of header menu -->
		<div id="wpbody">
			<div id="wpbody-content">
				<div class="wrap">
					<?php
					$this->Modules->loadDataById( $module );
					?>
					<div class="icon32 icon32-posts-post" id="icon-edit"><br></div>
					<h2 id="add-new-user"> View Module for <strong>"<?php echo $module['Module']['name'];?>"</strong></h2><br/>
					<?php
					if( strstr($this->Session->read('Message.flash.message'),'success') ){
					?>
					<div class="updated" id="message"><p><?php echo $this->Session->read('Message.flash.message');?></p></div>
					<?php
						$this->Session->delete('Message.flash');
					}
					?>
					<div id="dashboard_right_now" class="postbox ">
						<h3 class="hndle"><br />&nbsp;<span>Module Overview for <?php echo trim(html_entity_decode($module['Module']['name']));?>&nbsp;&nbsp;&nbsp;&nbsp;Status:&nbsp;<?php echo $this->Modules->getDisplayStatus();?>&nbsp;&nbsp;&nbsp;<strong><?php echo ($total_live_modules< 4)? $this->Modules->getContentStatus():'';?></strong>&nbsp;&nbsp;&nbsp;<strong><a href="<?php echo $this->Html->url(array("controller" => "modules","action"=>"edit",$module['Module']['id']));?>">Edit</a></strong><br /></span><br /></h3>
						<div class="inside">
							<div class="table table_content">
								<?php
								?>
								<table>
									<tbody>
										
										<tr><td><strong>Module Name</strong>:&nbsp;<?php echo trim(html_entity_decode($module['Module']['name']));?></td></tr>
										<tr><td>&nbsp;</td></tr>
										<tr><td><strong>Module Type</strong>:&nbsp;<?php echo trim(html_entity_decode($module['ModuleType']['name']));?></td></tr>
										<tr><td>&nbsp;</td></tr>
										<tr><td><strong>Module Short Description</strong>:&nbsp;<?php echo $this->Text->truncate(html_entity_decode($module['Module']['short_description']),30000);?></td></tr>
										<tr><td>&nbsp;</td></tr>
										<tr><td><strong>Module Long Description</strong>:&nbsp;<?php echo $this->Text->truncate(html_entity_decode($module['Module']['long_description']),30000);?></td></tr>
										<tr><td>&nbsp;</td></tr>
										<tr><td><strong>Module Background Image</strong>:&nbsp;<img src="<?php echo $this->Modules->getImageSource();?>" /></td></tr>
									</tbody>
								</table>
							</div>
						</div>
						<br/>
						<h3 class="hndle"><br />&nbsp;<span>Topics Overview for <?php echo trim($module['Module']['name']);?><br /></span><br/></h3>
						<div class="inside">
							<div class="table table_content">
								<table>
									<tbody>
								<?php
								
								if( isset($module['Topic']) && count($module['Topic'])){
								 	
									foreach( $module['Topic'] as $topic ){
										 $aTopicFiles =  $this->TopicFiles->loadTopicDataById($topic['id']);
								?>	
									<tr><td><strong>Topic Name <?php echo intval($module['ModuleType']['id']).'.'.intval($topic['display_sequence']);?>:&nbsp;<?php echo trim($topic['name']);?></strong></td></tr>
									<tr><td>&nbsp;</td></tr>
									<?php if(strlen($topic['description'])>0){?><tr><td>Topic Description:&nbsp;<?php echo trim($topic['description']);?></td></tr><?php }?>
									<tr><td>
									<?php	
									$aTopicFiles =  $this->TopicFiles->loadTopicDataById($topic['id']);
          							if( !empty($aTopicFiles) ){
										echo '<ul>';
										foreach( $aTopicFiles as $documents => $document ){   
 									?>
               						<li><strong><?php echo intval($module['ModuleType']['id']).'.'.intval($topic['display_sequence']).'.'.intval($document['TopicFiles']['display_sequence']);?></strong>
               						 &nbsp;<?php echo ucfirst(Sanitize::html(trim($document['TopicFiles']['raw_name'])));?>	
               						 <strong>Source</strong>:&nbsp;<?php echo ucfirst(Sanitize::html(trim($document['TopicFilesSources']['name'])));?> 
               						 <strong>Key</strong>:&nbsp;<?php echo (intval($document['TopicFiles']['isKeyArticle'])>0) ? 'Yes':'No';?>
               						 <strong>Description</strong>:&nbsp;<?php echo Sanitize::html(trim($document['TopicFiles']['description']));?>
               						 <strong>Author</strong>:&nbsp;<?php echo $document['TopicFiles']['author'];?>
               						 <strong>Tags</strong>:&nbsp;<?php echo Sanitize::html(trim($document['TopicFiles']['tags']));?></li>
               						 <?php
			        					}//enf of foreach
										echo '</ul>';
             						}
									?>
									</td></tr>
									<tr><td>&nbsp;</td></tr>
								<?php
									}
								}
								?></tbody>
								</table>
								
							</div>
						</div>
						<h3 class="hndle"><br />&nbsp;<span>Assessments Overview for <?php echo trim($module['Module']['name']);?><br /></span><br/></h3>
						<div class="inside">
							<div class="table table_content">
								<table>
									<tbody>
								<?php
								
								if( isset($module['Assessment']) && count($module['Assessment'])){
									foreach( $module['Assessment'] as $assessment ){
								?>	
									<tr><td><strong>Question <?php echo intval($module['ModuleType']['id']).'.'.intval($assessment['display_sequence']);?></strong>:&nbsp;<?php echo Sanitize::html(trim($assessment['question']));?></td></tr>
									<tr><td>&nbsp;</td></tr>
									<tr><td>
									<?php
									$aAnswer  = $this->AssessmentsAnswer->loadDataById($assessment['id']);	
									$aChoices =  $this->AssessmentsChoices->loadDataById($assessment['id']);
									$sAnswer = null; 
									if( !empty($aChoices) ){
										echo '<ul>';
             							foreach( $aChoices as $options ){
											$checked = ( $options['AssessmentsChoice']['id'] == $aAnswer[0]['AssessmentsAnswer']['assessments_choice_id'] && $aAnswer[0]['AssessmentsAnswer']['assessment_id'] == $assessment['id'] ) ?Sanitize::html(trim($options['AssessmentsChoice']['text'])):'';	   
               						?>
               						<li><strong>Choice <?php echo intval($options['AssessmentsChoice']['display_sequence']);?></strong>:&nbsp;<?php echo Sanitize::html(trim($options['AssessmentsChoice']['text']));?></li>
               						 <?php
			        						
             							}//enf of foreach
										echo '</ul>';

										foreach( $aChoices as $options ){
										   	if( $options['AssessmentsChoice']['id'] == $aAnswer[0]['AssessmentsAnswer']['assessments_choice_id'] && $aAnswer[0]['AssessmentsAnswer']['assessment_id'] == $assessment['id'] ){
												$sAnswer = Sanitize::html(trim($options['AssessmentsChoice']['text']));
												break;
											}
										}
             						}

									
									?>
									</td></tr>
									<?php
									
									if(isset($aAnswer) && count($aAnswer)>0 ){
									?>
									<tr><td>Answer:&nbsp;<strong><?php echo Sanitize::html(trim($sAnswer));?></strong></td></tr>
									<?php
 									}
									?>
									<tr><td>&nbsp;</td></tr>
									<?php
									$aReferences =  $this->AssessmentsReferences->loadDataById($assessment['id']);
               						if( !empty($aReferences) ){
										foreach( $aReferences as $reference ){
											foreach( $topicFiles as $topicFile ){
										 		if($reference['AssessmentsReference']['topics_file_id']==$topicFile['TopicFiles']['id']){
               						?>
                					<tr><td>Reference Material:&nbsp;<strong><?php echo trim($topicFile['TopicFiles']['name']);?></strong></td></tr>
                 				<?php
                 									}
												}
                 							}
								
								
										}
									}
								}
								?></tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="clear"></div>
			</div><!-- end wpbody-content -->
		</div><!-- end of wpbody -->
	</div>
	<div class="clear"></div>
</div>
<div id="dialog" title="Confirmation" style="display:none;"><div id="confirm_msg">&nbsp;</div></div>

<style type="text/css">
.wrap div.updated, .wrap div.error {
    margin: 5px 0 15px;
}
div.updated, .login .message {
    background-color: #FFFFE0;
    border-color: #E6DB55;
}
div.updated, div.error {
    border-radius: 3px 3px 3px 3px;
    border-style: solid;
    border-width: 1px;
    margin: 5px 15px 2px;
    padding: 0 0.6em;
}
</style>
<?php 
echo $this->Html->css(array('wizard','colorpicker','jquery.fileupload-ui'));
echo $this->Html->script(array(
		'jquery_plugins/jquery.smartwizard',
		'jquery_plugins/jquery.autosave',
		'jquery_plugins/jquery.colorpicker',
		'tiny_mce/jquery.tinymce',
		'jquery_plugins/jquery.tmpl.min',
        'jquery_plugins/jquery.iframe-transport',
        'jquery_plugins/jquery.fileupload',
        'jquery_plugins/jquery.fileupload-ui',
	)
);
?>
<script type="text/javascript">
/* <![CDATA[ */
validateForm = function( form ) {
    return !$( form ).find('.form-required').filter( function() { 
        return $('input:visible', this).val() == ''; 
    }).addClass( 'form-invalid' ).find('input:visible').change( function() { 
        $(this).closest('.form-invalid').removeClass( 'form-invalid' ); } 
    ).size();
}
             
function convertEntities(o) {
    var c, v;
    c = function(s) {
            if (/&[^;]+;/.test(s)) {
                    var e = document.createElement("div");
                    e.innerHTML = s;
                    return !e.firstChild ? s : e.firstChild.nodeValue;
            }
            return s;
    }

    if ( typeof o === 'string' ) {
            return c(o);
    } else if ( typeof o === 'object' ) {
            for (v in o) {
                    if ( typeof o[v] === 'string' ) {
                            o[v] = c(o[v]);
                    }
            }
    }
    return o;
}             
             
$(document).ready(function(){
	$('textarea.tinymce').tinymce({
		script_url : '/over-the-air/js/tiny_mce/tiny_mce.js',
        theme : "advanced",
        theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,fontselect,fontsizeselect",
        theme_advanced_buttons2 : "",                                
        theme_advanced_buttons3 : "",                        
        theme_advanced_buttons4 : "",
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_statusbar_location : "bottom",
        theme_advanced_resizing : true
   });

	
	$('#wizard').smartWizard(
		/*selected: 0,  // Selected Step, 0 = first step  
		keyNavigation: true, // Enable/Disable key navigation(left and right keys are used if enabled)
		enableAllSteps: false,  // Enable/Disable all steps on first load
		transitionEffect: 'fade', // Effect on navigation, none/fade/slide/slideleft
		contentCache:true, // cache step contents, if false content is fetched always from ajax url
		enableFinishButton: false, // makes finish button enabled always
		errorSteps:[],    // array of step numbers to highlighting as error steps
		labelNext:'Next', // label for Next button
		labelPrevious:'Previous', // label for Previous button
		labelFinish:'Save Finish',  // label for Finish button       
		onLeaveStep: null, // triggers when leaving a step
		onShowStep: null,  // triggers when showing a step
		onFinish: null  // triggers when Finish button is clicked*/
	);
	$('form *').autosave();

	$('#theme_colour').ColorPicker({
		onSubmit: function(hsb, hex, rgb, el) {
			$(el).val(hex);
			$(el).ColorPickerHide();
		},
		onBeforeShow: function () {
			$(this).ColorPickerSetColor(this.value);
		},
		onHide:function(){
			$(this).ColorPickerSetColor(this.value);	
		}
	}).bind('keyup', function(){
		$(this).ColorPickerSetColor(this.value);
	});

	$('form').live('submit',function(){
		if( validateForm($(this)) ){
			return true;
		}else{
			return false;
		}	
	});
});
/* ]]> */
</script>
<div id="wpwrap">
	<div id="adminmenuback"></div>
	<div id="adminmenuwrap">
		<div id="adminmenushadow"></div>
		<?php echo $this->element('menu'); ?>
	</div>
	<div id="wpcontent">
		<!--header menu -->
		<?php echo $this->element('user_navigation'); ?>
		<!-- end of header menu -->
		<div id="wpbody">
			<div id="wpbody-content">
				<div class="wrap">
					<div class="icon32 icon32-posts-post" id="icon-edit"><br></div>
					<h2>Add New</h2>
					<br />
					<div>
						<div id="side-info-column">
							<div id="post-body">
								<div >
									<div>
										<div id="wizard" class="swMain">
					  						<ul>
					  							<li>
						  							<a href="#step-1"><label class="stepNumber">1</label>
						                			<span class="stepDesc">Step 1<br /><small>Step 1 Module</small></span>
						            				</a>
					            				</li>
					  							<li>
					  								<a href="#step-2"><label class="stepNumber">2</label><span class="stepDesc">Step 2<br />
					                   				<small>Step 2 description</small></span></a>
					                   			</li>
					  							<li>
					  								<a href="#step-3"><label class="stepNumber">3</label><span class="stepDesc">Step 3<br />
					                   				<small>Step 3 description</small></span></a>
					                   			</li>
								  				<li><a href="#step-4"><label class="stepNumber">4</label><span class="stepDesc">
								                   Step 4<br />
								                   <small>Step 4 description</small>
								                </span>                   
								            	</a></li>
					  						</ul>
					  						<div id="step-1">	
					  							<form name="step1" id="step1"> 
					            					<table class="form-table">
														<tr class="form-field form-required">
															<th scope="row"><label for="module_name">Module Name<span class="description">(required)</span></label></th>
															<td><input type="text" autocomplete="off" name="data[Module][name]" id="module_name" /></td>
														</tr>
														<tr class="form-field form-required">
															<th scope="row"><label for="module_short_description">Short Description<span class="description">(required)</span></label></th>
															<td><textarea id="module_short_description" name="data[Module][short_description]" rows="10" cols="40" style="width: 100%" class="tinymce"></textarea></td>
														</tr>
														<tr class="form-field form-required">
															<th scope="row"><label for="module_long_description">Long Description<span class="description">(required)</span></label></th>
															<td><textarea id="module_long_description" name="data[Module][long_description]" rows="10" cols="40" style="width: 100%" class="tinymce"></textarea></td>
														</tr> 
														<tr class="form-field form-required">
															<th scope="row"><label for="theme_colour">Theme Colour<span class="description">(required)</span></label></th>
															<td><input type="text" value="FFFFFF" id="theme_colour" style="width:50px;"name="data[Module][theme_colour]" autocomplete="off" /></td>
														</tr>
														<tr class="form-field"><th scope="row">&nbsp;</th><td>&nbsp;</td></tr>
													</table>
												</form>
					            			</div>
					  						<div id="step-2">
					            					
										    </div>                      
					  						<div id="step-3">
					  							
					  						</div>
					  						<div id="step-4">
					  							
					                    	</div>
					                    </div>
					            	</div>
					            </div>
  							</div>
  							<div style="clear:both;"></div>
  							<div class="metabox-holder has-right-sidebar" id="poststuff">
								<div class="inner-sidebar" id="side-info-column">
									<div class="meta-box-sortables ui-sortable" id="side-sortables">
										<div class="postbox " id="submitdiv">
											<div title="Click to toggle" class="handlediv"><br></div>
											<h3 class="hndle"><span>Publish</span></h3>
											<div class="inside">
												<div id="submitpost" class="submitbox">
													<div id="minor-publishing">
														<div style="display:none;">
															<p class="submit"><input type="submit" value="Save" class="button" id="save" name="save"></p>
														</div>
														<div id="minor-publishing-actions">
															<div id="save-action">
															<input type="submit" class="button button-highlighted" tabindex="4" value="Save Draft" id="save-post" name="save">
															<img alt="" id="draft-ajax-loading" class="ajax-loading" src="http://localhost/wordpress/wp-admin/images/wpspin_light.gif" style="visibility: hidden;">
															</div>
															<div id="preview-action">
															<a tabindex="4" id="post-preview" target="wp-preview" href="http://localhost/wordpress/?p=11&amp;preview=true" class="preview button">Preview</a>
															<input type="hidden" value="" id="wp-preview" name="wp-preview">
															</div>
															<div class="clear"></div>
														</div>
														<div id="misc-publishing-actions">
															<div class="misc-pub-section">
																<label for="post_status">Status:</label>
																<span id="post-status-display">Draft</span>
																<a tabindex="4" class="edit-post-status hide-if-no-js" href="#post_status">Edit</a>
																<div class="hide-if-js" id="post-status-select">
																	<input type="hidden" value="draft" id="hidden_post_status" name="hidden_post_status">
																	<select tabindex="4" id="post_status" name="post_status">
																	<option value="pending">Pending Review</option>
																	<option value="draft" selected="selected">Draft</option>
																	</select>
																 	<a class="save-post-status hide-if-no-js button" href="#post_status">OK</a>
																 	<a class="cancel-post-status hide-if-no-js" href="#post_status">Cancel</a>
																</div>
															</div>
														</div>
														<div class="clear"></div>
													</div>
													<div id="major-publishing-actions">
														<div id="delete-action"><a href="" class="submitdelete deletion">Move to Trash</a></div>
														<div id="publishing-action">
														<img alt="" id="ajax-loading" class="ajax-loading" src="http://localhost/wordpress/wp-admin/images/wpspin_light.gif" style="visibility: hidden;">
																<input type="hidden" value="Publish" id="original_publish" name="original_publish">
																<input type="submit" accesskey="p" tabindex="5" value="Publish" class="button-primary" id="publish" name="publish"></div>
														<div class="clear"></div>
													</div>
												</div>
											</div>
										</div>
  									</div>
  								</div>
  							</div>
  						</div>
  					</div>	
  				</div>
			</div>
		</div>
	</div>
</div>
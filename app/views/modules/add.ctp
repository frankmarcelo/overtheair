<style type="text/css">
.wrap div.updated, .wrap div.error {
    margin: 5px 0 15px;
}
div.updated, .login .message {
    background-color: #FFFFE0;
    border-color: #E6DB55;
}
div.updated, div.error {
    border-radius: 3px 3px 3px 3px;
    border-style: solid;
    border-width: 1px;
    margin: 5px 15px 2px;
    padding: 0 0.6em;
}

.form-invalid textarea, .form-invalid select {
    border-color: #CC0000 !important;
}

.form-table_step4 {
    border-collapse: collapse;
    clear: both;
    margin-bottom: -8px;
    margin-top: 0.5em;
    width: 100%;
}
</style>
<?php 
echo $this->Html->css(array('wizard','colorpicker'));
echo $this->Html->script(array(
		'jquery_plugins/jquery.smartwizard',
		'jquery_plugins/jquery.autosave',
		'jquery_plugins/jquery.colorpicker',
		'tiny_mce/jquery.tinymce'
	)
);
?>
<script type="text/javascript">
/* <![CDATA[ */
var latestModuleInfo = '<?php echo intval($latest_module_version)?>'; 
var step1_visited = false;
var step2_visited = false;
var step3_visited = false;
var step4_visited = false;

validateForm = function( form ) {
    return !$( form ).find('.form-required').filter( function() { 
        return $(':input:visible', this).val() == ''; 
    }).addClass( 'form-invalid' ).find(':input:visible').change( function() { 
        $(this).closest('.form-invalid').removeClass( 'form-invalid' ); } 
    ).size();
}
             
function convertEntities(o) {
    var c, v;
    c = function(s) {
            if (/&[^;]+;/.test(s)) {
                    var e = document.createElement("div");
                    e.innerHTML = s;
                    return !e.firstChild ? s : e.firstChild.nodeValue;
            }
            return s;
    }

    if ( typeof o === 'string' ) {
            return c(o);
    } else if ( typeof o === 'object' ) {
            for (v in o) {
                    if ( typeof o[v] === 'string' ) {
                            o[v] = c(o[v]);
                    }
            }
    }
    return o;
}             

function leaveAStepCallback(obj){
    var step_num= obj.attr('rel');
    return validateSteps(step_num);
}
  
function onFinishCallback(){

	this.form = $('#step4');
	
	$(this.form).removeClass('.form-invalid');
 	if( validateForm(this.form) == false){ 
   		$('#wizard').smartWizard('showMessage','Please correct the errors in step4 and click next.');
   		$('#wizard').smartWizard('setError',{stepnum:'4',iserror:true});         
 	}else{

		if( $(".msgBox") ){
	 		$(".msgBox").fadeOut("slow",function(){
	     		$(this).remove();
	     	});
	    }	
		$.ajax({
			cache: false  ,
			type:  "POST" ,
			url:   "<?php echo $this->Html->url(array("controller" => "modules","action"=>"stepFour"));?>" ,
			data:  $("#step4").serializeArray(),
			dataType: "json" ,
			success: function(data){
				var evalData = eval(data);
				if(typeof(evalData)=='object'){
					//make a topic reference
					$("#confirm").empty().html('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>You have successfull created a module. You are now being redirected to that page.</p>');
					$('#dialog2').dialog({modal:true,hide:'explode'});
					this.timer = setTimeout(function (){
						$("#dialog2").dialog('close');
			    		window.location = '<?php echo $this->Html->url(array("controller" => "modules","action"=>"view")).DS;?>'+parseInt(evalData.module_id);
					},2000);
				}
			}
		});
 	}
}


function validateAllSteps(){
    var isStepValid = true;
    if(validateStep1() == false){
      	isStepValid = false;
      	$('#wizard').smartWizard('setError',{stepnum:1,iserror:true});         
    }else{
      	$('#wizard').smartWizard('setError',{stepnum:1,iserror:false});
    }
    
    if(validateStep3() == false){
      	isStepValid = false;
      	$('#wizard').smartWizard('setError',{stepnum:3,iserror:true});         
    }else{
      	$('#wizard').smartWizard('setError',{stepnum:3,iserror:false});
    }
    
    if(!isStepValid){
       	$('#wizard').smartWizard('showMessage','Please correct the errors in the steps and continue');
    }
    return isStepValid;
} 	
		
		
function validateSteps(step){
	var isStepValid = true;
	if(step == 1){
		this.form = $('#step1');
		$(this.form).removeClass('.form-invalid');
     	if( validateForm(this.form) == false || shortDescription() == false){ 
       		isStepValid = false; 
      		$('#wizard').smartWizard('showMessage','Please correct the errors in step'+step+ ' and click next.');
       		$('#wizard').smartWizard('setError',{stepnum:step,iserror:true});         
     	}else{
         	if( $(".msgBox") ){
         		$(".msgBox").fadeOut("slow",function(){
             		$(this).remove();
             	});
            }	
     		$('#wizard').smartWizard('setError',{stepnum:step,iserror:false});
     		$("#step1").submit();
     		$.ajax({
				cache: false  ,
				type:  "POST" ,
				url:   "<?php echo $this->Html->url(array("controller" => "modules","action"=>"stepOne"));?>" ,
				data:  $("#step1").serializeArray(),
				dataType: "html" ,
				success: function(data){
					$('#topics').empty().fadeIn("slow",function(){
						$(this).html(data);
					});
				}
			});
		}
  	}else if(step ==2){
  		this.form = $('#step2');
		$(this.form).removeClass('.form-invalid');
     	if( validateForm(this.form) == false || shortDescription() == false){ 
       		isStepValid = false; 
      		$('#wizard').smartWizard('showMessage','Please correct the errors in step'+step+ ' and click next.');
       		$('#wizard').smartWizard('setError',{stepnum:step,iserror:true});         
     	}else{
         	if( $(".msgBox") ){
         		$(".msgBox").fadeOut("slow",function(){
             		$(this).remove();
             	});
            }	
     		$('#wizard').smartWizard('setError',{stepnum:step,iserror:false});
     		//submit form
			$.ajax({
				cache: false  ,
				type:  "POST" ,
				url:   "<?php echo $this->Html->url(array("controller" => "modules","action"=>"stepTwo"));?>" ,
				data:  $("#step2").serializeArray(),
				dataType: "html" ,
				success: function(data){
					$('#topic_files').empty().fadeIn("slow",function(){
						$(this).html(data);
					});
				}
			});
		}
	}else if(step==3){
		if( $(".msgBox") ){
     		$(".msgBox").fadeOut("slow",function(){
         		$(this).remove();
         	});
        }	
 		$('#wizard').smartWizard('setError',{stepnum:step,iserror:false});
 		$.ajax({
			cache: false  ,
			type:  "POST" ,
			url:   "<?php echo $this->Html->url(array("controller" => "modules","action"=>"stepThree"));?>" ,
			data:  $("#step3").serializeArray(),
			dataType: "html" ,
			success: function(data){
				$('#assessments_div').empty().fadeIn("slow",function(){
					$(this).html(data);
				});
			}
		});
	}
	return isStepValid;
}

shortDescription = function() {
	var shortDesc = $.trim($("#module_short_description").val());
	var longDesc =  $.trim($("#module_long_description").val());
	var returnData = true;
	if( parseInt(shortDesc.length,10) < 1 ){
		$('#td_short').addClass('form-invalid').change(function(){
			$(this).closest('.form-invalid').removeClass( 'form-invalid' ); 
		});
		returnData= false;
	}

	if( parseInt(longDesc.length,10) < 1 ){
		$('#td_long').addClass('form-invalid').change(function(){
			$(this).closest('.form-invalid').removeClass( 'form-invalid' ); 
		});
		returnData= false;
	}
	return returnData;
}

function validateStep3(){
   var isValid = true;    
   //validate email  email
   var email = $('#email').val();
   if(email && email.length > 0){
      if(!isValidEmailAddress(email)){
        isValid = false;
        $('#msg_email').html('Email is invalid').show();           
      }else{
       $('#msg_email').html('').hide();
      }
   }else{
      isValid = false;
      $('#msg_email').html('Please enter email').show();
   }       
   return isValid;
}
 
$(document).ready(function(){
	'use strict';	
	$('#wizard') .css({'height': (($(window).height()) + 361)+'px'});
	
	/*$('textarea.tinymce').tinymce({
		script_url : '/over-the-air/js/tiny_mce/tiny_mce.js',
        theme : "advanced",
        theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,fontselect,fontsizeselect",
        theme_advanced_buttons2 : "",                                
        theme_advanced_buttons3 : "",                        
        theme_advanced_buttons4 : "",
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_statusbar_location : "bottom",
        theme_advanced_resizing : true
   	});*/

	<?php
	if(!isset($aModuleImageLookUp)){
	?>
		$("#confirm").empty().html('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Cannot create module with empty module images. You are now being redirected to that page.</p>');
		$('#dialog2').dialog({modal:true,hide:'explode'});
		this.timer = setTimeout(function (){
			$("#dialog2").dialog('close');
		    window.location = '<?php echo $this->Html->url(array("controller" => "modules","action"=>"upload"));?>';
		},2000);	
	<?php
	}
	?>
	
	$('#step-1,#step-2,#step-3,#step-4').scroll();
	
	$('#wizard').smartWizard({
		selected:0,
		keyNavigation: false, // Enable/Disable key navigation(left and right keys are used if enabled)
	    onLeaveStep: leaveAStepCallback, // triggers when leaving a step
		onFinish:onFinishCallback,
		labelNext: 'Next',
		transitionEffect: 'slide'
	});
	$('form *').autosave();

	$('#theme_colour').ColorPicker({
		onSubmit: function(hsb, hex, rgb, el) {
			$(el).val(hex);
			$(el).ColorPickerHide();
		},
		onBeforeShow: function () {
			$(this).ColorPickerSetColor(this.value);
		},
		onHide: function (colpkr) {
			$(colpkr).fadeOut(500);
			return false;
		},
		onChange: function (hsb, hex, rgb) {
			$(this).val(hex);
		}
	}).bind('keyup', function(){
		$(this).ColorPickerSetColor(this.value);
	}).bind('blur',function(){
		$(this).ColorPickerSetColor(this.value);	
	});

    <?php
    if(empty($topicFiles) || count($topicFiles)<1){//cannot create module without topics
	?>	
    	$("#confirm").empty().html('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Cannot create module with empty topic files. You are now being redirected to that page.</p>');
    	$('#dialog2').dialog({modal:true,hide:'explode'});
    	this.timer = setTimeout(function (){
    		$("#dialog2").dialog('close');
    	    window.location = '<?php echo $this->Html->url(array("controller" => "documents","action"=>"upload"));?>';
    	},2000);
    <?php
    }
    ?>
    		
    $('button[type="reset"]').live('click',function(){
    	$("#dialog").dialog('close');
    });

    $(window).resize(function() {
	    $("#dialog").dialog("option", "position", "center");
	});
	
	$(window).scroll(function() {
	    $("#dialog").dialog("option", "position", "center");
	});

	$(".topic_trigger").live('click',function(){
		var $iterator = parseInt($(".topic_body_content").size(),10)+1;
		$.get("<?php echo $this->Html->url(array("controller" => "modules","action"=>"addTopics"));?>",{ iterator:$iterator},
		function(data){
			$('.topic_body_content:last').after(data);
		});	
	});

	$(".assessments_trigger").live('click',function(){
		var $iterator = parseInt($(".asessessments").size(),10)+1;

		$.get("<?php echo $this->Html->url(array("controller" => "modules","action"=>"addAssessments"));?>",{ iterator:$iterator},
			function(data){
			$('.asessessments:last').after(data);
		});	
	});

	$('a[class*="topics_"]').live('click',function(){
		var className = $(this).attr('class');
		var classId = className.split("_");

		var $iterator = parseInt($(".topic_file_body_content_"+classId[1]).size(),10)+1;
		$.get("<?php echo $this->Html->url(array("controller" => "modules","action"=>"addTopicFiles"));?>",{ iterator:$iterator, topic_id:classId[1]},
		function(data){
			$(".topic_file_body_content_"+classId[1]+":last").after(data);
		});
	});

	$('a[class*="add_reference_"]').live('click',function(){
		var className = $(this).attr('class');
		var classId = className.split("_");

		$.get("<?php echo $this->Html->url(array("controller" => "modules","action"=>"addReference"));?>",{ reference_id:classId[2]},
		function(data){
			$(".add_another_reference_"+classId[2]+":last").after(data);
		});
	});
});
/* ]]> */
</script>
<div id="wpwrap">
	<div id="adminmenuback"></div>
	<div id="adminmenuwrap">
		<div id="adminmenushadow"></div>
		<?php echo $this->element('menu'); ?>
	</div>
	<div id="wpcontent">
		<!--header menu -->
		<?php echo $this->element('user_navigation'); ?>
		<!-- end of header menu -->
		<div id="wpbody">
			<div id="wpbody-content">
				<div class="wrap">
					<div class="icon32 icon32-posts-post" id="icon-edit"><br></div>
					<h2>Add New Module</h2>
					<br />
					<div>
						<div id="side-info-column">
							<div id="post-body">
								<div >
									<div>
										<!-- start of wizard -->
										<div id="wizard" class="swMain">
						  					<ul>
												<li><a href="#step-1"><label class="stepNumber">1</label><span class="stepDesc">Step 1<br /><small>Modules</small></span></a></li>
						  						<li><a href="#step-2"><label class="stepNumber">2</label><span class="stepDesc">Step 2<br /><small>Topics</small></span></a></li>
						  						<li><a href="#step-3"><label class="stepNumber">3</label><span class="stepDesc">Step 3<br /><small>Documents</small></span></a></li>
						  						<li><a href="#step-4"><label class="stepNumber">4</label><span class="stepDesc">Step 4<br /><small>Assessments</small></span></a></li>
						  					</ul>
	  										<div id="step-1" style="overflow: scroll; height:550px;" class="content">
	  											<h2 class="StepTitle">Step 1 Add Module</h2>
	  											<form name="step1" id="step1" method="post" action="<?php echo $this->Html->url(array("controller" => "modules","action"=>"add"));?>" onSubmit="return false;" accept-charset="utf-8">
					  								<input type="hidden" name="data[Module][id]" value="<?php echo $latest_module_version;?>" /> 
					            					<table class="form-table">
					            						<tr class="form-field form-required">
															<th scope="row"><label for="module_name"><strong>Classification</strong> (required)</label></th>
															<td><select name="data[Module][display_order]">
															<?php
															if(!empty($moduleTypes)){
																foreach($moduleTypes as $moduleType){
																	echo '<option value="'.$moduleType['ModuleType']['id'].'">'.$moduleType['ModuleType']['name'].'</option>';
																}
															}	
															?></select></td>
														</tr>
														<tr class="form-field form-required">
															<th scope="row"><label for="module_name"><strong>Title</strong> (required)</label></th>
															<td><input type="text" autocomplete="off" name="data[Module][name]" id="module_name" /></td>
														</tr>
														<tr class="form-field form-required" id="td_short">
															<th scope="row"><label for="module_short_description"><strong>Short Description</strong> (required)</label></th>
															<td><textarea style="visibility:visible;" id="module_short_description" name="data[Module][short_description]" rows="5" cols="30" style="width: 100%" class="tinymce"></textarea></td>
														</tr>
														<tr class="form-field form-required" id="td_long">
															<th scope="row"><label for="module_long_description"><strong>Long Description</strong> (required)</label></th>
															<td><textarea style="visibility:visible;" id="module_long_description" name="data[Module][long_description]" rows="5" cols="30" style="width: 100%" class="tinymce"></textarea></td>
														</tr> 
														<?php
														if(isset($aModuleImageLookUp)){
														?>
														<tr class="form-field form-required">
															<th scope="row"><label for="add_image"><strong>Select Module Image</strong> (required)</label></th>
															<td><select id="add_image" name="data[Module][modules_image_id]">
															<?php
															foreach( $aModuleImageLookUp as $aImageInfo ){
																echo '<option value="'.$aImageInfo['ModulesImage']['id'].'">'.$aImageInfo['ModulesImage']['raw_name'].'</option>';
															}
															?>
															</select></td>
														</tr>
														<?php
														}
														?>
														<tr class="form-field"><th scope="row">&nbsp;</th><td>&nbsp;</td></tr>
													</table>
												</form>
	  										</div>
	  										<div id="step-2" style="overflow: scroll;height:600px;" class="content">
	  											<h2 class="StepTitle">Step 2 Add Topics</h2>
	  											<div id="topics">&nbsp;</div>
	  										</div>                      
	  										<div id="step-3" class="content" style="overflow: scroll;height:600px;">
	  											<h2 class="StepTitle">Step 3 Add Documents</h2>
	  											<br /><div id="topic_files">&nbsp;</div><br/>
	  										</div>
											<div id="step-4" class="content" style="overflow: scroll;height:600px;">
												<h2 class="StepTitle">Step 4 Assessments</h2>
												<div id="assessments_div">&nbsp;</div><br/>
	        								</div>
	        							</div>
					                    <!-- end of wizard -->
					            	</div>
					            </div>
  							</div>
  						</div>
  					</div>	
  				</div>
			</div>
		</div>
	</div>
</div>
<div id="dialog2" title="Redirecting" style="display:none;"><div id="confirm">&nbsp;</div></div>
<div id="dialog" title="Add Module Image" style="display:none;"></div>
<?php #echo $this->element('sql_dump');?>
<?php

$aDisplayTopicsReference = $this->Session->read('aDisplayTopicsReference');
?>
<form name="step4" id="step4" method="post" onSubmit="return false;" accept-charset="utf-8">
	<?php
	if(!isset($edit)){
	?>
	<input type="hidden" name="data[Module][id]" value="<?php echo $this->Session->read('Module.id');?>" />
	<?php
	}else{
	?>
	<input type="hidden" name="data[Module][edit]" value="true" />
	<input type="hidden" name="data[Module][id]" value="<?php echo $module_id;?>" />
	<?php
	}
	?>
	<div class="assessmentswrapper" id="assessmentswrapper">
		<?php
		if(!isset($edit)){
		?>
		<h2><?php echo $this->Session->read('Module.name');?> (<?php echo $this->Session->read('ModuleType.ModuleType.name');?>)</h2>
		<?php
		}else{
		?>
		<h2><?php echo $module_name;?> (<?php echo $module_type;?>)</h2>
		<?php
		}

		if(!isset($edit)){
		?>
		<div class="asessessments">		
			<table class="form-table">
				<tr class="form-field form-required">
					<th scope="row"><label><strong>Question <?php echo $this->Session->read('Module.display_order');?>.1</strong> (required)</label></th>
					<td><input type="text" autocomplete="off" name="data[Assessment][question][1][]" style="width:350px;"/></td>
					<td><a style="cursor:pointer;" class="assessments_trigger">Add another assessment</a></span></td>
				</tr>
				<tr class="form-field form-required">
					<th scope="row"><label><strong>Select one as answer</strong> (required)</label></th>
					<td class="form-field form-required" colspan="2">
						<span class="form-field form-required">
						Choice 1: <input type="radio" name="data[AssessmentsChoice][id][1][]" value="1" checked="checked"/><input type="text" name="data[AssessmentsChoice][text][1][]" style="width:250px"/><br/>  
						Choice 2: <input type="radio" name="data[AssessmentsChoice][id][1][]" value="2" /><input type="text" name="data[AssessmentsChoice][text][1][]" style="width:250px"/><br/>
						Choice 3: <input type="radio" name="data[AssessmentsChoice][id][1][]" value="3" /><input type="text" name="data[AssessmentsChoice][text][1][]" style="width:250px"/><br/>
						Choice 4: <input type="radio" name="data[AssessmentsChoice][id][1][]" value="4" /><input type="text" name="data[AssessmentsChoice][text][1][]" style="width:250px"/><br/>
						</span>
					</td>
				</tr>
				<tr class="form-field form-required">
					<th scope="row"><label><strong>Reference Material</strong> (optional)</label></th>
					<td colspan="2"><div class="add_another_reference_1"><?php
					if(!empty($aDisplayTopicsReference)){
				?><select name="data[AssessmentsReference][id][1][]">
					<?php
						foreach( $aDisplayTopicsReference as $referenceMaterials ){
						echo '<option value="'.$referenceMaterials['id'].'">'.$this->Text->truncate($referenceMaterials['value'],'50',array('ending' => '...','html' => true)).'</option>';
					}				
				?>	
					</select><?php
					}
				?><a style="cursor:pointer;" class="add_reference_1">Add another reference</a></div></td>
				</tr>
			</table>
			<br/>
		</div>
		<?php
		}else{

			if( isset($module['Assessment']) && count($module['Assessment'])){
				foreach( $module['Assessment'] as $assessment ){
					$aChoices =  $this->AssessmentsChoices->loadDataById($assessment['id']); 
					$aAnswer  = $this->AssessmentsAnswer->loadDataById($assessment['id']);
					
					$aReferences =  $this->AssessmentsReferences->loadDataById($assessment['id']);
		?>
			<div class="asessessments" id="assessments_wrapper_<?php echo $assessment['id'];?>">		
			<table class="form-table">
				<tr class="form-field form-required">
					<th scope="row"><label><strong>Question <?php echo $module['Module']['display_order'];?>.<?php echo $assessment['display_sequence'];?></strong> (required)</label></th>
					<td><input type="text" autocomplete="off" name="data[Assessment][question][<?php echo $assessment['id'];?>][]" style="width:350px;" value="<?php echo $assessment['question'];?>" /></td>
					<td><a style="cursor:pointer;" class="assessments_trigger">Add another assessment</a><br /><a style="cursor:pointer;" id="delete_assessment_<?php echo $assessment['id'];?>">Delete Assessment</a></span></td>
				</tr>
				<tr class="form-field form-required">
					<th scope="row"><label><strong>Select one as answer</strong> (required)</label></th>
					<td class="form-field form-required" colspan="2">
						<span class="form-field form-required">
						<?php
						foreach( $aChoices as $options ){
							$checked = ( isset($aAnswer[0]['AssessmentsAnswer']['assessments_choice_id']) &&  $options['AssessmentsChoice']['id'] == $aAnswer[0]['AssessmentsAnswer']['assessments_choice_id'] && $aAnswer[0]['AssessmentsAnswer']['assessment_id'] == $assessment['id'] ) ?' checked="checked"':'';
							
						?>
						Choice <?php echo intval($options['AssessmentsChoice']['display_sequence']);?>: 
						<input <?php echo $checked;?> type="radio" name="data[AssessmentsChoice][id][<?php echo $assessment['id'];?>][]" value="<?php echo intval($options['AssessmentsChoice']['display_sequence']);?>" />
						<input value="<?php echo Sanitize::html(trim($options['AssessmentsChoice']['text']));?>" type="text" name="data[AssessmentsChoice][text][<?php echo $assessment['id'];?>][<?php echo $options['AssessmentsChoice']['id'];?>]" style="width:250px"/><br/>
						<?php
							unset($checked);
						}
						?>
						</span>
					</td>
				</tr>
				<tr class="form-field form-required">
					<th scope="row"><label><strong>Reference Material</strong> (optional)</label></th>
					<td>
					<?php
					if(!empty($aDisplayTopicsReference)){
                                                  if(!empty($aReferences)){
							foreach( $aReferences as $aReference ){
					?>	
					<div class="add_another_reference_<?php echo $assessment['id'];?>">
					<select name="data[AssessmentsReference][id][<?php echo $assessment['id'];?>][<?php $aReference['AssessmentsReference']['id'];?>]">
					<?php
							foreach( $aDisplayTopicsReference as $referenceMaterials ){
								$selected = ( $aReference['AssessmentsReference']['topics_file_id'] == $referenceMaterials['id'] ) ? ' selected="selected"': '';
								echo '<option value="'.$referenceMaterials['id'].'"'.$selected.'>'.$this->Text->truncate($referenceMaterials['value'],'50',array('ending' => '...','html' => true)).'</option>';
							}//end foreach				
					?>	
					</select>	
					</div>
					<?php
								
							}//end of foreach $aReferences
						}elseif(empty($aRerences)){
					?>
						<div class="add_another_reference_<?php echo $assessment['id'];?>">	
						<select name="data[AssessmentsReference][id][<?php echo $assessment['id'];?>][]">
						<?php
							foreach( $aDisplayTopicsReference as $referenceMaterials ){
								echo '<option value="'.$referenceMaterials['id'].'">'.$this->Text->truncate($referenceMaterials['value'],'50',array('ending' => '...','html' => true)).'</option>';
							}//end foreach				
						?>	
						</select></div><?php
						}
						?></td><td><a style="cursor:pointer;" class="add_reference_<?php echo $assessment['id'];?>">Add reference</a><br/><!--<a style="cursor:pointer;" id="delete_reference_">Delete reference</a>  --></td>
					<?php
					}//end aDisplayTopicsReference
					?>
				</tr>
			</table>
			<br />
			</div>
		<?php
				}
			}
		}?>
	</div>
</form>
<?php #echo $this->element('sql_dump'); ?>

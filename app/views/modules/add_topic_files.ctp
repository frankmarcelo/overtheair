<?php

$aShowTopic = $this->Session->read('aShowTopic');

$found = false;
foreach( $topicFiles as $topicFile ){
	if(file_exists(Configure::read('downloadLink').DS.'topics'.DS.$topicFile['TopicFiles']['source_file'])){
		$extension = pathinfo($topicFile['TopicFiles']['raw_name'], PATHINFO_EXTENSION);
		$topicFile['TopicFiles']['raw_name'] = str_replace(".".$extension,"",$topicFile['TopicFiles']['raw_name']);
		$found =true;
		break;
	}
}

if(!empty($aShowTopic) && count($aShowTopic)>0 ){
	foreach( $aShowTopic as $key => $topic ){

		if($topic['id']== $session_topic_id){
?>
	<div class="topic_file_body_content_<?php echo $topic['id'];?>">
  	<table class="form-table" tyle="width:950px;">
  		<tr style="width:950px;">
    		<td width="5%"><strong><?php echo $this->Session->read('Module.display_order');?>.<?php echo $key+1;?></strong></td>
    		<td width="10%"><input type="text" name="data[TopicFiles][display_sequence][<?php echo $topic['id'];?>][]" style="width:50px;" /></td>
	    	<td width="30%"><?php if(!empty($topicFiles)){?><select class="class_hidden_topics" name="data[TopicFiles][<?php echo $topic['id'];?>][]">
			<?php
			foreach( $topicFiles as $topicFile ){
				if(file_exists(Configure::read('downloadLink').DS.'topics'.DS.$topicFile['TopicFiles']['source_file'])){
					$extension = pathinfo($topicFile['TopicFiles']['raw_name'], PATHINFO_EXTENSION);
					$topicFile['TopicFiles']['raw_name'] = str_replace(".".$extension,"",$topicFile['TopicFiles']['raw_name']);
					echo '<option value="'.$topicFile['TopicFiles']['id'].'">'.$this->Text->truncate($topicFile['TopicFiles']['name'],'50',array('ending' => '...','html' => true)).'</option>';	
				}
			}?></select><?php }else{?>Error cannot proceed. Either all materials are assigned to topics or no available materials for assignment. <a href="<?php echo $this->Html->url(array("controller" => "documents","action"=>"upload"));?>" target="_blank">Upload now</a>?<?php }?>
			</td>
			<td width="35%" style="text-align:left;">Topic:<strong><?php echo Sanitize::clean($topic['name']);?></strong></td>
			<td width="20%"><?php if($found==true){?><a style="cursor:pointer" class="topics_<?php echo $topic['id'];?>">Assign material</a><?php }?></td>
		</tr>
    </table>
	</div>
<?php
		}
	}
}
?>
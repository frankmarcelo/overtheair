<style type="text/css">
.wrap div.updated, .wrap div.error {
    margin: 5px 0 15px;
}
div.updated, .login .message {
    background-color: #FFFFE0;
    border-color: #E6DB55;
}
div.updated, div.error {
    border-radius: 3px 3px 3px 3px;
    border-style: solid;
    border-width: 1px;
    margin: 5px 15px 2px;
    padding: 0 0.6em;
}

.form-invalid textarea, .form-invalid select {
    border-color: #CC0000 !important;
}

.form-table_step4 {
    border-collapse: collapse;
    clear: both;
    margin-bottom: -8px;
    margin-top: 0.5em;
    width: 100%;
}
</style>
<?php 
echo $this->Html->css(array('wizard','colorpicker'));
echo $this->Html->script(array(
		'jquery_plugins/jquery.smartwizard',
		'jquery_plugins/jquery.autosave',
		'jquery_plugins/jquery.colorpicker',
		'tiny_mce/jquery.tinymce'
	)
);
?>
<script type="text/javascript">
/* <![CDATA[ */
var latestModuleInfo = '<?php echo intval($latest_module_version)?>'; 
var step1_visited = false;
var step2_visited = false;
var step3_visited = false;
var step4_visited = false;

validateForm = function( form ) {
    return !$( form ).find('.form-required').filter( function() { 
        return $(':input:visible', this).val() == ''; 
    }).addClass( 'form-invalid' ).find(':input:visible').change( function() { 
        $(this).closest('.form-invalid').removeClass( 'form-invalid' ); } 
    ).size();
}
             
function convertEntities(o) {
    var c, v;
    c = function(s) {
            if (/&[^;]+;/.test(s)) {
                    var e = document.createElement("div");
                    e.innerHTML = s;
                    return !e.firstChild ? s : e.firstChild.nodeValue;
            }
            return s;
    }

    if ( typeof o === 'string' ) {
            return c(o);
    } else if ( typeof o === 'object' ) {
            for (v in o) {
                    if ( typeof o[v] === 'string' ) {
                            o[v] = c(o[v]);
                    }
            }
    }
    return o;
}             

function leaveAStepCallback(obj){
    var step_num= obj.attr('rel');
    return validateSteps(step_num);
}
  
function onFinishCallback(){

	this.form = $('#step4');
	
	$(this.form).removeClass('.form-invalid');
 	if( validateForm(this.form) == false){ 
   		$('#wizard').smartWizard('showMessage','Please correct the errors in step4 and click next.');
   		$('#wizard').smartWizard('setError',{stepnum:'4',iserror:true});         
 	}else{

		if( $(".msgBox") ){
	 		$(".msgBox").fadeOut("slow",function(){
	     		$(this).remove();
	     	});
	    }	
		$.ajax({
			cache: false  ,
			type:  "POST" ,
			url:   "<?php echo $this->Html->url(array("controller" => "modules","action"=>"stepFour"));?>" ,
			data:  $("#step4").serializeArray(),
			dataType: "json" ,
			success: function(data){
				var evalData = eval(data);
				if(typeof(evalData)=='object'){
					//make a topic reference
					$("#confirm").empty().html('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>You have successfull created a module. You are now being redirected to that page.</p>');
					$('#dialog2').dialog({modal:true,hide:'explode'});
					this.timer = setTimeout(function (){
						$("#dialog2").dialog('close');
			    		window.location = '<?php echo $this->Html->url(array("controller" => "modules","action"=>"view")).DS;?>'+parseInt(evalData.module_id);
					},2000);
				}
			}
		});
 	}
}


function validateAllSteps(){
    var isStepValid = true;
    if(validateStep1() == false){
      	isStepValid = false;
      	$('#wizard').smartWizard('setError',{stepnum:1,iserror:true});         
    }else{
      	$('#wizard').smartWizard('setError',{stepnum:1,iserror:false});
    }
    
    if(validateStep3() == false){
      	isStepValid = false;
      	$('#wizard').smartWizard('setError',{stepnum:3,iserror:true});         
    }else{
      	$('#wizard').smartWizard('setError',{stepnum:3,iserror:false});
    }
    
    if(!isStepValid){
       	$('#wizard').smartWizard('showMessage','Please correct the errors in the steps and continue');
    }
    return isStepValid;
} 	
		
		
function validateSteps(step){
	var isStepValid = true;
	if(step == 1){
		this.form = $('#step1');
		$(this.form).removeClass('.form-invalid');
     	if( validateForm(this.form) == false || shortDescription() == false){ 
       		isStepValid = false; 
      		$('#wizard').smartWizard('showMessage','Please correct the errors in step'+step+ ' and click next.');
       		$('#wizard').smartWizard('setError',{stepnum:step,iserror:true});         
     	}else{
         	if( $(".msgBox") ){
         		$(".msgBox").fadeOut("slow",function(){
             		$(this).remove();
             	});
            }	
     		$('#wizard').smartWizard('setError',{stepnum:step,iserror:false});
     		$("#step1").submit();
     		$.ajax({
				cache: false  ,
				type:  "POST" ,
				url:   "<?php echo $this->Html->url(array("controller" => "modules","action"=>"stepOne"));?>" ,
				data:  $("#step1").serializeArray(),
				dataType: "json" ,
				success: function(data){
					var evalData = eval(data);
				}
			});
		}
  	}else if(step ==2){
  		this.form = $('#step2');
		$(this.form).removeClass('.form-invalid');
     	if( validateForm(this.form) == false || shortDescription() == false){ 
       		isStepValid = false; 
      		$('#wizard').smartWizard('showMessage','Please correct the errors in step'+step+ ' and click next.');
       		$('#wizard').smartWizard('setError',{stepnum:step,iserror:true});         
     	}else{
         	if( $(".msgBox") ){
         		$(".msgBox").fadeOut("slow",function(){
             		$(this).remove();
             	});
            }	
     		$('#wizard').smartWizard('setError',{stepnum:step,iserror:false});
     		//submit form
			$.ajax({
				cache: false  ,
				type:  "POST" ,
				url:   "<?php echo $this->Html->url(array("controller" => "modules","action"=>"stepTwo"));?>" ,
				data:  $("#step2").serializeArray(),
				dataType: "json" ,
				success: function(data){
					var evalData = eval(data);
					if(typeof(evalData)=='object'){
						
						var htmlTemplate = '<form name="step3" id="step3" method="post" onSubmit="return false;" accept-charset="utf-8">';
						htmlTemplate +='<input type="hidden" name="data[Module][id]" value="<?php echo $latest_module_version;?>" />';
						htmlTemplate +='<div class="topicswrapper" id="topicswrapper"><table class="form-table" style="width:600px;">';
						for( i=0; i < evalData.length; i++){
							htmlTemplate +='<tr style="width:100%;text-align:left;"><td>Assign Topic Name: <strong>'+evalData[i].name+'</strong>&nbsp;&nbsp;&nbsp;';
							htmlTemplate +='to Topic File:&nbsp;&nbsp;'+$("#hidden_topics").html()+'<input type="hidden" autocomplete="off" name="data[Topic][id][]" value="'+evalData[i].id+'" /></div></td></tr>';
						}

						htmlTemplate +='<tr class="form-field"><th scope="row">&nbsp;</th><td>&nbsp;</td></tr></table></div></form>';
						$('#topic_files').empty().fadeIn("slow",function(){
							$(this).html(htmlTemplate);
						});

					}
				}
			});
		}
	}else if(step==3){
		if( $(".msgBox") ){
     		$(".msgBox").fadeOut("slow",function(){
         		$(this).remove();
         	});
        }	
 		$('#wizard').smartWizard('setError',{stepnum:step,iserror:false});
 		$.ajax({
			cache: false  ,
			type:  "POST" ,
			url:   "<?php echo $this->Html->url(array("controller" => "modules","action"=>"stepThree"));?>" ,
			data:  $("#step3").serializeArray(),
			dataType: "json" ,
			success: function(data){
				var evalData = eval(data);
				if(typeof(evalData)=='object'){
					var topic_html ='<select name="data[AssessmentsReference][id][]"><option value="0">Select your reference topic</option>';
					for( i = 0; i < evalData.length; i++ ){
						topic_html +='<option value="'+evalData[i].id+'">'+evalData[i].value+'</option>';
					}
					topic_html +='</select>';
					$('#reference_topics_step4').empty().fadeIn("slow",function(){
						$(this).html(topic_html);
					});
					//make a topic reference 
				}
			}
		});
	}
	return isStepValid;
}

shortDescription = function() {
	var shortDesc = $.trim($("#module_short_description").val());
	var longDesc =  $.trim($("#module_long_description").val());
	var returnData = true;
	if( parseInt(shortDesc.length,10) < 1 ){
		$('#td_short').addClass('form-invalid').change(function(){
			$(this).closest('.form-invalid').removeClass( 'form-invalid' ); 
		});
		returnData= false;
	}

	if( parseInt(longDesc.length,10) < 1 ){
		$('#td_long').addClass('form-invalid').change(function(){
			$(this).closest('.form-invalid').removeClass( 'form-invalid' ); 
		});
		returnData= false;
	}
	return returnData;
}


 
 function validateStep3(){
   var isValid = true;    
   //validate email  email
   var email = $('#email').val();
    if(email && email.length > 0){
      if(!isValidEmailAddress(email)){
        isValid = false;
        $('#msg_email').html('Email is invalid').show();           
      }else{
       $('#msg_email').html('').hide();
      }
    }else{
      isValid = false;
      $('#msg_email').html('Please enter email').show();
    }       
   return isValid;
 }
 
             
$(document).ready(function(){
	'use strict';	
	$('#wizard') .css({'height': (($(window).height()) + 361)+'px'});
	
	$('textarea.tinymce').tinymce({
		script_url : '/over-the-air/js/tiny_mce/tiny_mce.js',
        theme : "advanced",
        theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,fontselect,fontsizeselect",
        theme_advanced_buttons2 : "",                                
        theme_advanced_buttons3 : "",                        
        theme_advanced_buttons4 : "",
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_statusbar_location : "bottom",
        theme_advanced_resizing : true
   	});

	<?php
	if(!isset($aModuleImageLookUp)){
	?>
		$("#confirm").empty().html('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Cannot create module with empty module images. You are now being redirected to that page.</p>');
		$('#dialog2').dialog({modal:true,hide:'explode'});
		this.timer = setTimeout(function (){
			$("#dialog2").dialog('close');
		    window.location = '<?php echo $this->Html->url(array("controller" => "uploader","action"=>"index"));?>#tabs-2';
		},2000);	
	<?php
	}
	?>
	
	$('#step-1,#step-2,#step-3,#step-4').scroll();
	
	$('#wizard').smartWizard({
		selected:0,
		keyNavigation: false, // Enable/Disable key navigation(left and right keys are used if enabled)
	    onLeaveStep: leaveAStepCallback, // triggers when leaving a step
		onFinish:onFinishCallback,
		labelNext: 'Next',
		transitionEffect: 'slide'
	});
	$('form *').autosave();

	$('#theme_colour').ColorPicker({
		onSubmit: function(hsb, hex, rgb, el) {
			$(el).val(hex);
			$(el).ColorPickerHide();
		},
		onBeforeShow: function () {
			$(this).ColorPickerSetColor(this.value);
		},
		onHide: function (colpkr) {
			$(colpkr).fadeOut(500);
			return false;
		},
		onChange: function (hsb, hex, rgb) {
			$(this).val(hex);
		}
	}).bind('keyup', function(){
		$(this).ColorPickerSetColor(this.value);
	}).bind('blur',function(){
		$(this).ColorPickerSetColor(this.value);	
	});

    <?php
    if(empty($topicFiles) || count($topicFiles)<1){//cannot create module without topics
	?>	
    	$("#confirm").empty().html('<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Cannot create module with empty topic files. You are now being redirected to that page.</p>');
    	$('#dialog2').dialog({modal:true,hide:'explode'});
    	this.timer = setTimeout(function (){
    		$("#dialog2").dialog('close');
    	    window.location = '<?php echo $this->Html->url(array("controller" => "uploader","action"=>"index"));?>#tabs-3';
    	},2000);
    <?php
    }
    ?>
    		
    $('button[type="reset"]').live('click',function(){
    	$("#dialog").dialog('close');
    });

    $(window).resize(function() {
	    $("#dialog").dialog("option", "position", "center");
	});
	
	$(window).scroll(function() {
	    $("#dialog").dialog("option", "position", "center");
	});

	$(".topic_trigger").live('click',function(){
		var totalTables = parseInt($("#topicswrapper .form-table").size(),10);
		var lastTable = $('.topicswrapper:last').clone();
		var stepContainerHeight = parseInt($(".stepContainer").height());
		if( stepContainerHeight > 300 ){
			stepContainerHeight = stepContainerHeight+ 60;
			$(".stepContainer").height(stepContainerHeight);
		}
		$('.topicswrapper:last').before(lastTable);
	});

	$(".assessments_trigger").live('click',function(){
		var totalTables = parseInt($("#assessmentswrapper .form-table").size(),10);
		var lastTable = $('.assessmentswrapper:last').clone();
		var stepContainerHeight = parseInt($(".stepContainer").height());
		if( stepContainerHeight > 300 ){
			stepContainerHeight = stepContainerHeight+ 40;
			$(".stepContainer").height(stepContainerHeight);
		}
		$('.assessmentswrapper:last').before(lastTable);
		$('.assessmentswrapper:last input[type="radio"]').removeAttr("name").attr("name","data[AssessmentsChoice][id]["+totalTables+"][]");
		$('.assessmentswrapper:last input[type="radio"]:first').attr("checked","checked");
	});
});
/* ]]> */
</script>
<div id="wpwrap">
	<div id="adminmenuback"></div>
	<div id="adminmenuwrap">
		<div id="adminmenushadow"></div>
		<?php echo $this->element('menu'); ?>
	</div>
	<div id="wpcontent">
		<!--header menu -->
		<?php echo $this->element('user_navigation'); ?>
		<!-- end of header menu -->
		<div id="wpbody">
			<div id="wpbody-content">
				<div class="wrap">
					<div class="icon32 icon32-posts-post" id="icon-edit"><br></div>
					<h2>Add New Module</h2>
					<br />
					<div>
						<div id="side-info-column">
							<div id="post-body">
								<div >
									<div>
										<!-- start of wizard -->
										<div id="wizard" class="swMain">
						  					<ul>
												<li><a href="#step-1"><label class="stepNumber">1</label><span class="stepDesc">Step 1<br /><small>Modules</small></span></a></li>
						  						<li><a href="#step-2"><label class="stepNumber">2</label><span class="stepDesc">Step 2<br /><small>Topics</small></span></a></li>
						  						<li><a href="#step-3"><label class="stepNumber">3</label><span class="stepDesc">Step 3<br /><small>Documents</small></span></a></li>
						  						<li><a href="#step-4"><label class="stepNumber">4</label><span class="stepDesc">Step 4<br /><small>Assessments</small></span></a></li>
						  					</ul>
	  										<div id="step-1" style="overflow: scroll; height:550px;" class="content">
	  											<h2 class="StepTitle">Step 1 Modules</h2>
	  											<form name="step1" id="step1" method="post" action="<?php echo $this->Html->url(array("controller" => "modules","action"=>"add"));?>" onSubmit="return false;" accept-charset="utf-8">
					  								<input type="hidden" name="data[Module][id]" value="<?php echo $latest_module_version;?>" /> 
					            					<table class="form-table">
					            						<tr class="form-field form-required">
															<th scope="row"><label for="module_name">Classification<span class="description">(required)</span></label></th>
															<td>
															<select name="data[Module][display_order]">
															<?php
															if(!empty($moduleTypes)){
																foreach($moduleTypes as $moduleType){
																	echo '<option value="'.$moduleType['ModuleType']['id'].'">'.$moduleType['ModuleType']['name'].'</option>';
																}
															}	
															?>
															</select></td>
														</tr>
														<tr class="form-field form-required">
															<th scope="row"><label for="module_name">Title<span class="description">(required)</span></label></th>
															<td><input type="text" autocomplete="off" name="data[Module][name]" id="module_name" /></td>
														</tr>
														<tr class="form-field form-required" id="td_short">
															<th scope="row"><label for="module_short_description">Short Description<span class="description">(required)</span></label></th>
															<td><textarea style="visibility:visible;" id="module_short_description" name="data[Module][short_description]" rows="5" cols="30" style="width: 100%" class="tinymce"></textarea></td>
														</tr>
														<tr class="form-field form-required" id="td_long">
															<th scope="row"><label for="module_long_description">Long Description<span class="description">(required)</span></label></th>
															<td><textarea style="visibility:visible;" id="module_long_description" name="data[Module][long_description]" rows="5" cols="30" style="width: 100%" class="tinymce"></textarea></td>
														</tr> 
														<?php
														if(isset($aModuleImageLookUp)){
														?>
														<tr class="form-field form-required">
															<th scope="row"><label for="add_image">Select Module Image<span class="description">(required)</span></label></th>
															<td><select id="add_image" name="data[Module][modules_image_id]">
															<?php
															foreach( $aModuleImageLookUp as $aImageInfo ){
																echo '<option value="'.$aImageInfo['ModulesImage']['id'].'">'.$aImageInfo['ModulesImage']['raw_name'].'</option>';
															}
															?>
															</select></td>
														</tr>
														<?php
														}
														?>
														<tr class="form-field"><th scope="row">&nbsp;</th><td>&nbsp;</td></tr>
													</table>
												</form>
	  										</div>
	  										<div id="step-2" style="overflow: scroll;height:600px;" class="content">
	  											<h2 class="StepTitle">Step 2 Topics</h2>
	  											<div id="topics">
													<form name="step2" id="step2" method="post" onSubmit="return false;" accept-charset="utf-8">
														<input type="hidden" name="data[Module][id]" value="<?php echo $latest_module_version;?>" />
														<div class="topicswrapper" id="topicswrapper">
															<table class="form-table">
																<tr class="form-field form-required">
																	<th scope="row"><label>Topic Name<span class="description">(required)</span></label></th>
																	<td><input type="text" autocomplete="off" name="data[Topic][name][]" /></td>
																</tr>
																<!--
																<tr class="form-field form-required">
																	<th scope="row"><label>Topic Description<span class="description">(required)</span></label></th>
																	<td><input type="text" style="visibility:visible;" name="data[Topic][description][]" /></td>
																</tr>  -->
																<tr class="form-field"><th scope="row">&nbsp;</th><td><a class="topic_trigger" style="cursor:pointer;">Click here to add another Topic.</a></td></tr>
																<tr class="form-field"><th scope="row">&nbsp;</th><td>&nbsp;</td></tr>
															</table>
														</div>
													</form>
												</div>
	  										</div>                      
	  										<div id="step-3" class="content" style="overflow: scroll;height:600px;">
	  											<h2 class="StepTitle">Step 3 Documents</h2>
	  											<br />
	  											<div id="topic_files">&nbsp;</div>
	  											<br/>
	  										</div>
											<div id="step-4" class="content" style="overflow: scroll;height:600px;">
												<h2 class="StepTitle">Step 4 Assessments</h2>
												<div id="assessments_div">
													<form name="step4" id="step4" method="post" onSubmit="return false;" accept-charset="utf-8">
														<input type="hidden" name="data[Module][id]" value="<?php echo $latest_module_version;?>" />
														<div class="assessmentswrapper" id="assessmentswrapper">
															<table class="form-table">
																<tr class="form-field form-required">
																	<th scope="row"><label>Question<span class="description">(required)</span></label></th>
																	<td><input type="text" autocomplete="off" name="data[Assessment][question][]" /></td>
																</tr>
																<tr class="form-field form-required">
																	<th scope="row"><label>Select radio as answer<span class="description">(required)</span></label></th>
																	<td class="form-field form-required">
																		<span class="form-field form-required">Choice 1: <input type="radio" name="data[AssessmentsChoice][id][0][]" value="0" checked="checked"/><input type="text" name="data[AssessmentsChoice][text][0][]" style="width:250px"/><br/>  
																		Choice 2: <input type="radio" name="data[AssessmentsChoice][id][0][]" value="1" /><input type="text" name="data[AssessmentsChoice][text][1][]" style="width:250px"/><br/>
																		Choice 3: <input type="radio" name="data[AssessmentsChoice][id][0][]" value="2" /><input type="text" name="data[AssessmentsChoice][text][2][]" style="width:250px"/></span>
																	</td>
																</tr>
																<tr class="form-field form-required">
																	<th scope="row"><label>Reference Topic<span class="description">(optional)</span></label></th>
																	<td><div id="reference_topics_step4">&nbsp;</div></td>
																</tr>
																<tr class="form-field"><th scope="row">&nbsp;</th><td><a class="assessments_trigger" style="cursor:pointer;">Click here to add another Assessment.</a></td></tr>
																<tr class="form-field"><th scope="row">&nbsp;</th><td>&nbsp;</td></tr>
															</table>
														</div>
													</form>
												</div>
	  											<br/>
	        								</div>
	        							</div>
					                    <!-- end of wizard -->
					            	</div>
					            </div>
  							</div>
  						</div>
  					</div>	
  				</div>
			</div>
		</div>
	</div>
</div>
<div id="dialog2" title="Redirecting" style="display:none;"><div id="confirm">&nbsp;</div></div>
<div id="dialog" title="Add Module Image" style="display:none;"></div>
<div id="hidden_topics" style="display:none;">
<select class="class_hidden_topics" name="data[TopicFiles][id][]">
<?php
if(!empty($topicFiles)){
	
	foreach( $topicFiles as $topicFile ){
		echo '<option value="'.$topicFile['TopicFiles']['id'].'">'.$topicFile['TopicFiles']['raw_name'].'</option>';
	}
}
?></div>
</select>
<?php echo $this->element('sql_dump');?>
<?php
echo $this->Html->css(array('jquery.autocomplete'));
?>
<style type="text/css">
.wrap div.updated, .wrap div.error {
    margin: 5px 0 15px;
}
div.updated, .login .message {
    background-color: #FFFFE0;
    border-color: #E6DB55;
}
div.updated, div.error {
    border-radius: 3px 3px 3px 3px;
    border-style: solid;
    border-width: 1px;
    margin: 5px 15px 2px;
    padding: 0 0.6em;
}
</style>
<script type="text/javascript">
/* <![CDATA[ */
$(document).ready(function(){
	$("#q").focus();
	$("#q").autocomplete({
		source: "/over-the-air/js/ajax/users.php",   
        minLength: 3,
        autoFocus: true
	});

	$("#confirm_delete").live("click",function(){
		var elementId = $.trim($(this).attr('elementId'));
		var elementProperties = elementId.split("_");

		$("#cancel").attr('disabled',true);
        $(this).empty().html("Deleting user. Please wait...").attr('disabled',true);

		this.timer = setTimeout(function (){
            $.ajax({
        		cache: false  ,
		        type:  "POST" ,
		      	url:   "<?php echo $this->Html->url(array("controller" => "users","action" => "delete"));?>/"+elementProperties[0]+"/"+elementProperties[1]+"/",
		        data:  $(":input").serializeArray(),
		        dataType: "json" ,
		        success: function(data){
                	var response = eval(data);
                	$("#dialog").dialog('close');    
                	window.location.reload();       
                }
    		});
		},100);
	});

	$('.submitdelete').live('click',function(){
		var elementId = $.trim($(this).attr('id'));
		var elementProperties = elementId.split("_");	
		
		var title = '<strong>'+$.trim(elementProperties[0])+'</strong>';
        var dialog_message ='<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>';
        dialog_message +=title+' will be permanently deleted and cannot be recovered. Continue?</p><br />';
        dialog_message +='<p align="center"><button style="cursor:pointer;" class="ui-state-default ui-corner-all" type="button" id="cancel">Cancel</button>&nbsp;';
        dialog_message +='<button style="cursor:pointer;" class="ui-state-default ui-corner-all" style="font-color:#F0F0F0;" type="button" elementId="'+elementId+'" id="confirm_delete">Confirm delete</button>&nbsp;</p>';
        $("#confirm_msg").empty().html(dialog_message);
        $('#dialog').dialog({modal:true,hide:'explode'});
	});

	$('.resend').live('click',function(){
		var elementId = $.trim($(this).attr('id'));
		var elementProperties = elementId.split("_");

		$.ajax({
    		cache: false  ,
	        type:  "GET" ,
	      	url:   "<?php echo $this->Html->url(array("controller" => "users","action" => "resend"));?>/"+elementProperties[0]+"/"+elementProperties[1]+"/"
	    });
		
		var title = '<strong>'+$.trim(elementProperties[0])+'</strong>';
        var dialog_message ='<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>';
        dialog_message +='In a few moments '+title+' will receive an email regarding the download link.<br /><div align="center"><button style="cursor:pointer;" class="ui-state-default ui-corner-all" type="button" id="cancel2">Ok</button></div></p>';
        $("#confirm_msg2").empty().html(dialog_message);
        $('#dialog2').dialog({modal:true,hide:'explode'});
	});

	$("#cancel2").live("click",function(){
        $("#dialog2").dialog('close');
	});

	$("#cancel").live("click",function(){
        $("#dialog").dialog('close');
	});

	$(window).resize(function() {
	    $("#dialog").dialog("option", "position", "center");
	    $("#dialog2").dialog("option", "position", "center");
	});
	
	$(window).scroll(function() {
	    $("#dialog").dialog("option", "position", "center");
	    $("#dialog2").dialog("option", "position", "center");
	});


	$("#frmSearch input").keypress(function(e) {
        if (e.keyCode == 13) {
            $(this).closest("form").submit();
        }
	});
});
/* ]]> */
</script>
<div id="wpwrap">
	<div id="adminmenuback"></div>
	<div id="adminmenuwrap">
		<div id="adminmenushadow"></div>
		<?php echo $this->element('menu');?>
	</div>
	<div id="wpcontent">
		<!--header menu -->
		<?php echo $this->element('user_navigation'); ?>
		<!-- end of header menu -->
		<div id="wpbody">
			<div id="wpbody-content">
				<div class="wrap">
					<div class="icon32" id="icon-users"><br></div>
					<h2>Users<a class="add-new-h2" href="<?php echo $this->Html->url(array("controller" => "users","action"=>"add"));?>">Add New</a></h2>
					<?php
					if( strstr($this->Session->read('Message.flash.message'),'success') ){
					?>
					<div class="updated" id="message"><p><?php echo $this->Session->read('Message.flash.message');?></p></div>
					<?php
						$this->Session->delete('Message.flash');
					}elseif( strstr($this->Session->read('Message.flash.message'),'invalid') ){
					?>	
					<div class="error below-h2"><p><strong>ERROR</strong>: <?php echo $this->Session->read('Message.flash.message');?></p></div>
					<?php
						$this->Session->delete('Message.flash');
					}
					?>
					<ul class="subsubsub">
						<li class="all"><a class="current" href="<?php echo $this->Html->url(array("controller" => "users","action"=>"index"));?>">All <span class="count">(<?php echo count($users);?>)</span></a> |</li>
						<?php
						if(count($roles)>0){
							$roleCount = array();
							foreach($users as $userInfo){
								$roleCount[$userInfo['Role']['id']][] = $userInfo['User'];
							}

							foreach($roles as $rolesInfo){
						?>
						<li class="administrator"><?php echo $rolesInfo['Role']['title'];?> <span class="count">(<?php echo (isset($roleCount[$rolesInfo['Role']['id']])) ? count($roleCount[$rolesInfo['Role']['id']]):0;?>)</span></li>
						<?php
							}
						}
						?>
					</ul>
					<form id="frmSearch" name="frmSearch" method="get" action="<?php echo $this->Html->url(array("controller" => "users","action"=>"search"));?>">
						<p class="search-box">
							<label for="user-search-input" class="screen-reader-text">Search Users:</label>
							<input type="text" name="q" value="" id="q" class="text ui-widget-content ui-corner-all" />
							<input type="submit" value="Search Users" class="button" id="search-users" />
						</p>
						<div class="tablenav top">
							<table cellspacing="0" class="wp-list-table widefat fixed users">
								<thead>
								<tr>
									<th class="manage-column column-cb check-column" id="cb" scope="col">&nbsp;</th>
									<th class="manage-column column-username sortable desc" id="username" scope="col">
									<a href="<?php echo $this->Html->url(array("controller" => "users","action"=>"index"));?>/index/sort:username/direction:asc"><span>Username</span>
									<span class="sorting-indicator"></span></a></th>
									<th class="manage-column column-name sortable desc" id="name" scope="col">
									<a href="<?php echo $this->Html->url(array("controller" => "users","action"=>"index"));?>/index/sort:name/direction:asc"><span>Name</span>
									<span class="sorting-indicator"></span></a>
									</th>
									<th class="manage-column column-email sortable desc" id="email" scope="col">
									<a href="<?php echo $this->Html->url(array("controller" => "users","action"=>"index"));?>/index/sort:email/direction:asc"><span>E-mail</span>
									<span class="sorting-indicator"></span></a></th>
									<th class="manage-column column-role" id="role" scope="col">Role</th>
									<th class="manage-column column-posts num" id="posts" scope="col">Email App</th></tr>
								</thead>
								<tfoot>
								<tr>
									<th class="manage-column column-cb check-column" scope="col">&nbsp;</th>
									<th class="manage-column column-username sortable desc" scope="col">
										<a href="<?php echo $this->Html->url(array("controller" => "users","action"=>"index"));?>/index/sort:username/direction:asc"><span>Username</span>
										<span class="sorting-indicator"></span></a>
									</th>
									<th class="manage-column column-name sortable desc" scope="col">
										<a href="<?php echo $this->Html->url(array("controller" => "users","action"=>"index"));?>/index/sort:name/direction:asc"><span>Name</span>
										<span class="sorting-indicator"></span></a>
									</th>
									<th class="manage-column column-email sortable desc" scope="col">
										<a href="<?php echo $this->Html->url(array("controller" => "users","action"=>"index"));?>/index/sort:name/direction:asc"><span>E-mail</span>
										<span class="sorting-indicator"></span></a>
									</th>
									<th style="" class="manage-column column-role" scope="col">Role</th>
									<th class="manage-column column-posts num" scope="col">
									<?php 
									if( !empty($macos) && sizeof($macos) ){
										echo "Email App";	
									}
									?>
									</th>
								</tr>
								</tfoot>
								<tbody class="list:user" id="the-list">
								<?php
								if(is_array($users) && !empty($users) ){
									foreach( $users as $userInfo ){
										if($userInfo['User']['status'] == Configure::read('status_live') ){
								?>
								<tr class="alternate" id="user-1">
									<th class="check-column" scope="row">&nbsp;</th>
									<td class="username column-username">
									<?php 
									if( intval($userInfo['User']['id']) == intval($this->Session->read('Auth.User.id'))){
									?>								
									<strong><a href="<?php echo $this->Html->url(array("controller" => "users","action"=>"profile"));?>"><?php echo trim($userInfo['User']['username']);?></a></strong><br>
									<?php	
									}else{
										if( intval($userInfo['User']['id']) != intval(Configure::read('adminRoleId'))){
									?>	
									<strong><a href="<?php echo $this->Html->url(array("controller" => "users","action"=>"edit",trim($userInfo['User']['username']).DS.trim($userInfo['User']['token'])));?>"><?php echo trim($userInfo['User']['username']);?></a></strong><br>
									<?php
										}else{
									?>
									<strong><?php echo trim($userInfo['User']['username']);?></strong><br>
									<?php		
										}
									}
									?>
									<div class="row-actions">
										<?php 
										if( intval($userInfo['User']['id']) == intval($this->Session->read('Auth.User.id'))){
										?>													
										<span class="edit"><a href="<?php echo $this->Html->url(array("controller" => "users","action"=>"profile"));?>">Profile</a></span>
										<?php
										}else{
											if( intval($userInfo['User']['id']) != Configure::read('adminRoleId') ){ 
										?>	
 										<span class="edit">
 										<a href="<?php echo $this->Html->url(array("controller" => "users","action"=>"edit",trim($userInfo['User']['username']).DS.trim($userInfo['User']['token'])));?>">Edit</a>
 										</span>	
										<?php
											}
										}

										if($userInfo['User']['id'] > 1 ){
										?>
										<span class="delete"><a id="<?php echo trim($userInfo['User']['username']).'_'.trim($userInfo['User']['token']);?>" style="cursor:pointer;"class="submitdelete">Delete</a></span>
										<?php
										}
										?>
									</div>
									</td>
									<td class="name column-name"><?php echo trim($userInfo['User']['name']);?></td>
									<td class="email column-email"><a href="mailto:<?php echo trim($userInfo['User']['email']);?>"><?php echo trim($userInfo['User']['email']);?></a></td>
									<td class="role column-role"><?php echo $userInfo['Role']['title'];?></td>
									<td class="posts column-posts num">
									<?php 
											if( !empty($macos) ){
												foreach( $macos as $mac_app ){
													if( intval($userInfo['User']['download_type_id']) == intval($mac_app['Macosx']['download_type_id']) ){
									?><a class="resend" id="<?php echo trim($userInfo['User']['username']).'_'.trim($userInfo['User']['token']);?>" style="cursor:pointer;">Resend <?php echo trim($mac_app['DownloadType']['alias']);?></a><?php
													}
												}
											}else{
									?><a style="cursor:pointer;" href="<?php echo $this->Html->url(array("controller" => "macosx","action"=>"upload"));?>">Upload App To Resend</a>
									<?php
											}
								?></td></tr>
								<?php
										}//end if
									}//end foreach
								}//end if
								?>	
								</tbody>
							</table>
						</div>
					</form>
				</div>
			</div><!-- end wpbody-content -->
		</div><!-- end of wpbody -->
	</div>
	<div class="clear"></div>
</div>
<div id="dialog" title="Delete User" style="display:none;"><div id="confirm_msg">&nbsp;</div></div>
<div id="dialog2" title="Send download link" style="display:none;"><div id="confirm_msg2">&nbsp;</div></div>
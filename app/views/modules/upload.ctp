<?php
echo $this->Html->css(array(
            'jquery.fileupload-ui'
        ));

echo $this->Html->script(array(
            'jquery_plugins/jquery.tmpl.min',
            'jquery_plugins/jquery.iframe-transport',
            'jquery_plugins/jquery.fileupload',
            'jquery_plugins/jquery.fileupload-ui',
        ));

?>
<script type="text/javascript">
/* <![CDATA[ */
var startUpload = false;             

function startUploadFile(value){

	if(parseInt(value,10)==1){
		startUpload = true;
	}else{
		startUpload = false;
	}
}             
                        
$(document).ready(function(){
   'use strict';

    // Initialize the jQuery File Upload widget:
    $('#fileupload').fileupload({sequentialUploads: true,
    	destroy: function (e, data) {
            var that = $(this).data('fileupload');  
            var mydata = data.url;
            var nameOfFile = mydata.split("=");
            if ( confirm("Are you sure you want to delete material "+nameOfFile[1]+"?") == true ) {
                if (data.url) {
                    $.ajax(data)
                        .success(function () {
                            that._adjustMaxNumberOfFiles(1);
                            $(this).fadeOut(function () {
                                $(this).remove();
                            });
                        });
                } else {
                    data.context.fadeOut(function () {
                        $(this).remove();
                    });
                }
            }
        }
	}).bind('fileuploadstart', function (e) {
    	startUploadFile(1);
    }).bind('fileuploadadd', function (e, data) {
    	startUploadFile(1);	
	}).bind('fileuploaddestroy', function (e, data) {
	}).bind('fileuploaddone', function (e, data){
        startUploadFile(0);
		var dialog_message ='<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>';
        dialog_message +='Upload file(s) successful.';
        dialog_message +='</p>';
        $("#success").empty().html(dialog_message);
        $('#dialog3').dialog({modal:true,hide:'explode'});
        this.timer = setTimeout(function (){
            $("#dialog3").dialog('close');    
        },1000);
    });


    $('#leave').live('click',function(){
    	$("#dialog4").dialog('close');
    });

    $(window).bind('beforeunload', function(e) {
        if( startUpload == true ){
        	var dialog_message ='<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>';
            dialog_message +='Are you sure you want to leave the page while uploading? If yes please delete the file on upload queue or better to finish the upload.';
            dialog_message +='<div align="center"><button style="cursor:pointer;" class="ui-state-default ui-corner-all" type="button" id="leave">Cancel</button></div></p>';
            $("#confirm_msg4").empty().html(dialog_message);
            $('#dialog4').dialog({modal:true,hide:'explode'});
            return false;
        }
    });


    $(window).resize(function() {
	    $("#dialog").dialog("option", "position", "center");
	    $("#dialog2").dialog("option", "position", "center");
	    $("#dialog3").dialog("option", "position", "center");
	    $("#dialog4").dialog("option", "position", "center");
	});
	
	$(window).scroll(function() {
	    $("#dialog").dialog("option", "position", "center");
	    $("#dialog2").dialog("option", "position", "center");
	    $("#dialog3").dialog("option", "position", "center");
	    $("#dialog4").dialog("option", "position", "center");
	});
    

    // Load existing files:
    $.getJSON($('#fileupload form').prop('action'), function (files) {
        var fu = $('#fileupload').data('fileupload');
        fu._adjustMaxNumberOfFiles(-files.length);
        fu._renderDownload(files)
            .appendTo($('#fileupload .files'))
            .fadeIn(function () {
                // Fix for IE7 and lower:
                $(this).show();
            });
    });

    // Open download dialogs via iframes,
    // to prevent aborting current uploads:
    $('#fileupload .files a:not([target^=_blank])').live('click', function (e) {
        e.preventDefault();
        $('<iframe style="display:none;"></iframe>')
            .prop('src', this.href)
            .appendTo('body');
    });
});
/* ]]> */
</script>
<div id="wpwrap">
	<div id="adminmenuback"></div>
	<div id="adminmenuwrap">
		<div id="adminmenushadow"></div>
		<?php echo $this->element('menu');?>
	</div>
	<div id="wpcontent">
		<!--header menu -->
		<?php echo $this->element('user_navigation'); ?>
		<!-- end of header menu -->
		<div id="wpbody">
			<div id="wpbody-content">
				<div class="wrap">
					<div class="icon32" id="icon-upload"><br></div>
					<h2>Images Uploader</h2> 
                    <br />
					<div id="fileupload">
						<form id="fileuploadfrm" name="fileuploadfrm" action="/over-the-air/js/ajax/upload_modules.php" method="POST" enctype="multipart/form-data">
							<input type="hidden" name="currently_logged_in" id="currently_logged_in" value="<?php echo intval($this->Session->read('Auth.User.id'));?>" />
							<div class="fileupload-buttonbar">
								<label class="fileinput-button"><span>Add files...</span><input type="file" name="files[]" multiple></label>
								<button type="submit" class="start">Start upload</button>
								<button type="reset" class="cancel">Cancel upload</button>
								<button type="button" class="delete">Delete files</button>
							</div>
						</form>
						<div class="fileupload-content">
							<table class="files"></table>
							<div class="fileupload-progressbar"></div>
						</div>
					</div>
					<div>
					<script id="template-upload" type="text/x-jquery-tmpl">
						<tr class="template-upload{{if error}} ui-state-error{{/if}}">
        					<td class="preview"></td>
        					<td class="name">${name}</td>
        					<td class="size">${sizef}</td>
        					{{if error}}
            				<td class="error" colspan="2">Error:
                				{{if error === 'maxFileSize'}}File is too big
                				{{else error === 'minFileSize'}}File is too small
                				{{else error === 'acceptFileTypes'}}Filetype not allowed. 
                				{{else error === 'maxNumberOfFiles'}}Max number of files exceeded
                				{{else}}${error}
                				{{/if}}
            				</td>
        					{{else}}
            				<td class="progress"><div></div></td>
            				<td class="start"><button>Start</button></td>
        					{{/if}}
        					<td class="cancel"><button>Cancel</button></td>
    					</tr>
					</script>	
					<script id="template-download" type="text/x-jquery-tmpl">
					<tr class="template-download{{if error}} ui-state-error{{/if}}">
        				{{if error}}
            			<td></td>
            			<td class="name">${name}</td>
            			<td class="size">${sizef}</td>
            			<td class="error" colspan="2">Error:
                		{{if error === 1}}File exceeds upload_max_filesize (php.ini directive)
                		{{else error === 2}}File exceeds MAX_FILE_SIZE (HTML form directive)
                		{{else error === 3}}File was only partially uploaded
                		{{else error === 4}}No File was uploaded
                		{{else error === 5}}Missing a temporary folder
                		{{else error === 6}}Failed to write file to disk
                		{{else error === 7}}File upload stopped by extension
                		{{else error === 'maxFileSize'}}File is too big
                		{{else error === 'minFileSize'}}File is too small
                		{{else error === 'acceptFileTypes'}}Filetype not allowed
                		{{else error === 'maxNumberOfFiles'}}Max number of files exceeded
                		{{else error === 'uploadedBytes'}}Uploaded bytes exceed file size
                		{{else error === 'emptyResult'}}Empty file upload result
                		{{else}}${error}
                		{{/if}}
            			</td>
        				{{else}}	
            			<td class="preview">
                			{{if thumbnail_url}}
                    		<a href="${url}" target="_blank"><img src="${thumbnail_url}"></a>
                			{{/if}}
            			</td>
            			<td class="name">
                			<a href="${url}"{{if thumbnail_url}} target="_blank"{{/if}}>${name}</a>            </td>
            				<td class="size">${sizef}</td>
            				<td colspan="2">&nbsp;</td>
        				{{/if}}
        				<td class="delete">
            			<button data-type="${delete_type}" data-url="${delete_url}">Delete</button>
        				</td>
    				</tr>
					</script>
				</div>
				<div class="clear"></div>
			</div><!-- end wpbody-content -->
		</div><!-- end of wpbody -->
	</div>
</div>		
<div id="dialog" title="Save Changes Successful" style="display:none;"><div id="confirm_msg">&nbsp;</div></div>
<div id="dialog3" title="Upload Successful" style="display:none;"><div id="success">&nbsp;</div></div>		
<div id="dialog4" title="Leave uploading images page?" style="display:none;"><div id="confirm_msg4">&nbsp;</div></div>
<div id="dialog2" title="Delete file?" style="display:none;"><div id="confirm">&nbsp;</div></div>
<form name="step2" id="step2" method="post" onSubmit="return false;" accept-charset="utf-8">
	<?php
    if( !isset($edit) ||  $edit == false){
	?>
	<input type="hidden" name="data[Module][id]" value="<?php echo $this->Session->read('Module.id');?>" />
	<h2><?php echo $this->Session->read('Module.name');?> (<?php echo $this->Session->read('ModuleType.ModuleType.name');?>)</h2>
	<?php
	}else{
	?>
	<input type="hidden" name="data[Module][edit]" value="true" />
	<input type="hidden" name="data[Module][id]" value="<?php echo $Module['id'];?>" />
	<input type="hidden" name="data[Module][name]" value="<?php echo $Module['name'];?>" />
	<input type="hidden" name="data[Module][type]" value="<?php echo $ModuleType['ModuleType']['name'];?>" />
	<h2><?php echo $Module['name'];?> (<?php echo $ModuleType['ModuleType']['name'];?>)</h2>
	<?php
	}
	?>
	<div class="topicswrapper" id="topicswrapper">
		<?php
        if( !isset($edit) ||  $edit == false){      
			$sessionRead =$this->Session->read('Topic'); 
			if(empty($sessionRead)){
		?>
		<div class="topic_body_content">
			<table class="form-table" style="width:810px;text-align:center;">
				<tr class="form-field form-required" style="width:100%;">
					<th width="30%"><label><strong>Add Topic <?php echo $this->Session->read('Module.display_order');?>.1</strong> (required)</label></th>
					<td width="50%"><input type="text" autocomplete="off" name="data[Topic][name][]" width="400px;" /></td>
					<td width="20%"><a class="topic_trigger" style="cursor:pointer;">Add another Topic.</a></td>
				</tr>
			</table>
			<br />
		</div>
		<?php
			}else{
				$aTopicsSession = $this->Session->read('Topic.name');
				if(is_array($aTopicsSession)){
					foreach($aTopicsSession as $key => $aTopicSession){
		?>		
				<div class="topic_body_content">
					<table class="form-table" style="width:810px;text-align:center;">
						<tr class="form-field form-required" style="width:100%;">
							<th width="30%"><label><strong>Add Topic <?php echo $this->Session->read('Module.display_order');?>.<?php echo $key+1;?></strong> (required)</label></th>
							<td width="50%"><input type="text" autocomplete="off" name="data[Topic][name][]" width="400px;" value="<?php echo $aTopicSession;?>" /></td>
							<td width="20%"><a class="topic_trigger" style="cursor:pointer;">Add another Topic.</a></td>
						</tr>
					</table>
					<br />
				</div>
		<?php
					}
				}
			}
        }else{//start of editing

			$sessionRead = $Topic; 
			if(!empty($sessionRead)){
				if(is_array($sessionRead)){
					foreach($sessionRead as $key => $aTopicSession){
		?>		
				<div class="topic_body_content" id="topic_body_content_<?php echo $aTopicSession['id'];?>">
					<table class="form-table" style="width:810px;text-align:center;">
						<tr class="form-field form-required" style="width:100%;">
							<th width="30%"><label><strong>Edit Topic <?php echo $Module['display_order'];?>.<span class="topic_sequence"><?php echo $aTopicSession['display_sequence']?></span></strong> (required)</label></th>
							<td width="50%"><input type="text" autocomplete="off" name="data[Topic][name][<?php echo $aTopicSession['id'];?>]" width="400px;" value="<?php echo $aTopicSession['name'];?>" /></td>
							<td width="20%"><a class="topic_trigger" style="cursor:pointer;">Add Topic</a><br /><a class="topic_trigger_delete" id="topic_delete_<?php echo $aTopicSession['id'];?>" style="cursor:pointer;">Delete Topic</a></td>
						</tr>
					</table>
					<br />
				</div>
		<?php
					}
				}	
			}
		}//end of editing
        ?>
	</div>
</form>
<?php
#echo json_encode( $data );
?>
<?php #echo $this->element('sql_dump'); ?>

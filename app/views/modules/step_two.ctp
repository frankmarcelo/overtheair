<?php

$found = false;
foreach( $topicFiles as $topicFile ){
	if(file_exists(Configure::read('downloadLink').DS.'topics'.DS.$topicFile['TopicFiles']['source_file'])){
		$extension = pathinfo($topicFile['TopicFiles']['raw_name'], PATHINFO_EXTENSION);
		$topicFile['TopicFiles']['raw_name'] = str_replace(".".$extension,"",$topicFile['TopicFiles']['raw_name']);
		$found =true;
		break;
	}
}


if( isset($latest_module_version) && !isset($edit) ){
	$aShowTopic = $this->Session->read('aShowTopic');
?>
<form name="step3" id="step3" method="post" onSubmit="return false;" accept-charset="utf-8">
<input type="hidden" name="data[Module][id]" value="<?php echo $latest_module_version;?>" />
<div class="step3_wrapper">
<h2><?php echo $this->Session->read('Module.name');?> (<?php echo $this->Session->read('ModuleType.ModuleType.name');?>)</h2>
<div class="topicswrapper">
<?php
foreach( $aShowTopic as $key => $topic ){
?>
	<div class="topic_file_body_content_<?php echo $topic['id'];?>">
  	<table class="form-table" tyle="width:950px;">
  		<tr style="width:950px;">
    		<td width="5%"><strong><?php echo $this->Session->read('Module.display_order');?>.<?php echo $key+1;?></strong></td>
    		<td width="10%"><input type="text" name="data[TopicFiles][display_sequence][<?php echo $topic['id'];?>][]" style="width:50px;" /></td>
	    	<td width="30%"><?php if(!empty($topicFiles)){?><select class="class_hidden_topics" name="data[TopicFiles][<?php echo $topic['id'];?>][]">
			<?php
			foreach( $topicFiles as $topicFile ){
				if(file_exists(Configure::read('downloadLink').DS.'topics'.DS.$topicFile['TopicFiles']['source_file'])){
					$extension = pathinfo($topicFile['TopicFiles']['raw_name'], PATHINFO_EXTENSION);
					$topicFile['TopicFiles']['raw_name'] = str_replace(".".$extension,"",$topicFile['TopicFiles']['raw_name']);
					echo '<option value="'.$topicFile['TopicFiles']['id'].'">'.$this->Text->truncate($topicFile['TopicFiles']['name'],'50',array('ending' => '...','html' => true)).'</option>';	
				}
			}?></select><?php }else{?>Error cannot proceed. Either all materials are assigned to topics or no available materials for assignment. <a href="<?php echo $this->Html->url(array("controller" => "documents","action"=>"upload"));?>" target="_blank">Upload now</a>?<?php }?>
			</td>
			<td width="35%" style="text-align:left;">Topic:<strong><?php echo Sanitize::clean($topic['name']);?></strong></td>
			<td width="20%"><?php if($found==true){?><a style="cursor:pointer" class="topics_<?php echo $topic['id'];?>">Assign material</a><?php }?></td>
		</tr>
    </table>
	</div>
	<br />
<?php
}
?>
</div>
</div>
</form>
<?php
}else{
?>
<form name="step3" id="step3" method="post" onSubmit="return false;" accept-charset="utf-8">
	<input type="hidden" name="data[Module][edit]" value="true" />
	<input type="hidden" name="data[Module][id]" value="<?php echo $module_id;?>" />
	<input type="hidden" name="data[Module][name]" value="<?php echo $module_name;?>" />
	<input type="hidden" name="data[Module][type]" value="<?php echo $module_type;?>" />
	<div class="step3_wrapper">
	<h2><?php echo $module_name;?> (<?php echo $module_type;?>)</h2>
	<div class="topicswrapper">
	<?php
	if(!empty($aShowTopic) && count($aShowTopic)>0 ){
		$found = array();
		foreach( $aShowTopic as $topic_info ){
			if(!empty($moduleTopicFiles) && sizeof($moduleTopicFiles)>0){//existing
				foreach( $moduleTopicFiles as $key => $moduleTopicFile ){
					if( $moduleTopicFile['TopicFiles']['topic_id'] == $topic_info['id'] ){
						$found[] = $topic_info['id'];
	?>
	<div class="topic_file_body_content_<?php echo $moduleTopicFile['TopicFiles']['id'];?>" id="topic_file_body_content_<?php echo $moduleTopicFile['TopicFiles']['id'];?>">
  	<table class="form-table" tyle="width:950px;">
  		<tr style="width:950px;">
    		<td width="5%"><strong><?php echo $topic_info['module_display_order'];?>.<?php echo $moduleTopicFile['TopicFiles']['topic_id'];?></strong></td>
    		<td width="10%"><input type="text" name="data[TopicFiles][display_sequence][<?php echo $topic_info['id'];?>][<?php echo $moduleTopicFile['TopicFiles']['id'];?>]" style="width:50px;" value="<?php echo $moduleTopicFile['TopicFiles']['display_sequence'];?>" /></td>
	    	<td width="30%">
	    			<?php if(!empty($topicFiles)){?>
	    	<select class="class_hidden_topics" name="data[TopicFiles][<?php echo $topic_info['id'];?>][<?php echo $moduleTopicFile['TopicFiles']['id'];?>]">
			<?php
							foreach( $topicFiles as $topicFile ){
								if(file_exists(Configure::read('downloadLink').DS.'topics'.DS.$topicFile['TopicFiles']['source_file'])){
									$extension = pathinfo($topicFile['TopicFiles']['raw_name'], PATHINFO_EXTENSION);
									$topicFile['TopicFiles']['raw_name'] = str_replace(".".$extension,"",$topicFile['TopicFiles']['raw_name']);
									$selected = (  $moduleTopicFile['TopicFiles']['id'] == $topicFile['TopicFiles']['id'] ) ? ' selected="selected"':'';
									echo '<option value="'.$topicFile['TopicFiles']['id'].'"'.$selected.'>'.$this->Text->truncate($topicFile['TopicFiles']['name'],'50',array('ending' => '...','html' => true)).'</option>';	
								}		
							}//end foreaach
						}//end topic Files
			?></select>
			</td>
			<td width="35%" style="text-align:left;">Topic:<strong><?php echo Sanitize::clean($topic_info['name']);?></strong></td>
			<td width="20%"><?php if($found==true){?><a style="cursor:pointer" class="topics_<?php echo $topic_info['id'];?>">Assign material</a><br />
			<a style="cursor:pointer" class="delete_materials" id="trigger_delete_materials_<?php echo $moduleTopicFile['TopicFiles']['id'];?>">Delete material</a>
			<?php }?></td>
		</tr>
    </table>
	</div>
	<br />
	<?php	    
					}//end of if( $moduleTopicFile['TopicFiles']['topic_id'] == $topic_info['id'] )
				}
			}
		}
		
		foreach( $aShowTopic as $topic_info ){
			if( !in_array($topic_info['id'],$found) ){
		?>
		<div class="topic_file_body_content_<?php echo $topic_info['id'];?>" id="topic_file_body_content_<?php echo $topic_info['id'];?>">
  			<table class="form-table" tyle="width:950px;">
  				<tr style="width:950px;">
  					<td width="5%"><strong><?php echo $topic_info['module_display_order'];?>.<?php echo $topic_info['id'];?></strong></td>
  					<td width="10%"><input type="text" name="data[TopicFiles][display_sequence][<?php echo $topic['id'];?>][]" style="width:50px;" /></td>
	    			<td width="30%">
	    			<?php
	    			if(!empty($topicFiles)){
					?>
					<select class="class_hidden_topics" name="data[TopicFiles][<?php echo $topic_info['id'];?>][<?php echo $moduleTopicFile['TopicFiles']['id'];?>]">
						<?php
						foreach( $topicFiles as $topicFile ){
							if(file_exists(Configure::read('downloadLink').DS.'topics'.DS.$topicFile['TopicFiles']['source_file'])){
								$extension = pathinfo($topicFile['TopicFiles']['raw_name'], PATHINFO_EXTENSION);
								$topicFile['TopicFiles']['raw_name'] = str_replace(".".$extension,"",$topicFile['TopicFiles']['raw_name']);
								echo '<option value="'.$topicFile['TopicFiles']['id'].'">'.$this->Text->truncate($topicFile['TopicFiles']['name'],'50',array('ending' => '...','html' => true)).'</option>';	
							}	
						}?></select>
					<?php 
					}//end of topicFiles?>
					</select></td>
					<td width="35%" style="text-align:left;">Topic:<strong><?php echo Sanitize::clean($topic_info['name']);?></strong></td>
					<td width="20%"><?php if($found==true){?><a style="cursor:pointer" class="topics_<?php echo  $topic_info['id'];?>">Assign material</a><?php }?></td>
				</tr>
    		</table>
		</div>
		<br />
		<?php
			}
			//end of in array
		}//end of foreach
	}
	?>
	</div>
	</div>	
</form>		
<?php 
}
?>
<?php #echo $this->element('sql_dump'); ?>
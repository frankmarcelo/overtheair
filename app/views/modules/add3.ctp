<style type="text/css">
.wrap div.updated, .wrap div.error {
    margin: 5px 0 15px;
}
div.updated, .login .message {
    background-color: #FFFFE0;
    border-color: #E6DB55;
}
div.updated, div.error {
    border-radius: 3px 3px 3px 3px;
    border-style: solid;
    border-width: 1px;
    margin: 5px 15px 2px;
    padding: 0 0.6em;
}

.form-invalid textarea, .form-invalid select {
    border-color: #CC0000 !important;
}
</style>
<?php 
echo $this->Html->css(array('wizard','colorpicker','jquery.fileupload-ui'));
echo $this->Html->script(array(
		//'jquery_plugins/jquery.smartwizard',
		'jquery_plugins/jquery.autosave',
		'jquery_plugins/jquery.colorpicker',
		'tiny_mce/jquery.tinymce',
		'jquery_plugins/jquery.tmpl.min',
        'jquery_plugins/jquery.iframe-transport',
        'jquery_plugins/jquery.fileupload',
        'jquery_plugins/jquery.fileupload-ui',
	)
);
?>
<script type="text/javascript">
/* <![CDATA[ */
var latestModuleInfo = '<?php echo intval($latest_module_version)?>'; 
var step1_visited = false;
var step2_visited = false;
var step3_visited = false;
var step4_visited = false;
             
validateForm = function( form ) {
    return !$( form ).find('.form-required').filter( function() { 
        return $(':input:visible', this).val() == ''; 
    }).addClass( 'form-invalid' ).find(':input:visible').change( function() { 
        $(this).closest('.form-invalid').removeClass( 'form-invalid' ); } 
    ).size();
}
             
function convertEntities(o) {
    var c, v;
    c = function(s) {
            if (/&[^;]+;/.test(s)) {
                    var e = document.createElement("div");
                    e.innerHTML = s;
                    return !e.firstChild ? s : e.firstChild.nodeValue;
            }
            return s;
    }

    if ( typeof o === 'string' ) {
            return c(o);
    } else if ( typeof o === 'object' ) {
            for (v in o) {
                    if ( typeof o[v] === 'string' ) {
                            o[v] = c(o[v]);
                    }
            }
    }
    return o;
}             

function leaveAStepCallback(obj){
    var step_num= obj.attr('rel');
    return validateSteps(step_num);
}
  
function onFinishCallback(){
   	if(validateAllSteps()){
    	$('form').submit();
   	}
}


function validateAllSteps(){
    var isStepValid = true;
    if(validateStep1() == false){
      	isStepValid = false;
      	$('#wizard').smartWizard('setError',{stepnum:1,iserror:true});         
    }else{
      	$('#wizard').smartWizard('setError',{stepnum:1,iserror:false});
    }
    
    if(validateStep3() == false){
      	isStepValid = false;
      	$('#wizard').smartWizard('setError',{stepnum:3,iserror:true});         
    }else{
      	$('#wizard').smartWizard('setError',{stepnum:3,iserror:false});
    }
    
    if(!isStepValid){
       	$('#wizard').smartWizard('showMessage','Please correct the errors in the steps and continue');
    }
    return isStepValid;
} 	
		
		
function validateSteps(step){
	var isStepValid = true;
	console.log(step);
	if(step == 1){
		this.form = $('#step1');
		$(this.form).removeClass('.form-invalid');
     	if( validateForm(this.form) == false || shortDescription() == false){ 
       		isStepValid = false; 
      		$('#wizard').smartWizard('showMessage','Please correct the errors in step'+step+ ' and click next.');
       		$('#wizard').smartWizard('setError',{stepnum:step,iserror:true});         
     	}else{
         	if( $(".msgBox") ){
         		$(".msgBox").fadeOut("slow",function(){
             		$(this).remove();
             	});
            }	
     		$('#wizard').smartWizard('setError',{stepnum:step,iserror:false});
     		$("#step1").submit();
     		/*
     		$.ajax({
				cache: false  ,
				type:  "POST" ,
				url:   "<?php echo $this->Html->url(array("controller" => "modules","action"=>"stepOne"));?>" ,
				data:  $("#step1").serializeArray(),
				dataType: "json" ,
o				success: function(data){
					var evalData = eval(data);
				}
			});*/
		}
  	}else if(step ==2){
  		this.form = $('#step2');
		$(this.form).removeClass('.form-invalid');
     	if( validateForm(this.form) == false || shortDescription() == false){ 
       		isStepValid = false; 
      		$('#wizard').smartWizard('showMessage','Please correct the errors in step'+step+ ' and click next.');
       		$('#wizard').smartWizard('setError',{stepnum:step,iserror:true});         
     	}else{
         	if( $(".msgBox") ){
         		$(".msgBox").fadeOut("slow",function(){
             		$(this).remove();
             	});
            }	
     		$('#wizard').smartWizard('setError',{stepnum:step,iserror:false});
     		//submit form
			$.ajax({
				cache: false  ,
				type:  "POST" ,
				url:   "<?php echo $this->Html->url(array("controller" => "modules","action"=>"stepTwo"));?>" ,
				data:  $("#step2").serializeArray(),
				dataType: "json" ,
				success: function(data){
					var evalData = eval(data);
				}
			});
		}
	}
  	return isStepValid;
}

shortDescription = function() {
	var shortDesc = $.trim($("#module_short_description").val());
	var longDesc =  $.trim($("#module_long_description").val());
	var returnData = true;
	if( parseInt(shortDesc.length,10) < 1 ){
		$('#td_short').addClass('form-invalid').change(function(){
			$(this).closest('.form-invalid').removeClass( 'form-invalid' ); 
		});
		returnData= false;
	}

	if( parseInt(longDesc.length,10) < 1 ){
		$('#td_long').addClass('form-invalid').change(function(){
			$(this).closest('.form-invalid').removeClass( 'form-invalid' ); 
		});
		returnData= false;
	}
	return returnData;
}


 
 function validateStep3(){
   var isValid = true;    
   //validate email  email
   var email = $('#email').val();
    if(email && email.length > 0){
      if(!isValidEmailAddress(email)){
        isValid = false;
        $('#msg_email').html('Email is invalid').show();           
      }else{
       $('#msg_email').html('').hide();
      }
    }else{
      isValid = false;
      $('#msg_email').html('Please enter email').show();
    }       
   return isValid;
 }
 
             
$(document).ready(function(){
	'use strict';	
	$('textarea.tinymce').tinymce({
		script_url : '/over-the-air/js/tiny_mce/tiny_mce.js',
        theme : "advanced",
        theme_advanced_buttons1 : "save,newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,formatselect,fontselect,fontsizeselect",
        theme_advanced_buttons2 : "",                                
        theme_advanced_buttons3 : "",                        
        theme_advanced_buttons4 : "",
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_statusbar_location : "bottom",
        theme_advanced_resizing : true
   });

	
	$('#wizard').smartWizard({
		/*selected: 0,  // Selected Step, 0 = first step  
		keyNavigation: true, // Enable/Disable key navigation(left and right keys are used if enabled)
		enableAllSteps: false,  // Enable/Disable all steps on first load
		transitionEffect: 'fade', // Effect on navigation, none/fade/slide/slideleft
		contentCache:true, // cache step contents, if false content is fetched always from ajax url
		enableFinishButton: false, // makes finish button enabled always
		errorSteps:[],    // array of step numbers to highlighting as error steps
		labelNext:'Next', // label for Next button
		labelPrevious:'Previous', // label for Previous button
		labelFinish:'Save Finish',  // label for Finish button       
		onLeaveStep: null, // triggers when leaving a step
		onShowStep: null,  // triggers when showing a step
		onFinish: null  // triggers when Finish button is clicked*/
		selected:<?php echo $step;?>,
		onLeaveStep: leaveAStepCallback, // triggers when leaving a step
		onFinish:onFinishCallback,
		labelNext: 'Next',
		transitionEffect: 'slide',
		keyNavigation: true
	});
	$('form *').autosave();

	$('#theme_colour').ColorPicker({
		onSubmit: function(hsb, hex, rgb, el) {
			$(el).val(hex);
			$(el).ColorPickerHide();
		},
		onBeforeShow: function () {
			$(this).ColorPickerSetColor(this.value);
		},
		onHide: function (colpkr) {
			$(colpkr).fadeOut(500);
			return false;
		},
		onChange: function (hsb, hex, rgb) {
			$(this).val(hex);
		}
	}).bind('keyup', function(){
		$(this).ColorPickerSetColor(this.value);
	}).bind('blur',function(){
		$(this).ColorPickerSetColor(this.value);	
	});

	/*$('#step1').live('click',function(){
		if( validateForm($(this)) ){
			return true;
		}else{
			return false;
		}	
	});*/

	// Initialize the jQuery File Upload widget:
    $('#fileupload').fileupload();
    $('#fileupload_documents').fileupload();

    $.getJSON($('#fileupload_documents form').prop('action'), function (files) {
        var fu = $('#fileupload_documents').data('fileupload_documents');
        fu._adjustMaxNumberOfFiles(-files.length);
        fu._renderDownload(files)
            .appendTo($('#fileupload_documents .files'))
            .fadeIn(function () {
                // Fix for IE7 and lower:
                $(this).show();
            });
    });

    // Load existing files:
    $.getJSON($('#fileupload form').prop('action'), function (files) {
        var fu = $('#fileupload').data('fileupload');
        fu._adjustMaxNumberOfFiles(-files.length);
        fu._renderDownload(files)
            .appendTo($('#fileupload .files'))
            .fadeIn(function () {
                // Fix for IE7 and lower:
                $(this).show();
            });
    });

    // Open download dialogs via iframes,
    // to prevent aborting current uploads:
    $('#fileupload .files a:not([target^=_blank])').live('click', function (e) {
        e.preventDefault();
        $('<iframe style="display:none;"></iframe>')
            .prop('src', this.href)
            .appendTo('body');
    });

    $('#fileupload_documents .files a:not([target^=_blank])').live('click', function (e) {
        e.preventDefault();
        $('<iframe style="display:none;"></iframe>')
            .prop('src', this.href)
            .appendTo('body');
    });

    $('#add_module_image').live('click',function(){
    	$('#dialog').dialog({minWidth:585,modal:true,hide:'explode'});
    });

    $('button[type="reset"]').live('click',function(){
    	$("#dialog").dialog('close');
    });

    $(window).resize(function() {
	    $("#dialog").dialog("option", "position", "center");
	});
	
	$(window).scroll(function() {
	    $("#dialog").dialog("option", "position", "center");
	});

	$(".topic_trigger").live('click',function(){
		var totalTables = parseInt($("#topicswrapper .form-table").size(),10);
		var lastTable = $('#topicswrapper .form-table:last').clone();
		var stepContainerHeight = parseInt($(".stepContainer").height(),10)+200;
		//$('#step-2').height(parseInt($('#step-2').height(),10)+150);
		$(".stepContainer").height(stepContainerHeight);
		$('.form-table:last').after(lastTable);
	});
});
/* ]]> */
</script>
<div id="wpwrap">
	<div id="adminmenuback"></div>
	<div id="adminmenuwrap">
		<div id="adminmenushadow"></div>
		<?php echo $this->element('menu'); ?>
	</div>
	<div id="wpcontent">
		<!--header menu -->
		<?php echo $this->element('user_navigation'); ?>
		<!-- end of header menu -->
		<div id="wpbody">
			<div id="wpbody-content">
				<div class="wrap">
					<div class="icon32 icon32-posts-post" id="icon-edit"><br></div>
					<h2>Add New Module</h2>
					<br />
					<div>
						<div id="side-info-column">
							<div id="post-body">
								<div >
									<div>
										<!-- start of wizard -->
										<div id="wizard" class="swMain">
						  					<ul>
												<li><a href="#step-1" <?php echo ($step>0)? 'class="done" isdone="1" rel="1"':'';?>><label class="stepNumber">1</label><span class="stepDesc">Step 1<br /><small>Modules</small></span></a></li>
						  						<li><a href="#step-2"><label class="stepNumber">2</label><span class="stepDesc">Step 2<br /><small>Topics</small></span></a></li>
						  						<li><a href="#step-3"><label class="stepNumber">3</label><span class="stepDesc">Step 3<br /><small>Documents</small></span></a></li>
						  						<li><a href="#step-4"><label class="stepNumber">4</label><span class="stepDesc">Step 4<br /><small>Assessments</small></span></a></li>
						  						<li><a href="#step-5"><label class="stepNumber">5</label><span class="stepDesc">Step 5<br /><small>Finalise</small></span></a></li>
	  										</ul>
	  										<div id="step-1" style="height:478px;">
	  											<h2 class="StepTitle">Step 1 Modules</h2>
	  											<form name="step1" id="step1" method="post" action="<?php echo $this->Html->url(array("controller" => "modules","action"=>"add","step_two"));?>" accept-charset="utf-8">
					  								<input type="hidden" name="data[Module][id]" value="<?php echo $latest_module_version;?>" /> 
					            					<table class="form-table">
														<tr class="form-field form-required">
															<th scope="row"><label for="module_name">Module Name<span class="description">(required)</span></label></th>
															<td><input type="text" autocomplete="off" name="data[Module][name]" id="module_name" /></td>
														</tr>
														<tr class="form-field form-required" id="td_short">
															<th scope="row"><label for="module_short_description">Short Description<span class="description">(required)</span></label></th>
															<td><textarea style="visibility:visible;" id="module_short_description" name="data[Module][short_description]" rows="5" cols="30" style="width: 100%" class="tinymce"></textarea></td>
														</tr>
														<tr class="form-field form-required" id="td_long">
															<th scope="row"><label for="module_long_description">Long Description<span class="description">(required)</span></label></th>
															<td><textarea style="visibility:visible;" id="module_long_description" name="data[Module][long_description]" rows="5" cols="30" style="width: 100%" class="tinymce"></textarea></td>
														</tr> 
														<tr class="form-field form-required">
															<th scope="row"><label for="theme_colour">Add Image<span class="description">(required)</span></label></th>
															<td><a style="cursor:pointer;" id="add_module_image">Click here to add image.</a></td>
														</tr>
														<tr class="form-field"><th scope="row">&nbsp;</th><td>&nbsp;</td></tr>
													</table>
												</form>
	  										</div>
	  										<div id="step-2" style="height:250px;">
	  											<h2 class="StepTitle">Step 2 Topics</h2>
	  											<form name="step2" id="step2" method="post" action="<?php echo $this->Html->url(array("controller" => "modules","action"=>"add","step_two"));?>" accept-charset="utf-8">
						  							<input type="hidden" name="data[Module][id]" value="<?php echo $latest_module_version;?>" />
						  							<div class="topicswrapper" id="topicswrapper">
								  						<table class="form-table">
															<tr class="form-field form-required">
																<th scope="row"><label>Topic Name<span class="description">(required)</span></label></th>
																<td><input type="text" autocomplete="off" name="data[Topic][name][]" /></td>
															</tr>
															<tr class="form-field form-required">
																<th scope="row"><label>Topic Description<span class="description">(required)</span></label></th>
																<td><textarea style="visibility:visible;" name="data[Topic][description][]" rows="4" cols="30" style="width: 100%"></textarea></td>
															</tr>
															<tr class="form-field"><th scope="row">&nbsp;</th><td><a class="topic_trigger" style="cursor:pointer;">Click here to add another Topic.</a></td></tr>
															<tr class="form-field"><th scope="row">&nbsp;</th><td>&nbsp;</td></tr>
														</table>
													</div>
												</form>
											</div>                      
	  										<div id="step-3">
	  											
											</div>
											<div id="step-4">
	        								</div>
	        								<div id="step-5">
	        								</div>
						                </div>
					                    <!-- end of wizard -->
					            	</div>
					            </div>
  							</div>
  							<!--
  							<div class="metabox-holder has-right-sidebar" id="poststuff">
								<div class="inner-sidebar" id="side-info-column">
									<div class="meta-box-sortables ui-sortable" id="side-sortables">
										<div class="postbox " id="submitdiv">
											<div title="Click to toggle" class="handlediv"><br></div>
											<h3 class="hndle"><span>Publish</span></h3>
											<div class="inside">
												<div id="submitpost" class="submitbox">
													<div id="minor-publishing">
														<div style="display:none;">
															<p class="submit"><input type="submit" value="Save" class="button" id="save" name="save"></p>
														</div>
														<div id="minor-publishing-actions">
															<div id="save-action">
															<input type="submit" class="button button-highlighted" tabindex="4" value="Save Draft" id="save-post" name="save">
															<img id="draft-ajax-loading" class="ajax-loading" src="http://localhost/wordpress/wp-admin/images/wpspin_light.gif" style="visibility: hidden;">
															</div>
															<div id="preview-action">
															<a tabindex="4" id="post-preview" target="wp-preview" href="http://localhost/wordpress/?p=11&amp;preview=true" class="preview button">Preview</a>
															<input type="hidden" value="" id="wp-preview" name="wp-preview">
															</div>
															<div class="clear"></div>
														</div>
														<div id="misc-publishing-actions">
															<div class="misc-pub-section">
																<label for="post_status">Status:</label>
																<span id="post-status-display">Draft</span>
																<a tabindex="4" class="edit-post-status hide-if-no-js" href="#post_status">Edit</a>
																<div class="hide-if-js" id="post-status-select">
																	<input type="hidden" value="draft" id="hidden_post_status" name="hidden_post_status">
																	<select tabindex="4" id="post_status" name="post_status">
																	<option value="pending">Pending Review</option>
																	<option value="draft" selected="selected">Draft</option>
																	</select>
																 	<a class="save-post-status hide-if-no-js button" href="#post_status">OK</a>
																 	<a class="cancel-post-status hide-if-no-js" href="#post_status">Cancel</a>
																</div>
															</div>
														</div>
														<div class="clear"></div>
													</div>
													<div id="major-publishing-actions">
														<div id="delete-action"><a href="" class="submitdelete deletion">Move to Trash</a></div>
														<div id="publishing-action">
														<img alt="" id="ajax-loading" class="ajax-loading" src="http://localhost/wordpress/wp-admin/images/wpspin_light.gif" style="visibility: hidden;">
																<input type="hidden" value="Publish" id="original_publish" name="original_publish">
																<input type="submit" accesskey="p" tabindex="5" value="Publish" class="button-primary" id="publish" name="publish"></div>
														<div class="clear"></div>
													</div>
												</div>
											</div>
										</div>
  									</div>
  								</div>
  							</div>
  							  -->
  						</div>
  					</div>	
  				</div>
			</div>
		</div>
	</div>
</div>
<div id="dialog" title="Add Module Image" style="display:none;">
<div id="fileupload">
	<form action="/over-the-air/js/ajax/upload_modules.php?id=<?php echo intval($latest_module_version);?>" method="POST" enctype="multipart/form-data">
		<input type="hidden" name="currently_logged_in" id="currently_logged_in" value="<?php echo intval($this->Session->read('Auth.User.id'));?>" />
		<input type="hidden" name="latest_module_version" id="latest_module_version" value="<?php echo intval($latest_module_version);?>" />
		<div class="fileupload-buttonbar">
			<label class="fileinput-button"><span>Add files...</span><input type="file" name="files[]" multiple></label>
			<button type="submit" class="start">Start upload</button>
			<button type="reset" class="cancel">Cancel upload</button>
			<button type="button" class="delete">Delete files</button>
		</div>
	</form>
	<div class="fileupload-content">
		<table class="files"></table>
		<div class="fileupload-progressbar"></div>
	</div>
</div>
<div>
	<script id="template-upload" type="text/x-jquery-tmpl">
	<tr class="template-upload{{if error}} ui-state-error{{/if}}">
        <td class="preview"></td>
        <td class="name">${name}</td>
        <td class="size">${sizef}</td>
        {{if error}}
            <td class="error" colspan="2">Error:
                {{if error === 'maxFileSize'}}File is too big
                {{else error === 'minFileSize'}}File is too small
                {{else error === 'acceptFileTypes'}}Filetype not allowed
                {{else error === 'maxNumberOfFiles'}}Max number of files exceeded
                {{else}}${error}
                {{/if}}
            </td>
        {{else}}
            <td class="progress"><div></div></td>
            <td class="start"><button>Start</button></td>
        {{/if}}
        <td class="cancel"><button>Cancel</button></td>
    </tr>
	</script>	
	<script id="template-download" type="text/x-jquery-tmpl">
	<tr class="template-download{{if error}} ui-state-error{{/if}}">
        {{if error}}
            <td></td>
            <td class="name">${name}</td>
            <td class="size">${sizef}</td>
            <td class="error" colspan="2">Error:
                {{if error === 1}}File exceeds upload_max_filesize (php.ini directive)
                {{else error === 2}}File exceeds MAX_FILE_SIZE (HTML form directive)
                {{else error === 3}}File was only partially uploaded
                {{else error === 4}}No File was uploaded
                {{else error === 5}}Missing a temporary folder
                {{else error === 6}}Failed to write file to disk
                {{else error === 7}}File upload stopped by extension
                {{else error === 'maxFileSize'}}File is too big
                {{else error === 'minFileSize'}}File is too small
                {{else error === 'acceptFileTypes'}}Filetype not allowed
                {{else error === 'maxNumberOfFiles'}}Max number of files exceeded
                {{else error === 'uploadedBytes'}}Uploaded bytes exceed file size
                {{else error === 'emptyResult'}}Empty file upload result
                {{else}}${error}
                {{/if}}
            </td>
        {{else}}
            <td class="preview">
                {{if thumbnail_url}}
                    <a href="${url}" target="_blank"><img src="${thumbnail_url}"></a>
                {{/if}}
            </td>
            <td class="name">
                <a href="${url}"{{if thumbnail_url}} target="_blank"{{/if}}>${name}</a>            </td>
            <td class="size">${sizef}</td>
            <td colspan="2">&nbsp;</td>
        {{/if}}
        <td class="delete">
            <button data-type="${delete_type}" data-url="${delete_url}">Delete</button>
        </td>
    </tr>
	</script>
</div>
</div>
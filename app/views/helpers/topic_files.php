<?php

final class TopicFilesHelper extends AppHelper {
/**
 * Other helpers used by this helper
 *
 * @var array
 * @access public
 */
    public $helpers = array('Html');
    
    public $users = array('TopicFiles');
    
    public function loadTopicDataById( $topicId = null ){
    	$oTopicFiles = null;
    	if( isset($topicId) > 0){
	    $conditions[] = array( 
	      'AND' => array (
   	    	   'TopicFiles.topic_id' => $topicId,
        	   'TopicFiles.status = '.Configure::read('status_live')
                )
	    );
	   
            $this->TopicFiles = &ClassRegistry::init('TopicFiles');
            $oTopicFiles = $this->TopicFiles->find('all', array('conditions'=>$conditions,'order'=>'TopicFiles.display_sequence'));
	}
	return $oTopicFiles;
    }
        
    public function get( $key='' ){
        return ( isset($this->{$key}) ) ? $this->{$key} : null; 
    }
}

<?php

final class AssessmentsChoicesHelper extends AppHelper {
/**
 * Other helpers used by this helper
 *
 * @var array
 * @access public
 */
    public $helpers = array('Html');
    
    public $uses = array('AssessmentsChoice');
    
    public function loadDataById( $Id = null ){
  
    	$oChoices = null;
    	if( isset($Id) > 0){
	    $conditions[] = array( 
	      'AND' => array (
   	    	   'AssessmentsChoice.assessment_id' => $Id,
        	   'AssessmentsChoice.status = '.Configure::read('status_live')
                )
	    );
	   
            $this->AssessmentsChoice = &ClassRegistry::init('AssessmentsChoice');
            $this->AssessmentsChoice->contain();	
            $oChoices = $this->AssessmentsChoice->find('all', array('conditions'=>$conditions,'order'=>'AssessmentsChoice.id'));
	}
	return $oChoices;
    }
        
    public function get( $key='' ){
        return ( isset($this->{$key}) ) ? $this->{$key} : null; 
    }
}

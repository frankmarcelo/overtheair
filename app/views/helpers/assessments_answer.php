<?php

final class AssessmentsAnswerHelper extends AppHelper {
/**
 * Other helpers used by this helper
 *
 * @var array
 * @access public
 */
    public $helpers = array('Html');
    
    public $uses = array('AssessmentsAnswer','Topic');
    
    public function loadDataById( $assessmentId =null){
  
    	$oReferences = null;
    	if( $assessmentId > 0  ){
	    $conditions[] = array( 
	      'AND' => array (
        	   'AssessmentsAnswer.status = '.Configure::read('status_live'),
                   'AssessmentsAnswer.assessment_id = '.$assessmentId
                )
	    );
	   
            $this->AssessmentsAnswer = &ClassRegistry::init('AssessmentsAnswer');
            $this->AssessmentsAnswer->contain();	
            $oReferences = $this->AssessmentsAnswer->find('all', array('conditions'=>$conditions));
	}
	return $oReferences;
    }
        
    public function get( $key='' ){
        return ( isset($this->{$key}) ) ? $this->{$key} : null; 
    }
}

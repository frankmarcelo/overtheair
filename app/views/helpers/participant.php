<?php

class ParticipantHelper extends AppHelper {
/**
 * Other helpers used by this helper
 *
 * @var array
 * @access public
 */
    public $helpers = array('Html');
    
    public $users = array('Company','Programs','ProgramsParticipant','Participant');
    private $participantsTitle = null;
	/**
	 * TODO last attended
	 * 
	 */
    
    public function loadParticipantDataById( $participantId = null ){
    	$oParticipant = null;
    	if( isset($participantId) > 0){
    		$conditions[] = array( 
	        	'AND' => array (
	    			'Participant.id' => $participantId,
        			'Participant.status = '.Configure::read('status_live')
        		)
	    	);
	    	
	    	$this->Participant = &ClassRegistry::init('Participant');
	    	$oParticipant = $this->Participant->find($conditions);
		}
		return $oParticipant;
	}
        
	public function setParticipantData($options = array(),$sParticipantsTitle=null) {
		$this->participantsTitle = (!is_null($sParticipantsTitle) && $sParticipantsTitle) ? $sParticipantsTitle:null;
		if( $options && is_array($options) ){
            foreach( $options as $key => $value ){
               unset($this->{$key});
               if( strlen(trim($value)) > 0 ){
                  $key = strtolower($key);
                  $this->{$key} = trim($value);
               }
            }
        }
    }
    
	public function getSeoUri(){
        return ( isset($this->seo_name) ) ? $this->id.DIRECTORY_SEPARATOR.$this->seo_name: null;
    }
    
    public function getFullName(){
    	$sName  = null;
    	if( !isset($this->first_name) && !isset($this->last_name) ){
    		$sName = isset($this->full_name) ? $this->full_name: $sName;	
    		$sName = isset($this->nick_name) ? $this->nick_name: $sName;
    	}else{
    		$sName .= (isset($this->first_name) && !is_null($this->first_name)) ? trim($this->first_name): $sName;
    		$sName .= (isset($this->middle_name) && !is_null($this->middle_name)) ? ' '.trim($this->middle_name) :' ';
    		$sName .= (isset($this->last_name) && !is_null($this->last_name) ) ? ' '.trim($this->last_name): $sName;	
    	}
    	
    	if( strlen(trim($sName)) < 1 || is_null($sName) ){
    		return 'No Name';
    	}else{
    		return ucwords(strtolower($this->participantsTitle)).' '.ucwords($sName);
    	}
    }
    
    public function getNickName(){
    	return isset($this->nick_name) ? $this->nick_name: null;
    }
    
    public function getFullNameWithHtml(){
    	$sName = $this->getFullName();
    	return '<strong>'.$sName.'</strong>';
    }
    
    public function getFullAddress(){
    	$sAddress = null;
        if( isset($this->address) ){
            $sAddress .= ucwords($this->address);
        }
        
		if( isset($this->zip_code) ){
            $sAddress .= ' '.$this->zip_code;
        }
		return $sAddress;
    }
    
    public function getAkaName(){
    	return (isset($this->nick_name)) ? trim($this->nick_name):null;
    }
    
    public function getFax(){
    	return (isset($this->fax))? $this->fax:null;	
    }
    
    public function getMobile(){
    	return (isset($this->mobile))? $this->mobile:null;
    }
    
    public function getPhone(){
    	return (isset($this->phone))? $this->phone:null;
    }
    
    public function getEmail(){
    	return (isset($this->email))? $this->email:null;
    }
    
    public function getPosition(){
    	
    	$sPosition = null;
    	$sPosition .= (isset($this->position))? ucwords($this->position):null;
    	return $sPosition;
    }
    
    public function getNotes(){
	    $sNotes = html_entity_decode($this->get('notes'));
	    if(strlen($sNotes) > 0 ){
	    	$sNotes = substr($sNotes,0,150).'...';
	    }	
	    return $sNotes;
    }
	
    public function get( $key='' ){
        return ( isset($this->{$key}) ) ? $this->{$key} : null; 
    }
    
	public function getDisplayDelete(){
        if( $this->total_programs_attended < 1 ){
        	return '<a class="delete" title="'.$this->getFullName().'" seo="'.$this->getSeoUri().'" seouri="/mansmith/admin/participants/info/'.$this->getSeoUri().'" style="cursor:pointer;" id="all_participants_'.$this->id.'">Delete</a>';
        }
        return null;
    }
    
	public function getDisplayEdit(){
        return '<a class="edit_info" href="/mansmith/admin/participants/edit/'.$this->getSeoUri().'">Edit</a>';
    }
    
    public function getNewsletterButton(){
    	return ( $this->receive_newsletter == 0 ) ? '<a class="add_info" style="cursor:pointer;" id="'.$this->get('id').'">Add to mailing list</a>' :'<a class="remove_newsletter" id="'.$this->get('id').'" style="cursor:pointer;">Remove from mailing list</a>'; 	
    }
}
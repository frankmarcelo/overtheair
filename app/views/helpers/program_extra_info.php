<?php

class ProgramExtraInfoHelper extends AppHelper {
/**
 * Other helpers used by this helper
 *
 * @var array
 * @access public
 */
    public $helpers = array('Html');
    public $uses = array('Company','Participant');
    public $program = array();

    public function setExtraInfo($options = array()) {
       	$this->program = $options;
	}

    public function getProgramType(){
       	return ucwords($this->program['ProgramsType']['name']);
    }

    public function getProgramDivision(){
    	return ucwords($this->program['ProgramsDivision']['title']);
    }

    public function getSchedule(){
       $sSchedule = null;
       if( is_array($this->program['ProgramsSchedule']) && count($this->program['ProgramsSchedule']) > 0 ){
          foreach( $this->program['ProgramsSchedule'] as $program_schedule_key  => $program_schedule ){
          	  if( is_array($program_schedule) && $program_schedule){	
	              $sSchedule .= '<span id="date_'.$program_schedule['program_id'].'">
	                               <span class="date_info"><strong>'.date("D",strtotime($program_schedule['start_date'])).' '.
	                               date("d",strtotime($program_schedule['start_date'])).' '.
	                               date("M Y",strtotime($program_schedule['start_date'])).'</strong></span></span><br />'; 
	          }                 
          }       
       }
       return $sSchedule;
    }

    public function getProgramSpeakers($speakers=array()){
       $sSpeakers = null;       
       if( is_array($this->program['ProgramsSpeaker']) && count($this->program['ProgramsSpeaker']) > 0 ){
          foreach( $speakers  as $speaker_key => $speaker_data ){
             foreach( $this->program['ProgramsSpeaker'] as $program_key => $program_speaker ){
                if( $speaker_data['Speaker']['id'] == $program_speaker['speaker_id'] ){
                   $sSpeakers .= '<div><a href="mailto:'.$speaker_data['Speaker']['email'].'">'.$speaker_data['Speaker']['first_name'].' '.$speaker_data['Speaker']['last_name'].'</a></div>';
                }
             }
          } 
       } 
       return $sSpeakers;
    }
    
    public function getProgramRatePlans($rateplans=array()){
       $sRatePlan = null;
       $aPackageRatePlan = array();
       if( is_array($this->program['ProgramsRatePlan']) && count($this->program['ProgramsRatePlan']) > 0 ){
          foreach( $this->program['ProgramsRatePlan'] as $program_rate_plan ){
             $aPackageRatePlan[$program_rate_plan['name']][$program_rate_plan['rate_plan_id']] = 
               array( 
                 'amount' => $program_rate_plan['amount'] ,
                 'num_adults' => $program_rate_plan['num_of_adults'], 
                 'cut_off_date' => $program_rate_plan['cut_off_date']  
               );
          }    
       }


       	if( is_array($aPackageRatePlan) && count($aPackageRatePlan) >0 ){
          $sRatePlan ='<table class="resengine" style="width: 600px;"><tr><th>&nbsp;</th>';
          foreach( $rateplans as $rateplan_key => $rateplan ){
             $sRatePlan .='<th>'.$rateplan.'</th>';
          }
          $sRatePlan .='</tr>';
          foreach( $aPackageRatePlan as $key => $aRatePlan ){
             $sRatePlan .='<tr><td>'.str_replace('package_','Rate Plan ',$key).'</td>';
             foreach( $rateplans as $rateplan_key => $rateplan ){
                $sAmount = $aPackageRatePlan[$key][$rateplan_key]['amount'];
                $sAdults = $aPackageRatePlan[$key][$rateplan_key]['num_adults'];
                $sRatePlan .='<td>PHP '.$sAmount.' ('.$sAdults.'pax)</td>';
             }
             $sRatePlan .='</tr>';
          }
          $sRatePlan .='</table>';
       	}
       	return $sRatePlan;
    }
    
	public function getLatestCompany(){
		$sHtmlUrl = null;
		if( isset($this->program['ProgramsParticipant']) && sizeof($this->program['ProgramsParticipant'])>0 ){
			$companyId = $this->program['ProgramsParticipant'][0]['company_id'];
			$this->Company = &ClassRegistry::init('Company');
			$this->Company->recursive = -1;
			$this->Company->Behaviors->attach('Containable');
			$companies = $this->Company->find("all",array(
				'fields' => array('Company.id','Company.name','Company.seo_name'),
				'conditions'=> array(
					'Company.id' => $companyId,
					'Company.status' => Configure::read('status_live')
				),
				'limit'=> 1
			));
			
			if( is_array($companies) && sizeof($companies)>0){
				$sHtmlUrl .= '<span><strong>Latest Attending Company:</strong>&nbsp;';
				$sHtmlUrl .= '<a href="'.$this->Html->url(array('controller'=>'companies','action'=>'info',$companyId.DIRECTORY_SEPARATOR.$companies[0]['Company']['seo_name']));
				$sHtmlUrl .= '">'.ucwords($companies[0]['Company']['name']).'</a></span>';
			}
		}
		return $sHtmlUrl;
	}
	
	public function getLatestParticipant(){
		$sHtmlUrl = null;
		if( isset($this->program['ProgramsParticipant']) && sizeof($this->program['ProgramsParticipant'])>0 ){
			$companyId = $this->program['ProgramsParticipant'][0]['company_id'];
			$participantId = $this->program['ProgramsParticipant'][0]['participant_id'];
			
			$this->Participant = &ClassRegistry::init('Participant');
			$this->Participant->recursive = -1;
			$this->Participant->Behaviors->attach('Containable');
			$participants = $this->Participant->find("all",array(
				'fields' => array('Participant.id','Participant.email','Participant.first_name','Participant.last_name','Participant.seo_name'),
				'conditions'=> array(
						'Participant.id' => $participantId,
						'Participant.company_id' => $companyId,
						'Participant.status' => Configure::read('status_live')
					),
				'limit' => 1	
				)
			);
			
			if( is_array($participants) && sizeof($participants)>0){
				$sHtmlUrl .= '<span><strong>Latest Attending Participant:</strong>&nbsp;';
				$sHtmlUrl .= '<a href="'.$this->Html->url(array('controller'=>'participants','action'=>'info',$participantId.DIRECTORY_SEPARATOR.$participants[0]['Participant']['seo_name']));
				//$sHtmlUrl .= '">'.ucwords($participants['Participant']['first_name'].' '.$participants['Participant']['last_name']).'</a>&nbsp;&nbsp;Email:'.$participants['Participant']['email'].'</a>';
				$sHtmlUrl .= '">'.ucwords($participants[0]['Participant']['first_name'].' '.$participants[0]['Participant']['last_name']).'</a></span>';
			}
			
		}
		return $sHtmlUrl;
	}
	
	public function getProgramParticipantInfoById(){
	}

	public function getProgramParticipantPrimaryContact(){
		
	}
}
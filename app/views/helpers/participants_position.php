<?php

final class ParticipantsPositionHelper extends AppHelper {
	
	public function getParticipantsPositionById( $positionId = null ){
    	$this->ParticipantsPosition->recursive = -1;
    	$this->ParticipantsPosition = &ClassRegistry::init('ParticipantsPosition');
		$oParticipantsPosition = $this->ParticipantsPosition->findById($positionId);
		if( !empty($oParticipantsPosition) && $oParticipantsPosition ){
			return $oParticipantsPosition['ParticipantsPosition']['name'];		
		}
		return null;
	}    
}
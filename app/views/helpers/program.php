<?php

class ProgramHelper extends AppHelper {
/**
 * Other helpers used by this helper
 *
 * @var array
 * @access public
 */
    public $helpers = array('Html');
    public $components = array('Session');
    public $program = array();
    
	public function loadProgramDataById( $programId = null ){
    	$oProgram = null;
    	if( isset($programId) > 0){
    		$conditions[] = array( 
	        	'AND' => array (
	    			'Program.id' => $programId,
        			'Program.status = '.Configure::read('status_live')
        		)
	    	);
	    	$this->Program->recursive = -1;
			$this->Program = &ClassRegistry::init('Program');
			$oProgram = $this->Program->find($conditions);
		}
		return $oProgram;
	}

    public function setProgramData($options = array()) {

        if( $options && is_array($options) ){
            foreach( $options as $key => $value ){
               unset($this->{$key});
               if( strlen(trim($value)) > 0 ){
                  $key = strtolower($key);
                  $this->{$key} = trim($value);
               }
            }
        }
    }

    public function getTitle(){
        return ( isset($this->title) ) ? ucwords($this->title) : null; 	
    }

    public function getVenue(){
        return ( isset($this->venue_name) ) ? ucwords($this->venue_name): null;
    }

    public function getSeoUri(){
        return ( isset($this->seo_name) ) ? $this->id.DIRECTORY_SEPARATOR.$this->seo_name: null;
    }
 
    public function getFullAddress(){
        $sAddress = '<address>';
    	if( isset($this->venue_name) ){
    		$sAddress .= '<strong>'.$this->getVenue().'</strong>&nbsp;';
    	}
        
        if( isset($this->address) ){
            $sAddress .= ucwords($this->address);
        }

        if( isset($this->suburb) ){
            $sAddress .= ', '. ucwords($this->suburb);
        }
		$sAddress .='<br />';
		
        if( isset($this->city) ){
            $sAddress .= ' '.ucwords($this->city);
        }
        
        if( isset($this->country) ){
            $sAddress .= ' '.ucwords($this->country);
        }

        if( isset($this->zip_code) && ($this->zip_code) > 0 ){
            $sAddress .= ' '.$this->zip_code;
        }

        if( preg_match( '/^(\,)/',$sAddress) ){
            $sAddress = substr($sAddress,1);
        }
        $sAddress .='</address>';
        return $sAddress;
    }

    public function getLatitude(){
        return ( isset($this->latitude) ) ? $this->latitude: null;
    }
    
    public function getLongitude(){
        return ( isset($this->longitude) ) ? $this->longitude: null;
    }

    public function get( $key='' ){
        return ( isset($this->{$key}) ) ? $this->{$key} : null; 
    }

    public function getContact(){
        $sContact = null;
        if( isset($this->contact_name) ){
           $sContact .= ucwords($this->contact_name);
        }        

        if( isset($this->contact_email) ){
           $sContact .= '<a href="mailto:'.$this->contact_email.'">'.$this->contact_email.'</a>';
        }

        if( isset($this->contact_phone) ){
           $sContact .= '<br />Phone: '.$this->contact_phone;
        }
        return $sContact;
    }
    
    public function getTotalEarnings(){
        return (isset($this->total_earnings) && $this->total_earnings > 0 ) ? '<strong>Earnings:</strong>&nbsp;'.$this->total_earnings: null; 
    }
    
    public function getTotalBalanceOwing(){
        return (isset($this->total_money_owing) && $this->total_money_owing > 0 ) ? '<strong>Balance Owing:</strong>&nbsp;'.$this->total_money_owing: null; 
    }
    
    public function getTotalProgramClientParticipants(){
        return (isset($this->total_companies) && $this->total_companies > 0 ) ? '<strong>Companies:</strong>&nbsp;'.$this->total_companies: null; 
    }
    
    public function getTotalProgramParticipants(){
        return (isset($this->total_participants) && $this->total_participants > 0 ) ? '<strong>Participants:</strong>&nbsp;'.$this->total_participants: null; 
    }

    public function getDisplayDelete( $divHolder = 'all_programs'){
        if( $this->total_earnings < 1 ){
        	return '<a class="delete" title="'.$this->getTitle().'" seo="'.$this->getSeoUri().'" seouri="/mansmith/admin/programs/info/'.$this->getSeoUri().'" style="cursor:pointer;" id="'.strtolower($divHolder).'_'.$this->id.'">Delete</a>';
        }
        return null;
    }
    
	public function getDisplayEdit(){
        return '<a class="edit_info" href="/mansmith/admin/programs/edit/'.$this->getSeoUri().'">Edit</a>';
    }
    
	public function getAddParticipants(){
    	return '<a class="add_info" href="/mansmith/admin/programs/info/'.$this->getSeoUri().'#program_participants">Add Participants</a>';
    }
    
	public function getNotes(){
		$sNotes = (isset($this->content)) ? html_entity_decode($this->content):null;
		$sNotes = strip_tags($sNotes);
	    if(strlen($sNotes) > 0 ){
	    	$sNotes = substr($sNotes,0,150).'...';
	    }	
	    return $sNotes;
    }
}
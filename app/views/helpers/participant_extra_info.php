<?php

final class ParticipantExtraInfoHelper extends AppHelper {
/**
 * Other helpers used by this helper
 *
 * @var array
 * @access public
 */
    public $helpers = array('Html');
    public $uses = array('Program');
    public $participant = array();
    
	public function setExtraInfo($options = array()) {
	   $this->participant = $options;
    }
    
    public function getParticipantsGrouping(){
    	$sHtmlUrl = null;
    	if( isset($this->participant['ParticipantsGrouping']['name']) ){
    		$seo_name  = '?q=&grouping='.$this->participant['ParticipantsGrouping']['seo_name'];
    		$sHtmlUrl .= '<a href="'.$this->Html->url(array('controller'=>'participants','action'=>'search',$seo_name));
			$sHtmlUrl .= '">'.ucwords(strtolower($this->participant['ParticipantsGrouping']['name'])).'</a>';
    	}
    	return (isset($this->participant['ParticipantsGrouping']['name'])) ? $sHtmlUrl:null; 
    }
    
    public function getParticipantsTitle(){
    	return (isset($this->participant['ParticipantsTitle']['name'])) ? trim($this->participant['ParticipantsTitle']['name']):null;
    }
    
    public function getParticipantsCompany(){
    	$sHtmlUrl = null;
		if( isset($this->participant['Company']) && sizeof($this->participant['Company']) > 0){
			$companyId = $this->participant['Company']['id'];
			$seo_name  = $this->participant['Company']['seo_name'];
			$company_name = $this->participant['Company']['name'];	
			$sHtmlUrl .= '<a href="'.$this->Html->url(array('controller'=>'companies','action'=>'info',$companyId.DIRECTORY_SEPARATOR.$seo_name));
			$sHtmlUrl .= '">'.ucwords(strtolower($company_name)).'</a>';	
		}else{
			$sHtmlUrl .= '&nbsp;';
		}
		return $sHtmlUrl;
	}
	
	public function getLatestAttendedProgram(){
    	$sHtmlUrl = null;
		if( isset($this->participant['ProgramsParticipant']) && sizeof($this->participant['ProgramsParticipant']) > 0){
			$programId =$this->participant['ProgramsParticipant'][0]['program_id'];
			$this->Program = &ClassRegistry::init('Program');
			$this->Program->recursive = -1;
			$oProgram = $this->Program->findById($programId);
			if( is_array($oProgram) && $oProgram['Program']['id'] > 0){
				$seo_name = $oProgram['Program']['seo_name'];
				$program_name = $oProgram['Program']['title'];	
				$sHtmlUrl .= '<a href="'.$this->Html->url(array('controller'=>'programs','action'=>'info',$programId.DIRECTORY_SEPARATOR.$seo_name));
				$sHtmlUrl .= '">'.ucwords(strtolower($program_name)).'</a>';	
			}
		}else{
			$sHtmlUrl .= '&nbsp;';
		}
		return $sHtmlUrl;
	}
	
	public function getPosition(){
		$sHtmlUrl = null;
		/*if( isset($this->participant['ParticipantsPosition']) && sizeof($this->participant['ParticipantsPosition']) > 0){
			$sPosition = strtolower($this->participant['ParticipantsPosition']['name']);
			$sPosition = ($sPosition=='none')? '&nbsp;': $sPosition;
			$sHtmlUrl .= ucwords($sPosition);	
		}else{
			$sHtmlUrl .= '&nbsp;';
		}*/
		return $sHtmlUrl;
	}
}
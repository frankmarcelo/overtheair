<?php

final class ParticipantsTitleHelper extends AppHelper {

	public function getParticipantsTitleById( $titleId = null ){
    	$this->ParticipantsTitle->recursive = -1;
    	$this->ParticipantsTitle = &ClassRegistry::init('ParticipantsTitle');
		$oParticipantsTitle = $this->ParticipantsTitle->findById($titleId);
		if( !empty($oParticipantsTitle) && $oParticipantsTitle ){
			return $oParticipantsTitle['ParticipantsTitle']['name'];		
		}
		return null;
	}    
}
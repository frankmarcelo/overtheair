<?php

final class ModulesHelper extends AppHelper {
/**
 * Other helpers used by this helper
 *
 * @var array
 * @access public
 */
    public $helpers = array('Html','Text');
    private $modules = array();
    
    public function loadDataById( $args=array() ){
        if( isset($args['Module']) && isset($args['ModulesImage']) ){
			$this->modules = $args;
		}       
	}
	
	public function getDisplayStatus(){
		if( intval($this->modules['Module']['status']) == Configure::read('status_onhold') ){
			return 'On Hold';	
		}elseif(intval($this->modules['Module']['status']) == Configure::read('status_live')) {
			return 'Published';
		}
	}
	
	public function getShortDescription($index=true){
		if($index==true){
			$short = Sanitize::clean(nl2br(stripslashes(trim($this->modules['Module']['short_description']))), array('encode' => false));
			return $this->Text->truncate($short,'100',array('ending' => '...','html' => true));
		}else{
			return $short = Sanitize::clean(nl2br(stripslashes(trim($this->modules['Module']['short_description']))), array('encode' => false));
		}
	}
	
	public function getLongDescription($index=true){
		if($index==true){
			$long = Sanitize::clean(nl2br(stripslashes(trim($this->modules['Module']['long_description']))),array('encode' => false));
			return $this->Text->truncate($long,'1000',array('ending' => '...','html' => true));
		}else{
			return $long = Sanitize::clean(nl2br(stripslashes(trim($this->modules['Module']['long_description']))),array('encode' => false));
		}
	}
	
	public function getImageSource(){
		$sLink =  ($this->Html->url(array("controller" => "modules","action"=>"viewImg")));
		$sLink .= "?src=".$this->modules['ModulesImage']['raw_name'];
		return $sLink;
	}
	
	public function getContentStatus(){
		if( $this->modules['Module']['status'] == Configure::read('status_live')){
			//return '<a class="publish" id="unpublish_'.$this->modules['Module']['id'].'" style="cursor:pointer;">Unpublish</a>';
		}else{
			return '<a class="publish" id="publish_'.$this->modules['Module']['id'].'" style="cursor:pointer;">Publish</a>';
		}
	}
	
	public function getDisplayDelete(){
		return '<a class="delete" id="delete_'.$this->modules['Module']['id'].'" style="cursor:pointer;">Delete</a>';
	}
    
    public function get( $key='' ){
        return ( isset($this->{$key}) ) ? $this->{$key} : null; 
    }
}

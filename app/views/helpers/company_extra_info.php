<?php

class CompanyExtraInfoHelper extends AppHelper {
/**
 * Other helpers used by this helper
 *
 * @var array
 * @access public
 */
    public $helpers = array('Html');
    public $uses = array('Company','Participant','Program');
    public $company = array();

    public function setExtraInfo($options = array()) {
       $this->company = $options;
    }

    public function getCompanyAffiliationType(){
       return ucwords($this->company['CompaniesAffiliation']['name']);
    }

    public function getCompanyIndustryType(){
       return ucwords(trim(strtolower($this->company['CompaniesIndustry']['name'])));
    }

    public function getCompanyEmployees(){
	}
    
	public function getLatestParticipant(){
		$sHtmlUrl = null;
		if( isset($this->company['Participant']) && sizeof($this->company['Participant']) > 0){
			
			$participantId =$this->company['Participant'][0]['id'];
			$seo_name = $this->company['Participant'][0]['seo_name'];
			$first_name = $this->company['Participant'][0]['first_name'];
			$last_name = $this->company['Participant'][0]['last_name']; 
			$sHtmlUrl .= '<a href="'.$this->Html->url(array('controller'=>'participants','action'=>'info',$participantId.DIRECTORY_SEPARATOR.$seo_name));
			$sHtmlUrl .= '">'.ucfirst(strtolower($first_name)).' '.ucfirst(strtolower($last_name)).'</a>';
		}else{
			$sHtmlUrl .= '&nbsp;';
		}
		return $sHtmlUrl;
	}
	
	public function getLatestProgramAttended(){
		$sHtmlUrl = null;
		if( isset($this->company['ProgramsParticipant']) && sizeof($this->company['ProgramsParticipant']) > 0){
			$programId =$this->company['ProgramsParticipant'][0]['program_id'];
			$this->Program = &ClassRegistry::init('Program');
			$this->Program->recursive = -1;
			$oProgram = $this->Program->findById($programId);
			if( is_array($oProgram) && $oProgram['Program']['id'] > 0){
				$seo_name = $oProgram['Program']['seo_name'];
				$program_name = $oProgram['Program']['title'];	
				$sHtmlUrl .= '<a href="'.$this->Html->url(array('controller'=>'programs','action'=>'info',$programId.DIRECTORY_SEPARATOR.$seo_name));
				$sHtmlUrl .= '">'.ucwords(strtolower($program_name)).'</a>';	
			}
		}else{
			$sHtmlUrl .= '&nbsp;';
		}
		return $sHtmlUrl;
		
	}
}
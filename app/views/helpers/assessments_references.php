<?php

final class AssessmentsReferencesHelper extends AppHelper {
/**
 * Other helpers used by this helper
 *
 * @var array
 * @access public
 */
    public $helpers = array('Html');
    
    public $uses = array('AssessmentsReference','Topic');
    
    public function loadDataById( $referenceId = null, $topicId = null ){
  
    	$oReferences = null;
    	if( isset($referenceId) > 0){
	    $conditions[] = array( 
	      'AND' => array (
   	    	   'AssessmentsReference.assessment_id' => $referenceId,
        	   'AssessmentsReference.status = '.Configure::read('status_live')
                )
	    );
	   
            $this->AssessmentsReference = &ClassRegistry::init('AssessmentsReference');
            $this->AssessmentsReference->contain();	
            $oReferences = $this->AssessmentsReference->find('all', array('conditions'=>$conditions,'order'=>'AssessmentsReference.display_sequence'));
	}
	return $oReferences;
    }
        
    public function get( $key='' ){
        return ( isset($this->{$key}) ) ? $this->{$key} : null; 
    }
}

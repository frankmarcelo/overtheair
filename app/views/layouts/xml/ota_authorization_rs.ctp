<OTA_AuthorizationRS xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"  xmlns="http://greensplash.com/xml/manifest" TimeStamp="<?php echo date("c");?>" TransactionIdentifider="<?php echo $transactionIdentifier;?>" RequestingSource="<?php echo $requestingsource;?>">
  <?php
  if( $valid ){
  ?>
  <Success />
  <Manifest VersionNo="<?php echo $manifestversion;?>" DateCreated="<?php echo date("c",strtotime($manifestcreatedate));?>" DateUpdated="<?php echo date("c",strtotime($manifestupdatedate));?>" />
  <?php
  }else{
  ?>
<Errors><Error ShortText="Invalid username and password" Code="10001">Invalid username and password.</Error></Errors>
  <?php
  }
  ?> 
</OTA_AuthorizationRS>

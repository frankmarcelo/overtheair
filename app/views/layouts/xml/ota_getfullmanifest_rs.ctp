<OTA_GetFullManifestRS xmlns:xsi="<?php echo Configure::read('xmlns:xsi');?>"  xmlns="<?php echo Configure::read('xmlns');?>" TimeStamp="<?php echo date("c");?>" TransactionIdentifider="<?php echo $transactionIdentifier;?>" RequestingSource="<?php echo $requestingsource;?>"><?php
if( $valid && sizeof($modules) > 0 && is_array($modules) ){
?><Success /><Manifest VersionNo="<?php echo $manifestversion;?>" DateCreated="<?php echo date("c",strtotime($manifestcreatedate));?>"><Modules><?php
  $moduleIterator = 1;
  foreach( $modules as $key => $module ){//start of foreach
  ?><Module UniqueIdentifier="<?php echo $module['Module']['id'];?>" UniqueName="<?php echo ($module['ModuleType']['name']);?>"><Name><?php echo ($module['ModuleType']['name']);?></Name><Title><?php echo html_entity_decode(trim($module['Module']['name']),ENT_QUOTES);?></Title><ShortDescription><![CDATA[<?php echo html_entity_decode(trim($module['Module']['short_description']),ENT_QUOTES);?>]]></ShortDescription><LongDescription><![CDATA[<?php echo html_entity_decode($module['Module']['long_description'],ENT_QUOTES);?>]]></LongDescription><Image><?php if( !empty($module['ModulesImage']['raw_name']) ){ echo Sanitize::html($module['ModulesImage']['raw_name']);}?></Image><FileName FileSize="<?php echo trim($module['ModulesImage']['raw_size']);?>" FileType="<?php echo trim($module['ModulesImage']['raw_type']);?>"><![CDATA[<?php if( !empty($module['ModulesImage']['raw_name']) ){ echo Sanitize::html($module['ModulesImage']['raw_name']);}?>]]></FileName><Url><?php if( !empty($module['ModulesImage']['background_image']) ){ echo Configure::read('baseUri').DS.'over-the-air/otawebservices/v1.0/OTA_DownloadModuleFileRQ?src='.$module['ModulesImage']['background_image'];}?></Url><Color><?php echo Sanitize::html($module['Module']['theme_colour']);?></Color><DateCreated><?php echo date("c",strtotime($module['Module']['date_created'])); ?></DateCreated><Status><?php echo ($module['Module']['status']==Configure::read('status_live')) ? "LIVE":"DELETED";?></Status><LastModified><?php echo date("c",strtotime($module['Module']['date_modified'])); ?></LastModified><?php if( count($module['Topic']) > 0 ){?><Topics>
      <?php
      	$topicIterator = 1;
      	foreach( $module['Topic'] as $topic_key => $topic ){
      ?><Topic Name="<?php echo Sanitize::html(trim($topic['name']));?>" UniqueName="<?php echo intval($module['ModuleType']['id']).'.'.intval($topic['display_sequence']);?>" UniqueIdentifier="<?php echo Sanitize::html($topic['id']);?>"><?php
          $aTopicFiles =  $this->TopicFiles->loadTopicDataById($topic['id']);
		?><Name><?php echo intval($module['ModuleType']['id']).'.'.intval($topic['display_sequence']);?></Name><Description><?php echo $topic['description'];?></Description><Status><?php echo ($topic['status']==Configure::read('status_live')) ? "LIVE":"DELETED";?></Status><?php
          if( !empty($aTopicFiles) ){
             echo '<Documents>';
			 $documentIterator = 1;
             foreach( $aTopicFiles as $documents => $document ){    
               
        ?><Document UniqueIdentifier="<?php echo $document['TopicFiles']['id'];?>" UniqueName="<?php echo intval($module['ModuleType']['id']).'.'.intval($topic['display_sequence']).'.'.intval($document['TopicFiles']['display_sequence']);?>" Title="<?php echo Sanitize::html(trim($document['TopicFiles']['name']));?>" author="Administrator">
        <Source><?php echo ucfirst(Sanitize::html(trim($document['TopicFilesSources']['name'])));?></Source>
        <KeyArticle><?php echo (intval($document['TopicFiles']['isKeyArticle'])>0) ? 1:0;?></KeyArticle><Description><![CDATA[<?php echo Sanitize::html(trim($document['TopicFiles']['description']));?>]]></Description>
        <FileName FileSize="<?php echo trim($document['TopicFiles']['raw_size']);?>" FileType="<?php echo trim($document['TopicFiles']['raw_type']);?>"><?php if( !empty($document['TopicFiles']['raw_name']) ){echo trim($document['TopicFiles']['raw_name']);}?></FileName>
        <Url><?php if( !empty($document['TopicFiles']['source_file']) ){ echo Configure::read('baseUri').DS.'over-the-air/otawebservices/v1.0/OTA_DownloadModuleFileRQ?src='.trim($document['TopicFiles']['archive_source_file']);}?></Url>
        <Tags><?php echo $document['TopicFiles']['tags'];?></Tags>
        <Author><?php echo $document['TopicFiles']['author'];?></Author>
        <DateCreated><?php echo date("c",strtotime($document['TopicFiles']['date_created']));?></DateCreated>
        <DateModified><?php echo date("c",strtotime($document['TopicFiles']['date_modified']));?></DateModified>
        <Status><?php echo ($document['TopicFiles']['status']==Configure::read('status_live')) ? "LIVE":"DELETED";?></Status>
        </Document><?php
        		$documentIterator++;
             }//enf of foreach
             echo '</Documents>';     
          }
       ?></Topic><?php
       	  $topicIterator++;
      	}//end of foreach topics 
      ?></Topics><?php 
	 }
	  
	  
	  if( array_key_exists('Assessment',$module) ){	
        ?><Assessments>
            <?php
            $assessmentIterator=1;
            foreach( $module['Assessment']  as $assessment_key => $assessment ){ 
            ?>
         	<Assessment UniqueIdentifier="<?php echo $assessment['id'];?>" UniqueName="<?php echo intval($module['ModuleType']['id']).'.'.intval($assessment['display_sequence']);?>">
               <Question><![CDATA[<?php echo html_entity_decode(trim($assessment['question']),ENT_QUOTES);?>]]></Question>
               <DateCreated><?php echo date("c",strtotime($assessment['date_created']));?></DateCreated>
               <DateModified><?php echo date("c",strtotime($assessment['date_modified']));?></DateModified>
               <Status><?php echo ($assessment['status']==Configure::read('status_live')) ? "LIVE":"DELETED";?></Status>
               <?php
               $aChoices =  $this->AssessmentsChoices->loadDataById($assessment['id']); 
               if( !empty($aChoices) ){
                  echo '<Choices>';
                  foreach( $aChoices as $options ){        
               ?>
                 <Option UniqueName="<?php echo intval($module['ModuleType']['id']).'.'.intval($assessment['display_sequence']).'.'.intval($options['AssessmentsChoice']['display_sequence']);?>" UniqueIdentifier="<?php echo $options['AssessmentsChoice']['id'];?>"><![CDATA[<?php echo html_entity_decode(trim($options['AssessmentsChoice']['text']),ENT_QUOTES);?>]]></Option>
               <?php
                  }
                  echo '</Choices>';
               }
                $aAnswer  = $this->AssessmentsAnswer->loadDataById($assessment['id']);
               if( !empty($aAnswer) ){
				?>
               <Answer><?php echo Sanitize::html(trim($aAnswer[0]['AssessmentsAnswer']['assessments_choice_id']));;?></Answer>
               <?php
               }
				unset($aAnswer);

               $aReferences =  $this->AssessmentsReferences->loadDataById($assessment['id']);
               if( !empty($aReferences) ){
                  echo '<References>';
				  $referenceIterator=1;
                  foreach( $aReferences as $reference ){
               ?>
                 <Reference UniqueIdentifier="<?php echo $reference['AssessmentsReference']['id'];?>" UniqueName="<?php echo intval($module['ModuleType']['id']).'.'.intval($assessment['display_sequence']).'.'.intval($reference['AssessmentsReference']['display_sequence']);?>">
                     <Document><?php echo $reference['AssessmentsReference']['topics_file_id'];?></Document>
                     <Status><?php echo ($reference['AssessmentsReference']['status']==Configure::read('status_live')) ? "LIVE":"DELETED";?></Status>
                 </Reference>
               <?php
               		 $referenceIterator++;
                  }
                  echo '</References>';
               }
               unset($aReferences);
               ?>
            </Assessment>
            <?php
               $assessmentIterator++;	
            }
            ?> 
        </Assessments>
        <?php
        }
        ?>
      </Module>
  <?php 
  	 $moduleIterator++;
  }//end of foreach
  ?></Modules>
</Manifest>
<?php
     }else{
?><Errors><Error ShortText="<?php echo $errorMessage;?>" Code="<?php echo $errorCode;?>"><?php echo $errorMessage;?></Error></Errors>
<?php
     }
?></OTA_GetFullManifestRS>
<?php #echo $this->element('sql_dump'); ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Over-The-Air Administration Page::<?php echo $title_for_layout; ?></title>
    <?php
    if( $this->Session->read('Auth.User.role_id') < 1  ){
        echo $this->Html->css(array(
            'login',
            'colors-fresh'
        ));
        echo $this->Html->script(array(
            'jquery.min'
        ));

    }else{

        echo $this->Html->css(array(
            'login',
            'colors-freshv1',
			'ui',
			'ui/jquery.ui.all'
        ));
        echo $this->Html->script(array(
            'jquery.min',
            'jquery.ui.min',
            'jquery_plugins/jquery.slug',
            'jquery_plugins/jquery.cookie',
            'jquery_plugins/jquery.hoverIntent.minified',
            'jquery_plugins/jquery.elastic-1.6.1.js',
	    'jquery_plugins/jquery.bgiframe-2.1.2',
	    'jquery_plugins/jquery.ajaxQueue'
        ));
        echo $scripts_for_layout;
    }
    ?>
</head>
<body class="wp-admin js  index-php branch-3-2 version-3-2-1 admin-color-fresh">
<?php echo $content_for_layout;?>
</body>
</html>

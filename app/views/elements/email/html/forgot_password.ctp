<?php echo sprintf(__('Hello %s', true), $user['User']['name']); ?>,<br />
<?php
    $url = Router::url(array(
        'controller' => 'users',
        'action' => 'reset',
        $user['User']['username'],
        $activationKey,
    ), true);
    echo 'Please visit this link to reset your password <a href="'.$url.'">'.trim($url).'</a>';
?><br />
<?php echo __('If you do not want to set your own password, then please ignore this email.', true); ?>
<?php #echo sprintf(__('IP Address: %s', true), $_SERVER['REMOTE_ADDR']); ?>

<ul id="adminmenu">
	<li id="menu-dashboard" class="wp-first-item wp-has-submenu <?php echo ( strtolower($this->name) == 'settings' ) ? "wp-has-current-submenu wp-menu-open ":"";?>menu-top menu-top-first menu-icon-dashboard menu-top-last">
		<div class="wp-menu-image"><?php echo $this->Html->link('', '/');?></div>
		<div class="wp-menu-arrow"><div></div></div>
		<div class="wp-menu-toggle"><br></div>
		<a tabindex="1" class="wp-first-item wp-has-submenu <?php echo ( strtolower($this->name) == 'settings' ) ? "wp-has-current-submenu wp-menu-open ":"";?>menu-top menu-top-first menu-icon-dashboard menu-top-last" href="<?php echo $this->Html->url(array("controller" => "settings","action"=>"index"));?>">DASHBOARD</a>
		<div class="wp-submenu">
			<div class="wp-submenu-wrap">
				<div class="wp-submenu-head">DASHBOARD</div>
				<ul>
					<li class="wp-first-item current"><a tabindex="1" class="wp-first-item current" href="<?php echo $this->Html->url(array("controller" => "settings"));?>">HOME</a></li>
					<li><a tabindex="1" href="<?php echo $this->Html->url(array("controller" => "users","action"=>"logout"));?>">LOGOUT</a></li>
				</ul>
			</div>
		</div>
	</li>
	<li class="wp-menu-separator"><div class="separator"></div></li>
	<li id="menu-posts" class="wp-has-submenu <?php echo ( strtolower($this->name) == 'modules' && $this->action !='upload' ) ? "wp-has-current-submenu wp-menu-open ":"";?>menu-top menu-icon-post menu-top-first">
		<div class="wp-menu-image"><a href="<?php echo $this->Html->url(array("controller" => "modules","action"=>"index"));?>"><br></a></div>
		<div class="wp-menu-arrow"><div></div></div>
		<div class="wp-menu-toggle"><br></div>
		<a tabindex="1" class="wp-has-submenu <?php echo ( strtolower($this->name) == 'modules' && $this->action !='upload' ) ? "wp-has-current-submenu wp-menu-open ":"";?>menu-top menu-icon-post menu-top-first" href="<?php echo $this->Html->url(array("controller" => "modules","action"=>"index"));?>">MODULES</a>
		<div class="wp-submenu">
			<div class="wp-submenu-wrap">
				<div class="wp-submenu-head">MODULES</div>
				<ul>
					<?php
					if( isset($hasModule)){
					?>
					<li class="wp-first-item"><a tabindex="1" class="wp-first-item" href="<?php echo $this->Html->url(array("controller" => "modules","action"=>"index"));?>">ALL MODULES</a></li>
					<?php
					}
					?>
					<li><a tabindex="1" href="<?php echo $this->Html->url(array("controller" => "modules","action"=>"add"));?>">ADD NEW</a></li>
					<!--<li><a tabindex="1" href="<?php echo $this->Html->url(array("controller" => "modules","action"=>"upload"));?>">IMAGE UPLOADER</a></li>  -->
				</ul>
			</div>
		</div>
	</li>
	<?php
	if($this->Session->read('Auth.User.role_id')==1){//make sure user is admin
	?>
	<li id="menu-users" class="wp-has-submenu <?php echo ( strtolower($this->name) == 'users' ) ? "wp-has-current-submenu wp-menu-open ":"";?>menu-top menu-icon-users">
		<div class="wp-menu-image"><a href="<?php echo $this->Html->url(array("controller" => "users","action"=>"index"));?>"><br></a></div>
		<div class="wp-menu-arrow"><div></div></div>
		<div class="wp-menu-toggle"><br></div>
		<a tabindex="1" class="wp-has-submenu <?php echo ( strtolower($this->name) == 'users' ) ? "wp-has-current-submenu wp-menu-open ":"";?>menu-top menu-icon-users" href="<?php echo $this->Html->url(array("controller" => "users","action"=>"index"));?>">USERS</a>
		<div class="wp-submenu">
			<div class="wp-submenu-wrap">
				<div class="wp-submenu-head">USERS</div>
				<ul>
					<li class="wp-first-item"><a tabindex="1" class="wp-first-item" href="<?php echo $this->Html->url(array("controller" => "users","action"=>"index"));?>">ALL USERS</a></li>
					<li><a tabindex="1" href="<?php echo $this->Html->url(array("controller" => "users","action"=>"add"));?>">ADD NEW</a></li>
					<li><a tabindex="1" href="<?php echo $this->Html->url(array("controller" => "users","action"=>"profile"));?>">YOUR PROFILE</a></li>
				</ul>
			</div>
		</div>
	</li>
	<li id="menu-tools" class="wp-has-submenu <?php echo ( ( strtolower($this->name) == 'macosx' ) || 
														    ( (strtolower($this->name) == 'modules' ) && $this->action =='upload' ) ||
														   ( strtolower($this->name) == 'documents')
														   ) ? "wp-has-current-submenu wp-menu-open ":"";?>menu-top menu-icon-tools">
		<?php
		if( isset($macos) && !empty($macos) ){
		?>
		<div class="wp-menu-image"><a href="<?php echo $this->Html->url(array("controller" => "macosx","action"=>"index"));?>"><br></a></div>
		<?php
		}else{
		?>
		<div class="wp-menu-image"><a href="<?php echo $this->Html->url(array("controller" => "macosx","action"=>"upload"));?>"><br></a></div>
		<?php
		}
		?>
		<div class="wp-menu-arrow"><div></div></div>
		<div class="wp-menu-toggle"><br></div>
		<a tabindex="1" class="wp-has-submenu <?php echo ( ( strtolower($this->name) == 'macosx' ) || 
														   ( (strtolower($this->name) == 'modules' ) && $this->action =='upload' ) ||
														   ( strtolower($this->name) == 'documents')
														   )? "wp-has-current-submenu wp-menu-open ":"";?>menu-top menu-icon-tools" href="<?php echo $this->Html->url(array("controller" => "macosx","action"=>"upload"));?>">UPLOADER</a>
		<div class="wp-submenu">
			<div class="wp-submenu-wrap">
				<div class="wp-submenu-head">UPLOADER</div>
				<ul>
				<li><a tabindex="1" href="<?php echo $this->Html->url(array("controller" => "macosx","action"=>"upload"));?>">APPS</a></li>
				<li><a tabindex="1" href="<?php echo $this->Html->url(array("controller" => "modules","action"=>"upload"));?>">IMAGES</a></li>
				<li><a tabindex="1" href="<?php echo $this->Html->url(array("controller" => "documents","action"=>"upload"));?>">MATERIALS</a></li>
				</ul>
			</div>
		</div>
	</li>
	<?php
	}//end of make sure is admin
	?> 
	<!--<li id="menu-media" class="wp-has-submenu <?php echo ( strtolower($this->name) == 'documents' ) ? "wp-has-current-submenu wp-menu-open ":"";?>menu-top menu-icon-media">
    <?php
                if( isset($topicFiles) && !empty($topicFiles) ){
                ?>
                <div class="wp-menu-image"><a href="<?php echo $this->Html->url(array("controller" => "documents","action"=>"index"));?>"><br></a></div>
                <?php
                }else{
                ?>
                <div class="wp-menu-image"><a href="<?php echo $this->Html->url(array("controller" => "documents","action"=>"upload"));?>"><br></a></div>
                <?php
                }
                ?>
                <div class="wp-menu-arrow"><div></div></div>
                <div class="wp-menu-toggle"><br></div>
                <a tabindex="1" class="wp-has-submenu <?php echo ( strtolower($this->name) == 'documents' ) ? "wp-has-current-submenu wp-menu-open ":"";?>menu-top menu-icon-media" href="<?php echo $this->Html->url(array("controller" => "documents","action"=>"upload"));?>">TOPIC FILES</a>
                <div class="wp-submenu">
                        <div class="wp-submenu-wrap">
                                <div class="wp-submenu-head">TOPIC FILES</div>
                                <ul><li><a tabindex="1" href="<?php echo $this->Html->url(array("controller" => "documents","action"=>"upload"));?>">UPLOAD</a></li></ul>
                        </div>
                </div>
        </li>  -->
</ul>

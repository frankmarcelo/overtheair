<h2><?php echo $name; ?></h2>
<p class="error">
    <strong>Error: </strong>
    <?php echo sprintf(__("Access was forbidden to the requested address %s on this server.", true), "<strong>'{$message}'</strong>")?>
</p>

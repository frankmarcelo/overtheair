<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
   <!-- array of downloads. -->
   <key>items</key>
   <array>
      	<dict>
        	<!-- an array of assets to download -->
           	<key>assets</key>
            <array>
            <!-- software-package: the ipa to install. -->
            	<dict>
                <!-- required.  the asset kind. -->
                   	<key>kind</key>
                  	<string>software-package</string>
                  	<!-- optional.  md5 every n bytes.  will restart a chunk if md5 fails. -->
                   <key>url</key>
                   <string><?php
                   if(isset($source_file)){echo Configure::read('baseUri').DS.'over-the-air/otawebservices/v1.0/OTA_DownloadModuleFileRQ?src='.$source_file;?>&amp;mobileapp=true<?php }?></string>
                </dict>
               	<!-- display-image: the icon to display during download .-->
               	<dict>
                	<key>kind</key>
                    <string>display-image</string>
                    <!-- optional.  indicates if icon needs shine effect applied. -->
                   	<key>needs-shine</key>
                   	<true/>
                    <key>url</key>
                    <string><?php echo Configure::read('baseUri').DS.'over-the-air/';?>img/Icon-Small-50.png</string>
                </dict>
               	<dict>
                 	<key>kind</key>
                    <string>full-size-image</string>
                   	<key>url</key>
                   	<string><?php echo Configure::read('baseUri').DS.'over-the-air/';?>img/iTunesArtwork.png</string>
               	</dict>
            </array>
            <key>metadata</key>
            <dict>
            	<key>bundle-identifier</key>
            	<string>com.greensplash.genentech.GreenSplash</string>
               	<!-- optional (software only) -->
               	<key>bundle-version</key>
               	<string>1.<?php echo (isset($version)) ? $version: 0;?></string>
               	<!-- required.  the download kind. -->
               	<key>kind</key>
               	<string>software</string>
               	<!-- optional. displayed during download; typically company name -->
               	<key>subtitle</key>
               	<string>Apple</string>
               	<!-- required.  the title to display during the download. -->
               	<key>title</key>
               	<string>Greensplash</string>
            </dict>
        </dict>
   </array>
</dict>
</plist>

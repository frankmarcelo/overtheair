<style type="text/css">
.wrap div.updated, .wrap div.error {
    margin: 5px 0 15px;
}
div.updated, .login .message {
    background-color: #FFFFE0;
    border-color: #E6DB55;
}
div.updated, div.error {
    border-radius: 3px 3px 3px 3px;
    border-style: solid;
    border-width: 1px;
    margin: 5px 15px 2px;
    padding: 0 0.6em;
}
</style>
<div id="wpwrap">
	<div id="adminmenuback"></div>
	<div id="adminmenuwrap">
		<div id="adminmenushadow"></div>
		<?php echo $this->element('menu'); ?>
	</div>
	<div id="wpcontent">
		<!--header menu -->
		<?php echo $this->element('user_navigation'); ?>
		<!-- end of header menu -->
		<div id="wpbody">
			<div id="wpbody-content">
				<div class="wrap">
					<div class="icon32" id="icon-index"><br></div>
					<h2>Dashboard</h2>
					<?php
					if( strstr($this->Session->read('Message.flash.message'),'allowed') ){
					?>	
					<div class="error below-h2"><p><strong>ERROR</strong>: <?php echo $this->Session->read('Message.flash.message');?></p></div>
					<?php
						$this->Session->delete('Message.flash');
					}
					?>
					<div id="dashboard-widgets-wrap">
						<div class="metabox-holder" id="dashboard-widgets">
							<div style="width:49%;" class="postbox-container">
								<div class="meta-box-sortables ui-sortable" id="normal-sortables">
									<div class="postbox " id="dashboard_right_now">
										<h3 class="hndle"><span>Module Overview</span></h3><br/>
										<div class="inside">
											<div class="table table_content">
												<?php
												if(!empty($modules) && count($modules)>0){
												?>
									
												<table>
												<?php
													foreach( $modules as $module ){
												?>	
													<tr><td><strong><?php echo $module['ModuleType']['name'];?></strong>&nbsp;<a href="<?php echo $this->Html->url(array("controller" => "modules","action"=>"view",$module['Module']['id'].DS.str_replace(" ","-",trim($module['Module']['name']))));?>"><?php echo Sanitize::clean($module['Module']['name']);?></a></td></tr>
												<?php
													}
												?>
												</table>
												<?php
												}
												?>	
												<br />
												<div class="clear"></div>
											</div>
										</div>
									</div>
									<div class="postbox " id="dashboard_recent_comments">
										<h3 class="hndle"><span>Software Upload History</span></h3><br />
										<div class="inside">
											<div class="table table_content">
												<table>
													<tbody>
												<?php
												if( isset($macosapp)){
													foreach( $macosapp as $macos ){
											
												?>
													<tr class="first">
														<td class="t posts">OS : <a href="<?php echo $this->Html->url(array("controller" => "macosx","action"=>"download"));?>?src=<?php echo $macos['Macosx']['file_name'];?>"><strong><?php echo $macos['Macosx']['raw_name'];?></strong></a></td>
														<td class="t posts">Type: <strong><?php echo $macos['DownloadType']['name'];?></strong></td>
														<td class="t posts"><?php echo date("M j, Y, g:i a",strtotime($macos['Macosx']['date_created']));?></td>
													</tr>
												<?php
													}
												}
												?>
														</tbody>
												</table>
												<br />
												<div class="clear"></div>
											</div>
											<ul class="subsubsub">
												<li class="all"><a href="<?php echo $this->Html->url(array("controller" => "uploader","action"=>"index"));?>">View All </a> <span class="count">(<span class="pending-count"><?php echo $total_macos;?></span>)</span></a></li>
											</ul>
										</div>
									</div>
								</div>	
							</div>
							<div style="width:49%;" class="postbox-container">
								<div class="meta-box-sortables ui-sortable" id="normal-sortables">
									<div class="postbox " id="dashboard_recent_comments">
										<h3 class="hndle"><span>Unpublished Modules</span></h3><br />
										<div class="inside">
											<?php
											$iPending = 0;
											if( !empty($unpublished) && sizeof($unpublished)>0 ){
											?>
											<div class="table table_content">
												
												<div class="list:comment" id="the-comment-list">
													<div class="comment even thread-even depth-1 comment-item approved" id="comment-1">
														<?php
														foreach( $unpublished as $unpublish ){
															$this->Modules->loadDataById( $unpublish );
															if( $unpublish['Module']['status'] == Configure::read('status_onhold') ){
																$iPending++;
															}

												
														?>
														<div class="dashboard-comment-wrap">
															<h4 class="comment-meta">From <cite class="comment-author">
															<?php echo trim($unpublish['User']['username'])?></cite> on <a href="<?php echo $this->Html->url(array("controller" => "modules","action"=>"view",$unpublish['Module']['id']));?>"><?php echo trim($unpublish['Module']['name']);?></a> 
															<span class="approve">[<?php echo $this->Modules->getDisplayStatus();?>]</span></h4>
															<blockquote><p><?php echo $this->Modules->getShortDescription();?></p></blockquote>
															<!--<p class="row-actions">
															<span class="approve"><a title="Approve this comment" class="dim:the-comment-list:comment-1:unapproved:e7e7d3:e7e7d3:new=approved vim-a" href="">Publish</a></span>
															<span class="unapprove"><a title="Unapprove this comment" class="dim:the-comment-list:comment-1:unapproved:e7e7d3:e7e7d3:new=unapproved vim-u" href="comment.php?action=unapprovecomment&amp;p=1&amp;c=1&amp;_wpnonce=20e8291394">Unapprove</a></span>
															<span class="edit"> | <a title="Edit comment" href="comment.php?action=editcomment&amp;c=1">Edit</a></span>
															<span class="trash"> | <a title="Move this comment to the trash" class="delete:the-comment-list:comment-1::trash=1 delete vim-d vim-destructive" href="comment.php?action=trashcomment&amp;p=1&amp;c=1&amp;_wpnonce=fcb9e8aa29">Trash</a></span>
															</p>-->
														</div>
														<?php
														}
														?>
														<div class="clear"></div>			
													</div>
												</div>
											</div>
											<?php
											}
											?>
											<ul class="subsubsub">
												<li class="all"><a href="<?php echo $this->Html->url(array("controller" => "modules","action"=>"index"));?>">View All</a> |</li>
												<li class="moderated"><a href="<?php echo $this->Html->url(array("controller" => "modules","action"=>"index"));?>">Pending <span class="count">(<span class="pending-count"><?php echo $total_unpublished;?></span>)</span></a></li>
												<!--<li class="approved"><a href="">Approved</a> </li>  -->
											</ul>	
										</div>
									</div>
									<br />
									<div class="postbox " id="dashboard_right_now">
										<h3 class="hndle"><span>Recent Users</span></h3><br />
										<div class="inside">
											<?php
											$iTotalAdmin = 0;
											$iTotalEditor= 0;
											$iTotalTrainee =0;
											if( !empty($usersList) ){
												
											?>
											<div class="table table_content">
												<table>
													<tbody>
													<?php
													
													foreach( $usersList as $userList ){
														if( $userList['User']['id'] != $this->Session->read('Auth.User.id') ){
															$link = $this->Html->url(array("controller" => "users","action"=>"edit",$userList['User']['username'].DS.$userList['User']['token']));
														}else{
															$link = $this->Html->url(array("controller" => "users","action"=>"profile"));
														}
														
														if( $userList['User']['role_id'] == Configure::read('adminRoleId') ){
															$iTotalAdmin++;
														}elseif( $userList['User']['role_id'] == Configure::read('userRoleId') ){
															$iTotalTrainee++;
														}else{
															$iTotalEditor++;			
														}
													?>
														<tr class="first">
															<td class="t posts">Username: <a href="<?php echo $link;?>"><?php echo $userList['User']['username'];?></a></td>
															<td class="t posts">Name: <?php echo $userList['User']['name'];?></td>
															<td class="t posts"><?php echo $userList['Role']['title'];?></td>
														</tr>
													<?php
													}
													?>
													</tbody>
												</table>
												<br />
												<div class="clear"></div>
											</div>
											<?php
												
											}
											?>
											<ul class="subsubsub">
												<li class="all"><a href="<?php echo $this->Html->url(array("controller" => "users","action"=>"index"));?>">View All </a> <span class="count">(<span class="pending-count"><?php echo $total_users;?></span>)</span></a> |</li>
												<li class="moderated"><a href="<?php echo $this->Html->url(array("controller" => "users","action"=>"index"));?>"><?php echo $roles[1];?> <span class="count">(<span class="pending-count"><?php echo $iTotalAdmin;?></span>)</span></a>  |</li>
												<li class="moderated"><a href="<?php echo $this->Html->url(array("controller" => "users","action"=>"index"));?>"><?php echo $roles[2];?> <span class="count">(<span class="pending-count"><?php echo $iTotalEditor;?></span>)</span></a>   |</li>
												<li class="moderated"><a href="<?php echo $this->Html->url(array("controller" => "users","action"=>"index"));?>"><?php echo $roles[3];?> <span class="count">(<span class="pending-count"><?php echo $iTotalTrainee;?></span>)</span></a>  |</li>
												<!--<li class="approved"><a href="">Approved</a> </li>  -->
											</ul>	
										</div>
									</div>
								</div>	
							</div>
							<div class="clear"></div>
						</div>
					</div><!-- dashboard-widgets-wrap -->
				</div><!-- end of wrap -->
			</div>
		</div>
	</div>
</div>
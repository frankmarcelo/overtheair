<operations> 
  <operation>
    <name>OTA_AuthorizationRQ</name>
    <description></description>
    <url><?php echo $this->Html->url(array("admin"=>false,"controller" => "otawebservices","action" => "v1.0")).DS;?>OTA_AuthorizationRQ.xml</url>
  </operation>
  <operation>
    <name>OTA_GetLatestManifestRQ</name>
    <description></description>
    <url><?php echo $this->Html->url(array("admin"=>false,"controller" => "otawebservices","action" => "v1.0")).DS;?>OTA_GetLatestManifestRQ.xml</url>
  </operation>
  <operation>
    <name>OTA_DownloadModuleFileRQ</name>
    <description></description>
    <url><?php echo $this->Html->url(array("admin"=>false,"controller" => "otawebservices","action" => "v1.0")).DS;?>OTA_DownloadModuleFileRQ.xml</url>
  </operation>
  <operation>
    <name>OTA_GetFullManifestRQ</name>
    <description></description>
    <url><?php echo $this->Html->url(array("admin"=>false,"controller" => "otawebservices","action" => "v1.0")).DS;?>OTA_GetFullManifestRQ.xml</url>
  </operation>
</operations>

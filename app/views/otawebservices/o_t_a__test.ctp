<Manifest><Modules><?php
  foreach( $modules as $key => $module ){//start of foreach
  ?><Module DisplaySequence="<?php echo $module['Module']['display_order'];?>" UniqueIdentifier="<?php echo $module['Module']['id'];?>"><Title><?php echo Sanitize::html($module['Module']['name']);?></Title><ShortDescription><![CDATA[<?php echo Sanitize::html($module['Module']['short_description']);?>]]></ShortDescription><LongDescription><![CDATA[<?php echo Sanitize::html($module['Module']['long_description']);?>]]></LongDescription><Image><?php echo($module['Module']['background_image']);?></Image><Color><?php echo Sanitize::html($module['Module']['theme_colour']);?></Color><DateCreated><?php echo date("Y-m-d H:i:s",strtotime($module['Module']['date_created'])); ?></DateCreated><LastModified><?php echo date("Y-m-d H:i:s",strtotime($module['Module']['date_modified'])); ?></LastModified><?php if( count($module['Topic']) > 0 ){?><Topics>
      <?php
      foreach( $module['Topic'] as $topic_key => $topic ){
      ?><Topic Name="<?php echo Sanitize::html($topic['name']);?>" UniqueIdentifier="<?php echo Sanitize::html($topic['id']);?>" DisplaySequence="<?php echo Sanitize::html($topic['display_sequence']);?>"><?php
          $aTopicFiles =  $this->TopicFiles->loadTopicDataById($topic['id']);
          if( !empty($aTopicFiles) ){
             echo '<Documents>';
             foreach( $aTopicFiles as $documents => $document ){    
               
        ?><Document UniqueIdentifier="<?php echo $document['TopicFiles']['id'];?>" DisplaySequence="<?php echo $document['TopicFiles']['display_sequence'];?>" Title="<?php echo Sanitize::html($document['TopicFiles']['name']);?>" author="Administrator">
            <Description><![CDATA[<?php echo Sanitize::html($document['TopicFiles']);?>]]></Description>
            <FileName MediaTypeId="<?php echo $document['TopicFiles']['media_type_id'];?>"><?php echo $document['TopicFiles']['source_file'];?></FileName>
            <Tags><?php echo $document['TopicFiles']['tags'];?></Tags>
            <DateCreated><?php echo date("Y-m-d H:i:s",strtotime($document['TopicFiles']['date_created']));?></DateCreated>
            <DateModified><?php echo date("Y-m-d H:i:s",strtotime($document['TopicFiles']['date_modified']));?></DateModified>
          </Document><?php
             }//enf of foreach
             echo '</Documents>';     
          }
       ?></Topic><?php
      } 
      ?></Topics><?php }?>
        <?php
        if( count($module['Assessment']) > 0 ){ 
        ?><Assessments>
            <?php
            foreach( $module['Assessment']  as $assessment_key => $assessment ){ 
            ?>
            <Assessment UniqueIdentifier="<?php echo $assessment['id'];?>" DisplaySequence="<?php echo $assessment['display_sequence'];?>">
               <Question><![CDATA[<?php echo Sanitize::html($assessment['question']);?>]]></Question>
               <DateCreated><?php echo date("Y-m-d H:i:s",strtotime($assessment['date_created']));?></DateCreated>
               <DateModified><?php echo date("Y-m-d H:i:s",strtotime($assessment['date_modified']));?></DateModified>
               <?php
               $aChoices =  $this->AssessmentsChoices->loadDataById($assessment['id']); 
               if( !empty($aChoices) ){
                  echo '<Choices>';
                  foreach( $aChoices as $options ){        
               ?>
                 <Option UniqueIdentifier="<?php echo $options['AssessmentsChoice']['id'];?>" DisplaySequence="<?php echo $options['AssessmentsChoice']['display_sequence'];?>"><![CDATA[<?php echo Sanitize::html($options['AssessmentsChoice']['text']);?>]]></Option>
               <?php
                  }
                  echo '</Choices>';
               }

               $aAnswer  = $this->AssessmentsAnswer->loadDataById($assessment['id']);             
               if( !empty($aAnswer) ){
               ?>
               <Answer><?php echo $aAnswer[0]['AssessmentsAnswer']['assessment_choice_id'];?></Answer>
               <?php
               } 
 
               $aReferences =  $this->AssessmentsReferences->loadDataById($assessment['id']);
               if( !empty($aReferences) ){
                  echo '<References>'; 
                  foreach( $aReferences as $reference ){
               ?>
                 <Reference UniqueIdentifier="<?php echo $reference['AssessmentsReference']['id'];?>">
                     <Topic><?php echo $reference['AssessmentsReference']['topic_id'];?></Topic>
                 </Reference>
               <?php
                  }
                  echo '</References>';
               }
               ?>
            </Assessment>
            <?php
            }
            ?> 
        </Assessments>
        <?php
        }
        ?>
      </Module>
  <?php 
  }//end of foreach
  ?></Modules>
</Manifest>

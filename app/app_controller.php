<?php
/**
 * Application controller
 *
 * This file is the base controller of all other controllers
 *
 * PHP version 5
 *
 * @version  1.0
 */
class AppController extends Controller {
/**
 * Components
 *
  @var array
 * @access public
 */
    public $components = array(
        'Acl',
        'Session',
    	'RequestHandler',
    	'Security',
        'Cookie',
        'Zend',
        'DebugKit.Toolbar'
    );

    public $helpers = array(
        'Xml',
        'Cache',
        'Html',
        'Form',
        'Session',
        'Text',
        'Js',
        'Time',
        'Layout',
        'Custom',
        'Ajax',
        'Javascript'
    );

/**
 * Helpers
 *
 * @var array
 * @access public
 */
/**
 * Cache pagination results
 *
 * @var boolean
 * @access public
 */
    public $usePaginationCache = true;
    protected $macos = array();
/**
 * View
 *
 * @var string
 * @access public
 */

    public $uses = array('Role','Setting','Macosx','DownloadType');
    
    public function __construct() {
    	parent::__construct();
    }

    public function beforeFilter(){
    	$roles = $this->Role->find('all');
    	$settings = $this->Setting->find('list',array(
    			'fields' => array('key','value')
    		)
    	);
    	$this->set( compact('settings','roles') );
    	$this->DownloadType->contain();
        $downloadTypes = $this->DownloadType->find('all',array(
        		'conditions' => array(
					'DownloadType.status' => Configure::read('status_live'),
				)
			)
		);
        $macos = array(); 
        if(!empty($downloadTypes)){
        	$this->set(compact('downloadTypes'));
        	$aDownloadTypes = array();
        	foreach( $downloadTypes as $downloadType ){
        		$aDownloadTypes[] = array(
        			'download_type_id' => $downloadType['DownloadType']['id'],
        			'download_type_alias' => $downloadType['DownloadType']['alias'] 
        		);
        	}
        	
        	/*TODO
        	 * 
        	 */
        	if( !empty($aDownloadTypes) ){
        		foreach( $aDownloadTypes as $downloadTypeId ){
        			if( $downloadTypeId['download_type_id'] != Configure::read('bothSoftware') && $downloadTypeId['download_type_id'] > 0 ){
        				$this->Macosx->contain();
			        	$macos_data = $this->Macosx->find('all',array(
			        			'conditions' => array(
									'Macosx.status' => Configure::read('status_live'),
									'Macosx.download_type_id IN ('.$downloadTypeId['download_type_id'].')'    
								),
								'order' => array('Macosx.id'=>'DESC'),
								'limit' => 1
							)
						);
						
                        if( !empty($macos_data) && sizeof($macos_data)>0 ){
					    	$macos_data[0]['DownloadType']['alias'] = $downloadTypeId['download_type_alias'];
					    	$macos[] = $macos_data;
                        }
        			}
        		}
			}
			
			if( !empty($macos) ){
				$validateMacOs = array();
				foreach( $macos as $mac_app_data ){
					foreach( $mac_app_data as $mac_app ){
						$macos_source = dirname(dirname(dirname(__FILE__))).$settings['Site.ApplicationSource'].$mac_app['Macosx']['file_name'];
						if(file_exists($macos_source)){
							$validateMacOs[] = $mac_app;
							$this->set( $mac_app['DownloadType']['alias'], $macos_source);
						}
						unset($macos_source);
					}
				}
				if( count($validateMacOs)>0 ){
					$this->macos = $validateMacOs;
					$this->set('countmacos',count($validateMacOs));
					$this->set('macos',$validateMacOs);
				}
			}
		}
    }
}
